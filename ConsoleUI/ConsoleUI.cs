﻿using NetStorm.Common.Logging;
using NetStorm.CoreNG;
using NetStorm.CoreNG.Modules.Enumeration.Dns;
using NetStorm.CoreNG.PackageExtensions.DNS;
using Protocols.HTTP;
using Protocols.TCP;
using Protocols.UPnP;
using Protocols.WhoIs;
using System;
using System.Linq;
using System.Net;
using System.Xml.Linq;

namespace NetStorm.ConsoleUI
{
	class ConsoleUI
	{
		static void Main(string[] args)
		{
			var log = new ConsoleLog();
			log.AttachTo(LogEngine.GlobalLog);

			//LocalEnvironment.Analyze();
			//NicManager.Instance.RefreshNicList();
			//NicManager.Instance.PrimaryDeviceIndex = 2;

			//Upnp("192.168.70.172");
			Whois("0.0.0.0", "insecure-website.com");
			//Http("https://www.youtube.com");
			//Dns("9.9.9.9", "google.com");
		}

		//private static void Http(string host)
		//{
		//	var httpClient = new HttpClient();
		//	var res = httpClient.Get(new IPEndPoint(IPAddress.Any, 0), new Uri(host));

		//	Console.WriteLine($"{res}");
		//}

		private static void TcpPing(string hostOrIP)
		{
			var ping = new TcpPing();
			var res = ping.Send(hostOrIP, 21, System.Net.Sockets.AddressFamily.InterNetwork, 128, -1);
			LogEngine.GlobalLog.Log(res.ToString());
		}

		private static void Dns(string targetDns, string targetDomain)
		{
			var whois = new DnsZoneWalker("dns walker");
			whois.Domain = targetDomain;
			whois.TargetDnsServer = new IPEndPoint(IPAddress.Parse(targetDns), 53);
			whois.SetNic(NicManager.Instance.PrimaryDevice);
			foreach (var r in whois.GetResponses())
				Console.WriteLine(((NsecResourceRecord)r.AnswerRecords[0]).NextDomainName);
		}

		private static void Whois(string bindIP, string hostname)
		{
			var whois = WhoisCrawler.Query(new IPEndPoint(IPAddress.Parse(bindIP), 0), hostname);
			Console.WriteLine(string.Join(" > ", whois.Select(x => x.ResponseFrom)));
			Console.WriteLine($"\n\nOrganisation: {whois[^1].Organisation}");
			Console.WriteLine(whois[^1].Raw);
		}

		private static void Portscanner_PortDetected(object sender, EventArgs<Common.Classes.IPort> e)
		{
			if(e.Argument.Status != Common.Classes.PortStatus.Closed)
				Console.WriteLine($"{e.Argument.Number}/{e.Argument.Protocol} - {e.Argument.Status}");
		}

		private static void Upnp(string bindIP)
		{
			var a = new UPnPDiscovery();
			var discovery = a.DiscoverRootDevices(IPAddress.Parse(bindIP));
			discovery.Wait();
			var wc = new WebClient();
			foreach (var res in discovery.Result)
			{
				if (res.Item2.Headers.ContainsKey("LOCATION"))
				{
					var xml = XElement.Parse(wc.DownloadString(res.Item2.Headers["LOCATION"]));
					Console.WriteLine($"manufacturer: {GetElementFromPath(xml, "device/manufacturer")?.Value}");
					Console.WriteLine($"friendlyName: {GetElementFromPath(xml, "device/friendlyName")?.Value}");
					Console.WriteLine($"modelName: {GetElementFromPath(xml, "device/modelName").Value}");
					Console.WriteLine($"modelDescription: {GetElementFromPath(xml, "device/modelDescription")?.Value}");
					Console.WriteLine($"modelType: {GetElementFromPath(xml, "device/modelType")?.Value}");
					Console.WriteLine($"{xml}");
				}
			}
		}

		private static XElement GetElementFromPath(XElement element, string path)
		{
			XElement res = element;
			foreach (var elem in path.Split('/'))
			{
				res = res?.Element(res.Name.Namespace + elem);
			}

			return res;
		}
	}
}
