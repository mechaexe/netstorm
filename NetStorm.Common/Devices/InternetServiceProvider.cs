﻿using NetStorm.Common.Location;
using System.Net;

namespace NetStorm.Common.Devices
{
	public class InternetServiceProvider
	{
		public InternetServiceProvider(IPAddress iPAddress, bool usesNAT, IPInfo iPInfo)
		{
			IPAddress = iPAddress;
			UsesNAT = usesNAT;
			IPInfo = iPInfo;
		}

		public IPAddress IPAddress { get; private set; }
		public bool UsesNAT { get; private set; }
		public IPInfo IPInfo { get; private set; }

	}
}
