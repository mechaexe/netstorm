﻿using System.ComponentModel;
using NetStorm.Common.Classes;

namespace NetStorm.Common.Devices
{
	public interface INeighborEntry : INotifyPropertyChanged, IMacEntry
	{
		IIPStack IPv6Stack { get; set; }
	}
}
