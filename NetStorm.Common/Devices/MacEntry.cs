﻿using NetStorm.Common.Classes;
using System.ComponentModel;
using System.Net.NetworkInformation;
using System.Runtime.CompilerServices;

namespace NetStorm.Common.Devices
{
	abstract class MacEntry : IMacEntry
	{
		public PhysicalAddress Mac
		{
			get => _mac;
			set
			{
				if (value == null || !value.Equals(_mac))
				{
					_mac = value;
					OnPropertyChanged();
				}
			}
		}
		public IVendor MacVendor
		{
			get => _vendor;
			protected set
			{
				if (value == null || !value.Equals(_vendor))
				{
					_vendor = value;
					OnPropertyChanged();
				}
			}
		}

		private PhysicalAddress _mac;
		private IVendor _vendor;

		public event PropertyChangedEventHandler PropertyChanged;

		public MacEntry()
		{
			_mac = PhysicalAddress.None;
			_vendor = new Vendor();
		}

		protected virtual void OnPropertyChanged([CallerMemberName] string caller = "")
			=> PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(caller));
	}
}
