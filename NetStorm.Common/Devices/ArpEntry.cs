﻿using NetStorm.Common.Classes;

namespace NetStorm.Common.Devices
{
	class ArpEntry : MacEntry, IArpEntry
	{
		public IIPStack IPv4Stack
		{
			get => _ipv4Stack;
			set
			{
				if (value == null || !value.Equals(_ipv4Stack))
				{
					_ipv4Stack = value;
					OnPropertyChanged();
				}
			}
		}

		private IIPStack _ipv4Stack;

		public ArpEntry()
		{
			_ipv4Stack = new IPStack();
		}
	}
}
