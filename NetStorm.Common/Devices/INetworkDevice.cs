﻿using System;
using System.ComponentModel;
using NetStorm.Common.Collections;
using NetStorm.Common.Interfaces;
using NetStorm.Common.Location;

namespace NetStorm.Common.Devices
{
	public interface INetworkDevice : INotifyPropertyChanged, ICloneable, IMergable<INetworkDevice>
	{
		string Hostname { get; set; }
		INetworkInterface NetworkInterface { get; }
		IPortList Ports { get; }
		DeviceType DeviceType { get; set; }
		IIPInfo IPInfo { get; set; }
		OsMatrix OSMatrix { get; set; }
		IDeviceStatistics Statistics { get; }

		string GetHostIdentifier(IpStackMode stackMode);
		void Discovered();
	}
}
