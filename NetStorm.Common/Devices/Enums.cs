﻿using System;

namespace NetStorm.Common.Devices
{
	public enum DeviceState
	{
		Online,
		Offline
	}

	public enum DeviceType
	{
		LocalMachine = 0,
		Gateway = 1,
		Private = 2,
		Public = 3
	}

	[Flags]
	public enum IpStackMode
	{
		Disabled = 0,
		IPv4 = 1,
		IPv6 = 2,
		Dual = 3
	}

	[Flags]
	public enum NetworkAPI
	{
		Native = 0,
		LibPcap = 1,
		NPcap = 2
	}
}
