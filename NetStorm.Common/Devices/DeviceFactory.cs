﻿using System.Net;
using System.Net.NetworkInformation;
using NetStorm.Common.Classes;
using SharpPcap.LibPcap;

namespace NetStorm.Common.Devices
{
	public static class DeviceFactory
	{
		public static INeighborEntry GetNeighborEntry()
			=> new NeighborEntry();
		public static INeighborEntry GetNeighborEntry(string ipv6, PhysicalAddress mac)
			=> new NeighborEntry() { Mac = mac, IPv6Stack = new IPStack(ipv6) };

		public static IArpEntry GetArpEntry()
			=> new ArpEntry();
		public static IArpEntry GetArpEntry(string ipv4, PhysicalAddress mac)
			=> new ArpEntry() { Mac = mac, IPv4Stack = new IPStack(ipv4) };
		public static IArpEntry GetArpEntry(IPAddress ipv4, PhysicalAddress mac)
			=> new ArpEntry() { Mac = mac, IPv4Stack = new IPStack(ipv4) };

		public static INetworkInterface GetInterface()
			=> new NetworkInterface();

		public static INetworkInterface GetInterface(string ip, PhysicalAddress mac)
			=> GetInterface(IPAddress.Parse(ip), mac);
		public static INetworkInterface GetInterface(IPAddress ip, PhysicalAddress mac)
			=> new NetworkInterface(ip) { Mac = mac };
		public static INetworkInterface GetInterface(IIPStack ipStack, PhysicalAddress mac)
			=> new NetworkInterface()
			{
				Mac = mac,
				IPv4Stack =  ipStack.IPAddress.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork ? ipStack : new IPStack(),
				IPv6Stack = ipStack.IPAddress.AddressFamily == System.Net.Sockets.AddressFamily.InterNetworkV6 ? ipStack : new IPStack(false),
			};

		public static ILocalNetworkInterface GetLocalNetworkInterface()
			=> new LocalNetworkInterface();
		public static ILocalNetworkInterface GetLocalNetworkInterface(LibPcapLiveDevice device, ILocalNetworkInterface nic)
			=> new PcapNetworkInterface(device)
			{
				Id = nic.Id,
				InterfaceName = nic.InterfaceName,
				IPv4Stack = nic.IPv4Stack,
				IPv6Stack = nic.IPv6Stack,
				Mac = nic.Mac,
				Gateway = nic.Gateway
			};

		public static INetworkDevice GetNetworkDevice()
			=> new NetworkDevice();

		public static INetworkDevice GetNetworkDevice(INetworkInterface nic)
			=> new NetworkDevice() { NetworkInterface = nic ?? GetInterface() };
	}
}
