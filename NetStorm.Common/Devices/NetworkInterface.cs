﻿using System.ComponentModel;
using System.Net;
using System.Net.NetworkInformation;
using System.Runtime.CompilerServices;
using NetStorm.Common.Classes;

namespace NetStorm.Common.Devices
{
	class NetworkInterface : INetworkInterface
	{
		public PhysicalAddress Mac
		{
			get => _mac;
			set
			{
				if (_mac == null || !_mac.Equals(value))
				{
					_mac = value;
					OnPropertyChanged();
				}
			}
		}
		public IVendor MacVendor
		{
			get => _vendor;
			protected set
			{
				if (_vendor == null || !_vendor.Equals(value))
				{
					_vendor = value;
					OnPropertyChanged();
				}
			}
		}
		public IpStackMode StackMode
		{
			get => stackMode;
			protected set
			{
				if (stackMode != value)
				{
					stackMode = value;
					OnPropertyChanged();
				}
			}
		}

		public IIPStack IPv4Stack
		{
			get => ipv4Stack;
			set
			{
				if (ipv4Stack != value)
				{
					ipv4Stack = value;
					OnPropertyChanged();
					StackMode = GetStackMode();
				}
			}
		}

		public IIPStack IPv6Stack
		{
			get => ipv6Stack;
			set
			{
				if (ipv6Stack != value)
				{
					ipv6Stack = value;
					OnPropertyChanged();
					StackMode = GetStackMode();
				}
			}
		}

		public event PropertyChangedEventHandler PropertyChanged;

		private PhysicalAddress _mac;

		private IIPStack ipv4Stack, ipv6Stack;
		private IpStackMode stackMode;
		private IVendor _vendor;

		public NetworkInterface()
		{
			ipv4Stack = new IPStack();
			ipv6Stack = new IPStack(false);
			_mac = PhysicalAddress.None;
			_vendor = new Vendor("", "", "");
		}

		public NetworkInterface(IPAddress address)
		{
			ipv4Stack = address.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork ? new IPStack(address) : new IPStack();
			ipv6Stack = address.AddressFamily == System.Net.Sockets.AddressFamily.InterNetworkV6 ? new IPStack(address) : new IPStack(false);
			_mac = PhysicalAddress.None;
			_vendor = new Vendor("", "", "");
		}

		protected virtual IpStackMode GetStackMode()
		{
			var res = IpStackMode.Disabled;
			res |= (Supports(IpStackMode.IPv4) ? IpStackMode.IPv4 : IpStackMode.Disabled);
			res |= (Supports(IpStackMode.IPv6) ? IpStackMode.IPv6 : IpStackMode.Disabled);
			return res;
		}

		protected virtual void OnPropertyChanged([CallerMemberName] string caller = "")
			=> PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(caller));

		public bool Supports(IpStackMode ipStack)
		{
			if ((int)(ipStack & IpStackMode.Dual) == (int)IpStackMode.Dual)
			{
				return Supports(IpStackMode.IPv4) && Supports(IpStackMode.IPv6);
			}
			else if ((int)(ipStack & IpStackMode.IPv4) == (int)IpStackMode.IPv4)
			{
				return ipv4Stack != null && ipv4Stack.IPAddress != IPAddress.None;
			}
			else if ((int)(ipStack & IpStackMode.IPv6) == (int)IpStackMode.IPv6)
			{
				return ipv6Stack != null && ipv6Stack.IPAddress != IPAddress.IPv6None;
			}
			else
			{
				return false;
			}
		}

		public IIPStack GetPreferredStack(IpStackMode stackMode)
		{
			if (stackMode == IpStackMode.IPv4 || stackMode == IpStackMode.Dual)	//Prefer IPv4
			{
				return Supports(stackMode) ? ipv4Stack : ipv6Stack;
			}
			else
			{
				return Supports(stackMode) ? ipv6Stack : ipv4Stack;
			}
		}

		public void Merge(INetworkInterface @object)
		{
			MacVendor = Mac == PhysicalAddress.None ? @object.MacVendor : MacVendor;
			Mac = Mac == PhysicalAddress.None ? @object.Mac : Mac;

			if (!Supports(IpStackMode.IPv4) && @object.Supports(IpStackMode.IPv4))
				IPv4Stack = @object.IPv4Stack;

			if (!Supports(IpStackMode.IPv6) && @object.Supports(IpStackMode.IPv6))
				IPv6Stack = @object.IPv6Stack;

			StackMode = GetStackMode();
		}

		public IPEndPoint ToIpEndPoint(IpStackMode preferredStack, ushort port)
			=> new IPEndPoint(GetPreferredStack(preferredStack).IPAddress, port);
	}
}
