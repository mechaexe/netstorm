﻿using System.Net.NetworkInformation;
using NetStorm.Common.Classes;

namespace NetStorm.Common.Devices
{
	public interface IMacEntry
	{
		PhysicalAddress Mac { get; set; }
		IVendor MacVendor { get; }
	}
}
