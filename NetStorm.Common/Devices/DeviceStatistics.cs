﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace NetStorm.Common.Devices
{
	struct DeviceStatistics : IDeviceStatistics
	{
		public DateTime FirstSeen
		{
			get => fistSeen;
			set
			{
				if (value == null || !value.Equals(fistSeen))
				{
					fistSeen = value;
					OnPropertyChanged();
				}

			}
		}
		public DateTime LastSeen
		{
			get => lastSeen;
			set
			{
				if (value == null || !value.Equals(lastSeen))
				{
					lastSeen = value;
					OnPropertyChanged();
				}
			}
		}
		public DeviceState DeviceState
		{
			get => state;
			set
			{
				if (state != value)
				{
					state = value;
					OnPropertyChanged();
				}
			}
		}

		public event PropertyChangedEventHandler PropertyChanged;

		private DateTime fistSeen, lastSeen;
		private DeviceState state;

		private void OnPropertyChanged([CallerMemberName] string caller = "") => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(caller));

		public void Merge(IDeviceStatistics @object)
		{
			if (@object != null)
			{
				LastSeen = LastSeen < @object.LastSeen ? @object.LastSeen : LastSeen;
				DeviceState = LastSeen < @object.LastSeen ? @object.DeviceState : DeviceState;
			}
		}
	}
}
