﻿using NetStorm.Common.Worker.PacketReceiver;
using PacketDotNet;
using System;

namespace NetStorm.Common.Devices
{
	public interface ILocalNetworkInterface : INetworkInterface
	{
		INetworkDevice Gateway { get; set; }
		InternetServiceProvider ISP { get; set; }
		string InterfaceName { get; set; }
		string Id { get; set; }
		bool IsCapturing { get; }

		event EventHandler<Packet> PacketCaptured;
		event EventHandler CaptureStarted, CaptureStopped;

		bool Supports(NetworkAPI api);

		void StartCapture();

		void StopCapture();

		void ForceStop();

		/// <summary>
		/// Send a packet to sharppcap.dll
		/// </summary>
		/// <exception cref="SharpPcap.DeviceNotReadyException"></exception>
		/// <param name="packet"></param>
		void Transmit(Packet packet);

		IPacketTranceiver GetPacketTranceiver();
		IPacketTranceiver GetDedicatedPacketTranceiver();
	}
}
