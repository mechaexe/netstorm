﻿using NetStorm.Common.Classes;

namespace NetStorm.Common.Devices
{
	class NeighborEntry : MacEntry, INeighborEntry
	{
		public IIPStack IPv6Stack
		{
			get => _ipv6Stack;
			set
			{
				if (value == null || !value.Equals(_ipv6Stack))
				{
					_ipv6Stack = value;
					OnPropertyChanged();
				}
			}
		}

		private IIPStack _ipv6Stack;

		public NeighborEntry()
		{
			IPv6Stack = new IPStack(false);
		}
	}
}
