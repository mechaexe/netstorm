﻿using System;
using System.ComponentModel;
using NetStorm.Common.Interfaces;

namespace NetStorm.Common.Devices
{
	public interface IDeviceStatistics : INotifyPropertyChanged, IMergable<IDeviceStatistics>
	{
		DateTime FirstSeen { get; set; }
		DateTime LastSeen { get; set; }
		DeviceState DeviceState { get; set; }
	}
}
