﻿using NetStorm.Common.Classes;
using NetStorm.Common.Interfaces;
using System.Net;

namespace NetStorm.Common.Devices
{
	public interface INetworkInterface : IArpEntry, INeighborEntry, IMergable<INetworkInterface>
	{
		IpStackMode StackMode { get; }
		IIPStack GetPreferredStack(IpStackMode stackMode);
		IPEndPoint ToIpEndPoint(IpStackMode preferredStack, ushort port);
		bool Supports(IpStackMode ipStack);
	}
}
