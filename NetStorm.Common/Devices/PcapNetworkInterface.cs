﻿using PacketDotNet;
using SharpPcap;
using SharpPcap.LibPcap;

namespace NetStorm.Common.Devices
{
	class PcapNetworkInterface : LocalNetworkInterface
	{
		private readonly LibPcapLiveDevice _device;

		public PcapNetworkInterface(LibPcapLiveDevice device)
		{
			_device = device;
		}

		public override bool Supports(NetworkAPI api)
		{
			if (api == NetworkAPI.LibPcap)
				return _device is LibPcapLiveDevice;
			else return base.Supports(api);
		}

		protected override void OnCaptureStarted()
		{
			_device.Open(DeviceModes.Promiscuous);

			_device.OnPacketArrival += Device_OnPacketArrival;
			_device.StartCapture();
			base.OnCaptureStarted();
		}

		protected override void OnCaptureStopped()
		{
			_device.StopCapture();
			_device.OnPacketArrival -= Device_OnPacketArrival;
			_device.Close();
			base.OnCaptureStopped();
		}

		/// <summary>
		/// Send the packet to sharppcap.dll
		/// </summary>
		/// <exception cref="SharpPcap.DeviceNotReadyException"></exception>
		/// <param name="packet"></param>
		public override void Transmit(Packet packet)
			=> _device.SendPacket(packet.Bytes);

		private void Device_OnPacketArrival(object sender, PacketCapture e)
		{
			var packet = Packet.ParsePacket(LinkLayers.Ethernet, e.GetPacket().Data);
			if(packet != null)
				OnPacketCaptured(packet);
		}
	}
}
