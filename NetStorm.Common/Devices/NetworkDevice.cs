﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using NetStorm.Common.Collections;
using NetStorm.Common.Location;

namespace NetStorm.Common.Devices
{
	class NetworkDevice : INetworkDevice
	{
		public string Hostname
		{
			get => hostname;
			set
			{
				if (hostname != value)
				{
					hostname = value;
					OnPropertyChanged();
				}
			}
		}

		public INetworkInterface NetworkInterface
		{
			get => nic;
			set
			{
				if (nic != value)
				{
					nic = value;
					OnPropertyChanged();
				}
			}
		}

		public IPortList Ports { get; }

		public IIPInfo IPInfo
		{
			get => ipInfo;
			set
			{
				if (ipInfo != value)
				{
					ipInfo = value;
					OnPropertyChanged();
				}
			}
		}

		public OsMatrix OSMatrix
		{
			get => osMatrix;
			set
			{
				osMatrix = value;
				OnPropertyChanged();
			}
		}

		public DeviceType DeviceType
		{
			get => _type;
			set
			{
				if (_type != value)
				{
					_type = value;
					OnPropertyChanged();
				}
			}
		}

		public IDeviceStatistics Statistics { get; }

		public event PropertyChangedEventHandler PropertyChanged;

		private string hostname;
		private IIPInfo ipInfo;
		private OsMatrix osMatrix;
		private INetworkInterface nic;
		private DeviceType _type;

		public NetworkDevice()
		{
			Statistics = new DeviceStatistics();
			Ports = new PortListNew();
			nic = DeviceFactory.GetInterface();
			Statistics.PropertyChanged += (_, __) => OnPropertyChanged(nameof(Statistics));
			_type = DeviceType.Private;
		}

		protected void OnPropertyChanged([CallerMemberName] string caller = "") => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(caller));

		public void Discovered()
		{
			Statistics.DeviceState = DeviceState.Online;
			Statistics.LastSeen = DateTime.Now;
		}

		public string GetHostIdentifier(IpStackMode stackMode) => string.IsNullOrEmpty(Hostname) ? nic.GetPreferredStack(stackMode).IPAddress.ToString() : Hostname;

		public void Merge(INetworkDevice device)
		{
			DeviceType = DeviceType < device.DeviceType ? DeviceType : device.DeviceType;
			Statistics?.Merge(device.Statistics);
			NetworkInterface?.Merge(device.NetworkInterface);

			if (string.IsNullOrEmpty(hostname))
				Hostname = device.Hostname;

			IPInfo = IPInfo ?? device.IPInfo;
			OSMatrix = OSMatrix ?? device.OSMatrix;

			foreach (var p in device.Ports)
				Ports.AddOrReplace(p);
		}

		public object Clone() => MemberwiseClone();
	}
}
