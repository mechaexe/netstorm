﻿using System.ComponentModel;
using NetStorm.Common.Classes;

namespace NetStorm.Common.Devices
{
	public interface IArpEntry : INotifyPropertyChanged, IMacEntry
	{
		IIPStack IPv4Stack { get; set; }
	}
}
