﻿using NetStorm.Common.Logging;
using NetStorm.Common.Worker.PacketReceiver;
using PacketDotNet;
using System;
using System.Collections.Generic;
using System.Net.NetworkInformation;

namespace NetStorm.Common.Devices
{
	class LocalNetworkInterface : NetworkInterface, ILocalNetworkInterface
	{
		public string InterfaceName
		{
			get => _name;
			set
			{
				if (_name != value)
				{
					_name = value;
					OnPropertyChanged();
				}
			}
		}
		public string Id
		{
			get => _id;
			set
			{
				if (_id != value)
				{
					_id = value;
					OnPropertyChanged();
				}
			}
		}
		public InternetServiceProvider ISP 
		{ 
			get => _isp;
			set 
			{
				if(value == null || !value.Equals(_isp))
				{
					_isp = value;
					OnPropertyChanged();
				}
			} 
		}

		public bool IsCapturing => handles > 0;
		public INetworkDevice Gateway
		{
			get => _gateway;
			set
			{
				if (value == null || !value.Equals(Gateway))
				{
					_gateway = value;
					OnPropertyChanged();
				}
			}
		}

		public event EventHandler<Packet> PacketCaptured;
		public event EventHandler CaptureStarted;
		public event EventHandler CaptureStopped;

		protected int handles;
		private string _name, _id;
		private INetworkDevice _gateway;
		private InternetServiceProvider _isp;
		private readonly IPacketTranceiver _packetReceiver;

		public LocalNetworkInterface()
		{
			handles = 0;
			_gateway = null;
			_packetReceiver = new DefaultPacketTranceiver(this);
		}

		protected virtual void OnPacketCaptured(Packet packet) => PacketCaptured?.Invoke(this, packet);

		protected virtual void OnCaptureStarted()
		{
			OnPropertyChanged(nameof(IsCapturing));
			CaptureStarted?.Invoke(this, new EventArgs());
			LogEngine.GlobalLog.Log($"NIC {_name} opened");
		}

		protected virtual void OnCaptureStopped()
		{
			handles = 0;
			OnPropertyChanged(nameof(IsCapturing));
			CaptureStopped?.Invoke(this, new EventArgs());
			LogEngine.GlobalLog.Log($"NIC {_name} closed");
			GC.Collect();
		}

		public void StartCapture()
		{
			handles++;
			if (handles == 1)
			{
				OnCaptureStarted();
			}
		}

		public void StopCapture()
		{
			if (IsCapturing)
			{
				handles--;
				if (handles == 0)
				{
					OnCaptureStopped();
				}
			}
		}

		public void ForceStop()
		{
			if (handles > 0)
			{
				OnCaptureStopped();
			}
		}

		public virtual void Transmit(Packet packet) { }

		public override string ToString() => $"{InterfaceName}: {IPv4Stack.IPAddress}/{IPv4Stack.Network.Cidr} - {Mac.ToCannonicalString()}";

		public IPacketTranceiver GetPacketTranceiver() => _packetReceiver;

		public IPacketTranceiver GetDedicatedPacketTranceiver() => new DefaultPacketTranceiver(this);

		public virtual bool Supports(NetworkAPI api)
			=> api == NetworkAPI.Native;
	}
}
