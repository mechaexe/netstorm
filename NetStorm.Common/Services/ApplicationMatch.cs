﻿using NetStorm.Common.Interfaces;

namespace NetStorm.Common.Services
{
	public class ApplicationMatch : IMergable<ApplicationMatch>
	{
		public Application OS { get; }
		public int Match { get; set; }

		public ApplicationMatch(string os) : this(new Application(os), 0) { }
		public ApplicationMatch(Application oS, int match)
		{
			OS = oS;
			Match = match;
		}

		public void Merge(ApplicationMatch @object)
		{
			if (@object.OS.Equals(OS))
				Match += @object.Match;
		}
	}
}
