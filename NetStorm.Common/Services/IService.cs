﻿using NetStorm.Common.Interfaces;
using System.Collections.ObjectModel;

namespace NetStorm.Common.Services
{
	public interface IService : IMergable<IService>
	{
		Application Service { get; set; }
		ObservableCollection<string> Banners { get; }
	}
}
