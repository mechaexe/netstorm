﻿namespace NetStorm.Common.Services
{
	public struct Application
	{
		public string Name { get; }
		public string Version { get; }
		public string ApplicationInfo { get; }
		public Application(string name) : this(name, "", "") { }
		public Application(string name, string version) : this(name, version, "") { }
		public Application(string name, string version, string appInfo)
		{
			Name = name;
			Version = version;
			ApplicationInfo = appInfo;
		}

		public override string ToString() 
			=> Name + 
			(!string.IsNullOrEmpty(Version) ? $" ({Version})" : "") +
			(!string.IsNullOrEmpty(ApplicationInfo) ? $" {ApplicationInfo}" : "");
	}
}
