﻿using System.Collections.ObjectModel;

namespace NetStorm.Common.Services
{
	public class GenericService : IService
	{
		public ObservableCollection<string> Banners { get; }
		public Application Service { get; set; }

		public GenericService()
		{
			Banners = new ObservableCollection<string>();
		}

		public GenericService(IService service) : this()
		{
			if(service != null)
			{
				Service = service.Service;
				foreach (var b in service.Banners)
					Banners.Add(b);
			}
		}

		public override string ToString()
		{
			return $"{Service} - {string.Join(" ", Banners)}";
		}

		public virtual void Merge(IService @object)
		{
			if (@object != null && Service.Name == @object.Service.Name)
				foreach (var b in @object.Banners)
					if(!Banners.Contains(b))
						Banners.Add(b);
		}
	}
}
