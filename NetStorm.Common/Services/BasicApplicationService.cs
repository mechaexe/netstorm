﻿using System.Collections.Generic;
using System.Linq;

namespace NetStorm.Common.Services
{
	public class BasicApplicationService : GenericService
	{
		public string AdditionalInformation { get; set; }
		public List<Application> Applications { get; } = new List<Application>();

		public BasicApplicationService() { }

		public BasicApplicationService(GenericService service) : base(service) { }

		public BasicApplicationService(BasicApplicationService service) : base(service)
		{
			Applications = service.Applications;
			AdditionalInformation = service.AdditionalInformation;
		}

		public override string ToString()
			=> $"{Service} running on {string.Join(", ", Applications)}" + 
			(string.IsNullOrEmpty(AdditionalInformation) ? "" : $" ({AdditionalInformation})") +
			(Banners.Count > 0 ? $"({string.Join(" ", Banners)})" : "");

		public override void Merge(IService @object)
		{
			base.Merge(@object);
			if (@object is BasicApplicationService bas)
			{
				Applications.AddRange(bas.Applications.Where(x => !Applications.Select(app => app.Name).Contains(x.Name)));
				if (string.IsNullOrEmpty(AdditionalInformation))
					AdditionalInformation = bas.AdditionalInformation;
			}
		}
	}
}
