﻿using System;
using System.Collections.Generic;
using System.Xml.Linq;

namespace NetStorm.Common.Services
{
	public class UPnPService : BasicApplicationService
	{
		public Dictionary<Uri, XElement> Services { get; } = new Dictionary<Uri, XElement>();

		public UPnPService() { }

		public UPnPService(BasicApplicationService service) : base(service) { }

		public UPnPService(UPnPService service) : base(service)
		{
			Services = service.Services;
		}

		public override void Merge(IService @object)
		{
			base.Merge(@object);
			if (@object is UPnPService ups)
				foreach (var s in ups.Services)
					Services.TryAdd(s.Key, s.Value, true);
		}
	}
}
