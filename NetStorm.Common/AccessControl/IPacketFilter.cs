﻿using PacketDotNet;

namespace NetStorm.Common.AccessControl
{
	public interface IPacketFilter
	{
		IPacketFilter ChildFilter { get; set; }
		bool IsValid(EthernetPacket ethernetPacket);
	}
}
