﻿using PacketDotNet;

namespace NetStorm.Common.AccessControl
{
	public class TcpFilter : BasePacketFilter
	{
		public ushort SrcPort { get; set; } = 0;
		public ushort DstPort { get; set; } = 0;

		public override bool IsValid(EthernetPacket packet)
		{
			return base.IsValid(packet) &&
				packet.PayloadPacket.PayloadPacket is TcpPacket tcp &&
				(SrcPort == 0 || tcp.SourcePort == SrcPort) &&
				(DstPort == 0 || tcp.DestinationPort == DstPort);
		}
	}
}
