﻿using PacketDotNet;
using System.Net;

namespace NetStorm.Common.AccessControl
{
	public class IPPacketFilter : BasePacketFilter
	{
		public IPAddress SrcAddress { get; set; } = IPAddress.Any;
		public IPAddress DstAddress { get; set; } = IPAddress.Any;

		public override bool IsValid(EthernetPacket packet)
		{
			return base.IsValid(packet) &&
				packet.PayloadPacket is IPPacket ip &&
				(DstAddress == IPAddress.Any || ip.DestinationAddress.Equals(DstAddress)) &&
				(SrcAddress == IPAddress.Any || ip.SourceAddress.Equals(SrcAddress));
		}
	}
}
