﻿using PacketDotNet;

namespace NetStorm.Common.AccessControl
{
	public class BasePacketFilter : IPacketFilter
	{
		public IPacketFilter ChildFilter { get; set; }

		public virtual bool IsValid(EthernetPacket ethernetPacket)
		{
			return ChildFilter == null || ChildFilter.IsValid(ethernetPacket);
		}
	}
}
