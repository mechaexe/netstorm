﻿namespace NetStorm.Common.Logging
{
	public enum LogLevel
	{
		Debug,
		Normal,
		Error,
		Critical
	}
}
