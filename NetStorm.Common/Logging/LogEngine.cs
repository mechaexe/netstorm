﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace NetStorm.Common.Logging
{
	public sealed class LogEngine
	{
		public static LogEngine GlobalLog { get; } = new LogEngine();
		public event EventHandler<EventArgs<LogEntry>> OnLogged
		{
			add
			{
				_onLogged += value;
				Log("Logger attached", LogLevel.Debug, new StackTrace().GetFrame(1).GetMethod().ReflectedType.Name);
			}
			remove
			{
				Log("Logger removed", LogLevel.Debug, new StackTrace().GetFrame(1).GetMethod().ReflectedType.Name);
				_onLogged -= value;
			}
		}

		private event EventHandler<EventArgs<LogEntry>> _onLogged;
		public void Log(string text, LogLevel level = LogLevel.Debug, [CallerMemberName] string caller = "")
			=> _onLogged?.Invoke(this, new EventArgs<LogEntry>(new LogEntry(text, caller, level)));

		private LogEngine() { }
	}
}
