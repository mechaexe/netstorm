﻿using System;
using System.Diagnostics;

namespace NetStorm.Common.Logging
{
	public class DebugLog : BaseLogger
	{
		protected override void LogEngine_OnLogged(object sender, EventArgs<LogEntry> e)
			=> Debug.WriteLine(e.Argument);
	}
}
