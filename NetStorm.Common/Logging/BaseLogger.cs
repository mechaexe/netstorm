﻿using System;

namespace NetStorm.Common.Logging
{
	public abstract class BaseLogger : ILogger
	{
		public virtual void AttachTo(LogEngine logEngine)
			=> logEngine.OnLogged += LogEngine_OnLogged;

		public void RemoveFrom(LogEngine logEngine)
		=> logEngine.OnLogged -= LogEngine_OnLogged;

		protected abstract void LogEngine_OnLogged(object sender, EventArgs<LogEntry> e);
	}
}
