﻿namespace NetStorm.Common.Logging
{
	public interface ILogger
	{
		void AttachTo(LogEngine logEngine);
		void RemoveFrom(LogEngine logEngine); 
	}
}
