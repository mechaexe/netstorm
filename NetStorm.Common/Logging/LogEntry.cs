﻿using System;

namespace NetStorm.Common.Logging
{
	public class LogEntry
	{
		public string Message { get; }
		public string Caller { get; }
		public DateTime LogTime { get; }
		public LogLevel Level { get; }

		public LogEntry(string msg, string caller) :
			this(msg, caller, LogLevel.Debug)
		{ }

		public LogEntry(string msg, string caller, LogLevel level)
		{
			LogTime = DateTime.Now;
			Message = msg;
			Caller = caller;
			Level = level;
		}

		public override string ToString()
		{
			return $"[{LogTime}] ({Level}) {Caller}: {Message}";
		}
	}
}
