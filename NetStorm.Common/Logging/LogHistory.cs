﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace NetStorm.Common.Logging
{
	public class LogHistory : BaseLogger
	{
		private readonly List<LogEntry> _logs = new List<LogEntry>();
		protected override void LogEngine_OnLogged(object sender, EventArgs<LogEntry> e)
		{
			lock (_logs)
				_logs.Add(e.Argument);
		}

		public void Print()
		{
			Debug.WriteLine("--- DEBUG HISTORY ---");
			lock(_logs)
				foreach (var log in _logs)
					Debug.WriteLine(log);
		}
	}
}
