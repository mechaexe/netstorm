﻿using System;

namespace NetStorm.Common.Logging
{
	public class ConsoleLog : BaseLogger
	{
		protected override void LogEngine_OnLogged(object sender, EventArgs<LogEntry> e)
			=> Console.WriteLine(e.Argument);
	}
}
