﻿namespace NetStorm.Common.Interfaces
{
	public interface IMergable<T>
	{
		void Merge(T @object);
	}
}
