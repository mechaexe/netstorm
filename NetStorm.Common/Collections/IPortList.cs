﻿using System.Collections.Generic;
using System.Collections.Specialized;
using NetStorm.Common.Classes;

namespace NetStorm.Common.Collections
{
	public interface IPortList : ICollection<IPort>, INotifyCollectionChanged
	{
		IEnumerable<IPort> this[ushort portNumber] { get; }
		IPort this[ushort portNumber, Protocol protocol] { get; }
		bool Contains(ushort portNumber);
		bool Contains(ushort portNumber, Protocol protocol);
		void AddOrReplace(IPort newPort);
	}
}