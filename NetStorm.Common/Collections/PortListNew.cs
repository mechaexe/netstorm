﻿using NetStorm.Common.Classes;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;

namespace NetStorm.Common.Collections
{
	class PortListNew : IPortList
	{
		public int Count => _ports.Count;

		public bool IsReadOnly => false;

		public IEnumerable<IPort> this[ushort portNumber] => _ports.Values.Where(p => p.Number == portNumber);

		public IPort this[ushort portNumber, Protocol protocol] => _ports[(portNumber, protocol)];

		public event NotifyCollectionChangedEventHandler CollectionChanged;

		private readonly Dictionary<(ushort, Protocol), IPort> _ports = new Dictionary<(ushort, Protocol), IPort>();
		private readonly object _lockObj = new object();

		public void Add(IPort item)
		{
			lock(_lockObj)
				_ports.Add((item.Number, item.Protocol), item);
			CollectionChanged?.Invoke(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, item));
		}

		public void Clear()
		{
			lock(_lockObj)
				_ports.Clear();
			CollectionChanged?.Invoke(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
		}

		public bool Remove(IPort item)
		{
			lock(_lockObj)
				return _ports.Remove((item.Number, item.Protocol));
		}

		public bool Remove(ushort portNumber)
		{
			int removed = 0;
			foreach (var p in this[portNumber].ToArray())
				removed += Remove(p) ? 1 : 0;
			return removed > 0;
		}

		public bool Contains(ushort portNumber)
		{
			foreach (var p in _ports.Keys)
				if (p.Item1 == portNumber)
					return true;
			return false;
		}

		public bool Contains(IPort item)
		{
			lock(_lockObj)
				return _ports.ContainsKey((item.Number, item.Protocol));
		}

		public void CopyTo(IPort[] array, int arrayIndex)
			=> _ports.Values.CopyTo(array, arrayIndex);

		public IEnumerator<IPort> GetEnumerator()
			=> _ports.Values.GetEnumerator();

		IEnumerator IEnumerable.GetEnumerator()
			=> GetEnumerator();

		public void AddOrReplace(IPort newPort)
		{
			if (Contains(newPort))
			{
				_ports[(newPort.Number, newPort.Protocol)].Merge(newPort);
				CollectionChanged?.Invoke(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Replace, new[] { newPort }, new[] { _ports[(newPort.Number, newPort.Protocol)] }));
			}
			else Add(newPort);
		}

		public bool Contains(ushort portNumber, Protocol protocol)
		{
			lock (_lockObj)
				return _ports.ContainsKey((portNumber, protocol));
		}
	}
}
