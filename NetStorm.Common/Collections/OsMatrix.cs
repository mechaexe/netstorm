﻿using NetStorm.Common.Interfaces;
using NetStorm.Common.Services;
using System.Collections;
using System.Collections.Generic;

namespace NetStorm.Common.Collections
{
	public class OsMatrix : IReadOnlyCollection<ApplicationMatch>, IMergable<OsMatrix>
	{
		public ApplicationMatch this[string os] => _matches[os];
		public int Count => _matches.Count;
		private readonly Dictionary<string, ApplicationMatch> _matches = new Dictionary<string, ApplicationMatch>();

		public OsMatrix()
		{
			_matches = new Dictionary<string, ApplicationMatch>()
			{
				{ "macos", new ApplicationMatch("MacOS") },
				{ "ios", new ApplicationMatch("IOS") },

				{ "winwork", new ApplicationMatch("Windows Workgroups") },
				{ "win95", new ApplicationMatch("Windows 95") },
				{ "winnt3", new ApplicationMatch("Windows NT 3.XX") },
				{ "winnt4", new ApplicationMatch("Windows NT 4.XX") },
				{ "win10", new ApplicationMatch("Windows 10") },

				{ "linux", new ApplicationMatch("Linux") },
				{ "android", new ApplicationMatch("Android") },
				{ "bsd", new ApplicationMatch("BSD") },

				{ "solaris", new ApplicationMatch("Solaris") },
				{ "aix", new ApplicationMatch("AIX") },
				{ "cisco", new ApplicationMatch("Cisco") }
			};
		}

		public List<Application> HighestMatches()
		{
			var highest = HighestMatch();
			var res = new List<Application>(_matches.Count);
			if (highest > 0)
			{
				foreach (var a in _matches)
				{
					if (a.Value.Match == highest)
						res.Add(a.Value.OS);
				}
			}
			return res;
		}

		public int HighestMatch()
		{
			int highest = 0;

			foreach (var a in _matches)
				highest = highest < a.Value.Match ? a.Value.Match : highest;

			return highest;
		}

		public IEnumerator<ApplicationMatch> GetEnumerator()
			=> _matches.Values.GetEnumerator();

		IEnumerator IEnumerable.GetEnumerator()
			=> GetEnumerator();

		public void Merge(OsMatrix @object)
		{
			if(@object != null)
			{
				foreach (var osm in @object._matches)
					_matches[osm.Key].Merge(osm.Value);
			}
		}
	}
}
