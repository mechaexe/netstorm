﻿namespace NetStorm.Common.Location
{
	public class IPInfo : IIPInfo
	{
		public ILocation Location { get; }

		public string ISP { get; }
		public string AS { get; }
		public string Organisation { get; }

		public IPInfo() : this(null, "", "", "") { }

		public IPInfo(ILocation location, string ISP, string AS, string org)
		{
			Location = location;
			this.ISP = ISP;
			this.AS = AS;
			Organisation = org;
		}

		public string ToPrintString()
		{
			string text = "";
			if (!string.IsNullOrEmpty(ISP))
			{
				text = $"ISP: {ISP}\n";
			}

			if (!string.IsNullOrEmpty(Organisation))
			{
				text += $"ORG: {Organisation}\n";
			}

			text += Location.ToString();

			return text;
		}

		public static bool operator ==(IPInfo left, IPInfo right)
		{
			if (left is null || right is null)
			{
				return left is null && right is null;
			}
			else
			{
				return left.ISP == right.ISP && left.AS == right.AS && left.Organisation == right.Organisation && left.Location == right.Location;
			}
		}

		public static bool operator !=(IPInfo left, IPInfo right) => !(left == right);

		public override bool Equals(object obj)
		{
			if (!(obj is IPInfo i) || obj == null)
			{
				return false;
			}
			else return this == i;
		}

		public override string ToString() => $"[IPInfo] ISP: { ISP } Country: {Location.Country } Region: { Location.Region }";

		public override int GetHashCode() => base.GetHashCode();
	}
}
