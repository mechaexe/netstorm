﻿namespace NetStorm.Common.Location
{
	public interface ILocation
	{
		string City { get; }
		string Country { get; }
		string CountryCode { get; }
		string Region { get; }
		string Timezone { get; }
		int ZIP { get; }
		ICoordinates Coordinates { get; }
	}
}