﻿namespace NetStorm.Common.Location
{
	public struct Coordinates : ICoordinates
	{
		public decimal Longitude { get; }
		public decimal Latitude { get; }

		public Coordinates(decimal lon, decimal lat)
		{
			Longitude = lon;
			Latitude = lat;
		}

		public override string ToString()
		{
			return Longitude + "|" + Latitude;
		}

		public override bool Equals(object obj)
		{
			if (!(obj is ICoordinates c) || obj == null)
			{
				return false;
			}

			return this == c;
		}

		public override int GetHashCode()
			=> Longitude.GetHashCode() + Latitude.GetHashCode();

		public static bool operator ==(Coordinates left, ICoordinates right)
		{
			return left.Latitude == right.Latitude &&
				left.Longitude == right.Longitude;
		}

		public static bool operator !=(Coordinates left, ICoordinates right)
			=> !(left == right);
	}
}
