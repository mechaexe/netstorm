﻿namespace NetStorm.Common.Location
{
	public class Location : ILocation
	{
		public ICoordinates Coordinates { get; }
		public string City { get; }
		public string Country { get; }
		public string CountryCode { get; }
		public string Region { get; }
		public string Timezone { get; }
		public int ZIP { get; }

		public Location() : this(new Coordinates(), "", "", "", "", -1, "") { }

		public Location(ICoordinates coordinates, string country, string countryCode, string region, string city, int zip, string timezone)
		{
			Coordinates = coordinates;
			Country = country;
			CountryCode = countryCode;
			Region = region;
			City = city;
			ZIP = zip;
			Timezone = timezone;
		}

		public override bool Equals(object obj)
		{
			if (!(obj is Location l) || obj == null)
			{
				return false;
			}

			return this == l;
		}

		public override string ToString()
		{
			var text = "";
			if (!string.IsNullOrEmpty(Region) || !string.IsNullOrEmpty(Country))
			{
				if (!string.IsNullOrEmpty(Region) && !string.IsNullOrEmpty(Country))
				{
					text += $"{Region}, {Country}\n";
				}
				else if (string.IsNullOrEmpty(Region))
				{
					text += $"{Region}\n";
				}
				else
				{
					text += $"{Country}\n";
				}
			}

			if (ZIP > 0 || !string.IsNullOrEmpty(City))
			{
				if (ZIP > 0 && !string.IsNullOrEmpty(City))
				{
					text = text + ZIP + ", " + City;
				}
				else if (ZIP > 0)
				{
					text += ZIP;
				}
				else
				{
					text += City;
				}
			}
			return text;
		}

		public override int GetHashCode()
		{
			return base.GetHashCode();
		}

		public static bool operator ==(Location left, ILocation right)
		{
			return left.Country == right.Country &&
				left.CountryCode == right.CountryCode &&
				left.Region == right.Region &&
				left.Timezone == right.Timezone &&
				left.ZIP == right.ZIP &&
				left.Coordinates == right.Coordinates;
		}

		public static bool operator !=(Location left, ILocation right)
		{
			return !(left == right);
		}
	}
}
