﻿namespace NetStorm.Common.Location
{
	public interface IIPInfo
	{
		string AS { get; }
		string ISP { get; }
		string Organisation { get; }
		ILocation Location { get; }

		string ToPrintString();
	}
}