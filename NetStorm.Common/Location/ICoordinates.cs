﻿namespace NetStorm.Common.Location
{
	public interface ICoordinates
	{
		decimal Latitude { get; }
		decimal Longitude { get; }
	}
}