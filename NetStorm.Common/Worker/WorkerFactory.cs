﻿using NetStorm.Common.Worker.Routing;

namespace NetStorm.Common.Worker
{
	public static class WorkerFactory
	{
		public static ICancellationToken GetCancellationToken() => new CancellationToken(50);

		public static ICancellationToken GetCancellationToken(int pollTime) => new CancellationToken(pollTime);

		public static INicRouter GetNicRouter(string name) => new SingleNicRouter(name);
	}
}
