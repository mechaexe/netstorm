﻿using System;

namespace NetStorm.Common.Worker
{
	public interface ICancellationToken
	{
		bool CanContinue();
		bool CanContinue(double millis);
		bool CanContinue(TimeSpan timeSpan);
		void WaitForExit();
		void Reset();
		void Set();
	}
}