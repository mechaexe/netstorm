﻿using NetStorm.Common.Devices;

namespace NetStorm.Common.Worker.Routing
{
	public interface INicRouter : INicWorker
	{
		void SetRoute(INetworkInterface source, INetworkInterface destination);
		void StopAndClearRoutes();
	}
}