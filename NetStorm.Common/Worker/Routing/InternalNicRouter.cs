﻿using NetStorm.Common.Devices;
using NetStorm.Common.Worker.PacketReceiver;
using PacketDotNet;
using System.Collections.Generic;
using System.Net;
using System.Net.NetworkInformation;

namespace NetStorm.Common.Worker.Routing
{
	class InternalNicRouter : BaseNicWorker
	{
		public override string Name { get; }

		private IPacketTranceiver _packetReceiver;

		private Dictionary<PhysicalAddress, PhysicalAddress> _hwMapping;
		private Dictionary<PhysicalAddress, IPAddress> _l3Mapping;

		public InternalNicRouter(string name)
		{
			Name = name;
			_hwMapping = new Dictionary<PhysicalAddress, PhysicalAddress>();
			_l3Mapping = new Dictionary<PhysicalAddress, IPAddress>();
		}

		public void AddRoute(IPAddress sourceIp, PhysicalAddress sourceMac, IPAddress destIp, PhysicalAddress destMac)
		{
			lock (_hwMapping)
			{
				_hwMapping.TryAdd(sourceMac, destMac);
				_hwMapping.TryAdd(destMac, sourceMac);
			}
			lock (_l3Mapping)
			{
				_l3Mapping.TryAdd(sourceMac, sourceIp);
				_l3Mapping.TryAdd(destMac, destIp);
			}
		}

		public void RemoveRouteFor(PhysicalAddress hwAddr)
		{
			lock (_hwMapping)
			{
				_hwMapping.Remove(hwAddr);
			}
			lock (_l3Mapping)
			{
				_l3Mapping.Remove(hwAddr);
			}
			if (_hwMapping.Count == 0 && Running)
				Abort();
		}

		public void ClearRoutes()
		{
			_hwMapping.Clear();
			_l3Mapping.Clear();
		}

		private void OnPacketReceived(Packet packet, ILocalNetworkInterface nic)
		{
			if (packet is EthernetPacket eth &&
				_hwMapping.ContainsKey(eth.SourceHardwareAddress))
			{
				//var ipStack = ipPack.SourceAddress.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork ? IpStackMode.IPv4 : IpStackMode.IPv6;

				eth.DestinationHardwareAddress = _hwMapping[eth.SourceHardwareAddress];
				eth.SourceHardwareAddress = nic.Mac;

				//ipPack.SourceAddress = nic.GetPreferredStack(ipStack).IPAddress;
				//ipPack.DestinationAddress = _l3Mapping[eth.DestinationHardwareAddress];
				//ipPack.UpdateCalculatedValues();

				eth.UpdateCalculatedValues();
				nic.Transmit(eth);
			}
		}

		public override void Stop()
		{
			base.Stop();
			ClearRoutes();
		}

		public override void Abort()
		{
			_packetReceiver.Revoke(OnPacketReceived);
			Running = false;
		}

		public override bool Start()
		{
			if (!Running)
			{
				_packetReceiver = Nic.GetPacketTranceiver();
				_packetReceiver.Register(OnPacketReceived);
				Running = true;
			}
			return Running;
		}

		public override void WaitForExit() { }
	}
}
