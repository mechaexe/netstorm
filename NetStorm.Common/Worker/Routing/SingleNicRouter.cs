﻿using NetStorm.Common.Devices;

namespace NetStorm.Common.Worker.Routing
{
	class SingleNicRouter : InternalNicRouter, INicRouter
	{
		public SingleNicRouter(string name) : base(name)
		{
		}

		public void StopAndClearRoutes()
		{
			Abort();
			ClearRoutes();
		}

		public void SetRoute(INetworkInterface source, INetworkInterface destination)
		{
			ClearRoutes();
			if(source.Supports(IpStackMode.IPv4) && destination.Supports(IpStackMode.IPv4))
				AddRoute(source.IPv4Stack.IPAddress, source.Mac, destination.IPv4Stack.IPAddress, source.Mac);
			if (source.Supports(IpStackMode.IPv6) && destination.Supports(IpStackMode.IPv6))
				AddRoute(source.IPv6Stack.IPAddress, source.Mac, destination.IPv6Stack.IPAddress, source.Mac);
		}
	}
}
