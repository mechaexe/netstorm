﻿using NetStorm.Common.Logging;
using System;

namespace NetStorm.Common.Worker
{
	public abstract class BaseWorker : IWorker
	{
		public abstract string Name { get; }
		public bool Running
		{
			get => _running;
			protected set
			{
				if (_running != value)
				{
					_running = value;
					if (_running)
					{
						LogEngine.GlobalLog.Log($"{Name} started execution");
						Started?.Invoke(this, new EventArgs());
					}
					else
					{
						LogEngine.GlobalLog.Log($"{Name} stopped execution");
						Stopped?.Invoke(this, new EventArgs());
					}
				}
			}
		}

		public event EventHandler Stopped, Started;
		private bool _running;

		public virtual void Stop()
		{
			if(Running)
			{
				Abort();
				WaitForExit();
			}
		}

		public abstract void Abort();
		public abstract void WaitForExit();
		public abstract bool Start();
	}
}
