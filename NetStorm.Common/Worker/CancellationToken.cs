﻿using System;
using System.Threading;

namespace NetStorm.Common.Worker
{
	class CancellationToken : ICancellationToken
	{
		private readonly int pollTime;
		private bool canContinue;

		public CancellationToken(int pollTime) { this.pollTime = pollTime; }

		public bool CanContinue() => canContinue;

		public bool CanContinue(double millis)
		{
			var rest = (int)(millis % pollTime);

			for (int i = 0; i < millis / pollTime; i++)
			{
				if (canContinue)
					Thread.Sleep(pollTime);
				else
					return false;
			}

			Thread.Sleep(rest);

			return canContinue;
		}

		public bool CanContinue(TimeSpan timeSpan) => CanContinue(timeSpan.TotalMilliseconds);

		public void Set() => canContinue = true;

		public void Reset() => canContinue = false;

		public void WaitForExit()
		{
			while (canContinue)
				Thread.Sleep(pollTime);
		}
	}
}
