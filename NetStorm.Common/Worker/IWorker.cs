﻿using System;

namespace NetStorm.Common.Worker
{
	public interface IWorker
	{
		bool Running { get; }
		string Name { get; }
		event EventHandler Stopped, Started;
		void Abort();
		void WaitForExit();
		bool Start();
		void Stop();
	}
}
