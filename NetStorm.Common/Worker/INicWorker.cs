﻿using NetStorm.Common.Devices;

namespace NetStorm.Common.Worker
{
	public interface INicWorker : IWorker
	{
		ILocalNetworkInterface Nic { get; }
		void SetNIC(ILocalNetworkInterface nic);
	}
}