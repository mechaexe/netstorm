﻿using NetStorm.Common.Devices;

namespace NetStorm.Common.Worker
{
	public abstract class BaseNicWorker : BaseWorker, INicWorker
	{
		public ILocalNetworkInterface Nic { get; private set; }

		public virtual void SetNIC(ILocalNetworkInterface nic)
		{
			var running = Running;
			if (running)
				Stop();

			if(Nic != null)
				Nic.CaptureStopped -= Nic_CaptureStopped;

			Nic = nic;

			if (Nic != null)
				Nic.CaptureStopped += Nic_CaptureStopped;   //Add hook to automatically abort worker when NIC closes

			if (running)
				Start();
		}

		private void Nic_CaptureStopped(object sender, System.EventArgs e)
			=> Abort();
	}
}
