﻿using NetStorm.Common.Devices;
using PacketDotNet;
using System.Collections.Generic;
using System.Threading;

namespace NetStorm.Common.Worker.PacketReceiver
{
	class DefaultPacketTranceiver : BaseWorker, IPacketTranceiver
	{
		public override string Name => "Packet receiver";

		private Thread _bgWorker;
		private OnPacketsReceived _onPacketsReceived;
		private ICancellationToken _cancellationToken;
		private List<Packet> _packets;
		private readonly ILocalNetworkInterface _nic;

		public DefaultPacketTranceiver(ILocalNetworkInterface nic)
		{
			_onPacketsReceived = null;
			_nic = nic;
			_nic.CaptureStopped += (_, __) => Abort();
		}

		private void Nic_PacketCaptured(object sender, Packet e)
		{
			if(Running)
				lock (_packets)
					_packets.Add(e);
		}

		private void DoWork()
		{
			List<Packet> packetsCopy = new List<Packet>();
			do
			{
				lock (_packets)
				{
					packetsCopy.AddRange(_packets);
					_packets.Clear();
				}
				foreach(var packet in packetsCopy)
					_onPacketsReceived?.Invoke(packet, _nic);
				packetsCopy.Clear();
			} while (_cancellationToken.CanContinue(250));

			_cancellationToken = null;
			_packets = null;
			Running = false;
		}

		public override bool Start()
		{
			if (_nic != null && !Running)
			{
				_cancellationToken = WorkerFactory.GetCancellationToken();
				_packets = new List<Packet>();
				_nic.PacketCaptured += Nic_PacketCaptured;
				_nic.StartCapture();
				_cancellationToken.Set();

				_bgWorker = new Thread(DoWork)
				{
					IsBackground = true,
					Name = $"{Name} thread"
				};

				_bgWorker.Start();
				Running = true;
			}
			return Running;
		}

		public void Register(OnPacketsReceived onPacketsReceived)
		{
			_onPacketsReceived += onPacketsReceived;
			Start();
		}

		public void Revoke(OnPacketsReceived onPacketsReceived)
		{
			if(_onPacketsReceived != null)	//Prevent accidental call of Abort()
			{
				_onPacketsReceived -= onPacketsReceived;
				if (_onPacketsReceived == null)
					Abort();
			}
		}

		public override void Abort()
		{
			_cancellationToken?.Reset();
			if(_nic != null)
			{
				_nic.PacketCaptured -= Nic_PacketCaptured;
				_nic.StopCapture();
			}
		}

		public override void WaitForExit() 
			=> _bgWorker?.Join();

		public void SendPackets(params Packet[] packets)
		{
			foreach (var p in packets)
				_nic.Transmit(p);
		}
	}

	public delegate void OnPacketsReceived(Packet packet, ILocalNetworkInterface nic);
}
