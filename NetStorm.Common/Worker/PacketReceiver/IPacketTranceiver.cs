﻿using PacketDotNet;

namespace NetStorm.Common.Worker.PacketReceiver
{
	public interface IPacketTranceiver : IWorker
	{
		void Register(OnPacketsReceived onPacketsReceived);
		void Revoke(OnPacketsReceived onPacketsReceived);
		void SendPackets(params Packet[] packets);
	}
}