﻿using NetStorm.Common.Services;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace NetStorm.Common.Classes
{
	public class Port : IPort
	{
		public ushort Number { get; }
		public PortStatus Status
		{
			get => portStatus;
			set
			{
				if(portStatus != value)
				{
					portStatus = value;
					OnPropertyChanged();
				}
			}
		}
		public Protocol Protocol
		{
			get => protocol;
			set
			{
				if (protocol != value)
				{
					protocol = value;
					OnPropertyChanged();
				}
			}
		}
		public IService Service
		{
			get => service;
			set
			{
				if(service == null || !service.Equals(value))
				{
					service = value;
					OnPropertyChanged();
				}
			}
		}

		public event PropertyChangedEventHandler PropertyChanged;
		private IService service;
		private PortStatus portStatus;
		private Protocol protocol;

		public Port(ushort number) : this(number, null, Protocol.Unknown, PortStatus.Unknown)
		{ }

		public Port(ushort number, IService service, Protocol protocol, PortStatus status)
		{
			Number = number;
			Service = service;
			Protocol = protocol;
			Status = status;
		}

		protected void OnPropertyChanged([CallerMemberName] string caller = "")
			=> PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(caller));

		public override bool Equals(object obj) => obj is IPort p && Number.Equals(p.Number) && (int)(Protocol & p.Protocol) == (int)p.Protocol;

		public override int GetHashCode() => Number.GetHashCode();

		public static IPort GetPort(ushort number, Protocol protocol, PortStatus status) => new Port(number, null, protocol, status);

		public void Merge(IPort @object)
		{
			if(Equals(@object))
			{
				Status = @object.Status;
				Service?.Merge(@object.Service);
			}
		}
	}
}
