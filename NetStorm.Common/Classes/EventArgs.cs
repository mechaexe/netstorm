﻿namespace System
{
	public class EventArgs<T> : EventArgs
	{
		public T Argument { get; }

		public EventArgs(T argument)
		{
			Argument = argument;
		}
	}
}
