﻿using System.Net;

namespace NetStorm.Common.Classes
{
	public class IPStack : IIPStack
	{
		public IPAddress IPAddress { get; }
		public IPNetwork Network { get; }

		public IPStack(bool ipv4 = true) : this(ipv4 ? IPAddress.None : IPAddress.IPv6None, (byte)(ipv4 ? 32 : 128))
		{ }

		public IPStack(string address) : this(IPAddress.Parse(address), IPNetwork.Parse(address, new CidrGuess()))
		{ }

		public IPStack(IPAddress address) : this(address, IPNetwork.Parse(address.ToString(), new CidrGuess()))
		{ }

		public IPStack(IPAddress address, IPNetwork network)
		{
			IPAddress = address;
			Network = network;
		}

		public IPStack(IPAddress address, byte prefix)
		{
			IPAddress = address;
			Network = IPNetwork.Parse(IPAddress.ToString(), prefix);
		}

		public override bool Equals(object obj)
			=> obj is IPStack stack && IPAddress.Equals(stack.IPAddress) && stack.Network.Equals(Network);

		public override int GetHashCode()
			=> IPAddress.GetHashCode() + Network.GetHashCode();

		public override string ToString()
			=> IPAddress.ToString() + "/" + Network.Cidr;
	}

	class CidrGuess : ICidrGuess
	{
		public bool TryGuessCidr(string ip, out byte cidr)
		{
			var ipaddr = IPAddress.Parse(ip);
			if (ipaddr.IsIPv6LinkLocal)
			{
				cidr = 127;
				return true;
			}
			else
			{
				return IPNetwork.TryGuessCidr(ip, out cidr);
			}
		}
	}
}
