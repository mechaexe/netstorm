﻿namespace NetStorm.Common.Classes
{
	class Vendor : IVendor
	{
		public string Manufacturer { get; }
		public string Address { get; }
		public string Prefix { get; }

		public Vendor() : this("", "", "") { }

		public Vendor(string manufacturer, string address, string prefix)
		{
			Manufacturer = manufacturer;
			Address = address;
			Prefix = prefix;
		}
	}
}
