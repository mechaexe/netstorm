﻿using System.Net;

namespace NetStorm.Common.Classes
{
	public interface IIPStack
	{
		IPAddress IPAddress { get; }
		IPNetwork Network { get; }
	}
}