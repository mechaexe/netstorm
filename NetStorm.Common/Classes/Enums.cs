﻿using System;

namespace NetStorm.Common.Classes
{
	[Flags]
	public enum PortStatus
	{
		Closed = 1,
		Filtered = 2,
		Open = Filtered << 1,
		Unknown = Closed | Filtered | Open
	}

	public enum Protocol
	{
		Tcp,
		Udp,
		Sctp,
		Dccp,
		Unknown = -1
	}
}
