﻿using NetStorm.Common.Interfaces;
using NetStorm.Common.Services;
using System.ComponentModel;

namespace NetStorm.Common.Classes
{
	public interface IPort : INotifyPropertyChanged, IMergable<IPort>
	{
		ushort Number { get; }
		Protocol Protocol { get; }
		IService Service { get; set; }
		PortStatus Status { get; set; }
	}
}