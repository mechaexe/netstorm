﻿namespace NetStorm.Common.Classes
{
	public interface IVendor
	{
		string Address { get; }
		string Manufacturer { get; }
		string Prefix { get; }
	}
}