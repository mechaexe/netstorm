﻿namespace System.Xml.Linq
{
	public static class XDocumentExtentions
	{
		public static XElement GetElementFromPath(this XElement element, string path)
		{
			XElement res = element;
			foreach (var elem in path.Split('/'))
			{
				if (res != null)
					res = res?.Element(res.Name.Namespace + elem);
				else break;
			}

			return res;
		}
	}
}
