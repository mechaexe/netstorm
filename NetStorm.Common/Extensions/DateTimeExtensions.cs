﻿
namespace System
{
	public static class DateTimeExtensions
	{
		public static string ToDurationTextEN(this TimeSpan timeSpan)
			=> ToDurationText(timeSpan, new string[] { "Days", "Hours", "Minutes", "Seconds" });

		public static string ToDurationText(this TimeSpan timeSpan, string[] abbreviations)
		{
			var res = "";
			if (timeSpan.Days > 0)
				res += $"{timeSpan.Days} {abbreviations[0]}";
			if (timeSpan.Hours > 0)
				res += $"{timeSpan.Hours} {abbreviations[1]}";
			if (timeSpan.Minutes > 0)
				res += $"{timeSpan.Minutes} {abbreviations[2]}";
			if (timeSpan.Seconds > 0)
				res += $"{timeSpan.Seconds} {abbreviations[3]}";
			return res;
		}
	}
}
