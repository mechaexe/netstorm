﻿using System.Collections.Generic;

namespace System.Linq
{
	public static class ListExtensions
	{
		public static IEnumerable<T> GetElementsWithType<T>(this List<T> me, Type t)
			=> me.Where(x => x.GetType().Equals(t));

		public static bool RemoveType<T>(this List<T> me, Type t)
		{
			int deleted = 0;
			foreach (var r in me.GetElementsWithType(t).ToArray())
				deleted += me.Remove(r) ? 1 : 0;
			return deleted > 0;
		}
	}
}
