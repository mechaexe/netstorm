﻿namespace System.Net.NetworkInformation
{
	public static class PhysicalAddressExtension
	{
		private static readonly Random random = new Random();
		public static readonly PhysicalAddress Broadcast = PhysicalAddress.Parse("FFFFFFFFFFFF");
		public static readonly PhysicalAddress Null = PhysicalAddress.Parse("000000000000");

		public static string ToVendorString(this PhysicalAddress val)
		{
			var retVal = "";

			if (val.ToString().Length >= 6)
			{
				retVal = val.ToString().Substring(0, 6);
			}

			return retVal;
		}

		public static PhysicalAddress GenerateRandom()
		{
			var buffer = new byte[6];
			random.NextBytes(buffer);
			return new PhysicalAddress(buffer);
		}

		public static bool TryParse(string mac, out PhysicalAddress pmac)
			=> (pmac = Parse(mac)) != PhysicalAddress.None;

		public static PhysicalAddress Parse(string mac)
		{
			var pmac = PhysicalAddress.None;
			var replaceables = new[] {
				':',
				'-',
				'.',
				'_'
			};
			try { pmac = PhysicalAddress.Parse(string.Join("", mac.Split(replaceables)).ToUpper()); }
			catch { }
			return pmac;
		}

		public static string ToCannonicalString(this PhysicalAddress val)
			=> val.ToString().AddStringInterval(2, ":");
	}
}
