﻿using System.Collections.Generic;
using System.Linq;

namespace System.Net
{
	public enum IpClass
	{
		A,
		B,
		C,
		D,
		E,
		Classless
	}

	public static class IPAddressExtensions
	{
		public static readonly IPNetwork CarrierGradeNat = IPNetwork.Parse("100.64.0.0", 10);

		public static readonly Dictionary<IpClass, IPNetwork> NetworkClassesV4 = new Dictionary<IpClass, IPNetwork>
		{
			{ IpClass.A, IPNetwork.Parse("0.0.0.0", 1) },
			{ IpClass.B, IPNetwork.Parse("128.0.0.0", 2) },
			{ IpClass.C, IPNetwork.Parse("192.0.0.0", 3) },
			{ IpClass.D, IPNetwork.Parse("224.0.0.0", 4) },	//Mcast addresses
			{ IpClass.E, IPNetwork.Parse("240.0.0.0", 4) }	//IANA Reserved
		};

		public static readonly IPNetwork[] PrivateNetworksV4 = new IPNetwork[]
		{
			IPNetwork.Parse("127.0.0.0", 8),	// Loopback addresses
			IPNetwork.Parse("169.254.0.0", 16),	// APIPA
			IPNetwork.Parse("10.0.0.0", 8),
			IPNetwork.Parse("172.16.0.0", 12),
			IPNetwork.Parse("192.168.0.0", 16)
		};

		public static readonly IPNetwork[] PrivateNetworksV6 = new IPNetwork[]
		{
			IPNetwork.Parse("::1", 128),	//Loopback
			IPNetwork.Parse("fd00::", 8)	//Link-local
		};

		public static IpClass GetIpClass(this IPAddress address)
		{
			foreach (var ipn in NetworkClassesV4)
				if (ipn.Value.Contains(address))
					return ipn.Key;
			return IpClass.Classless;
		} 

		public static bool IsPrivate(this IPAddress address)
		{
			if (address.AddressFamily == Sockets.AddressFamily.InterNetwork)
			{
				return PrivateNetworksV4.Any(x => x.Contains(address));
			}
			else
			{
				return PrivateNetworksV6.Any(x => x.Contains(address));
			}
		}
	}
}
