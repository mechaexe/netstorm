﻿namespace System.Collections.Generic
{
	public static class DictionaryExtensions
	{
		public static bool TryAdd<TKey, TValue>(this IDictionary<TKey, TValue> dict, TKey key, TValue value, bool replaceIfNotNull = false)
		{
			bool containsKey = dict.ContainsKey(key);

			if (!containsKey)
				dict.Add(key, value);
			else if (replaceIfNotNull && value != null)
				dict[key] = value;

			return !containsKey;
		}
	}
}
