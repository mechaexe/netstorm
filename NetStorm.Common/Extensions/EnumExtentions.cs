﻿using NetStorm.Common.Devices;

namespace System.Net.Sockets
{
	public static class EnumExtentions
	{
		public static IpStackMode ToStackMode(this AddressFamily addressFamily)
			=> addressFamily == AddressFamily.InterNetwork ? IpStackMode.IPv4 :
			addressFamily == AddressFamily.InterNetworkV6 ? IpStackMode.IPv6 :
			IpStackMode.Disabled;
	}
}
