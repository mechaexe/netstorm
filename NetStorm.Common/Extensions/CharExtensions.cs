﻿namespace NetStorm.Common.Extensions
{
	public static class CharExtensions
	{
		public static bool IsPrintable(this char c)
			=> c >= 32 && c <= 126;
	}
}
