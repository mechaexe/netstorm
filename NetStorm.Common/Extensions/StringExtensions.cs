﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text.RegularExpressions;

namespace System
{
	public static class StringExtensions
	{
		/// <summary>
		/// Reads until a specific character is found
		/// </summary>
		/// <param name="self"></param>
		/// <param name="startIndex">Start index</param>
		/// <param name="until">Read until the character is found (it will be excluded)</param>
		/// <returns></returns>
		public static string ReadUntil(this string self, int startIndex, char until)
		{
			var res = "";
			for (int i = startIndex; i < self.Length && self[i] != until; i++)
				res += self[i];
			return res;
		}

		public static PhysicalAddress ToPhysicalAddress(this string mac)
		{
			string[] removables = new string[] { ":", "-", ".", "_", " " };
			mac = mac.ToUpper();
			for (int i = 0; i < removables.Length; i++)
			{
				mac = mac.Replace(removables[i], "");
			}

			PhysicalAddress retVal;
			try { retVal = PhysicalAddress.Parse(mac); }
			catch { retVal = PhysicalAddress.None; }

			return retVal;
		}

		public static string AddStringInterval(this string me, int interval, string str)
		{
			var output = me;	// Copy string
			for (int i = interval; i < output.Length; i += interval + str.Length)
				output = output.Insert(i, str);
			return output;
		}

		public static string[] RemoveEmptyEntries(this string[] input)
		{
			return input.Where((val) => val != string.Empty).ToArray();
		}

		public static string[] Split(this string s, string @string)
		{
			@string = Regex.Escape(@string);
			return Regex.Split(s, @string);
		}

		public static string RemoveEmptyLines(this string input)
		{
			return Regex.Replace(input, @"^\s*$\n|\r", string.Empty, RegexOptions.Multiline).TrimEnd();
		}

		public static bool StartsWithAny(this string str, IEnumerable<string> strings)
		{
			foreach (var s in strings)
				if (str.StartsWith(s))
					return true;
			return false;
		}

		public static string RemoveWhitespace(this string str, string joinChar = "")
		{
			return string.Join(joinChar, str.Split(default(string[]), StringSplitOptions.RemoveEmptyEntries));
		}
	}
}
