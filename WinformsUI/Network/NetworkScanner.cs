﻿using NetStorm.GUI.Enumeration.TextProvider;
using NetStorm.Common.Devices;
using NetStorm.CoreNG;
using NetStorm.CoreNG.Modules.NetworkScanner;
using NetStorm.CoreNG.Modules.NetworkScanner.Detection;
using NetStorm.CoreNG.Modules.Transformation;
using NetStorm.CoreNG.Worker;
using NetStorm.UILib;
using NetStorm.UILib.Coloring;
using NetStorm.UILib.Controls;
using NetStorm.UILib.Features;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Net.NetworkInformation;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NetStorm.GUI.Network
{
	public partial class NetworkScanner : UserControl, INetStormPage
	{
		public delegate void SetActivePage(int index);

		private IWorkerNG _worker;
		private ILocalNetworkInterface _localInterface;
		private INetworkDevice _currentDevice;

		private readonly IDropShadow dropShadow;
		private readonly INetworkScanner scanner;
		private readonly List<INetworkDevice> deviceList;
		private readonly SetInfoText setInfoText;
		private readonly DeviceDetails deviceDetailsPage;
		private readonly SetActivePage setActivePage;
		private readonly CompositeTransformator transformator;
		private readonly CompositeDetector detector;

		public NetworkScanner(INetworkScanner scanner, SetInfoText setInfoText, DeviceDetails deviceDetailsPage, SetActivePage action)
		{
			InitializeComponent();
			deviceList = new List<INetworkDevice>();
			dropShadow = FeatureFactory.CreateDropShadow(optionWrapper);

			this.scanner = scanner;
			this.scanner.Detector = detector = new CompositeDetector();
			this.scanner.Transformator = transformator = new CompositeTransformator();
			transformator.AddTransformation(new StatisticsTransform());

			this.setInfoText = setInfoText;
			this.deviceDetailsPage = deviceDetailsPage;
			setActivePage = action;
		}

		public void SetLocalInterface(ILocalNetworkInterface nic)
		{
			_localInterface = nic;
			deviceDetailsPage.SetLocalInterface(nic);
		}

		private void ToggleTransformation(ITransformator transformator, bool add)
		{
			if (add)
				this.transformator.AddTransformation(transformator);
			else
				this.transformator.RemoveTransformation(transformator.GetType());
		}

		private void SetDetectionMethod(bool arp, bool nd, bool upnp)
		{
			detector.RemoveAllDetectors();
			if (!(arp || nd || upnp))
				detector.AddDetector(DetectionFactory.GetPassiveResolver());
			else
			{
				if (arp)
					detector.AddDetector(DetectionFactory.GetArpResolver(LocalEnvironment.Api));
				if (nd)
					detector.AddDetector(DetectionFactory.GetNeighborResolver());
				if (upnp)
					detector.AddDetector(DetectionFactory.GetUPnPResolver());
			}
		}

		private void ActionButton_Click(object sender, EventArgs e)
		{
			if (NicManager.Instance.Count > 0 && NicManager.Instance.PrimaryDeviceIndex >= 0)
				SetLocalInterface(NicManager.Instance.PrimaryDevice);

			if (scanner.Running)
				scanner.Stop();
			else if (_localInterface != null)
			{
				var items = deviceView.Items;
				deviceView.Items.Clear();
				try
				{
					var ops = new NetworkOptions(_localInterface.IPv4Stack.Network, _localInterface);
					scanner.SetOptions(ops);
					scanner.Start(_localInterface.GetPacketTranceiver(), _worker);
					ndScanToggle.Enabled =
						arpScanToggle.Enabled =
						upnpScanToggle.Enabled = false;
					deviceList.Clear();
					setInfoText($"Scanning {ops.NetworkV4}");
				}
				catch
				{
					deviceView.Items.AddRange(items);
				}
			}
		}

		#region Device printing
		private void DeviceView_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
		{
			if ((e.Item is AutoUpdateListviewItem<INetworkDevice> item))
			{
				PrintDeviceInfo(item.Object);
			}
		}

		private void HistoryView_AfterSelect(object sender, TreeViewEventArgs e)
		{
			if (!_worker.Running && e.Node.Tag is ScanResult scanRes)
			{
				setInfoText($"You are viewing a scan from {scanRes.ModuleStarted} ({scanRes.Options.NetworkV4})");
				deviceView.Items.Clear();
				deviceList.Clear();
				deviceList.AddRange(scanRes.Devices);
				FilterDevices(searchBox.Text);
			}
		}

		private void PrintDeviceToListview(string search, INetworkDevice device)
		{
			if (search == "" ||
				(!string.IsNullOrEmpty(device.Hostname) && device.Hostname.ToLower().Contains(search.ToLower())) ||
				device.NetworkInterface.GetPreferredStack(IpStackMode.IPv4).ToString().Contains(search) ||
				device.NetworkInterface.Mac.ToString().ToLower().Contains(search.Replace(":", "").Replace(".", "").Replace("-", "").ToLower()))
			{
				lock (deviceView.Items)
				{
					deviceView.Items.Add(CreateListviewItem(device));
				}
			}
		}

		private void PrintDeviceInfo(INetworkDevice device)
		{
			if(_currentDevice != null)
				_currentDevice.PropertyChanged -= CurrentDevice_PropertyChanged;

			_currentDevice = device;
			deviceBoard.Text = new DeviceNetworkTextProvider().GetText(device);
			UpdateInfo(device);
			_currentDevice.PropertyChanged += CurrentDevice_PropertyChanged;
			deviceDetailsPage.SetDevice(device);
		}

		private void UpdateInfo(INetworkDevice device)
		{
			titleLabel.IndicatorColor = device.Statistics.DeviceState == DeviceState.Offline ? Colors.Orange : Colors.Green;
			titleLabel.Text = device.GetHostIdentifier(IpStackMode.IPv4);
		}

		private void CurrentDevice_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			if(_currentDevice != null)
			{
				deviceBoard.Invoke(new Action(() => deviceBoard.Text = new DeviceNetworkTextProvider().GetText(_currentDevice)));
				titleLabel.Invoke(new Action(() => UpdateInfo(_currentDevice)));
			}
		}
		#endregion

		#region Device filtering
		private void FilterDevices(string text)
		{
			deviceView.Items.Clear();
			foreach (var d in deviceList)
			{
				PrintDeviceToListview(text, d);
			}
		}

		private async void SearchBox_TextChanged(object sender, EventArgs e)
		{
			var changed = false;
			var oldText = searchBox.Text;
			await Task.Run(async () =>
			{
				await Task.Delay(200);
				changed = oldText != searchBox.Text;
			});
			
			if (!changed)
			{
				FilterDevices(oldText);
			}
		}
		#endregion

		private ListViewItem CreateListviewItem(INetworkDevice e)
		{
			var updateItems = new[]
			{
				FeatureFactory.GetAutoUpdateField(e.NetworkInterface, () => e.NetworkInterface.GetPreferredStack(IpStackMode.IPv4).IPAddress.ToString()),
				FeatureFactory.GetAutoUpdateField(e.NetworkInterface, e.NetworkInterface.Mac.ToCannonicalString, nameof(e.NetworkInterface.Mac)),
				FeatureFactory.GetAutoUpdateField(e, () => e.Hostname, nameof(e.Hostname)),
				FeatureFactory.GetAutoUpdateField(e.NetworkInterface, () => e.NetworkInterface.MacVendor.Manufacturer, nameof(e.NetworkInterface.MacVendor)),
			};
			var item = new AutoUpdateListviewItem<INetworkDevice>(e);
			var a = GetGroup(e);
			item.ForeColor = a.Item3;
			item.BackColor = a.Item2;
			item.Group = a.Item1;
			item.OnPaint += Item_OnPaint;
			foreach (var ui in updateItems)
			{
				item.AddField(ui);
			}

			return item;
		}

		private void Item_OnPaint(object sender, INetworkDevice e)
		{
			var func = new Action(() =>
			{
				var item = (ListViewItem)sender;
				var a = GetGroup(e);
				item.ForeColor = a.Item3;
				item.BackColor = a.Item2;
				item.Group = a.Item1;
			});
			if (InvokeRequired)
				deviceView.Invoke(func);
			else
				func();
		}

		private (ListViewGroup, Color, Color) GetGroup(INetworkDevice device)
		{
			ListViewGroup resGroup = deviceView.Groups[3];
			Color resBackColor = Colors.Red;
			Color resForeColor = Colors.Light;

			if (!string.IsNullOrEmpty(device.Hostname))
			{
				resGroup = deviceView.Groups[2];
				resBackColor = Colors.Blue;
				resForeColor = Colors.Light;
			}
			if (device.DeviceType == DeviceType.Gateway)
			{
				resGroup = deviceView.Groups[1];
				resBackColor = Colors.Brown;
				resForeColor = Colors.Light;
			}
			if (device.Statistics.DeviceState == DeviceState.Offline)
			{
				resGroup = deviceView.Groups[3];
				resBackColor = deviceView.BackColor;
				resForeColor = deviceView.ForeColor;
			}
			if (device.DeviceType == DeviceType.LocalMachine)
			{
				resGroup = deviceView.Groups[0];
				resBackColor = Colors.Green;
				resForeColor = Colors.Light;
			}

			return (resGroup, resBackColor, resForeColor);
		}

		private void Scanner_DeviceDetected(object sender, INetworkDevice e)
		{
			var addToList = new Action(() =>
			{
				lock (deviceList)
				{
					deviceList.Add(e);
				}

				PrintDeviceToListview(searchBox.Text, e);
			});
			if (InvokeRequired)
				deviceView.Invoke(addToList);
			else
				addToList();
		}

		private void HookEvents()
		{
			actionButton.Click += ActionButton_Click;
			scanner.DeviceDetected += Scanner_DeviceDetected;
			scanner.ModuleStateChanged += Scanner_ModuleStateChanged;
			searchBox.TextChanged += SearchBox_TextChanged;

			historyView.AfterSelect += HistoryView_AfterSelect;
			deviceView.ItemSelectionChanged += DeviceView_ItemSelectionChanged;
			deviceView.MouseDoubleClick += (_, __) => setActivePage(1);

			var toggle = new Action(() => { SetDetectionMethod(arpScanToggle.Checked, ndScanToggle.Checked, upnpScanToggle.Checked); });
			arpScanToggle.CheckedChanged += (_, __) => toggle();
			ndScanToggle.CheckedChanged += (_, __) => toggle();
			upnpScanToggle.CheckedChanged += (_, __) => toggle();

			rtScanToggle.CheckedChanged += (_, __) => _worker = rtScanToggle.Checked ? (IWorkerNG)new TimerWorkerNg() { Interval = 30000} : new OneShotWorker() { Interval = 30000 };

			resolveHostnameToggle.CheckedChanged += (_, __) => ToggleTransformation(TransformationFactory.GetDnsTransform(), resolveHostnameToggle.Checked);
			fingerprintToggle.CheckedChanged += (_, __) => ToggleTransformation(TransformationFactory.GetFingerprintTransform(), fingerprintToggle.Checked);
			upnpGrabberToggle.CheckedChanged += (_, __) => ToggleTransformation(TransformationFactory.GetUPnPTransform(), upnpGrabberToggle.Checked);
		}

		private void Scanner_ModuleStateChanged(object sender, CoreNG.Modules.StateArgs e)
		{
			var fun = new Action(() =>
			{
				actionButton.Text = _worker.Running ? "Stop" : "Start";
				ndScanToggle.Enabled =
					arpScanToggle.Enabled =
					upnpScanToggle.Enabled = !scanner.Running;

				if (!e.Running)
				{
					var res = scanner.GetResult();
					var node = new TreeNode($"[{DateTime.Now}] {res.Options.NetworkV4} - {res.Devices.Count}")
					{
						Tag = res
					};
					historyView.Nodes.Insert(1, node);
				}
			});
			if (InvokeRequired)
				Invoke(fun);
			else
				fun();
		}

		private void ApplySettings()
		{
			resolveHostnameToggle.Checked =
				upnpGrabberToggle.Checked =
				rtScanToggle.Checked = 
				ndScanToggle.Checked = 
				arpScanToggle.Checked =
				upnpScanToggle.Checked = true;
		}

		protected override void OnLoad(EventArgs e)
		{
			dropShadow.AddControl(optionPanel);
			HookEvents();
			ApplySettings();
			base.OnLoad(e);
		}

		public void SetStyling(ColorScheme colorScheme)
		{
			ColorizeRecursevly(panel2, colorScheme.PrimaryColors, false);
			ColorizeRecursevly(optionPanel, colorScheme.PrimaryColors, true);

			dropShadow.ColorEnd = colorScheme.SecondaryColors.BackColor;
			dropShadow.ColorBegin = ControlPaint.Dark(dropShadow.ColorEnd, 0.01f);

			dropShadow.ShadowSize = 8;
			searchBox.Radius = new CornerRadius(5);
			actionButton.Text = _worker.Running ? "Stop" : "Start";

			searchBox.MouseEffect = true;

			titleLabel.AutoEllipsis = true;
			titleLabel.Font = Fonts.Big;
			titleLabel.Padding = new Padding(20, 0, 0, 0);
			titleLabel.Size = new Size(300, 20);

			deviceBoard.Size = new Size(300, panel1.Height - titleLabel.Height - historyView.Height - 10);
			deviceBoard.Padding = new Padding(20, 20, 10, 0);
			deviceBoard.Font = Fonts.Default;

			foreach (Control c in panel2.Controls)
			{
				c.Margin = new Padding(0, (panel2.Height - c.Height) / 2, 15, 0);
			}
		}

		private void ColorizeRecursevly(Control parent, IColorTable colorTable, bool includeParent)
		{
			if (includeParent)
			{
				if (parent is INetStormControl isc)
					isc.ColorTable = colorTable;
				else
				{
					parent.BackColor = colorTable.BackColor;
					parent.ForeColor = colorTable.ForeColor;
				}
			}

			foreach (Control c in parent.Controls)
			{
				ColorizeRecursevly(c, colorTable, true);
			}
		}
	}
}
