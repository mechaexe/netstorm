﻿
namespace NetStorm.GUI.Network
{
	partial class DeviceDetails
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.controlPanel = new System.Windows.Forms.FlowLayoutPanel();
			this.titleLabel = new NetStorm.UILib.Controls.StatusLabel();
			this.deviceBoard = new UILib.Controls.NSTextBox();
			this.portInfoPanel = new NetStorm.GUI.Panels.FlowPanel();
			this.portListPanel = new NetStorm.GUI.Panels.PortscanPanel();
			this.resolveHostnameButton = new NetStorm.UILib.Controls.Button();
			this.osFingerprintButton = new NetStorm.UILib.Controls.Button();
			this.spoofBtn = new NetStorm.UILib.Controls.Button();
			this.controlPanel.SuspendLayout();
			this.portInfoPanel.SuspendLayout();
			this.portListPanel.SuspendLayout();
			this.SuspendLayout();
			// 
			// controlPanel
			// 
			this.controlPanel.Controls.Add(this.resolveHostnameButton);
			this.controlPanel.Controls.Add(this.osFingerprintButton);
			this.controlPanel.Controls.Add(this.spoofBtn);
			this.controlPanel.Dock = System.Windows.Forms.DockStyle.Top;
			this.controlPanel.Location = new System.Drawing.Point(0, 0);
			this.controlPanel.Margin = new System.Windows.Forms.Padding(0);
			this.controlPanel.Name = "controlPanel";
			this.controlPanel.Size = new System.Drawing.Size(1000, 70);
			this.controlPanel.Padding = new System.Windows.Forms.Padding(320, 0, 0, 0);
			this.controlPanel.TabIndex = 15;
			// 
			// titleLabel
			// 
			this.titleLabel.Location = new System.Drawing.Point(14, 28);
			this.titleLabel.Margin = new System.Windows.Forms.Padding(0);
			this.titleLabel.Name = "titleLabel";
			this.titleLabel.Size = new System.Drawing.Size(300, 70);
			this.titleLabel.TabIndex = 0;
			this.titleLabel.Text = "Device";
			this.titleLabel.IndicatorSize = 15;
			this.titleLabel.Padding = new System.Windows.Forms.Padding(10);
			// 
			// resolveHostnameButton
			// 
			this.resolveHostnameButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
			this.resolveHostnameButton.BorderWidth = 1F;
			this.resolveHostnameButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(239)))), ((int)(((byte)(241)))));
			this.resolveHostnameButton.Location = new System.Drawing.Point(320, 20);
			this.resolveHostnameButton.Margin = new System.Windows.Forms.Padding(0, 20, 18, 0);
			this.resolveHostnameButton.Name = "resolveHostnameButton";
			this.resolveHostnameButton.Padding = new System.Windows.Forms.Padding(5, 5, 5, 5);
			this.resolveHostnameButton.Size = new System.Drawing.Size(125, 24);
			this.resolveHostnameButton.TabIndex = 25;
			this.resolveHostnameButton.Text = "Resolve Hostname";
			this.resolveHostnameButton.TextAlignment = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// osFingerprintButton
			// 
			this.osFingerprintButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
			this.osFingerprintButton.BorderWidth = 1F;
			this.osFingerprintButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(239)))), ((int)(((byte)(241)))));
			this.osFingerprintButton.Location = new System.Drawing.Point(463, 20);
			this.osFingerprintButton.Margin = new System.Windows.Forms.Padding(0, 20, 18, 0);
			this.osFingerprintButton.Name = "osFingerprintButton";
			this.osFingerprintButton.Padding = new System.Windows.Forms.Padding(5, 5, 5, 5);
			this.osFingerprintButton.Size = new System.Drawing.Size(99, 24);
			this.osFingerprintButton.TabIndex = 24;
			this.osFingerprintButton.Text = "OS Fingerprint";
			this.osFingerprintButton.TextAlignment = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// spoofBtn
			// 
			this.spoofBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
			this.spoofBtn.BorderWidth = 1F;
			this.spoofBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(239)))), ((int)(((byte)(241)))));
			this.spoofBtn.Location = new System.Drawing.Point(580, 20);
			this.spoofBtn.Margin = new System.Windows.Forms.Padding(0, 20, 18, 0);
			this.spoofBtn.Name = "spoofBtn";
			this.spoofBtn.Padding = new System.Windows.Forms.Padding(5, 5, 5, 5);
			this.spoofBtn.Size = new System.Drawing.Size(91, 24);
			this.spoofBtn.TabIndex = 26;
			this.spoofBtn.Text = "Spoof Device";
			this.spoofBtn.TextAlignment = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// deviceBoard
			// 
			this.deviceBoard.Anchor = System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom;
			this.deviceBoard.Size = new System.Drawing.Size(300, 100);
			this.deviceBoard.Location = new System.Drawing.Point(10, 69);
			this.deviceBoard.Radius = new UILib.Features.CornerRadius(0, 0, 10, 10);
			this.deviceBoard.Margin = new System.Windows.Forms.Padding(0);
			this.deviceBoard.Multiline = true;
			this.deviceBoard.ReadOnly = true;
			this.deviceBoard.Name = "deviceBoard";
			// 
			// portInfoPanel
			// 
			this.portInfoPanel.Anchor = System.Windows.Forms.AnchorStyles.Right | System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom;
			this.portInfoPanel.Name = "portInfoPanel";
			this.portInfoPanel.Padding = new System.Windows.Forms.Padding(10);
			this.portInfoPanel.Title = "Additional port info";
			this.portInfoPanel.IndicatorSize = 0;
			//
			// portListPanel
			//
			this.portListPanel.Anchor = System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right | System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom;
			this.portListPanel.Title.Text = "Ports";
			this.portListPanel.Title.Width = 50;
			// 
			// DeviceDetails
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.controlPanel);
			this.Controls.Add(this.titleLabel);
			this.Controls.Add(this.deviceBoard);
			this.Controls.Add(this.portInfoPanel);
			this.Controls.Add(this.portListPanel);
			this.Name = "DeviceDetails";
			this.Size = new System.Drawing.Size(1000, 500);
			this.controlPanel.ResumeLayout(false);
			this.portInfoPanel.ResumeLayout(false);
			this.portListPanel.ResumeLayout(false);
			this.controlPanel.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.FlowLayoutPanel controlPanel;
		private NetStorm.GUI.Panels.PortscanPanel portListPanel;
		private NetStorm.GUI.Panels.FlowPanel portInfoPanel;
		private NetStorm.UILib.Controls.StatusLabel titleLabel;
		private NetStorm.UILib.Controls.NSTextBox deviceBoard;
		private NetStorm.UILib.Controls.Button spoofBtn;
		private NetStorm.UILib.Controls.Button resolveHostnameButton;
		private NetStorm.UILib.Controls.Button osFingerprintButton;
	}
}
