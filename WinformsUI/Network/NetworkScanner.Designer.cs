﻿using NetStorm.UILib.Features;

namespace NetStorm.GUI.Network
{
	partial class NetworkScanner
	{
		/// <summary> 
		/// Erforderliche Designervariable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Verwendete Ressourcen bereinigen.
		/// </summary>
		/// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Vom Komponenten-Designer generierter Code

		/// <summary> 
		/// Erforderliche Methode für die Designerunterstützung. 
		/// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
		/// </summary>
		private void InitializeComponent()
		{
			System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("History");
			System.Windows.Forms.ListViewGroup listViewGroup1 = new System.Windows.Forms.ListViewGroup("", System.Windows.Forms.HorizontalAlignment.Left);
			System.Windows.Forms.ListViewGroup listViewGroup2 = new System.Windows.Forms.ListViewGroup("", System.Windows.Forms.HorizontalAlignment.Left);
			System.Windows.Forms.ListViewGroup listViewGroup3 = new System.Windows.Forms.ListViewGroup("", System.Windows.Forms.HorizontalAlignment.Left);
			System.Windows.Forms.ListViewGroup listViewGroup4 = new System.Windows.Forms.ListViewGroup("", System.Windows.Forms.HorizontalAlignment.Left);
			System.Windows.Forms.ListViewGroup listViewGroup5 = new System.Windows.Forms.ListViewGroup("", System.Windows.Forms.HorizontalAlignment.Left);
			this.panel2 = new System.Windows.Forms.FlowLayoutPanel();
			this.searchBox = new NetStorm.UILib.Controls.NSTextBox();
			this.actionButton = new NetStorm.UILib.Controls.Button();
			this.panel1 = new System.Windows.Forms.Panel();
			this.historyView = new NetStorm.UILib.Controls.TreeViewEx();
			this.deviceBoard = new NetStorm.UILib.Controls.NSTextBox();
			this.titleLabel = new UILib.Controls.StatusLabel();
			this.optionWrapper = new System.Windows.Forms.Panel();
			this.optionPanel = new System.Windows.Forms.FlowLayoutPanel();
			this.label1 = new System.Windows.Forms.Label();
			this.rtScanToggle = new NetStorm.UILib.Controls.ToggleButton();
			this.arpScanToggle = new NetStorm.UILib.Controls.ToggleButton();
			this.ndScanToggle = new NetStorm.UILib.Controls.ToggleButton();
			this.upnpScanToggle = new NetStorm.UILib.Controls.ToggleButton();
			this.optionHeader = new System.Windows.Forms.Label();
			this.resolveHostnameToggle = new NetStorm.UILib.Controls.ToggleButton();
			this.fingerprintToggle = new NetStorm.UILib.Controls.ToggleButton();
			this.upnpGrabberToggle = new NetStorm.UILib.Controls.ToggleButton();
			this.deviceView = new NetStorm.UILib.Controls.ListViewEx();
			this.ipAddress = new System.Windows.Forms.ColumnHeader();
			this.macAddress = new System.Windows.Forms.ColumnHeader();
			this.hostname = new System.Windows.Forms.ColumnHeader();
			this.manufacturer = new System.Windows.Forms.ColumnHeader();
			this.panel2.SuspendLayout();
			this.panel1.SuspendLayout();
			this.optionWrapper.SuspendLayout();
			this.optionPanel.SuspendLayout();
			this.SuspendLayout();
			// 
			// titleLabel
			// 
			this.titleLabel.Location = new System.Drawing.Point(0, 0);
			this.titleLabel.Margin = new System.Windows.Forms.Padding(0);
			this.titleLabel.Name = "titleLabel";
			this.titleLabel.Size = new System.Drawing.Size(300, 30);
			this.titleLabel.TabIndex = 0;
			this.titleLabel.Text = "Device";
			this.titleLabel.IndicatorSize = 15;
			this.titleLabel.Padding = new System.Windows.Forms.Padding(10);
			this.titleLabel.Dock = System.Windows.Forms.DockStyle.Top;
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.searchBox);
			this.panel2.Controls.Add(this.actionButton);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel2.Location = new System.Drawing.Point(0, 0);
			this.panel2.Margin = new System.Windows.Forms.Padding(0);
			this.panel2.Name = "panel2";
			this.panel2.Padding = new System.Windows.Forms.Padding(320, 0, 0, 0);
			this.panel2.Size = new System.Drawing.Size(785, 70);
			this.panel2.TabIndex = 20;
			// 
			// searchBox
			// 
			this.searchBox.BackColor = System.Drawing.SystemColors.ControlDark;
			this.searchBox.BorderWidth = 0F;
			this.searchBox.CueText = "Search";
			this.searchBox.Icon = null;
			this.searchBox.Location = new System.Drawing.Point(324, 20);
			this.searchBox.Margin = new System.Windows.Forms.Padding(4, 20, 4, 3);
			this.searchBox.MouseEffect = true;
			this.searchBox.Name = "searchBox";
			this.searchBox.Radius = new CornerRadius(5);
			this.searchBox.Size = new System.Drawing.Size(300, 28);
			this.searchBox.TabIndex = 18;
			this.searchBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// actionButton
			// 
			this.actionButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
			this.actionButton.BorderWidth = 1F;
			this.actionButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(239)))), ((int)(((byte)(241)))));
			this.actionButton.Location = new System.Drawing.Point(632, 20);
			this.actionButton.Margin = new System.Windows.Forms.Padding(4, 20, 4, 3);
			this.actionButton.MouseEffect = true;
			this.actionButton.Name = "actionButton";
			this.actionButton.Padding = new System.Windows.Forms.Padding(7);
			this.actionButton.Radius = new CornerRadius(5);
			this.actionButton.Size = new System.Drawing.Size(80, 28);
			this.actionButton.TabIndex = 17;
			this.actionButton.Text = "Start/Stop";
			this.actionButton.TextAlignment = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.historyView);
			this.panel1.Controls.Add(this.deviceBoard);
			this.panel1.Controls.Add(this.titleLabel);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
			this.panel1.Location = new System.Drawing.Point(0, 69);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(320, 431);
			this.panel1.TabIndex = 21;
			// 
			// historyView
			// 
			this.historyView.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.historyView.Dock = System.Windows.Forms.DockStyle.Fill;
			this.historyView.Location = new System.Drawing.Point(0, 236);
			this.historyView.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
			this.historyView.Name = "historyView";
			treeNode1.Name = "description";
			treeNode1.Text = "History";
			this.historyView.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode1});
			this.historyView.Size = new System.Drawing.Size(320, 195);
			this.historyView.TabIndex = 17;
			// 
			// deviceBoard
			// 
			this.deviceBoard.BorderWidth = 0F;
			this.deviceBoard.Dock = System.Windows.Forms.DockStyle.Top;
			this.deviceBoard.Location = new System.Drawing.Point(10, 90);
			this.deviceBoard.MouseEffect = false;
			this.deviceBoard.Multiline = true;
			this.deviceBoard.Name = "deviceBoard";
			this.deviceBoard.Size = new System.Drawing.Size(320, 236);
			this.deviceBoard.TabIndex = 0;
			// 
			// optionWrapper
			// 
			this.optionWrapper.Controls.Add(this.optionPanel);
			this.optionWrapper.Dock = System.Windows.Forms.DockStyle.Right;
			this.optionWrapper.Location = new System.Drawing.Point(785, 0);
			this.optionWrapper.Margin = new System.Windows.Forms.Padding(0);
			this.optionWrapper.Name = "optionWrapper";
			this.optionWrapper.Padding = new System.Windows.Forms.Padding(12, 0, 0, 0);
			this.optionWrapper.Size = new System.Drawing.Size(215, 500);
			this.optionWrapper.TabIndex = 23;
			// 
			// optionPanel
			// 
			this.optionPanel.BackColor = System.Drawing.Color.White;
			this.optionPanel.Controls.Add(this.label1);
			this.optionPanel.Controls.Add(this.rtScanToggle);
			this.optionPanel.Controls.Add(this.arpScanToggle);
			this.optionPanel.Controls.Add(this.ndScanToggle);
			this.optionPanel.Controls.Add(this.upnpScanToggle);
			this.optionPanel.Controls.Add(this.optionHeader);
			this.optionPanel.Controls.Add(this.resolveHostnameToggle);
			this.optionPanel.Controls.Add(this.fingerprintToggle);
			this.optionPanel.Controls.Add(this.upnpGrabberToggle);
			this.optionPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.optionPanel.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
			this.optionPanel.Location = new System.Drawing.Point(12, 0);
			this.optionPanel.Margin = new System.Windows.Forms.Padding(9, 0, 0, 0);
			this.optionPanel.Name = "optionPanel";
			this.optionPanel.Size = new System.Drawing.Size(203, 500);
			this.optionPanel.TabIndex = 2;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(6, 6);
			this.label1.Margin = new System.Windows.Forms.Padding(6, 6, 6, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(75, 15);
			this.label1.TabIndex = 0;
			this.label1.Text = "Scan options";
			// 
			// rtScanToggle
			// 
			this.rtScanToggle.BorderWidth = 0F;
			this.rtScanToggle.Checked = false;
			this.rtScanToggle.Location = new System.Drawing.Point(12, 27);
			this.rtScanToggle.Margin = new System.Windows.Forms.Padding(12, 6, 0, 0);
			this.rtScanToggle.MouseEffect = false;
			this.rtScanToggle.Name = "rtScanToggle";
			this.rtScanToggle.Padding = new System.Windows.Forms.Padding(0, 0, 10, 0);
			this.rtScanToggle.Size = new System.Drawing.Size(190, 25);
			this.rtScanToggle.TabIndex = 1;
			this.rtScanToggle.Text = "Realtime-Scan";
			// 
			// arpScanToggle
			// 
			this.arpScanToggle.BorderWidth = 0F;
			this.arpScanToggle.Checked = false;
			this.arpScanToggle.Location = new System.Drawing.Point(12, 58);
			this.arpScanToggle.Margin = new System.Windows.Forms.Padding(12, 6, 0, 0);
			this.arpScanToggle.MouseEffect = false;
			this.arpScanToggle.Name = "arpScanToggle";
			this.arpScanToggle.Padding = new System.Windows.Forms.Padding(0, 0, 10, 0);
			this.arpScanToggle.Size = new System.Drawing.Size(190, 25);
			this.arpScanToggle.TabIndex = 4;
			this.arpScanToggle.Text = "ARP Scan (IPv4)";
			// 
			// ndScanToggle
			// 
			this.ndScanToggle.BorderWidth = 0F;
			this.ndScanToggle.Checked = false;
			this.ndScanToggle.Location = new System.Drawing.Point(12, 89);
			this.ndScanToggle.Margin = new System.Windows.Forms.Padding(12, 6, 0, 0);
			this.ndScanToggle.MouseEffect = false;
			this.ndScanToggle.Name = "ndScanToggle";
			this.ndScanToggle.Padding = new System.Windows.Forms.Padding(0, 0, 10, 0);
			this.ndScanToggle.Size = new System.Drawing.Size(190, 25);
			this.ndScanToggle.TabIndex = 4;
			this.ndScanToggle.Text = "ND Scan (IPv6)";
			// 
			// upnpScanToggle
			// 
			this.upnpScanToggle.BorderWidth = 0F;
			this.upnpScanToggle.Checked = false;
			this.upnpScanToggle.Location = new System.Drawing.Point(12, 120);
			this.upnpScanToggle.Margin = new System.Windows.Forms.Padding(12, 6, 0, 0);
			this.upnpScanToggle.MouseEffect = false;
			this.upnpScanToggle.Name = "upnpScanToggle";
			this.upnpScanToggle.Padding = new System.Windows.Forms.Padding(0, 0, 10, 0);
			this.upnpScanToggle.Size = new System.Drawing.Size(190, 25);
			this.upnpScanToggle.TabIndex = 4;
			this.upnpScanToggle.Text = "UPnP Scan";
			// 
			// optionHeader
			// 
			this.optionHeader.AutoSize = true;
			this.optionHeader.Location = new System.Drawing.Point(6, 151);
			this.optionHeader.Margin = new System.Windows.Forms.Padding(6, 6, 6, 0);
			this.optionHeader.Name = "optionHeader";
			this.optionHeader.Size = new System.Drawing.Size(90, 15);
			this.optionHeader.TabIndex = 0;
			this.optionHeader.Text = "Resolve options";
			// 
			// resolveHostnameToggle
			// 
			this.resolveHostnameToggle.BorderWidth = 0F;
			this.resolveHostnameToggle.Checked = false;
			this.resolveHostnameToggle.Location = new System.Drawing.Point(12, 172);
			this.resolveHostnameToggle.Margin = new System.Windows.Forms.Padding(12, 6, 0, 0);
			this.resolveHostnameToggle.MouseEffect = false;
			this.resolveHostnameToggle.Name = "resolveHostnameToggle";
			this.resolveHostnameToggle.Padding = new System.Windows.Forms.Padding(0, 0, 10, 0);
			this.resolveHostnameToggle.Size = new System.Drawing.Size(190, 25);
			this.resolveHostnameToggle.TabIndex = 2;
			this.resolveHostnameToggle.Text = "Resolve Hostname";
			// 
			// fingerprintToggle
			// 
			this.fingerprintToggle.BorderWidth = 0F;
			this.fingerprintToggle.Checked = false;
			this.fingerprintToggle.Location = new System.Drawing.Point(12, 203);
			this.fingerprintToggle.Margin = new System.Windows.Forms.Padding(12, 6, 0, 0);
			this.fingerprintToggle.MouseEffect = false;
			this.fingerprintToggle.Name = "fingerprintToggle";
			this.fingerprintToggle.Padding = new System.Windows.Forms.Padding(0, 0, 10, 0);
			this.fingerprintToggle.Size = new System.Drawing.Size(190, 25);
			this.fingerprintToggle.TabIndex = 3;
			this.fingerprintToggle.Text = "Fingerprint";
			// 
			// upnpGrabberToggle
			// 
			this.upnpGrabberToggle.BorderWidth = 0F;
			this.upnpGrabberToggle.Checked = false;
			this.upnpGrabberToggle.Location = new System.Drawing.Point(12, 234);
			this.upnpGrabberToggle.Margin = new System.Windows.Forms.Padding(12, 6, 0, 0);
			this.upnpGrabberToggle.MouseEffect = false;
			this.upnpGrabberToggle.Name = "upnpGrabberToggle";
			this.upnpGrabberToggle.Padding = new System.Windows.Forms.Padding(0, 0, 10, 0);
			this.upnpGrabberToggle.Size = new System.Drawing.Size(190, 25);
			this.upnpGrabberToggle.TabIndex = 2;
			this.upnpGrabberToggle.Text = "UPnP Grabber";
			// 
			// deviceView
			// 
			this.deviceView.AutoSizeLastColumn = true;
			this.deviceView.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.deviceView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.ipAddress,
            this.macAddress,
            this.hostname,
            this.manufacturer});
			this.deviceView.Dock = System.Windows.Forms.DockStyle.Fill;
			this.deviceView.FullRowSelect = true;
			listViewGroup1.Header = "";
			listViewGroup1.Name = "host";
			listViewGroup2.Header = "";
			listViewGroup2.Name = "gateway";
			listViewGroup3.Header = "";
			listViewGroup3.Name = "hasHostname";
			listViewGroup4.Header = "";
			listViewGroup4.Name = "withoutHostname";
			listViewGroup5.Header = "";
			listViewGroup5.Name = "offline";
			this.deviceView.Groups.AddRange(new System.Windows.Forms.ListViewGroup[] {
            listViewGroup1,
            listViewGroup2,
            listViewGroup3,
            listViewGroup4,
            listViewGroup5});
			this.deviceView.HideSelection = false;
			this.deviceView.Location = new System.Drawing.Point(320, 69);
			this.deviceView.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
			this.deviceView.MultiSelect = false;
			this.deviceView.Name = "deviceView";
			this.deviceView.Size = new System.Drawing.Size(465, 431);
			this.deviceView.TabIndex = 24;
			this.deviceView.UseCompatibleStateImageBehavior = false;
			this.deviceView.View = System.Windows.Forms.View.Details;
			// 
			// ipAddress
			// 
			this.ipAddress.Text = "IP Address";
			this.ipAddress.Width = 100;
			// 
			// macAddress
			// 
			this.macAddress.Text = "MAC Address";
			this.macAddress.Width = 133;
			// 
			// hostname
			// 
			this.hostname.Text = "Hostname";
			this.hostname.Width = 150;
			// 
			// manufacturer
			// 
			this.manufacturer.Text = "Manufacturer";
			this.manufacturer.Width = 65;
			// 
			// NetworkScanner
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.deviceView);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.optionWrapper);
			this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
			this.Name = "NetworkScanner";
			this.Size = new System.Drawing.Size(1000, 500);
			this.panel2.ResumeLayout(false);
			this.panel1.ResumeLayout(false);
			this.optionWrapper.ResumeLayout(false);
			this.optionPanel.ResumeLayout(false);
			this.optionPanel.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.FlowLayoutPanel panel2;
		private UILib.Controls.NSTextBox searchBox;
		private UILib.Controls.Button actionButton;
		private System.Windows.Forms.Panel panel1;
		private UILib.Controls.TreeViewEx historyView;
		private UILib.Controls.NSTextBox deviceBoard;
		private NetStorm.UILib.Controls.StatusLabel titleLabel;
		private System.Windows.Forms.Panel optionWrapper;
		private System.Windows.Forms.FlowLayoutPanel optionPanel;
		private System.Windows.Forms.Label optionHeader;
		private UILib.Controls.ToggleButton rtScanToggle;
		private UILib.Controls.ToggleButton resolveHostnameToggle;
		private UILib.Controls.ToggleButton fingerprintToggle;
		private UILib.Controls.ToggleButton arpScanToggle;
		private UILib.Controls.ListViewEx deviceView;
		private System.Windows.Forms.ColumnHeader ipAddress;
		private System.Windows.Forms.ColumnHeader macAddress;
		private System.Windows.Forms.ColumnHeader hostname;
		private System.Windows.Forms.ColumnHeader manufacturer;
		private UILib.Controls.ToggleButton upnpScanToggle;
		private System.Windows.Forms.Label label1;
		private UILib.Controls.ToggleButton ndScanToggle;
		private UILib.Controls.ToggleButton upnpGrabberToggle;
	}
}
