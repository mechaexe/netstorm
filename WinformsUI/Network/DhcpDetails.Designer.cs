﻿
namespace NetStorm.GUI.Network
{
	partial class DhcpDetails
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.controlPanel = new System.Windows.Forms.Panel();
			this.closeButton = new NetStorm.UILib.Controls.Button();
			this.dhcpErrorLabel = new System.Windows.Forms.Label();
			this.titleLabel = new System.Windows.Forms.Label();
			this.dhcpAquisitionPanel = new System.Windows.Forms.TableLayoutPanel();
			this.dhcpRequestLabel = new NetStorm.UILib.Controls.StatusLabel();
			this.dhcpAckLabel = new NetStorm.UILib.Controls.StatusLabel();
			this.dhcpOfferLabel = new NetStorm.UILib.Controls.StatusLabel();
			this.serverDiscoveryLabel = new NetStorm.UILib.Controls.StatusLabel();
			this.dhcpInfoPanel = new NetStorm.UILib.Controls.NSPanel();
			this.dhcpDiscoverPanel = new NetStorm.UILib.Controls.NSPanel();
			this.dhcpDiscoverFlowPanel = new NetStorm.UILib.Controls.NSFlowLayoutPanel();
			this.dhcpInfoFlowPanel = new NetStorm.UILib.Controls.NSFlowLayoutPanel();
			this.dhcpInfoLabel = new NetStorm.UILib.Controls.StatusLabel();
			this.dhcpAquisitionPanel = new System.Windows.Forms.Panel();
			this.dhcpDiscoverLabel = new NetStorm.UILib.Controls.StatusLabel();
			this.controlPanel.SuspendLayout();
			this.dhcpAquisitionPanel.SuspendLayout();
			this.dhcpDiscoverPanel.SuspendLayout();
			this.dhcpInfoPanel.SuspendLayout();
			this.SuspendLayout();
			// 
			// controlPanel
			// 
			this.controlPanel.Controls.Add(this.titleLabel);
			this.controlPanel.Controls.Add(this.closeButton);
			this.controlPanel.Dock = System.Windows.Forms.DockStyle.Top;
			this.controlPanel.Location = new System.Drawing.Point(0, 0);
			this.controlPanel.Margin = new System.Windows.Forms.Padding(0);
			this.controlPanel.Name = "controlPanel";
			this.controlPanel.Size = new System.Drawing.Size(1000, 70);
			this.controlPanel.TabIndex = 15;
			// 
			// closeButton
			// 
			this.closeButton.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right;
			this.closeButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
			this.closeButton.BorderWidth = 1F;
			this.closeButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(239)))), ((int)(((byte)(241)))));
			this.closeButton.Location = new System.Drawing.Point(940, 15);
			this.closeButton.Margin = new System.Windows.Forms.Padding(20, 20, 0, 0);
			this.closeButton.MouseEffect = true;
			this.closeButton.Name = "closeButton";
			this.closeButton.Padding = new System.Windows.Forms.Padding(10);
			this.closeButton.Radius = new NetStorm.UILib.Features.CornerRadius(25);
			this.closeButton.Size = new System.Drawing.Size(40, 40);
			this.closeButton.TabIndex = 25;
			this.closeButton.Text = "X";
			this.closeButton.TextAlignment = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// dhcpErrorLabel
			// 
			this.dhcpErrorLabel.AutoSize = true;
			this.dhcpErrorLabel.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom;
			this.dhcpErrorLabel.Location = new System.Drawing.Point(0, 0);
			this.dhcpErrorLabel.Margin = new System.Windows.Forms.Padding(10, 0, 0, 0);
			this.dhcpErrorLabel.Name = "dhcpErrorLabel";
			this.dhcpErrorLabel.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
			this.dhcpErrorLabel.Size = new System.Drawing.Size(409, 45);
			this.dhcpErrorLabel.TabIndex = 1;
			this.dhcpErrorLabel.Text = "DHCP Error";
			// 
			// titleLabel
			// 
			this.titleLabel.AutoSize = true;
			this.titleLabel.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left;
			this.titleLabel.Location = new System.Drawing.Point(10, 0);
			this.titleLabel.Margin = new System.Windows.Forms.Padding(0);
			this.titleLabel.Name = "titleLabel";
			this.titleLabel.Padding = new System.Windows.Forms.Padding(0);
			this.titleLabel.Size = new System.Drawing.Size(409, 45);
			this.titleLabel.Text = "DHCP Overview";
			// 
			// dhcpRequestLabel
			// 
			this.dhcpRequestLabel.BorderWidth = 0F;
			this.dhcpRequestLabel.IndicatorColor = System.Drawing.Color.Black;
			this.dhcpRequestLabel.IndicatorSize = 20;
			this.dhcpRequestLabel.Location = new System.Drawing.Point(400, 0);
			this.dhcpRequestLabel.Margin = new System.Windows.Forms.Padding(0, 0, 0, 10);
			this.dhcpRequestLabel.MouseEffect = false;
			this.dhcpRequestLabel.Name = "dhcpRequestLabel";
			this.dhcpRequestLabel.Padding = new System.Windows.Forms.Padding(10);
			this.dhcpRequestLabel.Size = new System.Drawing.Size(200, 37);
			this.dhcpRequestLabel.TabIndex = 21;
			this.dhcpRequestLabel.Text = "Request";
			// 
			// dhcpAckLabel
			// 
			this.dhcpAckLabel.BorderWidth = 0F;
			this.dhcpAckLabel.IndicatorColor = System.Drawing.Color.Black;
			this.dhcpAckLabel.IndicatorSize = 20;
			this.dhcpAckLabel.Location = new System.Drawing.Point(600, 0);
			this.dhcpAckLabel.Margin = new System.Windows.Forms.Padding(0, 0, 0, 10);
			this.dhcpAckLabel.MouseEffect = false;
			this.dhcpAckLabel.Name = "dhcpAckLabel";
			this.dhcpAckLabel.Padding = new System.Windows.Forms.Padding(10);
			this.dhcpAckLabel.Size = new System.Drawing.Size(180, 37);
			this.dhcpAckLabel.TabIndex = 22;
			this.dhcpAckLabel.Text = "Acknowledge";
			// 
			// dhcpOfferLabel
			// 
			this.dhcpOfferLabel.BorderWidth = 0F;
			this.dhcpOfferLabel.IndicatorColor = System.Drawing.Color.Black;
			this.dhcpOfferLabel.IndicatorSize = 20;
			this.dhcpOfferLabel.Location = new System.Drawing.Point(200, 0);
			this.dhcpOfferLabel.Margin = new System.Windows.Forms.Padding(0, 0, 0, 10);
			this.dhcpOfferLabel.MouseEffect = false;
			this.dhcpOfferLabel.Name = "dhcpOfferLabel";
			this.dhcpOfferLabel.Padding = new System.Windows.Forms.Padding(10);
			this.dhcpOfferLabel.Size = new System.Drawing.Size(200, 37);
			this.dhcpOfferLabel.TabIndex = 20;
			this.dhcpOfferLabel.Text = "Offer";
			// 
			// serverDiscoveryLabel
			// 
			this.serverDiscoveryLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
			| System.Windows.Forms.AnchorStyles.Right)));
			this.serverDiscoveryLabel.BorderWidth = 0F;
			this.serverDiscoveryLabel.IndicatorColor = System.Drawing.Color.Black;
			this.serverDiscoveryLabel.IndicatorSize = 20;
			this.serverDiscoveryLabel.Location = new System.Drawing.Point(500, 0);
			this.serverDiscoveryLabel.Margin = new System.Windows.Forms.Padding(0);
			this.serverDiscoveryLabel.MouseEffect = false;
			this.serverDiscoveryLabel.Name = "serverDiscoveryLabel";
			this.serverDiscoveryLabel.Padding = new System.Windows.Forms.Padding(10);
			this.serverDiscoveryLabel.Size = new System.Drawing.Size(500, 36);
			this.serverDiscoveryLabel.TabIndex = 18;
			this.serverDiscoveryLabel.Text = "Discovererd Servers";
			// 
			// dhcpInfoPanel
			// 
			this.dhcpInfoPanel.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom;
			this.dhcpInfoPanel.Controls.Add(this.dhcpInfoLabel);
			this.dhcpInfoPanel.Controls.Add(this.dhcpInfoFlowPanel);
			this.dhcpInfoPanel.Margin = new System.Windows.Forms.Padding(0);
			this.dhcpInfoPanel.Name = "dhcpInfoPanel";
			this.dhcpInfoPanel.Padding = new System.Windows.Forms.Padding(10);
			this.dhcpInfoPanel.TabIndex = 2;
			this.dhcpInfoPanel.Radius = new NetStorm.UILib.Features.CornerRadius(10);
			// 
			// dhcpDiscoverPanel
			// 
			this.dhcpDiscoverPanel.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom;
			this.dhcpDiscoverPanel.Controls.Add(this.serverDiscoveryLabel);
			this.dhcpDiscoverPanel.Controls.Add(this.dhcpDiscoverFlowPanel);
			this.dhcpDiscoverPanel.Name = "dhcpDiscoverPanel";
			this.dhcpDiscoverPanel.Padding = new System.Windows.Forms.Padding(10);
			this.dhcpDiscoverPanel.TabIndex = 3;
			this.dhcpDiscoverPanel.Radius = new NetStorm.UILib.Features.CornerRadius(10);
			// 
			// dhcpInfoFlowPanel
			// 
			this.dhcpInfoFlowPanel.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom;
			this.dhcpInfoFlowPanel.Margin = new System.Windows.Forms.Padding(0);
			this.dhcpInfoFlowPanel.Name = "dhcpInfoFlowPanel";
			this.dhcpInfoFlowPanel.Padding = new System.Windows.Forms.Padding(10);
			this.dhcpInfoFlowPanel.TabIndex = 2;
			this.dhcpInfoFlowPanel.Radius = new NetStorm.UILib.Features.CornerRadius(10);
			this.dhcpInfoFlowPanel.AutoScroll = true;
			// 
			// dhcpDiscoverFlowPanel
			// 
			this.dhcpDiscoverFlowPanel.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom;
			this.dhcpDiscoverFlowPanel.Name = "dhcpDiscoverFlowPanel";
			this.dhcpDiscoverFlowPanel.Padding = new System.Windows.Forms.Padding(10);
			this.dhcpDiscoverFlowPanel.TabIndex = 3;
			this.dhcpDiscoverFlowPanel.Radius = new NetStorm.UILib.Features.CornerRadius(10);
			this.dhcpDiscoverFlowPanel.AutoScroll = true;
			// 
			// dhcpInfoLabel
			// 
			this.dhcpInfoLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
			| System.Windows.Forms.AnchorStyles.Right)));
			this.dhcpInfoLabel.BorderWidth = 0F;
			this.dhcpInfoLabel.IndicatorColor = System.Drawing.Color.Black;
			this.dhcpInfoLabel.IndicatorSize = 20;
			this.dhcpInfoLabel.Location = new System.Drawing.Point(0, 0);
			this.dhcpInfoLabel.Margin = new System.Windows.Forms.Padding(0);
			this.dhcpInfoLabel.MouseEffect = false;
			this.dhcpInfoLabel.Name = "dhcpInfoLabel";
			this.dhcpInfoLabel.Padding = new System.Windows.Forms.Padding(10);
			this.dhcpInfoLabel.Size = new System.Drawing.Size(500, 36);
			this.dhcpInfoLabel.TabIndex = 18;
			this.dhcpInfoLabel.Text = "Info";
			// 
			// dhcpAquisitionPanel
			// 
			this.dhcpAquisitionPanel.Controls.Add(this.dhcpDiscoverLabel);
			this.dhcpAquisitionPanel.Controls.Add(this.dhcpOfferLabel);
			this.dhcpAquisitionPanel.Controls.Add(this.dhcpRequestLabel);
			this.dhcpAquisitionPanel.Controls.Add(this.dhcpAckLabel);
			this.dhcpAquisitionPanel.Dock = System.Windows.Forms.DockStyle.Top;
			this.dhcpAquisitionPanel.Location = new System.Drawing.Point(0, 69);
			this.dhcpAquisitionPanel.Name = "dhcpAquisitionPanel";
			this.dhcpAquisitionPanel.Size = new System.Drawing.Size(1000, 45);
			this.dhcpAquisitionPanel.TabIndex = 18;
			// 
			// dhcpDiscoverLabel
			// 
			this.dhcpDiscoverLabel.BorderWidth = 0F;
			this.dhcpDiscoverLabel.IndicatorColor = System.Drawing.Color.Black;
			this.dhcpDiscoverLabel.IndicatorSize = 20;
			this.dhcpDiscoverLabel.Location = new System.Drawing.Point(0, 0);
			this.dhcpDiscoverLabel.Margin = new System.Windows.Forms.Padding(0);
			this.dhcpDiscoverLabel.MouseEffect = false;
			this.dhcpDiscoverLabel.Name = "dhcpDiscoverLabel";
			this.dhcpDiscoverLabel.Padding = new System.Windows.Forms.Padding(10);
			this.dhcpDiscoverLabel.Size = new System.Drawing.Size(1000, 40);
			this.dhcpDiscoverLabel.TabIndex = 19;
			this.dhcpDiscoverLabel.Text = "Discover";
			// 
			// DhcpDetailsNG
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.dhcpAquisitionPanel);
			this.Controls.Add(this.controlPanel);
			this.Controls.Add(this.dhcpErrorLabel);
			this.Controls.Add(this.dhcpInfoPanel);
			this.Controls.Add(this.dhcpDiscoverPanel);
			this.Name = "DhcpDetailsNG";
			this.Size = new System.Drawing.Size(1000, 500);
			this.controlPanel.ResumeLayout(false);
			this.dhcpAquisitionPanel.ResumeLayout(false);
			this.dhcpDiscoverPanel.ResumeLayout(false);
			this.dhcpInfoPanel.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Panel controlPanel;
		private System.Windows.Forms.Panel dhcpAquisitionPanel;
		private NetStorm.UILib.Controls.Button closeButton;
		private System.Windows.Forms.Label dhcpErrorLabel;
		private System.Windows.Forms.Label titleLabel;
		private NetStorm.UILib.Controls.StatusLabel serverDiscoveryLabel;
		private NetStorm.UILib.Controls.StatusLabel dhcpInfoLabel;
		private NetStorm.UILib.Controls.StatusLabel dhcpRequestLabel;
		private NetStorm.UILib.Controls.StatusLabel dhcpAckLabel;
		private NetStorm.UILib.Controls.StatusLabel dhcpOfferLabel;
		private NetStorm.UILib.Controls.StatusLabel dhcpDiscoverLabel;
		private NetStorm.UILib.Controls.NSPanel dhcpDiscoverPanel;
		private NetStorm.UILib.Controls.NSPanel dhcpInfoPanel;
		private NetStorm.UILib.Controls.NSFlowLayoutPanel dhcpDiscoverFlowPanel;
		private NetStorm.UILib.Controls.NSFlowLayoutPanel dhcpInfoFlowPanel;
	}
}
