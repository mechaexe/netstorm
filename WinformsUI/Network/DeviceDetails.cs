﻿using NetStorm.GUI.Enumeration.TextProvider;
using NetStorm.Common.Classes;
using NetStorm.Common.Devices;
using NetStorm.CoreNG.Modules.Portscanner;
using NetStorm.CoreNG.Modules.Spoofer;
using NetStorm.CoreNG.Modules.Transformation;
using NetStorm.CoreNG.Worker;
using NetStorm.UILib;
using NetStorm.UILib.Coloring;
using NetStorm.UILib.Controls;
using NetStorm.UILib.Features;
using System;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NetStorm.GUI.Network
{
	public partial class DeviceDetails : UserControl, INetStormPage
	{
		private ColorScheme _colorScheme;
		private INetworkDevice _device;
		private ILocalNetworkInterface _interface;
		private readonly UILib.Controls.Button portscanButton;
		private readonly CompositeDeviceTextProvider _defaultDeviceText;
		private readonly ObservableCollection<Portscanner> _portscanners;
		private readonly ObservableCollection<INetStormWorker<ISpoofer>> _spoofers;

		public DeviceDetails(ObservableCollection<Portscanner> portscanners, ObservableCollection<INetStormWorker<ISpoofer>> spoofers)
		{
			InitializeComponent();

			portscanButton = NetStorm.GUI.ControlHelper.CreateActionButton("Scan");
			portListPanel.AddControlToTitle(portscanButton);

			_defaultDeviceText = new CompositeDeviceTextProvider();
			_defaultDeviceText.DeviceTextProviders.AddRange(new IDeviceTextProvider[]
			{
				new DeviceNetworkTextProvider(),
				new IpInfoTextProvider(),
				new DeviceStatsTextProvider()
			});

			_portscanners = portscanners;
			_spoofers = spoofers;
		}

		private void UpdateAll()
		{
			if (InvokeRequired)
			{
				titleLabel.Invoke(new Action(() => UpdateTitle(_device)));
				deviceBoard.Invoke(new Action(() => UpdateDeviceBoard(_device)));
			}
			else
			{
				UpdateTitle(_device);
				UpdateDeviceBoard(_device);
			}
		}

		private void ClearAll()
		{
			titleLabel.Text = "Device";
			deviceBoard.Text = "";
			portListPanel.ListView.Items.Clear();
			portInfoPanel.ControlPanel.Controls.Clear();
		}

		private void Device_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			if (Visible && _device != null)
				UpdateAll();
		}

		public void SetLocalInterface(ILocalNetworkInterface localInterface)
			=> _interface = localInterface;

		public void SetDevice(INetworkDevice device)
		{
			ClearAll();
			portscanButton.Enabled = true;
			if (_device != null)
			{
				_device.PropertyChanged -= Device_PropertyChanged;
				_device.Ports.CollectionChanged -= Ports_CollectionChanged;
			}
			_device = device;
			if(_device != null)
			{
				_device.PropertyChanged += Device_PropertyChanged;
				_device.Ports.CollectionChanged += Ports_CollectionChanged;
				UpdateTitle(_device);
				UpdateDeviceBoard(_device);
				UpdatePortList(_device);
			}
		}

		private void UpdateTitle(INetworkDevice device)
		{
			titleLabel.IndicatorColor = device.Statistics.DeviceState == DeviceState.Offline ? Colors.Orange : Colors.Green;
			titleLabel.Text = device.GetHostIdentifier(IpStackMode.IPv4);
		}

		private void UpdateDeviceBoard(INetworkDevice device)
			=> deviceBoard.Text = _defaultDeviceText.GetText(device).Replace("\n", Environment.NewLine);

		private async Task PerformLookup(INetworkDevice device, ITransformator transformator)
		{
			if (device != null)
				await Task.Run(() => transformator.TransformDevice(device, true));
		}

		#region Spoofing

		private void UpdateSpoofButton()
		{
			var spoofed = false;
			var spoofable = false;

			if (_device != null && (spoofable = ArpSpoofer.IsSpoofable(_device.NetworkInterface)))
				spoofed = IsSpoofed(_device.NetworkInterface.IPv4Stack.IPAddress);

			var toggleText = new Action(() =>
			{
				spoofBtn.Text = (spoofed ? "Pardon" : "Spoof") + " Device";
				spoofBtn.Enabled = spoofable;
				spoofBtn.ColorTable = spoofed ? _colorScheme.AccentColors : _colorScheme.SecondaryColors;
			});
			if (InvokeRequired)
				spoofBtn.Invoke(toggleText);
			else
				toggleText();
		}

		private bool IsSpoofed(IPAddress ip)
			=> _spoofers.Any(x => x.Module.Target1.IPv4Stack.IPAddress.Equals(ip));

		private void Pardon(IPAddress ip)
		{
			foreach (var m in _spoofers)
			{
				if (m.Module.Target1.IPv4Stack.IPAddress.Equals(ip))
				{
					m.Abort();
					_spoofers.Remove(m);
					return;
				}
			}
		}

		private void SpoofBtn_Click(object sender, EventArgs e)
		{
			if (_device != null && _interface != null)
			{
				if (IsSpoofed(_device.NetworkInterface.IPv4Stack.IPAddress))
				{
					Pardon(_device.NetworkInterface.IPv4Stack.IPAddress);
				}
				else
				{
					var spf = new ArpSpoofer(Guid.NewGuid().ToString())
					{
						SpoofDirection = SpoofDirection.Both,
						SpoofedMac = PhysicalAddress.Parse("AFFEDEADBEEF"),
						Target1 = _device.NetworkInterface,
						Target2 = _interface.Gateway.NetworkInterface
					};
					spf.SetNic(_interface);

					var worker = new TimerWorker<ISpoofer>(spf.Identifier, spf) { CycleDurationSeconds = 10 };
					if (worker.Start())
						_spoofers.Add(worker);
				}
			}
		}

		#endregion

		#region Port

		private void PortView_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
		{
			if (e.Item != null && e.Item.Tag is IPort p)
				ShowPortInfo(p);
		}

		private void UpdatePortList(INetworkDevice device)
		{
			portListPanel.ListView.Items.Clear();
			foreach (var p in device.Ports.Where(p => p.Status == GetPortStatusFilter()).ToArray())
				AddPortToView(p);
		}

		private void Ports_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
		{
			if(e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Add && e.NewItems[0] is IPort p && p.Status == GetPortStatusFilter())
			{
				if (InvokeRequired)
					portListPanel.ListView.Invoke(new Action(() => AddPortToView(p)));
				else
					AddPortToView(p);
			}
		}

		private void AddPortToView(IPort p)
		{
			var lvi = new ListViewItem(p.Number.ToString())
			{
				Tag = p
			};
			lvi.SubItems.AddRange(new string[]
			{
					p.Protocol.ToString(),
					p.Status.ToString(),
					p.Service?.Service.Name ?? ""
			});
			p.PropertyChanged += (_, __) =>
			{
				if (__.PropertyName == nameof(p.Service) && p.Service != null)
					portListPanel.ListView.Invoke(new Action(() => lvi.SubItems[3].Text = p.Service.Service.Name));
			};
			portListPanel.ListView.Items.Add(lvi);
		}

		private void ShowPortInfo(IPort p)
		{
			var textProviders = new IPortTextProvider[]
			{
				new PortTextProvider(),
				new UPnPTextProvider()
			};

			portInfoPanel.ControlPanel.Controls.Clear();

			foreach(var tp in textProviders)
			{
				var txt = tp.GetPortText(p);
				if (!string.IsNullOrEmpty(txt))
					portInfoPanel.ControlPanel.Controls.Add(CreatePortInfoPanel($"{p.Number} - {p.Protocol}", txt));
			}
			
			if(p.Service != null && p.Service.Banners.Count > 0)
				portInfoPanel.ControlPanel.Controls.Add(CreatePortInfoPanel("Banners", string.Join("\n", p.Service.Banners)));
		}

		private Board2 CreatePortInfoPanel(string title, string text)
			=> new Board2()
			{
				TitleText = title,
				ShowTitle = true,
				Text = text,
				ColorTable = _colorScheme.PrimaryColors,
				Radius = new CornerRadius(10),
				Width = portInfoPanel.ControlPanel.Width - 50,
				AutoHeight = true,
				MaximumSize = new Size(portInfoPanel.ControlPanel.Width - 50, 300),
				Padding = new Padding(10)
			};

		private PortStatus GetPortStatusFilter()
		{
			return PortStatus.Open;
		}

		#endregion

		protected override void OnLoad(EventArgs e)
		{
			portListPanel.ListView.ItemSelectionChanged += PortView_ItemSelectionChanged;
			portscanButton.Click += PortscanButton_Click;
			spoofBtn.Click += SpoofBtn_Click;
			osFingerprintButton.Click += async (_, __) => await PerformLookup(_device, new PingFingerprintTransform());
			resolveHostnameButton.Click += async (_, __) => await PerformLookup(_device, new DnsTransform());
			_spoofers.CollectionChanged += (_, __) => UpdateSpoofButton();
			base.OnLoad(e);
			ClearAll();
		}

		private void PortscanButton_Click(object sender, EventArgs e)
		{
			if(_device != null && _interface != null)
			{
				portscanButton.Enabled = false;
				var scanner = portListPanel.CreatePortscanner(_device, _interface);
				scanner.ModuleStateChanged += Portscanner_StateChanged;
				scanner.Start(_interface.GetPacketTranceiver(), new OneShotWorker());
				_portscanners.Add(scanner);
			}
		}

		private void Portscanner_StateChanged(object sender, CoreNG.Modules.StateArgs e)
		{
			if (!e.Running)
			{
				portscanButton.Invoke(new Action(() => portscanButton.Enabled = true));
				_portscanners.Remove((Portscanner)sender);
			}
		}

		protected override void OnVisibleChanged(EventArgs e)
		{
			base.OnVisibleChanged(e);
			if (Visible && _device != null)
				UpdateAll();
		}

		private void ConfigurePanels()
		{
			const int margin = 20;
			const int portInfoWidth = 350;

			titleLabel.AutoEllipsis = true;
			titleLabel.Font = Fonts.Big;
			titleLabel.Padding = new Padding(0);
			titleLabel.Location = new Point(margin, controlPanel.Height);
			titleLabel.Size = new Size(300, 20);

			deviceBoard.Location = new Point(margin, titleLabel.Location.Y + titleLabel.Height + 10);
			deviceBoard.Size = new Size(300, Height - deviceBoard.Location.Y);
			deviceBoard.Padding = new Padding(0);

			portListPanel.Location = new Point(320, controlPanel.Height);
			portListPanel.Size = new Size(Width - margin * 4 - deviceBoard.Width - portInfoWidth, Height - controlPanel.Height);

			portInfoPanel.Title = "Additional port info";
			portInfoPanel.Location = new Point(portListPanel.Location.X + portListPanel.Width + margin, portListPanel.Location.Y);
			portInfoPanel.Size = new Size(portInfoWidth, portListPanel.Height);
		}

		public void SetStyling(ColorScheme colorScheme)
		{
			_colorScheme = colorScheme;

			titleLabel.ColorTable =
				deviceBoard.ColorTable = colorScheme.SecondaryColors;
			controlPanel.BackColor = colorScheme.SecondaryColors.BackColor;

			foreach (Control c in controlPanel.Controls)
			{
				if (c is UILib.Controls.Button b)
				{
					b.BorderWidth = 1;
					b.AutoSize = true;
					b.Margin = new Padding(0, (controlPanel.Height - b.Height) / 2, 15, 0);
				}
			}
			portscanButton.ColorTable = colorScheme.PrimaryColors;

			spoofBtn.Radius = new CornerRadius(5);
			spoofBtn.ColorTable = colorScheme.SecondaryColors;
			spoofBtn.Font = Fonts.Default;

			portListPanel.SetStyling(colorScheme);
			portInfoPanel.SetStyling(colorScheme);
			ConfigurePanels();
		}
	}
}
