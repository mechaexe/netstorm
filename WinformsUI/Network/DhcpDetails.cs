﻿using NetStorm.CoreNG;
using NetStorm.UILib;
using NetStorm.UILib.Coloring;
using NetStorm.UILib.Controls;
using NetStorm.UILib.Features;
using Protocols.DHCP;
using Protocols.DHCP.Client;
using Protocols.DHCP.Protocol;
using Protocols.DHCP.Protocol.DhcpOptions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NetStorm.GUI.Network
{
	public partial class DhcpDetails : UserControl, INetStormPage
	{
		private DhcpClient _client;
		private ColorScheme _colorScheme;
		private readonly PhysicalAddress _physicalAddress;
		private readonly string[] _dhcpErrorList;
		private bool _isTestRunning;

		public DhcpDetails()
		{
			InitializeComponent();
			_physicalAddress = PhysicalAddress.Parse("AFFEDEADBEEF");
			_dhcpErrorList = new string[]
			{
				"The requested IP {1} got rejected.\nIt looks like all addresses are used.\nPlease make sure that your DHCP scope is large enough.",
				"We could not gather any DHCP information. Try one of the following steps:\n" +
				"- Make sure DHCP is enabled and correctly configured on your router\n" +
				$"- Whitelist the following MAC address: {_physicalAddress.ToCannonicalString()}"
			};
		}

		protected override void OnLoad(EventArgs e)
		{
			closeButton.Click += (_,__) => Hide();
			base.OnLoad(e);
		}

		public async Task StartCompleteDhcpTest(Board dhcpInfoBoard)
		{
			if (NicManager.Instance.PrimaryDeviceIndex >= 0 &&
				NicManager.Instance.Count > 0 &&
				NicManager.Instance.PrimaryDevice.Supports(NetStorm.Common.Devices.IpStackMode.IPv4) &&
				!_isTestRunning)
			{
				_isTestRunning = true;
				ResetIndicators();
				var addr = NicManager.Instance.PrimaryDevice.IPv4Stack.IPAddress;
				await AuqireIpTest(addr, dhcpInfoBoard);
				await StartDiscovery(addr, NicManager.Instance.PrimaryDevice.Mac);
				_isTestRunning = false;
			}
		}

		private void ResetIndicators()
		{
			foreach (Control c in dhcpAquisitionPanel.Controls)
				if (c is StatusLabel sl)
					sl.IndicatorColor = _colorScheme.SecondaryColors.BackColor;

			dhcpDiscoverLabel.Text = "Discover";
			dhcpOfferLabel.Text = "Offer";
			dhcpAckLabel.Text = "Acknowledge";
			serverDiscoveryLabel.Text = "Discovererd Servers";

			dhcpDiscoverFlowPanel.Controls.Clear();
			dhcpInfoFlowPanel.Controls.Clear();

			dhcpInfoLabel.IndicatorColor =
				serverDiscoveryLabel.IndicatorColor = _colorScheme.SecondaryColors.BackColor;
		}

		private async Task AuqireIpTest(IPAddress bindAddress, Board resultBoard)
		{
			dhcpDiscoverLabel.IndicatorColor = Colors.Yellow;
			var progress = new Progress<DhcpMessage>();
			var boardContent = new string[2];
			var display = new EventHandler<DhcpMessage>((_, m) =>
			{
				if (m.DhcpOptions.ContainsKey(DhcpOption.DhcpAddress) &&
					m.DhcpOptions.ContainsKey(DhcpOption.DhcpMessageType))
				{
					switch((DhcpMessageType)m.DhcpOptions[DhcpOption.DhcpMessageType].Contents[0])
					{
						case DhcpMessageType.Offer:
							dhcpDiscoverLabel.IndicatorColor = Colors.Green;
							dhcpOfferLabel.IndicatorColor = Colors.Green;
							dhcpRequestLabel.IndicatorColor = Colors.Yellow;
							
							boardContent[0] = ((IpOption)m.DhcpOptions[DhcpOption.DhcpAddress]).Addresses[0].ToString();
							boardContent[1] = m.YourIpAddress.ToString();

							dhcpDiscoverLabel.Text = $"Discover: {boardContent[0]}";
							dhcpOfferLabel.Text = $"Offer: {boardContent[1]}";
							break;

						case DhcpMessageType.Nak:
							dhcpRequestLabel.IndicatorColor = Colors.Green;
							dhcpAckLabel.IndicatorColor = Colors.Red;
							dhcpAckLabel.Text = "Acknowledge: Rejected";
							boardContent[1] = string.Format(_dhcpErrorList[0], boardContent[1]);
							break;

						case DhcpMessageType.Ack:
							dhcpRequestLabel.IndicatorColor = Colors.Green;
							dhcpAckLabel.IndicatorColor = Colors.Green;
							boardContent[1] = GetDhcpMessageContent(m);
							break;

						default:
							break;
					}
				}
			});
			progress.ProgressChanged += display;
			_client = new DhcpClient(_physicalAddress, "", bindAddress);
			try
			{
				if (await _client.AcquireIp(IPAddress.Any, progress))
					_client.Release();
				else boardContent[1] = _dhcpErrorList[1];
			}
			catch {  }

			if (string.IsNullOrEmpty(boardContent[0]))
				boardContent[0] = "Unknown";

			dhcpErrorLabel.Text = boardContent[1];
			ShowDhcpInfoOnBoard(boardContent[0], boardContent[1], resultBoard);
		}

		private async Task StartDiscovery(IPAddress bindAddress, PhysicalAddress localMac)
		{
			var replys = 0;
			var progress = new Progress<DhcpMessage>();
			var res = new Dictionary<string, DhcpMessage>();

			dhcpErrorLabel.Hide();
			dhcpInfoPanel.Show();
			dhcpDiscoverPanel.Show();

			progress.ProgressChanged += (_, m) =>
			{
				if (m.DhcpOptions.ContainsKey(DhcpOption.DhcpAddress) &&
					m.DhcpOptions.ContainsKey(DhcpOption.DhcpMessageType) &&
					!res.ContainsKey(((IpOption)m.DhcpOptions[DhcpOption.DhcpAddress]).Addresses[0].ToString()))
				{
					res.Add(((IpOption)m.DhcpOptions[DhcpOption.DhcpAddress]).Addresses[0].ToString(), m);
					if ((DhcpMessageType)m.DhcpOptions[DhcpOption.DhcpMessageType].Contents[0] == DhcpMessageType.Ack)
						DisplayInfo(m, dhcpInfoFlowPanel);
					else if ((DhcpMessageType)m.DhcpOptions[DhcpOption.DhcpMessageType].Contents[0] == DhcpMessageType.Offer)
					{
						serverDiscoveryLabel.Text = $"Discovererd Servers: {res.Count}";
						DisplayInfo(m, dhcpDiscoverFlowPanel);
					}
				}
			};

			// ------------------------------------------------------- DHCP info
			dhcpInfoLabel.IndicatorColor = Colors.Yellow;
			_client = new DhcpClient(localMac, "", bindAddress);
			replys += (await _client.RequestInfo(progress)).Length;
			dhcpInfoLabel.IndicatorColor = res.Count > 0 ? Colors.Green : Colors.Red;

			res.Clear();

			// ------------------------------------------------------- DHCP discover
			serverDiscoveryLabel.IndicatorColor = Colors.Yellow;
			_client = new DhcpClient(_physicalAddress, "", bindAddress);
			replys += (await _client.Discover(IPAddress.Any, progress, true)) == null ? 0 : 1;
			serverDiscoveryLabel.IndicatorColor = res.Count > 0 ? Colors.Green : Colors.Red;

			if (replys == 0)
			{
				dhcpInfoPanel.Hide();
				dhcpDiscoverPanel.Hide();
				dhcpErrorLabel.Show();
			}
		}

		private string GetDhcpMessageContent(DhcpMessage message)
		{
			var res = "";
			if (message.DhcpOptions.ContainsKey(DhcpOption.AddressTime))
			{
				var lTime = (TimeOption)message.DhcpOptions[DhcpOption.AddressTime];
				res += $"Lease time: {lTime.Time.ToDurationTextEN()}\n";
			}
			if (message.DhcpOptions.ContainsKey(DhcpOption.AddressRenewal))
			{
				var lTime = (TimeOption)message.DhcpOptions[DhcpOption.AddressRenewal];
				res += $"Renew time: {lTime.Time.ToDurationTextEN()}\n";
			}
			if (message.DhcpOptions.ContainsKey(DhcpOption.AddressRebind))
			{
				var lTime = (TimeOption)message.DhcpOptions[DhcpOption.AddressRebind];
				res += $"Rebind time: {lTime.Time.ToDurationTextEN()}\n";
			}
			if (message.DhcpOptions.ContainsKey(DhcpOption.SubnetMask))
			{
				var snet = (IpOption)message.DhcpOptions[DhcpOption.SubnetMask];
				res += $"Subnet: {snet.Addresses[0]}\n";
			}
			if (message.DhcpOptions.ContainsKey(DhcpOption.Router))
				res += $"Gateway: {((IpOption)message.DhcpOptions[DhcpOption.Router]).Addresses[0]}\n";
			res += "\n";
			if (message.DhcpOptions.ContainsKey(DhcpOption.DomainNameSuffix))
				res += $"Domain: {((StringOption)message.DhcpOptions[DhcpOption.DomainNameSuffix]).String}\n";
			if (message.DhcpOptions.ContainsKey(DhcpOption.DomainNameServer))
			{
				res += "DNS-Server: ";
				res += string.Join(", ", ((IpOption)message.DhcpOptions[DhcpOption.DomainNameServer]).Addresses.Select(x => x.ToString()));
			}

			return res;
		}

		private void ShowDhcpInfoOnBoard(string ip, string content, Board board)
		{
			board.SetTitle(() => $"DHCP Server - { ip }");
			board.SetContent(() => content);
		}

		private void DisplayInfo(DhcpMessage message, Control parentControl)
		{
			parentControl.Controls.Add(new Board2
			{
				Size = new Size(380, 150),
				MaximumSize = new Size(380, 300),
				AutoHeight = true,
				Radius = new CornerRadius(10),
				ColorTable = _colorScheme.PrimaryColors,
				Padding = new Padding(10),
				Font = Fonts.Default,
				ShowTitle = true,
				TitleText = $"DHCP Server - { ((IpOption)message.DhcpOptions[DhcpOption.DhcpAddress]).Addresses[0] }",
				Text = GetDhcpMessageContent(message)
			});
		}

		private void ConfigurePanels()
		{
			const int labelWidth = 200;
			const int margin = 10;

			dhcpDiscoverPanel.Size =
				dhcpInfoPanel.Size = new Size(450, Height - dhcpAquisitionPanel.Height - controlPanel.Height - margin * 2);

			dhcpDiscoverPanel.MaximumSize =
				dhcpInfoPanel.MaximumSize = new Size(450, Height - dhcpAquisitionPanel.Height - controlPanel.Height - margin * 2);

			dhcpInfoPanel.Location = new Point((Width - dhcpInfoPanel.Width) / 4 - margin * 4, controlPanel.Height + dhcpAquisitionPanel.Height + margin);
			dhcpDiscoverPanel.Location = new Point(dhcpInfoPanel.Location.X + dhcpInfoPanel.Width + margin, dhcpInfoPanel.Location.Y);

			serverDiscoveryLabel.Location =
				dhcpInfoLabel.Location = new Point(margin, margin);

			serverDiscoveryLabel.Size =
				dhcpInfoLabel.Size = new Size(dhcpDiscoverPanel.Width - margin * 2, 40);

			dhcpInfoFlowPanel.Size =
				dhcpDiscoverFlowPanel.Size = new Size(serverDiscoveryLabel.Width, dhcpInfoPanel.Height - margin * 3 - dhcpInfoLabel.Height);

			dhcpInfoFlowPanel.Location =
				dhcpDiscoverFlowPanel.Location = new Point(margin, serverDiscoveryLabel.Height + margin * 2);

			dhcpInfoPanel.Radius =
				dhcpDiscoverPanel.Radius =
				dhcpInfoFlowPanel.Radius =
				dhcpDiscoverFlowPanel.Radius = new CornerRadius(10);

			dhcpInfoLabel.Font = serverDiscoveryLabel.Font = Fonts.Big;
			dhcpErrorLabel.Font =
				titleLabel.Font = Fonts.Title;

			dhcpErrorLabel.Visible = false;
			dhcpErrorLabel.Location = new Point((Width - dhcpErrorLabel.Width) / 2, dhcpInfoPanel.Location.Y);

			var titleMargin = (controlPanel.Height - titleLabel.Height) / 2;
			titleLabel.Location = new Point(titleMargin, titleMargin);

			var nextX = dhcpAquisitionPanel.Width / 2 - (labelWidth * 4 + margin * 3) / 2;
			closeButton.Radius = new CornerRadius(closeButton.Width / 2);
			foreach (Control c in dhcpAquisitionPanel.Controls)
				if (c is StatusLabel sl)
				{
					sl.Anchor = AnchorStyles.None;
					sl.ColorTable = _colorScheme.PrimaryColors;
					sl.Margin = new Padding(0);
					sl.Radius = new CornerRadius(0, 0, 10, 10);
					sl.Width = labelWidth;
					sl.Height = 40;
					sl.Location = new Point(nextX, -1);
					nextX += margin + sl.Width;
				}
		}

		public void SetStyling(ColorScheme colorScheme)
		{
			_colorScheme = colorScheme;
			closeButton.ColorTable = colorScheme.PrimaryColors;

			dhcpInfoLabel.ColorTable =
				serverDiscoveryLabel.ColorTable = colorScheme.PrimaryColors;

			dhcpInfoPanel.ColorTable =
				dhcpDiscoverPanel.ColorTable = colorScheme.PrimaryColors;

			dhcpInfoFlowPanel.ColorTable =
				dhcpDiscoverFlowPanel.ColorTable = colorScheme.SecondaryColors;

			titleLabel.BackColor =
				controlPanel.BackColor = colorScheme.PrimaryColors.BackColor;

			titleLabel.ForeColor = colorScheme.PrimaryColors.ForeColor;
			dhcpErrorLabel.ForeColor = colorScheme.SecondaryColors.ForeColor;

			ResetIndicators();
			ConfigurePanels();
		}
	}
}
