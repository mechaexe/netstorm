﻿using NetStorm.Common.Devices;
using NetStorm.Common.Worker;
using NetStorm.CoreNG;
using NetStorm.CoreNG.Modules;
using NetStorm.CoreNG.Modules.IntrusionDetection;
using NetStorm.CoreNG.Modules.NetworkScanner;
using NetStorm.CoreNG.Modules.Portscanner;
using NetStorm.CoreNG.Modules.RessourceExhauster;
using NetStorm.CoreNG.Modules.Spoofer;
using NetStorm.CoreNG.Tools.IpInfo;
using NetStorm.CoreNG.Worker;
using NetStorm.UILib;
using NetStorm.UILib.Animations;
using NetStorm.UILib.Coloring;
using NetStorm.UILib.Controls;
using NetStorm.UILib.Features;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NetStorm.GUI
{
	partial class MainWindow : BaseWindow
	{
		private IDragger dragger, titleDragger;
		private readonly IAnimation<float> fadeOut;
		private readonly List<INetStormPage> pages;
		private readonly List<IWorker> workers;
		private readonly List<IModuleNG> _modules;
		private readonly ObservableCollection<CoreNG.Modules.Portscanner.Portscanner> portscanners;
		private readonly ObservableCollection<INetStormWorker<ISpoofer>> spoofers;
		private CancellationTokenSource _ispInfoCancellationSource;
		private ColorScheme colorScheme;

		public MainWindow()
		{
			AutoScaleMode = AutoScaleMode.None;
			colorScheme = Colors.GetColorScheme();

			InitializeComponent();
			_modules = new List<IModuleNG>();
			workers = new List<IWorker>();
			pages = new List<INetStormPage>();
			portscanners = new ObservableCollection<Portscanner>();
			spoofers = new ObservableCollection<INetStormWorker<ISpoofer>>();

			fadeOut = AnimationFactory.CreateFloatAnimator(this, nameof(Opacity));
			fadeOut.AnimationFinished += (_, __) => Close();
		}

		private void BeginClosing()
		{
			AbortWorkers();
			UnsetControlButtons();
			fadeOut.Animate(-0.1f, 0);
		}

		#region DHCP Test
		private async Task TestDhcpCapabilities()
		{
			testDhcpBtn.Enabled = false;
			testDhcpBtn.Text = "Testing...";
			await dhcpDetailsPage.StartCompleteDhcpTest(dhcpBoard);
			testDhcpBtn.Enabled = true;
			testDhcpBtn.Text = "Test DHCP";
		}
		#endregion

		#region NIC Events

		private void AbortWorkers()
		{
			workers.ForEach(x => x.Abort());
			_modules.ForEach(x => x.Stop());
			foreach (var m in portscanners.ToArray())
				m.Stop();
			foreach (var m in spoofers)
				m.Abort();
			workers.ForEach(x => x.WaitForExit());
		}

		private void Instance_InterfacesChanged(object sender, EventArgs e)
		{
			if (InvokeRequired)
			{
				devicePicker.Invoke(new Action(ResetUI));
			}
			else
			{
				ResetUI();
			}
		}

		private async void DevicePicker_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (devicePicker.SelectedIndex >= 0)
			{
				devicePicker.Enabled = false;
				_ispInfoCancellationSource?.Cancel();
				_ispInfoCancellationSource = new CancellationTokenSource();
				if(NicManager.Instance.PrimaryDeviceIndex >= 0)
					NicManager.Instance.PrimaryDevice.StopCapture();
				NicManager.Instance.PrimaryDeviceIndex = devicePicker.SelectedIndex;
				devicePicker.Enabled = true;
				await UpdateNicInformationDisplay(NicManager.Instance.PrimaryDevice, _ispInfoCancellationSource.Token);
				dhcpBoard.SetContent(() => "");
			}
		}

		private async Task UpdateNicInformationDisplay(ILocalNetworkInterface nic, CancellationToken cancellationToken)
		{
			networkInfo.SetContent(() =>
			{
				var res = "";
				res += $"Network: {nic.GetPreferredStack(IpStackMode.IPv4)}\n";
				if (nic.Gateway != null)
					res += $"Gateway: {nic.Gateway.NetworkInterface.GetPreferredStack(IpStackMode.IPv4)}\n";
				if (nic.ISP != null)
					res += $"Public IP: {nic.ISP.IPAddress}\n";
				return res;
			});
			ispInfoBoard.Hide();

			try
			{
				var ispInfo = await Task.Run(() => IpInfoFactory.GetIpInfoIsResolver().ResolveISP(nic.GetPreferredStack(IpStackMode.IPv4).IPAddress, null), cancellationToken);
				if (ispInfo != null)
				{
					var loc = ispInfo.IPInfo.Location;
					nic.ISP = ispInfo;
					networkInfo.Invalidate();   //Update network info board
					ispInfoBoard.SetContent(() =>
					{
						var res = "";
						res += $"ISP: {ispInfo.IPInfo.ISP}\n";
						res += $"Carrier grade NAT: { (nic.ISP.UsesNAT ? "yes" : "no") }\n\n";
						res += loc.Region == "" ? "" : loc.Region;
						res += loc.Country == "" ? loc.Region == "" ? "" : "\n" : loc.Region == "" ? loc.Country + "\n" : ", " + loc.Country + "\n";
						res += loc.ZIP == -1 ? "" : loc.ZIP.ToString();
						res += loc.City == "" ? "" : loc.ZIP == -1 ? loc.City : ", " + loc.City;
						return res;
					});
					ispInfoBoard.Show();
				}
			}
			catch (TaskCanceledException) { }
		}

		#endregion

		#region Board configuration
		private void ConfigureNetworkBoard(INetworkScanner scanner)
		{
			var func = new Action(() =>
			{
				if (InvokeRequired)
					scannerBoard.Invoke(new Action(scannerBoard.Invalidate));
				else
					scannerBoard.Invalidate();
			});
			scannerBoard.Text = "Local Network";
			scannerBoard.WordWrap = true;
			scannerBoard.SetContent(() =>
			{
				var scanRes = scanner.GetResult();
				var res = "Status: " + (scanner.Running ? "Running" : "Inactive") + "\n";
				res += $"Network: {scanRes.Options.NetworkV4}\n";
				res += $"Devices: {scanRes.Devices.Count}";
				return res;
			});

			scanner.ModuleStateChanged += (_, __) => func();
			scanner.DeviceDetected += (_, __) => func();
		}

		private void ConfigureSpooferBoard(Board board, ObservableCollection<INetStormWorker<ISpoofer>> spoofers)
		{
			board.Text = "Spoofer";
			board.WordWrap = true;
			board.SetContent(() =>
			{
				var res = "Status: " + (spoofers.Any(x => x.Running) ? "Running" : "Inactive") + "\n";
				res += $"Targets: {spoofers.Count}\n";
				return res;
			});

			spoofers.CollectionChanged += (_, __) =>
			{
				if (InvokeRequired)
				{
					board.Invoke(new Action(scannerBoard.Invalidate));
				}
				else
				{
					board.Invalidate();
				}
			};
		}

		private void ConfigurePortscannerBoard(Board board, ObservableCollection<CoreNG.Modules.Portscanner.Portscanner> scanners)
		{
			board.Text = "Enumeration & Portscanner";
			board.WordWrap = true;
			board.SetContent(() =>
			{
				var res = "Status: " + (scanners.Any(x => x.Running) ? "Running" : "Inactive") + "\n";
				res += $"Targets: {scanners.Count}\n";
				if(scanners.Count > 0)
					res += $"Latest Target: {scanners.Last().GetResult().Options.TargetIp}";
				return res;
			});

			scanners.CollectionChanged += (_, __) =>
			{
				if (InvokeRequired)
				{
					board.Invoke(new Action(scannerBoard.Invalidate));
				}
				else
				{
					board.Invalidate();
				}
			};
		}
		#endregion

		#region Modules
		private void AddModulesAndPages()
		{
			var networkScanner = new CoreNG.Modules.NetworkScanner.NetworkScanner("network scanner");
			var dhcpStarvation = new DhcpStarvation();
			var ids = new ArpDetector("arp detector");

			// Init network page
			var networkPage = new TabGUI();
			var deviceDetails = new Network.DeviceDetails(portscanners, spoofers);
			networkPage.AddControl("Network", new Network.NetworkScanner(networkScanner, SetInfoText, deviceDetails, networkPage.SetActivePage));
			networkPage.AddControl("Device Details", deviceDetails);

			// Init enumeration page
			var enumerationPage = new TabGUI();
			enumerationPage.AddControl("Portscanner", new Enumeration.BasicEnumeration(portscanners));

			// Init vector page
			var vectorPage = new TabGUI();
			vectorPage.AddControl("ARP Spoofer", new Vectors.ArpSpoofer(spoofers, SetInfoText));
			vectorPage.AddControl("Other", new Vectors.MiscVectors(SetInfoText, dhcpStarvation));

			// Init IDS page
			var idsPage = new TabGUI();
			idsPage.AddControl("ARP Shield", new IntrusionDetection.ArpDetector(ids, SetInfoText));

			var buttons = new[]
			{
				AssignModuleButton("Dashboard", null, null),
				AssignModuleButton("Network", networkPage, scannerBoard),
				AssignModuleButton("Enumeration", enumerationPage, portscannerBoard),
				AssignModuleButton("Intrusion Detection", idsPage),
				AssignModuleButton("Attack vectors", vectorPage, spooferBoard)
			};

			ConfigureNetworkBoard(networkScanner);
			ConfigurePortscannerBoard(portscannerBoard, portscanners);
			ConfigureSpooferBoard(spooferBoard, spoofers);

			_modules.AddRange(new IModuleNG[]
			{
				networkScanner
			});

			workers.AddRange(new IWorker[]
			{
				dhcpStarvation,
				ids
			});

			foreach (var b in buttons)
			{
				navigationPanel.Controls.Add(b);
			}

			// Open dashboard
			OpenModule(-1, buttons[0]);
		}

		private void SetInfoText(string text)
		{
			if (InvokeRequired)
			{
				statusbar.Invoke(new Action(() => toolStripStatusLabel1.Text = text));
			}
			else
			{
				toolStripStatusLabel1.Text = text;
			}
		}

		private CheckboxButton AssignModuleButton(string name, UserControl uc = null, Board board = null)
		{
			var btn = CreateModuleButton(name);
			if (uc == null)
			{
				btn.Click += (_, __) => OpenModule(-1, btn);
			}
			else
			{
				var cnt = pages.Count;    // Use actual value, not reference. Otherwise the last module will always be open
				btn.Click += (_, __) => OpenModule(cnt, btn);

				if (board != null)
				{
					board.MouseEffect = true;
					board.Click += (_, __) => OpenModule(cnt, btn);
				}

				uc.Dock = DockStyle.Fill;
				Controls.Add(uc);
				pages.Add((INetStormPage)uc);
				uc.BringToFront();
				uc.Show();
			}
			return btn;
		}

		private CheckboxButton CreateModuleButton(string text)
		{
			return new CheckboxButton
			{
				CheckedColorTable = colorScheme.SecondaryColors,
				Text = text,
				Size = new Size(navigationPanel.Width - 20, 40),
				Margin = UISettings.ButtonMargin,
				TextAlignment = HorizontalAlignment.Center
			};
		}

		private void OpenModule(int index, CheckboxButton source)
		{
			foreach (Control c in navigationPanel.Controls)
			{
				if (c is CheckboxButton btn)
				{
					btn.Checked = btn == source;
				}
			}

			if (index < 0)
			{
				foreach (var m in pages)
				{
					m.Hide();
				}
				dhcpDetailsPage.Hide();
			}
			else
			{
				pages[index].Show();
				pages[index].BringToFront();
			}
		}

		#endregion

		#region Control hooks / Overrides
		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			SuspendLayout();
			AddModulesAndPages();
			
			MakeDraggable();
			HookControls();

			SetStyling();
			ResetUI();

			ResumeLayout();
			PerformLayout();
		}

		private void HookControls()
		{
			testDhcpBtn.MouseClick += async (_, __) =>
			{
				dhcpDetailsPage.Show();
				dhcpDetailsPage.BringToFront();
				await TestDhcpCapabilities();
			};
			dhcpBoard.Click += (_, __) =>
			{
				dhcpDetailsPage.Show();
				dhcpDetailsPage.BringToFront();
			};

			minimizeButton.Click += (_, __) => Minimize();
			closeButton.Click += (_, __) => BeginClosing();
			devicePicker.SelectedIndexChanged += DevicePicker_SelectedIndexChanged;
			
			NicManager.Instance.InterfacesChanged += Instance_InterfacesChanged;
			//NicManager.Instance.InterfaceStopped += Instance_InterfaceStopped;
		}

		private void UnsetControlButtons()
		{
			minimizeButton.Click -= (_, __) => Minimize();
			closeButton.Click -= (_, __) => BeginClosing();
		}

		protected override void OnResizeBegin(EventArgs e)
		{
			SuspendLayout();
			base.OnResizeBegin(e);
		}

		protected override void OnResizeEnd(EventArgs e)
		{
			base.OnResizeEnd(e);
			ResumeLayout();
		}
		#endregion

		#region Styling

		private void ResetUI()
		{
			dhcpBoard.SetContent(() => "");
			dhcpBoard.SetTitle(() => "DHCP Server");
			dhcpBoard.Visible = NicManager.Instance.Count > 0;
			devicePicker.DataSource = null;
			devicePicker.DataSource = NicManager.Instance.Select(x => $"{x.InterfaceName} ({x.GetPreferredStack(IpStackMode.IPv4).IPAddress}/{x.IPv4Stack.Network.Cidr})").ToList();
		}

		private void MakeDraggable()
		{
			titleDragger = FeatureFactory.CreateDragger(titleLabel, this);
			titleDragger.Enabled = true;
			dragger = FeatureFactory.CreateDragger(headerPanel, this);
			dragger.Enabled = true;
		}

		private void Minimize() => WindowState = WindowState == FormWindowState.Maximized ?
				FormWindowState.Normal : FormWindowState.Minimized;

		private void SetStyling()
		{
			var darkTable = new DarkColorTable(Colors.Light, Colors.Dark, Colors.Dark);
			SetStyleRecursevly(this, colorScheme.SecondaryColors, true);
			SetStyleRecursevly(headerPanel, darkTable, true);
			SetStyleRecursevly(navigationPanel, darkTable, true);
			SetStyleRecursevly(modulePanel, colorScheme.PrimaryColors, false);
			SetStyleRecursevly(networkPanel, colorScheme.PrimaryColors, false);
			SetModuleBoardStyle(modulePanel.Controls, true);
			SetModuleBoardStyle(networkPanel.Controls, false);

			closeButton.ColorTable = new LightColorTable(Colors.Light, Colors.Red, Colors.Red);
			closeButton.Radius = minimizeButton.Radius = new CornerRadius(0);

			toolStripStatusLabel1.BackColor = statusbar.BackColor;
			toolStripStatusLabel1.ForeColor = statusbar.ForeColor;
			toolStripStatusLabel1.Text = "";

			titleLabel.Font = Fonts.Title;
			titleLabel.Location = new Point(10, (headerPanel.Height - titleLabel.Height) / 2);

			MinimumSize = new Size(1200, 600);

			devicePicker.ForeColor = Colors.Dark;
			devicePicker.BackColor = Colors.Light;
			devicePicker.Width = 400;
			devicePicker.Location = new Point(
				minimizeButton.Location.X - minimizeButton.Margin.Left - devicePicker.Margin.Right - devicePicker.Width,
				(headerPanel.Height - devicePicker.Height) / 2);

			//ispInfoBoard.WordWrap = true;
			networkInfo.WordWrap = true;
			dhcpBoard.MouseEffect = true;

			dhcpDetailsPage.SetStyling(colorScheme);
			foreach (var m in pages)
				m.SetStyling(colorScheme);
		}

		private void SetModuleBoardStyle(Control.ControlCollection boards, bool mouseEffect)
		{
			foreach (var c in boards)
				if (c is Board b)
				{
					b.MouseEffect = mouseEffect;
					b.Radius = new CornerRadius(10);
				}
		}

		private void SetStyleRecursevly(Control parent, IColorTable colorTable, bool includeParent)
		{
			if (includeParent)
			{
				if (parent.Controls.Count == 0)
					parent.Font = Fonts.Default;
				if (parent is INetStormControl isc)
				{
					isc.ColorTable = colorTable;
					isc.Radius = isc is UILib.Controls.Button ? new CornerRadius(5) : new CornerRadius(0);
					isc.MouseEffect = isc is UILib.Controls.Button;
					parent.Padding = parent is Board ? new Padding(5) : parent.Padding;
				}
				else
				{
					parent.BackColor = colorTable.BackColor;
					parent.ForeColor = colorTable.ForeColor;
				}
			}

			foreach (Control c in parent.Controls)
				SetStyleRecursevly(c, colorTable, true);
		}
		#endregion
	}

	public delegate void SetInfoText(string text);
}
