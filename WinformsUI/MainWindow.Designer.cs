﻿namespace NetStorm.GUI
{
	partial class MainWindow
	{
		/// <summary>
		/// Erforderliche Designervariable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Verwendete Ressourcen bereinigen.
		/// </summary>
		/// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Vom Windows Form-Designer generierter Code

		/// <summary>
		/// Erforderliche Methode für die Designerunterstützung.
		/// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
		/// </summary>
		private void InitializeComponent()
		{
			this.navigationPanel = new System.Windows.Forms.FlowLayoutPanel();
			this.statusbar = new NetStorm.UILib.Controls.StatusStripEx();
			this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
			this.headerPanel = new NetStorm.UILib.Controls.NSPanel();
			this.minimizeButton = new NetStorm.UILib.Controls.Button();
			this.devicePicker = new System.Windows.Forms.ComboBox();
			this.closeButton = new NetStorm.UILib.Controls.Button();
			this.panel1 = new System.Windows.Forms.Panel();
			this.modulePanel = new System.Windows.Forms.FlowLayoutPanel();
			this.scannerBoard = new NetStorm.UILib.Controls.Board();
			this.portscannerBoard = new NetStorm.UILib.Controls.Board();
			this.spooferBoard = new NetStorm.UILib.Controls.Board();
			this.networkPanel = new System.Windows.Forms.FlowLayoutPanel();
			this.networkInfo = new NetStorm.UILib.Controls.Board();
			this.dhcpBoard = new NetStorm.UILib.Controls.Board();
			this.testDhcpBtn = new NetStorm.UILib.Controls.Button();
			this.ispInfoBoard = new NetStorm.UILib.Controls.Board();
			this.titleLabel = new System.Windows.Forms.Label();
			this.dhcpDetailsPage = new NetStorm.GUI.Network.DhcpDetails();
			this.statusbar.SuspendLayout();
			this.headerPanel.SuspendLayout();
			this.panel1.SuspendLayout();
			this.modulePanel.SuspendLayout();
			this.networkPanel.SuspendLayout();
			this.dhcpBoard.SuspendLayout();
			this.dhcpDetailsPage.SuspendLayout();
			this.SuspendLayout();
			//
			// dhcpDetailsPage
			//
			this.dhcpDetailsPage.Name = "dhcpDetailsPage";
			this.dhcpDetailsPage.Dock = System.Windows.Forms.DockStyle.Fill;
			//
			// titleLabel
			//
			this.titleLabel.Name = "titleLabel";
			this.titleLabel.Text = "NetStorm";
			this.titleLabel.AutoSize = true;
			// 
			// navigationPanel
			// 
			this.navigationPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(33)))), ((int)(((byte)(33)))));
			this.navigationPanel.Dock = System.Windows.Forms.DockStyle.Left;
			this.navigationPanel.Location = new System.Drawing.Point(0, 35);
			this.navigationPanel.Name = "navigationPanel";
			this.navigationPanel.Size = new System.Drawing.Size(200, 685);
			this.navigationPanel.TabIndex = 10;
			// 
			// statusbar
			// 
			this.statusbar.GripStyle = System.Windows.Forms.ToolStripGripStyle.Visible;
			this.statusbar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
			this.statusbar.Location = new System.Drawing.Point(200, 698);
			this.statusbar.Name = "statusbar";
			this.statusbar.Size = new System.Drawing.Size(1213, 22);
			this.statusbar.TabIndex = 12;
			this.statusbar.Text = "statusStripEx1";
			// 
			// toolStripStatusLabel1
			// 
			this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
			this.toolStripStatusLabel1.Size = new System.Drawing.Size(118, 17);
			this.toolStripStatusLabel1.Text = "toolStripStatusLabel1";
			// 
			// headerPanel
			// 
			this.headerPanel.Controls.Add(this.titleLabel);
			this.headerPanel.Controls.Add(this.minimizeButton);
			this.headerPanel.Controls.Add(this.devicePicker);
			this.headerPanel.Controls.Add(this.closeButton);
			this.headerPanel.Dock = System.Windows.Forms.DockStyle.Top;
			this.headerPanel.Location = new System.Drawing.Point(0, 0);
			this.headerPanel.Margin = new System.Windows.Forms.Padding(0);
			this.headerPanel.Name = "headerPanel";
			this.headerPanel.Size = new System.Drawing.Size(1413, 35);
			this.headerPanel.TabIndex = 9;
			// 
			// minimizeButton
			// 
			this.minimizeButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
			this.minimizeButton.BorderWidth = 0F;
			this.minimizeButton.Dock = System.Windows.Forms.DockStyle.Right;
			this.minimizeButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(33)))), ((int)(((byte)(33)))));
			this.minimizeButton.Location = new System.Drawing.Point(1313, 0);
			this.minimizeButton.MouseEffect = true;
			this.minimizeButton.Name = "minimizeButton";
			this.minimizeButton.Size = new System.Drawing.Size(50, 35);
			this.minimizeButton.TabIndex = 6;
			this.minimizeButton.Text = "_";
			this.minimizeButton.TextAlignment = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// devicePicker
			// 
			this.devicePicker.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.devicePicker.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.devicePicker.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.devicePicker.FormattingEnabled = true;
			this.devicePicker.Location = new System.Drawing.Point(1058, 7);
			this.devicePicker.Name = "devicePicker";
			this.devicePicker.Size = new System.Drawing.Size(250, 23);
			this.devicePicker.TabIndex = 5;
			// 
			// closeButton
			// 
			this.closeButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
			this.closeButton.BorderWidth = 0F;
			this.closeButton.Dock = System.Windows.Forms.DockStyle.Right;
			this.closeButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(33)))), ((int)(((byte)(33)))));
			this.closeButton.Location = new System.Drawing.Point(1363, 0);
			this.closeButton.MouseEffect = true;
			this.closeButton.Name = "closeButton";
			this.closeButton.Size = new System.Drawing.Size(50, 35);
			this.closeButton.TabIndex = 1;
			this.closeButton.Text = "X";
			this.closeButton.TextAlignment = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.modulePanel);
			this.panel1.Controls.Add(this.networkPanel);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel1.Location = new System.Drawing.Point(200, 35);
			this.panel1.Margin = new System.Windows.Forms.Padding(0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(1213, 663);
			this.panel1.TabIndex = 13;
			// 
			// modulePanel
			// 
			this.modulePanel.Controls.Add(this.scannerBoard);
			this.modulePanel.Controls.Add(this.portscannerBoard);
			this.modulePanel.Controls.Add(this.spooferBoard);
			this.modulePanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.modulePanel.Location = new System.Drawing.Point(0, 0);
			this.modulePanel.Margin = new System.Windows.Forms.Padding(0);
			this.modulePanel.Name = "modulePanel";
			this.modulePanel.Size = new System.Drawing.Size(913, 663);
			this.modulePanel.TabIndex = 10;
			// 
			// scannerBoard
			// 
			this.scannerBoard.Location = new System.Drawing.Point(10, 10);
			this.scannerBoard.Margin = new System.Windows.Forms.Padding(10, 10, 0, 0);
			this.scannerBoard.MouseEffect = false;
			this.scannerBoard.Name = "scannerBoard";
			this.scannerBoard.Size = new System.Drawing.Size(280, 150);
			this.scannerBoard.TabIndex = 0;
			this.scannerBoard.Text = "Scanner";
			this.scannerBoard.WordWrap = false;
			// 
			// portscannerBoard
			// 
			this.portscannerBoard.Location = new System.Drawing.Point(300, 10);
			this.portscannerBoard.Margin = new System.Windows.Forms.Padding(10, 10, 0, 0);
			this.portscannerBoard.MouseEffect = false;
			this.portscannerBoard.Name = "portscannerBoard";
			this.portscannerBoard.Size = new System.Drawing.Size(280, 150);
			this.portscannerBoard.TabIndex = 1;
			this.portscannerBoard.Text = "Portscanner";
			this.portscannerBoard.WordWrap = false;
			// 
			// spooferBoard
			// 
			this.spooferBoard.Location = new System.Drawing.Point(590, 10);
			this.spooferBoard.Margin = new System.Windows.Forms.Padding(10, 10, 0, 0);
			this.spooferBoard.MouseEffect = false;
			this.spooferBoard.Name = "spooferBoard";
			this.spooferBoard.Size = new System.Drawing.Size(280, 150);
			this.spooferBoard.TabIndex = 2;
			this.spooferBoard.Text = "Spoofer";
			this.spooferBoard.WordWrap = false;
			// 
			// networkPanel
			// 
			this.networkPanel.Controls.Add(this.networkInfo);
			this.networkPanel.Controls.Add(this.dhcpBoard);
			this.networkPanel.Controls.Add(this.ispInfoBoard);
			this.networkPanel.Dock = System.Windows.Forms.DockStyle.Right;
			this.networkPanel.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
			this.networkPanel.Location = new System.Drawing.Point(913, 0);
			this.networkPanel.Margin = new System.Windows.Forms.Padding(0);
			this.networkPanel.Name = "networkPanel";
			this.networkPanel.Size = new System.Drawing.Size(300, 663);
			this.networkPanel.TabIndex = 9;
			// 
			// networkInfo
			// 
			this.networkInfo.Location = new System.Drawing.Point(10, 10);
			this.networkInfo.Margin = new System.Windows.Forms.Padding(10, 10, 0, 0);
			this.networkInfo.MouseEffect = false;
			this.networkInfo.Name = "networkInfo";
			this.networkInfo.Size = new System.Drawing.Size(280, 150);
			this.networkInfo.TabIndex = 1;
			this.networkInfo.Text = "Network";
			this.networkInfo.WordWrap = false;
			// 
			// dhcpBoard
			// 
			this.dhcpBoard.Controls.Add(this.testDhcpBtn);
			this.dhcpBoard.Location = new System.Drawing.Point(10, 170);
			this.dhcpBoard.Margin = new System.Windows.Forms.Padding(10, 10, 0, 0);
			this.dhcpBoard.MouseEffect = false;
			this.dhcpBoard.Name = "dhcpBoard";
			this.dhcpBoard.Size = new System.Drawing.Size(280, 150);
			this.dhcpBoard.TabIndex = 2;
			this.dhcpBoard.WordWrap = false;
			// 
			// testDhcpBtn
			// 
			this.testDhcpBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
			this.testDhcpBtn.BorderWidth = 1F;
			this.testDhcpBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(239)))), ((int)(((byte)(241)))));
			this.testDhcpBtn.Location = new System.Drawing.Point(190, 109);
			this.testDhcpBtn.Margin = new System.Windows.Forms.Padding(12);
			this.testDhcpBtn.MouseEffect = true;
			this.testDhcpBtn.Name = "testDhcpBtn";
			this.testDhcpBtn.Padding = new System.Windows.Forms.Padding(5);
			this.testDhcpBtn.Size = new System.Drawing.Size(79, 29);
			this.testDhcpBtn.TabIndex = 27;
			this.testDhcpBtn.Text = "Test DHCP";
			this.testDhcpBtn.TextAlignment = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// ispInfoBoard
			// 
			this.ispInfoBoard.Location = new System.Drawing.Point(10, 330);
			this.ispInfoBoard.Margin = new System.Windows.Forms.Padding(10, 10, 0, 0);
			this.ispInfoBoard.MouseEffect = false;
			this.ispInfoBoard.Name = "ispInfoBoard";
			this.ispInfoBoard.Size = new System.Drawing.Size(280, 150);
			this.ispInfoBoard.TabIndex = 2;
			this.ispInfoBoard.Text = "ISP";
			this.ispInfoBoard.WordWrap = true;
			// 
			// MainWindow
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1413, 720);
			this.ControlBox = false;
			this.Controls.Add(this.dhcpDetailsPage);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.statusbar);
			this.Controls.Add(this.navigationPanel);
			this.Controls.Add(this.headerPanel);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.MaximumSize = new System.Drawing.Size(1920, 1040);
			this.MinimumSize = new System.Drawing.Size(1080, 720);
			this.Name = "MainWindow";
			this.Opacity = 0.30000001192092896D;
			this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
			this.Text = "NetStorm";
			this.statusbar.ResumeLayout(false);
			this.statusbar.PerformLayout();
			this.headerPanel.ResumeLayout(false);
			this.panel1.ResumeLayout(false);
			this.modulePanel.ResumeLayout(false);
			this.networkPanel.ResumeLayout(false);
			this.dhcpBoard.ResumeLayout(false);
			this.dhcpDetailsPage.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion
		private UILib.Controls.NSPanel headerPanel;
		private System.Windows.Forms.ComboBox devicePicker;
		private UILib.Controls.Button closeButton;
		private System.Windows.Forms.FlowLayoutPanel navigationPanel;
		private UILib.Controls.Button minimizeButton;
		private UILib.Controls.StatusStripEx statusbar;
		private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.FlowLayoutPanel networkPanel;
		private UILib.Controls.Board networkInfo;
		private UILib.Controls.Board ispInfoBoard;
		private System.Windows.Forms.FlowLayoutPanel modulePanel;
		private System.Windows.Forms.Label titleLabel;
		private UILib.Controls.Board scannerBoard;
		private UILib.Controls.Board portscannerBoard;
		private UILib.Controls.Board spooferBoard;
		private UILib.Controls.Board dhcpBoard;
		private UILib.Controls.Button testDhcpBtn;
		private NetStorm.GUI.Network.DhcpDetails dhcpDetailsPage;
	}
}

