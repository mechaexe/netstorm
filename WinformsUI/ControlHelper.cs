﻿using NetStorm.UILib;
using NetStorm.UILib.Features;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace NetStorm.GUI
{
	class ControlHelper
	{
		public static readonly int MARGIN = 10;
		public static readonly int DEFAULT_HEIGHT = Fonts.Default.Height + MARGIN;
		public static int GetDefaultWidth(string text)
			=> text.Length * 7 + MARGIN;

		public static Size GetDefaultSize(string text)
			=> new Size(GetDefaultWidth(text), DEFAULT_HEIGHT);

		public static NetStorm.UILib.Controls.Button CreateActionButton(string text)
			=> new NetStorm.UILib.Controls.Button
			{
				BackColor = Colors.Dark,
				ForeColor = Colors.Light,
				Font = Fonts.Default,
				BorderWidth = 1F,
				MouseEffect = true,
				AutoSize = false,
				Radius = new CornerRadius(5),
				Size = GetDefaultSize(text),
				Text = text,
				TextAlignment = HorizontalAlignment.Center
			};

		public static void XSpaceControlsEvenly(int offset_left, params Control[] controls)
		{
			foreach(var c in controls)
			{
				c.Location = new Point(offset_left, c.Location.Y);
				offset_left += c.Width + MARGIN;
			}
		}

		public static void XSpaceControlsEvenlyReverse(int offset_right, params Control[] controls)
		{
			foreach (var c in controls)
			{
				offset_right -= c.Width;
				c.Location = new Point(offset_right, c.Location.Y);
				offset_right -= MARGIN;
			}
		}
	}
}
