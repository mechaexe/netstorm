﻿using NetStorm.UILib.Coloring;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace NetStorm.GUI
{
	public partial class TabGUI : UserControl, INetStormPage
	{
		private readonly List<UserControl> children;
		private int activePage = 0;

		public TabGUI()
		{
			InitializeComponent();
			children = new List<UserControl>();
		}

		public int AddControl(string tabName, UserControl control)
		{
			control.Dock = DockStyle.Fill;
			navigation.Buttons.Add(tabName);
			children.Add(control);
			Controls.Add(control);
			BringToFront();
			return navigation.Buttons.IndexOf(tabName);
		}

		public void SetActivePage(int index)
		{
			navigation.SelectedIndex = index;
			SuspendLayout();
			children[activePage].Hide();
			children[index].Show();
			navigationPanel.BringToFront();
			ResumeLayout();
			activePage = index;
		}

		protected override void OnLoad(EventArgs e)
		{
			foreach (var c in children)
			{
				c.Hide();
				c.BringToFront();
			}
			navigation.SelectedIndexChanged += (_, __) => SetActivePage(navigation.SelectedIndex);
			base.OnLoad(e);
		}

		public void SetStyling(ColorScheme colorScheme)
		{
			navigation.ColorTable = colorScheme.SecondaryColors;
			navigation.CheckedColorTable = colorScheme.AccentColors;

			foreach (INetStormPage m in children)
				m.SetStyling(colorScheme);

			var y = (navigationPanel.Height - navigation.Height) / 2;
			navigation.Location = new Point(y, y);
			navigation.Update();
			SetActivePage(0);
		}
	}
}
