﻿using System;
using System.Drawing;
using System.Windows.Forms;
using NetStorm.UILib;
using NetStorm.UILib.Animations;

namespace NetStorm.GUI
{
	partial class Splash : BaseWindow
	{
		public Splash()
		{
			InitializeComponent();
		}

		public void InitClose()
		{
			var anim = AnimationFactory.CreateFloatAnimator(this, "Opacity");
			anim.Animate(-0.1f, 0);
			anim.AnimationFinished += Anim_AnimationFinished;
		}

		private void Anim_AnimationFinished(object sender, System.EventArgs e)
		{
			Close();
		}

		private void PaintTriangle(Graphics g)
		{
			var offset_y = 50;
			var multiplier = 30;

			var triangle = new Point[] {
				new Point(0, 0 + offset_y),
				new Point(0, 2 * multiplier + offset_y),
				new Point(1 * multiplier, 1 * multiplier + offset_y)
			};

			var colors = Colors.GetColorScheme().PrimaryColors;
			using SolidBrush b = new SolidBrush(colors.Accent);
			g.FillPolygon(b, triangle);
			b.Dispose();
		}

		protected override void OnLoad(EventArgs e)
		{
			Font = Fonts.Default;
			var colors = Colors.GetColorScheme().PrimaryColors;
			BackColor = colors.BackColor;
			ForeColor = colors.ForeColor;

			titleLabel.Font = Fonts.Title;
			titleLabel.Text = "NetStorm";
			subtitleLabel.Text = "by MXE";
			base.OnLoad(e);

		}

		protected override void OnPaint(PaintEventArgs e)
		{
			PaintTriangle(e.Graphics);
			base.OnPaint(e);
		}

		protected override void WndProc(ref Message m)
		{
			base.WndProc(ref m);
			if (WindowState == FormWindowState.Maximized)
			{
				WindowState = FormWindowState.Normal;
			}
		}
	}
}
