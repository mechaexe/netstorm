﻿using NetStorm.UILib;
using NetStorm.UILib.Coloring;
using NetStorm.UILib.Controls;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace NetStorm.GUI.Panels
{
	public partial class CustomPanel : UserControl, INetStormPage
	{
		public StatusLabel Title => titleLabel;

		private readonly List<Control> _bodyControls, _titleControls;

		public CustomPanel()
		{
			InitializeComponent();

			_bodyControls = new List<Control>();
			_titleControls = new List<Control>() { paginator };
			paginator.SelectedIndexChanged += Paginator_SelectedIndexChanged;
		}

		private int GetYControlOffset(Control c)
			=> (titleLabel.Height + c.Height) / 2 - c.Height + titleLabel.Location.Y;

		public void AddControlToTitle(Control c)
		{
			mainPanel.SuspendLayout();
			mainPanel.Controls.Add(c);
			_titleControls.Add(c);

			c.Location = new Point(0, GetYControlOffset(c));
			c.Anchor = AnchorStyles.Top | AnchorStyles.Right;

			c.Show();
			c.BringToFront();
			ControlHelper.XSpaceControlsEvenlyReverse(mainPanel.Width - ControlHelper.MARGIN, _titleControls.ToArray());
			mainPanel.ResumeLayout(true);
		}

		public void AddControlToBody(Control c, string name, bool openNewPage)
		{
			paginator.Buttons.Add(name);
			ControlHelper.XSpaceControlsEvenlyReverse(mainPanel.Width - ControlHelper.MARGIN, _titleControls.ToArray());

			c.Location = new Point(titleLabel.Location.X, titleLabel.Location.Y + titleLabel.Height + ControlHelper.MARGIN);
			c.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
			c.Size = new Size(
				Width - ControlHelper.MARGIN * 2,
				Height - paginator.Height - paginator.Location.Y - ControlHelper.MARGIN * 2);

			_bodyControls.Add(c);
			Controls.Add(c);
			if(openNewPage)
				paginator.SelectedIndex = paginator.Buttons.Count - 1;
		}

		private void Paginator_SelectedIndexChanged(object sender, EventArgs e)
		{
			foreach (var c in _bodyControls)
				c.Hide();
			_bodyControls[paginator.SelectedIndex].Show();
			_bodyControls[paginator.SelectedIndex].BringToFront();
		}

		public virtual void SetStyling(ColorScheme colorScheme)
		{
			mainPanel.Radius = new NetStorm.UILib.Features.CornerRadius(10);

			paginator.ColorTable = colorScheme.PrimaryColors;
			paginator.CheckedColorTable = colorScheme.AccentColors;
			titleLabel.ColorTable = 	
				mainPanel.ColorTable = colorScheme.PrimaryColors;

			titleLabel.Font = Fonts.Big;
			titleLabel.Location = new Point(ControlHelper.MARGIN, ControlHelper.MARGIN);
			titleLabel.Size = new Size(mainPanel.Width - 20, titleLabel.Height);

			paginator.Height = ControlHelper.DEFAULT_HEIGHT;
			paginator.Location = new Point(mainPanel.Width - paginator.Width - ControlHelper.MARGIN, GetYControlOffset(paginator));
		}
	}
}
