﻿
namespace NetStorm.GUI.Panels
{
	partial class CustomPanel
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.titleLabel = new NetStorm.UILib.Controls.StatusLabel();
			this.mainPanel = new NetStorm.UILib.Controls.NSPanel();
			this.paginator = new NetStorm.UILib.Controls.MultiButton();
			this.mainPanel.SuspendLayout();
			this.SuspendLayout();
			// 
			// titleLabel
			// 
			this.titleLabel.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right;
			this.titleLabel.AutoEllipsis = true;
			this.titleLabel.AutoSize = false;
			this.titleLabel.Location = new System.Drawing.Point(10, 10);
			this.titleLabel.Margin = new System.Windows.Forms.Padding(0);
			this.titleLabel.Size = new System.Drawing.Size(40, 20);
			this.titleLabel.Name = "titleLabel";
			this.titleLabel.Text = "Lorem ipsum";
			this.titleLabel.TabIndex = 0;
			// 
			// paginator
			// 
			this.paginator.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right;
			this.paginator.AutoSize = true;
			this.paginator.Location = new System.Drawing.Point(10, 10);
			this.paginator.Margin = new System.Windows.Forms.Padding(0);
			this.paginator.Padding = new System.Windows.Forms.Padding(4);
			this.paginator.Radius = new NetStorm.UILib.Features.CornerRadius(5);
			this.paginator.Size = new System.Drawing.Size(40, 20);
			this.paginator.Name = "paginator";
			// 
			// mainPanel
			// 
			this.mainPanel.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right;
			this.mainPanel.BorderWidth = 0F;
			this.mainPanel.Controls.Add(this.paginator);
			this.mainPanel.Controls.Add(this.titleLabel);
			this.mainPanel.Location = new System.Drawing.Point(0, 0);
			this.mainPanel.MouseEffect = false;
			this.mainPanel.Name = "mainPanel";
			this.mainPanel.Padding = new System.Windows.Forms.Padding(10);
			this.mainPanel.Size = new System.Drawing.Size(100, 100);
			this.mainPanel.TabIndex = 3;
			// 
			// ListViewPanel
			// 
			this.Controls.Add(mainPanel);
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Name = "ListViewPanel";
			this.Size = new System.Drawing.Size(100, 100);
			this.mainPanel.ResumeLayout(false);
			this.ResumeLayout(false);
		}

		#endregion
		private NetStorm.UILib.Controls.NSPanel mainPanel;
		private NetStorm.UILib.Controls.StatusLabel titleLabel;
		private NetStorm.UILib.Controls.MultiButton paginator;
	}
}
