﻿using NetStorm.UILib;
using NetStorm.UILib.Coloring;
using NetStorm.UILib.Controls;
using System;
using System.Windows.Forms;

namespace NetStorm.GUI.Panels
{
	public partial class ListViewPanel : CustomPanel
	{
		public ListView ListView => listView;

		private readonly ListViewEx listView;

		public ListViewPanel()
		{
			InitializeComponent();
			listView = new ListViewEx();
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			AddControlToBody(listView, "View", true);
		}

		private void InitialiseListview(ColorScheme colorScheme)
		{
			// portlist
			listView.ForeColor = colorScheme.PrimaryColors.ForeColor;
			listView.BackColor = colorScheme.PrimaryColors.BackColor;
			listView.AutoSizeLastColumn = true;
			listView.BorderStyle = BorderStyle.None;
			listView.Name = "listView";
			listView.Font = Fonts.Default;
		}

		public override void SetStyling(ColorScheme colorScheme)
		{
			base.SetStyling(colorScheme);
			InitialiseListview(colorScheme);
		}
	}
}
