﻿using NetStorm.UILib;
using NetStorm.UILib.Coloring;
using NetStorm.UILib.Controls;
using System.Drawing;
using System.Windows.Forms;

namespace NetStorm.GUI.Panels
{
	public partial class FlowPanel : UserControl, INetStormPage
	{
		public string Title
		{
			get => titleLabel.Text;
			set => titleLabel.Text = value;
		}
		public int IndicatorSize
		{
			get => titleLabel.IndicatorSize;
			set => titleLabel.IndicatorSize = value;
		}
		public Color IndicatorColor
		{
			get => titleLabel.IndicatorColor;
			set => titleLabel.IndicatorColor = value;
		}
		public NSFlowLayoutPanel ControlPanel => flowPanel;
		public FlowPanel()
		{
			InitializeComponent();
		}

		public void SetStyling(ColorScheme colorScheme)
		{
			flowPanel.Radius = mainPanel.Radius = new NetStorm.UILib.Features.CornerRadius(10);
			titleLabel.ColorTable = 	
				mainPanel.ColorTable = colorScheme.PrimaryColors;
			flowPanel.ColorTable = colorScheme.SecondaryColors;
			titleLabel.Font = Fonts.Big;
		}
	}
}
