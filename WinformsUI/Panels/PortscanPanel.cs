﻿using NetStorm.Common.Devices;
using NetStorm.CoreNG;
using NetStorm.CoreNG.Modules.Portscanner;
using NetStorm.CoreNG.Modules.Portscanner.Tcp;
using NetStorm.CoreNG.Modules.ServiceScanner;
using NetStorm.UILib;
using NetStorm.UILib.Coloring;
using NetStorm.UILib.Controls;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace NetStorm.GUI.Panels
{
	public partial class PortscanPanel : ListViewPanel
	{
		private IServiceScanner _serviceScanner;
		private ComboBox methodPicker;
		private readonly NSTextBox selectedPortTB;
		
		private readonly string[] _portSelection = new string[]
		{
			"21-23, 37, 53, 69, 111, 115, 123, 161, 194, 199, 554, 563, 631, 992, 994, 1720, 1723", //Administration
			"80, 443, 3124, 3128, 5800, 7000, 8008, 8080, 8081, 8443, 9080, 9443, 11371, 12443, 16080",	//HTTP
			"25, 109-110, 143, 209, 465, 587, 993, 995", //Mail
			"88, 464, 749-751, 761, 4444", //Kerberos
			"389, 636, 7389, 7636", //LDAP
			"66, 118, 150, 1433, 1434, 3050, 3306, 5432, 27017-27019",	//Database
			"123, 135, 137-139, 143, 445, 500, 593, 1025, 1433, 1434, 1900, 3372, 3398, 5000",	//MS
			"3283, 3389, 5500, 5800, 5900",	//RDP,
			"1-1024", //Well-Known
			$"1-{ushort.MaxValue - 1}"	//All
		};

		public PortscanPanel()
		{
			InitializeComponent();
			selectedPortTB = new NSTextBox()
			{
				Radius = new NetStorm.UILib.Features.CornerRadius(10),
				Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right,
				Multiline = true,
				MouseEffect = true,
			};
		}

		public PortscanOptions GetOptions(INetworkDevice target, ILocalNetworkInterface nic)
			=> new PortscanOptions(target, nic, PortSelector.Parse(selectedPortTB.Text), _serviceScanner);

		public Portscanner CreatePortscanner(INetworkDevice target, ILocalNetworkInterface nic)
		{
			var id = Guid.NewGuid();
			Portscanner portscanner = null;
			switch (methodPicker.SelectedIndex)
			{
				case 0:
					portscanner = new SynScan(id.ToString());
					break;
				case 1:
					portscanner = new FinScan(id.ToString());
					break;
				case 2:
					portscanner = new NullScan(id.ToString());
					break;
				case 3:
					portscanner = new XmasScan(id.ToString());
					break;
				case 4:
					portscanner = new ConnectScan(id.ToString());
					break;
			}
			portscanner.SetOptions(GetOptions(target, nic));
			return portscanner;
		}

		private void InitializePortlistPanel(ColorScheme colorScheme)
		{
			const int margin = 10;

			// portlist
			ListView.FullRowSelect = true;
			ListView.HideSelection = false;
			ListView.MultiSelect = false;
			ListView.UseCompatibleStateImageBehavior = false;
			ListView.View = View.Details;
			ListView.Columns.AddRange(new ColumnHeader[]
			{
				CreateHeader("Port", 60),
				CreateHeader("Protocol", 60),
				CreateHeader("Status", 75),
				CreateHeader("Service", 127)
			});

			// settings page
			var settings = new Panel()
			{
				BackColor = colorScheme.PrimaryColors.BackColor,
				ForeColor = colorScheme.PrimaryColors.ForeColor,
				Size = new Size(100, 500),
				Font = Fonts.Default
			};
			var bgrabber = new ToggleButton()
			{
				Anchor = AnchorStyles.Top | AnchorStyles.Left,
				Padding = new Padding(5, 0, 5, 0),
				ColorTable = colorScheme.PrimaryColors,
				Text = "Bannergrabbing",
				Size = new Size(200, 30),
			};
			bgrabber.CheckedChanged += (_, __) => _serviceScanner = bgrabber.Checked ? ServiceScannerFactory.GetFullServiceScanner() : null;
			bgrabber.Checked = true;

			var portPicker = CreateCombobox("Default", "Administration", "HTTP", "Mail", "Kerberos", "LDAP", "DBs", "Microsoft", "RDP", "Well-Known", "All");
			portPicker.SelectedIndexChanged += (_, __) =>
			{
				if (portPicker.SelectedIndex != 0)
					selectedPortTB.Text = _portSelection[portPicker.SelectedIndex - 1];
				else
					selectedPortTB.Text = string.Join(", ", _portSelection[0..8]);
			};
			portPicker.SelectedIndex = 0;

			methodPicker = CreateCombobox("SYN", "FIN", "NULL", "XMAS", "Connect");
			methodPicker.Enabled = LocalEnvironment.Api != NetworkAPI.Native;
			methodPicker.SelectedIndex = methodPicker.Enabled ? 0 : methodPicker.Items.Count - 1;

			var methodPanel = new TableLayoutPanel
			{
				Anchor = AnchorStyles.Top | AnchorStyles.Left,
				ColumnCount = 2,
				Name = "methodPanel",
				Padding = new Padding(0),
				RowCount = 2,
				Size = new Size(200, Fonts.Default.Height * 4),
			};
			methodPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
			methodPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
			methodPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50F));
			methodPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50F));

			methodPanel.Controls.Add(new Label() { Text = "Scan Method", TextAlign = ContentAlignment.MiddleLeft, AutoSize = false, Dock = DockStyle.Fill }, 0, 1);
			methodPanel.Controls.Add(new Label() { Text = "Ports (Preset)", TextAlign = ContentAlignment.MiddleLeft, AutoSize = false, Dock = DockStyle.Fill }, 0, 0);
			methodPanel.Controls.Add(portPicker, 1, 0);
			methodPanel.Controls.Add(methodPicker, 1, 1);

			selectedPortTB.Size = new Size(settings.Width, settings.Height - bgrabber.Height - methodPanel.Height - margin * 2);
			selectedPortTB.ColorTable = colorScheme.SecondaryColors;
			selectedPortTB.Padding = new Padding(0, 10, 0, 10);

			settings.Controls.AddRange(new Control[]
			{
				bgrabber,
				methodPanel,
				selectedPortTB
			});

			// Adjust inner control location
			int y = 0;
			foreach (Control c in settings.Controls)
			{
				c.Location = new Point(0, y);
				y += c.Height + margin;
			}

			// add pages
			AddControlToBody(settings, "Options", false);
		}

		private ColumnHeader CreateHeader(string text, int width)
			=> new ColumnHeader() { Text = text, Width = width };
		private ComboBox CreateCombobox(params string[] items)
		{
			var comboBox = new ComboBox
			{
				Dock = DockStyle.Fill,
				DropDownStyle = ComboBoxStyle.DropDownList,
				FormattingEnabled = true,
				Margin = new Padding(4, 3, 4, 3),
				Size = new Size(85, 23)
			};

			comboBox.Items.AddRange(items);
			return comboBox;
		}

		public override void SetStyling(ColorScheme colorScheme)
		{
			base.SetStyling(colorScheme);
			InitializePortlistPanel(colorScheme);
		}
	}
}
