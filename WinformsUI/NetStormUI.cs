﻿using NetStorm.GUI;
using NetStorm.Common.Logging;
using NetStorm.CoreNG;
using System;
using System.Threading;
using System.Windows.Forms;

namespace NetStorm.GUI
{
	static class NetStormUI
	{
		/// <summary>
		/// Der Haupteinstiegspunkt für die Anwendung.
		/// </summary>
		[STAThread]
		static void Main()
		{
			var loggers = new ILogger[]
			{
				new LogHistory(),
				new DebugLog(),
			};
			foreach (var logger in loggers)
				logger.AttachTo(LogEngine.GlobalLog);

			AppDomain.CurrentDomain.UnhandledException += (_, __) => LogEngine.GlobalLog.Log(__.ExceptionObject.ToString(), LogLevel.Critical);

			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);

			Bootstrap();
			Application.Run(new MainWindow());
			NicManager.Instance.CloseAll();

			((LogHistory)loggers[0]).Print();
		}

		static void Bootstrap()
		{
			using (var splash = new Splash())
			{
				var splashThread = new Thread(() => Application.Run(splash));
				splashThread.Start();

				LoadAssets();

				splash.Invoke(new Action(() =>
				{
					splash.InitClose();
					splash.Dispose();
				}));
				splashThread.Join();
			}
		}

		static void LoadAssets()
		{
			LocalEnvironment.Analyze();
			NicManager.Instance.RefreshNicList();
		}
	}
}
