﻿using NetStorm.Common.Classes;
using NetStorm.Common.Devices;
using NetStorm.CoreNG;
using NetStorm.CoreNG.Modules.Portscanner;
using NetStorm.CoreNG.Modules.Transformation;
using NetStorm.CoreNG.Tools;
using NetStorm.CoreNG.Worker;
using NetStorm.GUI.Enumeration.TextProvider;
using NetStorm.UILib;
using NetStorm.UILib.Coloring;
using NetStorm.UILib.Controls;
using NetStorm.UILib.Features;
using Protocols.ICMP;
using System;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NetStorm.GUI.Enumeration
{
	public partial class BasicEnumeration : UserControl, INetStormPage
	{
		private ColorScheme _colorScheme;
		private INetworkDevice _device;
		private ILocalNetworkInterface _interface;
		private UILib.Controls.Button portscanButton, traceButton;
		private readonly CompositeDeviceTextProvider _defaultDeviceText;
		private readonly ObservableCollection<Portscanner> _portscanners;

		public BasicEnumeration(ObservableCollection<Portscanner> portscanners)
		{
			InitializeComponent();
			InitializeCustomComponent();

			_defaultDeviceText = new CompositeDeviceTextProvider();
			_defaultDeviceText.DeviceTextProviders.AddRange(new IDeviceTextProvider[]
			{
				new DeviceNetworkTextProvider(),
				new IpInfoTextProvider(),
				new LocationTextProvider(),
				new DeviceStatsTextProvider()
			});

			_portscanners = portscanners;
		}

		private void InitializeCustomComponent()
		{
			portscanButton = ControlHelper.CreateActionButton("Scan");
			traceButton = ControlHelper.CreateActionButton("Trace");

			portListPanel.AddControlToTitle(portscanButton);
			tracePanel.AddControlToTitle(traceButton);

			tracePanel.ListView.FullRowSelect = true;
			tracePanel.ListView.HideSelection = false;
			tracePanel.ListView.MultiSelect = false;
			tracePanel.ListView.UseCompatibleStateImageBehavior = false;
			tracePanel.ListView.View = View.Details;
			tracePanel.ListView.Columns.AddRange(new[]
			{
				CreateHeader("Hop", 100),
				CreateHeader("TTL", 100),
				CreateHeader("Machine", 100)
			});
		}

		private ColumnHeader CreateHeader(string text, int width)
			=> new ColumnHeader() { Text = text, Width = width };

		private void Device_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			if (Visible && _device != null)
				UpdateAll();
		}

		public void SetLocalInterface(ILocalNetworkInterface localInterface)
			=> _interface = localInterface;

		public void SetDevice(INetworkDevice device)
		{
			ClearAll();
			portscanButton.Enabled = true;
			if (_device != null)
			{
				_device.PropertyChanged -= Device_PropertyChanged;
				_device.Ports.CollectionChanged -= Ports_CollectionChanged;
			}
			_device = device;
			if(_device != null)
			{
				_device.PropertyChanged += Device_PropertyChanged;
				_device.Ports.CollectionChanged += Ports_CollectionChanged;
				UpdateTitle(_device);
				UpdateDeviceBoard(_device);
				UpdatePortList(_device);
			}
		}

		#region Device board

		private void UpdateAll()
		{
			if (InvokeRequired)
			{
				titleLabel.Invoke(new Action(() => UpdateTitle(_device)));
				deviceBoard.Invoke(new Action(() => UpdateDeviceBoard(_device)));
			}
			else
			{
				UpdateTitle(_device);
				UpdateDeviceBoard(_device);
			}
		}

		private void ClearAll()
		{
			titleLabel.Text = "Device";
			deviceBoard.Text = "";
			tracePanel.ListView.Items.Clear();
			portListPanel.ListView.Items.Clear();
			portInfoPanel.ControlPanel.Controls.Clear();
		}

		private void UpdateTitle(INetworkDevice device)
		{
			titleLabel.IndicatorColor = device.Statistics.DeviceState == DeviceState.Offline ? Colors.Orange : Colors.Green;
			titleLabel.Text = device.GetHostIdentifier(IpStackMode.IPv4);
		}

		private void UpdateDeviceBoard(INetworkDevice device)
			=> deviceBoard.Text = _defaultDeviceText.GetText(device).Replace("\n", Environment.NewLine);

		#endregion

		#region Portscan

		private void PortscanButton_Click(object sender, EventArgs e)
		{
			if (_device != null && _interface != null)
			{
				portscanButton.Enabled = false;
				var scanner = portListPanel.CreatePortscanner(_device, _interface);
				scanner.ModuleStateChanged += Portscanner_StateChanged;
				scanner.Start(_interface.GetPacketTranceiver(), new OneShotWorker());
				_portscanners.Add(scanner);
			}
		}

		private void Portscanner_StateChanged(object sender, CoreNG.Modules.StateArgs e)
		{
			if (!e.Running)
			{
				portscanButton.Invoke(new Action(() => portscanButton.Enabled = true));
				_portscanners.Remove((Portscanner)sender);
			}
		}

		private void PortView_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
		{
			if (e.Item != null && e.Item.Tag is IPort p)
				ShowPortInfo(p);
		}

		private void UpdatePortList(INetworkDevice device)
		{
			portListPanel.ListView.Items.Clear();
			foreach (var p in device.Ports.Where(p => p.Status == GetPortStatusFilter()).ToArray())
				AddPortToView(p);
		}

		private void Ports_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
		{
			if(e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Add && e.NewItems[0] is IPort p && p.Status == GetPortStatusFilter())
			{
				if (InvokeRequired)
					portListPanel.ListView.Invoke(new Action(() => AddPortToView(p)));
				else
					AddPortToView(p);
			}
		}

		private void AddPortToView(IPort p)
		{
			var lvi = new ListViewItem(p.Number.ToString())
			{
				Tag = p
			};
			lvi.SubItems.AddRange(new string[]
			{
					p.Protocol.ToString(),
					p.Status.ToString(),
					p.Service?.Service.Name ?? ""
			});
			p.PropertyChanged += (_, __) =>
			{
				if (__.PropertyName == nameof(p.Service) && p.Service != null)
					portListPanel.ListView.Invoke(new Action(() => lvi.SubItems[3].Text = p.Service.Service.Name));
			};
			portListPanel.ListView.Items.Add(lvi);
		}

		private void ShowPortInfo(IPort p)
		{
			var textProviders = new IPortTextProvider[]
			{
				new PortTextProvider(),
				new UPnPTextProvider()
			};

			portInfoPanel.ControlPanel.Controls.Clear();

			foreach(var tp in textProviders)
			{
				var txt = tp.GetPortText(p);
				if (!string.IsNullOrEmpty(txt))
					portInfoPanel.ControlPanel.Controls.Add(CreatePortInfoPanel($"{p.Number} - {p.Protocol}", txt));
			}
			
			if(p.Service != null && p.Service.Banners.Count > 0)
				portInfoPanel.ControlPanel.Controls.Add(CreatePortInfoPanel("Banners", string.Join("\n", p.Service.Banners)));
		}

		private Board2 CreatePortInfoPanel(string title, string text)
			=> new Board2()
			{
				TitleText = title,
				ShowTitle = true,
				Text = text,
				ColorTable = _colorScheme.PrimaryColors,
				Radius = new CornerRadius(10),
				Width = portInfoPanel.ControlPanel.Width - 50,
				AutoHeight = true,
				MaximumSize = new Size(portInfoPanel.ControlPanel.Width - 50, 300),
				Padding = new Padding(10)
			};

		private PortStatus GetPortStatusFilter()
		{
			return PortStatus.Open;
		}

		#endregion

		#region Trace

		private async void TraceButton_Click(object sender, EventArgs e)
		{
			if(_device != null &&
				_device.NetworkInterface.StackMode != IpStackMode.Disabled)
			{
				traceButton.Enabled = false;
				tracePanel.ListView.Items.Clear();
				await Task.Run(() => Trace(_device.NetworkInterface.GetPreferredStack(IpStackMode.IPv4).IPAddress.ToString()));
				traceButton.Enabled = true;
			}
		}

		private void Trace(string destination)
		{
			int currentHop = 1;
			foreach(var trace in TraceRoute.TraceIcmp(destination, 30, 500))
			{
				var lvi = new ListViewItem(currentHop.ToString());
				lvi.SubItems.Add(trace.Options != null ? trace.Options.Ttl.ToString() : "-");
				lvi.SubItems.Add(trace.Address.ToString());
				tracePanel.ListView.Invoke(new Action(() => tracePanel.ListView.Items.Add(lvi)));
				currentHop++;
			}
		}

		#endregion

		#region History View

		private void HistoryView_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
		{
			if (e.Button == MouseButtons.Left &&
				e.Node != null &&
				e.Node.Tag is INetworkDevice device)
				SetDevice(device);
		}

		private void AddToHistory(INetworkDevice device)
		{
			var node = new TreeNode($"[{DateTime.Now}]: {device.GetHostIdentifier(IpStackMode.IPv4)}");
			node.Tag = device;
			historyView.Nodes.Insert(1, node);
		}

		#endregion

		private async void ActionButton_Click(object sender, EventArgs e)
		{
			if (NicManager.Instance.Count > 0 && NicManager.Instance.PrimaryDeviceIndex >= 0)
				SetLocalInterface(NicManager.Instance.PrimaryDevice);

			var target = searchBox.Text;
			if (!string.IsNullOrEmpty(target) && _interface != null)
			{
				actionButton.Enabled = false;
				ClearAll();
				titleLabel.IndicatorColor = Colors.Orange;
				titleLabel.Text = target;
				var device = await Task.Run(() => DeviceInitialiser.Initialise(target));
				if (device.NetworkInterface.StackMode != IpStackMode.Disabled)
				{
					var resolve = Task.Run(() => TransformDevice(device));
					AddToHistory(device);
					SetDevice(device);
					await resolve;
				}
				else titleLabel.IndicatorColor = Colors.Red;
				actionButton.Enabled = true;
			}
		}

		private INetworkDevice TransformDevice(INetworkDevice device)
		{
			var deviceResolver = new CompositeTransformator();
			if (device.NetworkInterface.Supports(IpStackMode.IPv4) &&
				_interface.Supports(IpStackMode.IPv4) &&
				_interface.IPv4Stack.Network.Contains(device.NetworkInterface.IPv4Stack.IPAddress))
				deviceResolver.AddTransformation(new ArpTransform());
			
			deviceResolver.AddTransformation(new DnsTransform());
			deviceResolver.AddTransformation(new PingFingerprintTransform());
			deviceResolver.AddTransformation(new IPInfoTransform());

			return deviceResolver.TransformDevice(device, false);
		}

		protected override void OnLoad(EventArgs e)
		{
			portListPanel.ListView.ItemSelectionChanged += PortView_ItemSelectionChanged;
			historyView.NodeMouseClick += HistoryView_NodeMouseClick;
			portscanButton.Click += PortscanButton_Click;
			actionButton.Click += ActionButton_Click;
			traceButton.Click += TraceButton_Click;
			base.OnLoad(e);
			ClearAll();
		}

		protected override void OnVisibleChanged(EventArgs e)
		{
			base.OnVisibleChanged(e);
			if (Visible && _device != null)
				UpdateAll();
		}

		private void ConfigurePanels()
		{
			const int margin = 20;
			const int portInfoWidth = 350;

			titleLabel.AutoEllipsis = true;
			titleLabel.Font = Fonts.Big;
			titleLabel.Padding = new Padding(0);
			titleLabel.Location = new Point(margin, controlPanel.Height);
			titleLabel.Size = new Size(300, 20);

			deviceBoard.Location = new Point(margin, titleLabel.Location.Y + titleLabel.Height + 10);
			deviceBoard.Size = new Size(300, Height - deviceBoard.Location.Y - 200);
			deviceBoard.Padding = new Padding(0);

			historyView.Size = new Size(300, 200);
			historyView.Location = new Point(0, Height - historyView.Height);

			portListPanel.Location = new Point(320, controlPanel.Height);
			portListPanel.Size = new Size(Width - margin * 4 - deviceBoard.Width - portInfoWidth, Height - controlPanel.Height - 200 - margin);

			portInfoPanel.Title = "Additional port info";
			portInfoPanel.Location = new Point(portListPanel.Location.X + portListPanel.Width + margin, portListPanel.Location.Y);
			portInfoPanel.Size = new Size(portInfoWidth, portListPanel.Height + 200 + margin);

			tracePanel.Location = new Point(portListPanel.Location.X, portListPanel.Location.Y + portListPanel.Height + margin);
			tracePanel.Size = new Size(portListPanel.Width, Height - portListPanel.Height - margin * 4 - margin / 2);
		}

		public void SetStyling(ColorScheme colorScheme)
		{
			_colorScheme = colorScheme;

			historyView.BackColor = colorScheme.SecondaryColors.BackColor;
			historyView.ForeColor = colorScheme.SecondaryColors.ForeColor;

			titleLabel.ColorTable =
				deviceBoard.ColorTable = colorScheme.SecondaryColors;
			controlPanel.BackColor = colorScheme.SecondaryColors.BackColor;
			actionButton.ColorTable = 
				searchBox.ColorTable = colorScheme.PrimaryColors;

			actionButton.Font = Fonts.Default;
			actionButton.Text = "Search";
			actionButton.BorderWidth = 1;
			actionButton.AutoSize = false;
			actionButton.Margin = new Padding(0, (controlPanel.Height - actionButton.Height) / 2, 15, 0);
			actionButton.Height = searchBox.Height;
			actionButton.Radius = searchBox.Radius = new CornerRadius(5);

			searchBox.MouseEffect = true;

			foreach (Control c in controlPanel.Controls)
				c.Margin = new Padding(0, (controlPanel.Height - c.Height) / 2, 15, 0);

			portscanButton.ColorTable =
				traceButton.ColorTable = colorScheme.PrimaryColors;

			portListPanel.SetStyling(colorScheme);
			portInfoPanel.SetStyling(colorScheme);
			tracePanel.SetStyling(colorScheme);
			ConfigurePanels();
		}
	}
}
