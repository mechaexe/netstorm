﻿using NetStorm.Common.Devices;

namespace NetStorm.GUI.Enumeration.TextProvider
{
	interface IDeviceTextProvider
	{
		string GetText(INetworkDevice device);
	}
}
