﻿using NetStorm.Common.Devices;

namespace NetStorm.GUI.Enumeration.TextProvider
{
	class IpInfoTextProvider : IDeviceTextProvider
	{
		public string GetText(INetworkDevice device)
		{
			var res = "";
			if (device.IPInfo != null)
			{
				res += $"Organisation: {device.IPInfo.Organisation}\n";
				res += $"ISP: {device.IPInfo.ISP}";
			}
			return res;
		}
	}
}
