﻿using NetStorm.Common.Devices;
using System.Collections.Generic;
using System.Linq;

namespace NetStorm.GUI.Enumeration.TextProvider
{
	class CompositeDeviceTextProvider : IDeviceTextProvider
	{
		public string TextJoin { get; set; } = "\n\n";
		public List<IDeviceTextProvider> DeviceTextProviders { get; } = new List<IDeviceTextProvider>();
		public string GetText(INetworkDevice device)
			=> string.Join(TextJoin, DeviceTextProviders.Where(x => !string.IsNullOrEmpty(x.GetText(device))).Select(x => x.GetText(device)));
	}
}
