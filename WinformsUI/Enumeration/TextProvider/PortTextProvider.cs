﻿using NetStorm.Common.Classes;
using NetStorm.Common.Services;

namespace NetStorm.GUI.Enumeration.TextProvider
{
	class PortTextProvider : IPortTextProvider
	{
		public string GetPortText(IPort port)
		{
			var res = "";
			if(port.Service != null)
			{
				res += port.Service.Service.ToString();
				if (port.Service is BasicApplicationService bas)
					res += $"\n{string.Join("\n", bas.Applications)}" + (string.IsNullOrEmpty(bas.AdditionalInformation) ? "" : $"\n\n{bas.AdditionalInformation}");
				if (port.Service is UPnPService ups)
					res += $"\n\n{string.Join("\n", ups.Services.Keys)}";

				//res += "\n\n" + string.Join('\n', port.Service.Banners);
			}
			return res;
		}
	}
}
