﻿using NetStorm.Common.Devices;

namespace NetStorm.GUI.Enumeration.TextProvider
{
	class LocationTextProvider : IDeviceTextProvider
	{
		public string GetText(INetworkDevice device)
		{
			var res = "";
			if (device.IPInfo != null && device.IPInfo.Location != null)
			{
				var loc = device.IPInfo.Location;
				if (!string.IsNullOrEmpty(loc.Country) || !string.IsNullOrEmpty(loc.Region))
				{
					if (!string.IsNullOrEmpty(loc.Country) && !string.IsNullOrEmpty(loc.Region))
						res = $"{loc.Region}, {loc.Country}";
					else if (!string.IsNullOrEmpty(loc.Region))
						res = $"{loc.Region}";
					else res = $"{loc.Country}";
				}
				if (!string.IsNullOrEmpty(loc.City) || loc.ZIP != -1)
				{
					res += "\n";
					if (!string.IsNullOrEmpty(loc.City) && loc.ZIP != -1)
						res += $"{loc.ZIP}, {loc.City}";
					else if (!string.IsNullOrEmpty(loc.City))
						res += $"{loc.City}";
					else res += $"{loc.ZIP}";
				}
			}
			return res.Trim();
		}
	}
}
