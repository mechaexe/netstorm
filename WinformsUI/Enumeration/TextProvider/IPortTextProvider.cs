﻿using NetStorm.Common.Classes;

namespace NetStorm.GUI.Enumeration.TextProvider
{
	interface IPortTextProvider
	{
		string GetPortText(IPort port);
	}
}
