﻿using NetStorm.Common.Devices;
using System;
using System.Net.NetworkInformation;

namespace NetStorm.GUI.Enumeration.TextProvider
{
	class DeviceNetworkTextProvider : IDeviceTextProvider
	{
		public string GetText(INetworkDevice device)
		{
			var res = "";
			res += device.NetworkInterface.Supports(IpStackMode.IPv4) ? $"{device.NetworkInterface.IPv4Stack.IPAddress}{ Environment.NewLine }" : "";
			res += device.NetworkInterface.Supports(IpStackMode.IPv6) ? $"{device.NetworkInterface.IPv6Stack.IPAddress}{ Environment.NewLine }" : "";
			res += device.NetworkInterface.Mac != PhysicalAddress.None ? $"{device.NetworkInterface.Mac.ToCannonicalString()}{ Environment.NewLine }" : "";
			res += !string.IsNullOrEmpty(device.Hostname) ? $"{device.Hostname}{ Environment.NewLine }" : "";
			res += !string.IsNullOrEmpty(device.NetworkInterface.MacVendor.Manufacturer) ? $"{device.NetworkInterface.MacVendor.Manufacturer}{ Environment.NewLine }" : "";
			res += device.OSMatrix == null ? "" : string.Join(" / ", device.OSMatrix.HighestMatches());
			return  res.Trim();
		}
	}
}
