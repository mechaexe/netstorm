﻿using NetStorm.Common.Services;
using Protocols.UPnP;
using System;
using System.Xml.Linq;

namespace NetStorm.GUI.Enumeration.TextProvider
{
	class UPnPTextProvider : IPortTextProvider
	{
		public string GetPortText(NetStorm.Common.Classes.IPort port)
		{
			var res = "";
			if (port.Number == UPnPDiscovery.UPnPPort)
			{
				if (port.Service is UPnPService service)
				{
					foreach(var svc in service.Services)
					{
						if(svc.Value != null)
						{
							res += $"{svc.Key.LocalPath}: {Environment.NewLine}";
							var pois = new (string, string)[]
							{
								("Manufacturer", "device/manufacturer"),
								("Name", "device/friendlyName"),
								("Model", "device/modelName"),
								("Description", "device/modelDescription"),
								("Type", "device/modelType")
							};
							foreach (var poi in pois)
							{
								try
								{
									var value = svc.Value.GetElementFromPath(poi.Item2)?.Value;
									if (!string.IsNullOrEmpty(value))
										res += $"{poi.Item1}: {value + Environment.NewLine}";
								}
								catch { }
							}
							res += Environment.NewLine;
						}
					}
				}
			}
			return res.Trim();
		}
	}
}
