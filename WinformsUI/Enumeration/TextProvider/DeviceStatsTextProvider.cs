﻿using NetStorm.Common.Devices;

namespace NetStorm.GUI.Enumeration.TextProvider
{
	class DeviceStatsTextProvider : IDeviceTextProvider
	{
		public string GetText(INetworkDevice device)
		{
			var res = $"First discovered: { device.Statistics.FirstSeen }\n";
			res += $"Last seen: { device.Statistics.LastSeen }";
			return res;
		}
	}
}
