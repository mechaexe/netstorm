﻿
namespace NetStorm.GUI.Enumeration
{
	partial class BasicEnumeration
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.controlPanel = new System.Windows.Forms.FlowLayoutPanel();
			this.titleLabel = new NetStorm.UILib.Controls.StatusLabel();
			this.deviceBoard = new UILib.Controls.NSTextBox();
			this.tracePanel = new NetStorm.GUI.Panels.ListViewPanel();
			this.portInfoPanel = new NetStorm.GUI.Panels.FlowPanel();
			this.portListPanel = new NetStorm.GUI.Panels.PortscanPanel();
			this.traceView = new System.Windows.Forms.ListView();
			this.searchBox = new NetStorm.UILib.Controls.NSTextBox();
			this.actionButton = new NetStorm.UILib.Controls.Button();
			this.historyView = new UILib.Controls.TreeViewEx();
			this.controlPanel.SuspendLayout();
			this.portInfoPanel.SuspendLayout();
			this.portListPanel.SuspendLayout();
			this.tracePanel.SuspendLayout();
			this.SuspendLayout();
			// 
			// controlPanel
			// 
			this.controlPanel.Controls.Add(this.searchBox);
			this.controlPanel.Controls.Add(this.actionButton);
			this.controlPanel.Dock = System.Windows.Forms.DockStyle.Top;
			this.controlPanel.Location = new System.Drawing.Point(0, 0);
			this.controlPanel.Margin = new System.Windows.Forms.Padding(0);
			this.controlPanel.Name = "controlPanel";
			this.controlPanel.Size = new System.Drawing.Size(1000, 70);
			this.controlPanel.Padding = new System.Windows.Forms.Padding(320, 0, 0, 0);
			this.controlPanel.TabIndex = 15;
			// 
			// titleLabel
			// 
			this.titleLabel.Location = new System.Drawing.Point(14, 28);
			this.titleLabel.Margin = new System.Windows.Forms.Padding(0);
			this.titleLabel.Name = "titleLabel";
			this.titleLabel.Size = new System.Drawing.Size(300, 70);
			this.titleLabel.TabIndex = 0;
			this.titleLabel.Text = "Device";
			this.titleLabel.IndicatorSize = 15;
			this.titleLabel.Padding = new System.Windows.Forms.Padding(10);
			// 
			// searchBox
			// 
			this.searchBox.BackColor = System.Drawing.SystemColors.ControlDark;
			this.searchBox.BorderWidth = 0F;
			this.searchBox.CueText = "Lookup";
			this.searchBox.Icon = null;
			this.searchBox.Location = new System.Drawing.Point(0, 20);
			this.searchBox.MouseEffect = true;
			this.searchBox.Name = "searchBox";
			this.searchBox.Size = new System.Drawing.Size(300, 28);
			this.searchBox.TabIndex = 15;
			this.searchBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// actionButton
			// 
			this.actionButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
			this.actionButton.BorderWidth = 1F;
			this.actionButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(239)))), ((int)(((byte)(241)))));
			this.actionButton.Location = new System.Drawing.Point(632, 20);
			this.actionButton.Margin = new System.Windows.Forms.Padding(4, 20, 4, 3);
			this.actionButton.MouseEffect = true;
			this.actionButton.Name = "actionButton";
			this.actionButton.Radius = new UILib.Features.CornerRadius(5);
			this.actionButton.Size = new System.Drawing.Size(80, 28);
			this.actionButton.TabIndex = 14;
			this.actionButton.Text = "Start/Stop";
			this.actionButton.TextAlignment = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// historyView
			// 
			this.historyView.Anchor = System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Bottom;
			this.historyView.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.historyView.Location = new System.Drawing.Point(300, 50);
			this.historyView.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
			this.historyView.Name = "historyView";
			this.historyView.Nodes.Add("History");
			this.historyView.Size = new System.Drawing.Size(320, 143);
			this.historyView.TabIndex = 2;
			// 
			// deviceBoard
			// 
			this.deviceBoard.Anchor = System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom;
			this.deviceBoard.Size = new System.Drawing.Size(300, 50);
			this.deviceBoard.Location = new System.Drawing.Point(10, 69);
			this.deviceBoard.Radius = new UILib.Features.CornerRadius(0, 0, 10, 10);
			this.deviceBoard.Margin = new System.Windows.Forms.Padding(0);
			this.deviceBoard.Multiline = true;
			this.deviceBoard.ReadOnly = true;
			this.deviceBoard.Name = "deviceBoard";
			// 
			// tracePanel
			// 
			this.tracePanel.Anchor = System.Windows.Forms.AnchorStyles.Right | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Bottom;
			this.tracePanel.Name = "tracePanel";
			this.tracePanel.Padding = new System.Windows.Forms.Padding(10);
			this.tracePanel.Title.Text = "Trace";
			this.tracePanel.Title.IndicatorSize = 0;
			// 
			// portInfoPanel
			// 
			this.portInfoPanel.Anchor = System.Windows.Forms.AnchorStyles.Right | System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom;
			this.portInfoPanel.Name = "portInfoPanel";
			this.portInfoPanel.Padding = new System.Windows.Forms.Padding(10);
			this.portInfoPanel.Title = "Additional port info";
			this.portInfoPanel.IndicatorSize = 0;
			//
			// portListPanel
			//
			this.portListPanel.Anchor = System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right | System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom;
			this.portListPanel.Title.Text = "Ports";
			this.portListPanel.Title.Width = 50;
			// 
			// DeviceDetails
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.controlPanel);
			this.Controls.Add(this.titleLabel);
			this.Controls.Add(this.deviceBoard);
			this.Controls.Add(this.portInfoPanel);
			this.Controls.Add(this.portListPanel);
			this.Controls.Add(this.tracePanel);
			this.Controls.Add(this.historyView);
			this.Name = "DeviceDetails";
			this.Size = new System.Drawing.Size(1000, 500);
			this.controlPanel.ResumeLayout(false);
			this.portInfoPanel.ResumeLayout(false);
			this.portListPanel.ResumeLayout(false);
			this.controlPanel.PerformLayout();
			this.tracePanel.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.FlowLayoutPanel controlPanel;
		private System.Windows.Forms.ListView traceView;
		private UILib.Controls.TreeViewEx historyView;
		private NetStorm.GUI.Panels.PortscanPanel portListPanel;
		private NetStorm.GUI.Panels.FlowPanel portInfoPanel;
		private NetStorm.GUI.Panels.ListViewPanel tracePanel;
		private NetStorm.UILib.Controls.NSTextBox searchBox;
		private NetStorm.UILib.Controls.Button actionButton;
		private NetStorm.UILib.Controls.StatusLabel titleLabel;
		private NetStorm.UILib.Controls.NSTextBox deviceBoard;
	}
}
