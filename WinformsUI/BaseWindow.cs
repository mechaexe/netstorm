﻿using System;
using System.Drawing;
using System.Windows.Forms;
using NetStorm.UILib.Animations;

namespace NetStorm.GUI
{
	public class BaseWindow : Form
	{
		private const int gripSize = 16;
		private const int WM_NCHITTEST = 0x84;
		private const int CS_DROPSHADOW = 0x00020000;
		private const int WM_NCPAINT = 0x0085;
		private const int WS_EX_COMPOSITED = 0x02000000;

		private readonly IAnimation<float> fadeInOut;

		private FormWindowState lastWindowState;

		public BaseWindow()
		{
			DoubleBuffered = true;
			SetStyle(ControlStyles.OptimizedDoubleBuffer |
				ControlStyles.ResizeRedraw, true);

			AutoScaleMode = AutoScaleMode.Font;
			fadeInOut = AnimationFactory.CreateFloatAnimator(this, "Opacity");
		}

		protected override void OnClientSizeChanged(EventArgs e)
		{
			base.OnClientSizeChanged(e);
			if (lastWindowState != WindowState)
			{
				UpdateWindow();
				lastWindowState = WindowState;
			}
		}

		protected override void OnShown(EventArgs e)
		{
			Opacity = 0;
			Focus();
			WindowState = FormWindowState.Minimized;
			WindowState = FormWindowState.Normal;
			BringToFront();
			fadeInOut.Animate(0.05f, 1f);
			base.OnShown(e);
		}

		private void UpdateWindow()
		{
			SuspendLayout();
			MaximumSize = Screen.GetWorkingArea(this).Size;
			Invalidate();
			Update();
			Refresh();
			ResumeLayout();
		}

		protected override void OnResizeEnd(EventArgs e)
		{
			UpdateWindow();
			base.OnResizeEnd(e);
		}

		protected override CreateParams CreateParams
		{
			get
			{
				CreateParams cp = base.CreateParams;
				if (!CheckAeroEnabled())
				{
					cp.ClassStyle |= CS_DROPSHADOW;
				}

				cp.ExStyle |= WS_EX_COMPOSITED;
				return cp;
			}
		}

		private bool CheckAeroEnabled()
		{
			if (Environment.OSVersion.Version.Major >= 6)
			{
				int enabled = 0;
				NativeMethods.DwmIsCompositionEnabled(ref enabled);
				return (enabled == 1) ? true : false;
			}
			return false;
		}

		protected override void WndProc(ref Message m)
		{
			if (FormBorderStyle == FormBorderStyle.None)
			{
				switch (m.Msg)
				{
					case WM_NCPAINT:
						if (CheckAeroEnabled())
						{
							var v = 2;
							NativeMethods.DwmSetWindowAttribute(Handle, 2, ref v, 4);
							MARGINS margins = new MARGINS()
							{
								bottomHeight = 1,
								leftWidth = 0,
								rightWidth = 0,
								topHeight = 0
							};
							NativeMethods.DwmExtendFrameIntoClientArea(Handle, ref margins);
						}
						break;
					case WM_NCHITTEST:
						Point pos = new Point(m.LParam.ToInt32() & 0xffff, m.LParam.ToInt32() >> 16);
						pos = PointToClient(pos);
						if (pos.X >= ClientSize.Width - gripSize &&
							pos.Y >= ClientSize.Height - gripSize &&
							WindowState == FormWindowState.Normal &&
							SizeGripStyle == SizeGripStyle.Show)
						{
							m.Result = (IntPtr)17;
							return;
						}
						break;
					default:
						break;
				}
			}
			base.WndProc(ref m);
		}
	}
}
