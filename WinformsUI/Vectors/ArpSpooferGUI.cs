﻿using NetStorm.Common.Devices;
using NetStorm.Common.Worker;
using NetStorm.CoreNG;
using NetStorm.CoreNG.Modules.Spoofer;
using NetStorm.CoreNG.Tools;
using NetStorm.CoreNG.Worker;
using NetStorm.UILib.Coloring;
using NetStorm.UILib.Controls;
using NetStorm.UILib.Features;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.NetworkInformation;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NetStorm.GUI.Vectors
{
	public partial class ArpSpooferGUI : UserControl, INetStormPage
	{
		private readonly IDropShadow _dropShadow;
		private readonly ObservableCollection<ArpSpooferNG> _workers;
		private readonly SetInfoText _setInfoText;
		private ArpSpooferNG _currentSpoofer;

		public ArpSpooferGUI(ObservableCollection<ArpSpooferNG> workers, SetInfoText setInfoText)
		{
			InitializeComponent();

			this._workers = workers;
			this._setInfoText = setInfoText;
			_dropShadow = FeatureFactory.CreateDropShadow(optionWrapper);
		}

		#region Listview item creation & display
		private ListViewItem CreateListviewItem(INetStormWorker<ISpoofer> worker)
		{
			var spoofer = worker.Module;
			var updateItems = new[]
			{
				FeatureFactory.GetAutoUpdateField("ARP".ToString),
				FeatureFactory.GetAutoUpdateField(spoofer, () => spoofer.Target1.GetPreferredStack(IpStackMode.IPv4).ToString(), nameof(spoofer.Target1)),
				FeatureFactory.GetAutoUpdateField(spoofer, () => spoofer.Target1.Mac.ToCannonicalString(), nameof(spoofer.Target1)),
				FeatureFactory.GetAutoUpdateField(spoofer, () =>
				{
					var res = "";
					res += (int)(spoofer.SpoofDirection & SpoofDirection.Target1) == (int)SpoofDirection.Target1 ? "←" : "";
					res += spoofer.SpoofDirection == SpoofDirection.Both ? " | " : "";
					res += (int)(spoofer.SpoofDirection & SpoofDirection.Target2) == (int)SpoofDirection.Target2 ? "→" : "";
					return res;
				} , nameof(spoofer.SpoofDirection)),
				FeatureFactory.GetAutoUpdateField(spoofer, () => spoofer.Target2.GetPreferredStack(IpStackMode.IPv4).ToString(), nameof(spoofer.Target2)),
				FeatureFactory.GetAutoUpdateField(spoofer, () => spoofer.Target2.Mac.ToCannonicalString(), nameof(spoofer.Target2))
			};
			var item = new AutoUpdateListviewItem<ISpoofer>(spoofer)
			{
				Group = deviceView.Groups[0],
				Tag = worker
			};
			foreach (var ui in updateItems)
			{
				item.AddField(ui);
			}

			return item;
		}

		private void Modules_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
		{
			if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Add)
				foreach (INetStormWorker<ISpoofer> d in e.NewItems)
					DisplayListviewItem(CreateListviewItem(d));
			else if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Remove)
				foreach (INetStormWorker<ISpoofer> d in e.OldItems)
					DeleteListviewItem(d);
		}

		private void DisplayListviewItem(ListViewItem item)
		{
			if (deviceView.InvokeRequired)
				deviceView.Invoke(new Action<ListViewItem>(item => deviceView.Items.Add(item)));
			else
				deviceView.Items.Add(item);
		}

		private void DeleteListviewItem(INetStormWorker<ISpoofer> spoofer)
		{
			var removeFromList = new Action(() =>
			{
				foreach (AutoUpdateListviewItem<ISpoofer> lv in deviceView.Items)
				{
					if (lv.Object.Identifier == spoofer.Module.Identifier)
					{
						deviceView.Items.Remove(lv);
						break;
					}
				}
			});
			if (deviceView.InvokeRequired)
				deviceView.Invoke(removeFromList);
			else
				removeFromList();
		}
		#endregion

		#region Spoofer creation / deletion
		private async Task<bool> StartSpoofer(string text)
		{
			var successfull = false;
			if (NicManager.Instance.PrimaryDeviceIndex >= 0)
			{
				var device = await Task.Run(() => DeviceInitialiser.Initialise(text));
				var nic = NicManager.Instance.PrimaryDevice;
				if (device.NetworkInterface.Supports(IpStackMode.IPv4) &&
					device.NetworkInterface.Mac != PhysicalAddress.None &&
					nic.IPv4Stack.Network.Contains(device.NetworkInterface.IPv4Stack.IPAddress))
				{
					var s = CreateSpoofer($"spoofer {Guid.NewGuid()}", device.NetworkInterface, nic);
					try
					{
						s.Start(null, new TimerWorkerNg() { Interval = 3000 });
						_workers.Add(s);
						successfull = true;
					}
					catch { }
				}
			}
			return successfull;
		}

		private ArpSpooferNG CreateSpoofer(string identifier, INetworkInterface target, ILocalNetworkInterface nic)
		{
			var scanner = new ArpSpooferNG(identifier);
			scanner.SetOptions(new ArpSpoofOptions(tbUseLocalMac.Checked ? nic.Mac : PhysicalAddressExtension.GenerateRandom(),
					broadcastToggle.Checked,
					target,
					nic.Gateway.NetworkInterface,
					(routerPoisoningToggle.Checked ? SpoofDirection.Target2 : 0) | (clientPoisoningToggle.Checked ? SpoofDirection.Target1 : 0),
					nic));
			scanner.SetRouter(routingToggle.Checked ? WorkerFactory.GetNicRouter($"router for {identifier}") : null);
			
			return scanner;
		}

		private bool RemoveTarget(string ip)
		{
			foreach (ListViewItem lv in deviceView.Items)
				if (lv.Tag is ArpSpooferNG spoofer && spoofer.GetResult().SpoofOptions.Target1.IPv4Stack.IPAddress.ToString() == ip)
					return RemoveSpoofer(spoofer);

			return false;
		}

		private bool RemoveSpoofer(ArpSpooferNG spooferWorker)
		{
			if (_workers.Remove(spooferWorker))
			{
				spooferWorker.Stop();
				return true;
			}
			else return false;
		}

		private bool IsSpoofed(string ip)
			=> _workers.Any(x => x.GetResult().SpoofOptions.Target1.IPv4Stack.IPAddress.ToString() == ip);
		#endregion

		private void DeviceView_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
		{
			if (e.IsSelected && e.Item.Tag is ArpSpooferNG spoofer)
			{
				_currentSpoofer = null;
				DisplaySpooferSettings(spoofer);
				_currentSpoofer = spoofer;
				actionButton.Text = "Pardon";
				searchBox.Text = _currentSpoofer.GetResult().SpoofOptions.Target1.IPv4Stack.IPAddress.ToString();
			}
		}

		private SpoofDirection GetSpoofDirectionFromOptions()
		{
			SpoofDirection sd = 0;
			sd |= clientPoisoningToggle.Checked ? SpoofDirection.Target1 : 0;
			sd |= routerPoisoningToggle.Checked ? SpoofDirection.Target2 : 0;
			return sd;
		}

		private void ApplySpooferSettings(ArpSpooferNG spoofer)
		{
			if(spoofer != null)
			{
				var ops = spoofer.GetResult().SpoofOptions;
				ops = new ArpSpoofOptions(tbUseLocalMac.Checked ? ops.LocalInterface.Mac : PhysicalAddressExtension.GenerateRandom(),
					broadcastToggle.Checked,
					ops.Target1,
					ops.Target2,
					GetSpoofDirectionFromOptions(),
					ops.LocalInterface);
				spoofer.SetOptions(ops);
				if (routingToggle.Checked)
					spoofer.SetRouter(!spoofer.IsRouting ? WorkerFactory.GetNicRouter($"router for {spoofer.Identifier}") : null);
				else
					spoofer.SetRouter(null);
			}
		}

		private void DisplaySpooferSettings(ArpSpooferNG spoofer)
		{
			var ops = spoofer.GetResult().SpoofOptions;
			routerPoisoningToggle.Checked = (int)(ops.SpoofDirection & SpoofDirection.Target2) == (int)SpoofDirection.Target2;
			clientPoisoningToggle.Checked = (int)(ops.SpoofDirection & SpoofDirection.Target1) == (int)SpoofDirection.Target1;
			routingToggle.Checked = spoofer.IsRouting;
			broadcastToggle.Enabled = tbUseLocalMac.Enabled = true;
			broadcastToggle.Checked = ops.Broadcast;
			tbUseLocalMac.Checked = ops.SpoofedAddress.Equals(ops.LocalInterface.Mac);
		}

		private async void ActionButton_Click(object sender, EventArgs e)
		{
			if (NicManager.Instance.PrimaryDevice != null && searchBox.Text != "")
			{
				if (RemoveTarget(searchBox.Text))
					actionButton.Text = "Spoof";
				else
				{
					actionButton.Enabled = false;
					if (await StartSpoofer(searchBox.Text))
						searchBox.Text = "";
					actionButton.Enabled = true;
				}
			}
		}

		private void SearchBox_TextChanged(object sender, EventArgs e)
			=> actionButton.Text = IsSpoofed(searchBox.Text) ? "Pardon" : "Spoof";

		private void HookEvents()
		{
			actionButton.Click += ActionButton_Click;
			deviceView.ItemSelectionChanged += DeviceView_ItemSelectionChanged;
			_workers.CollectionChanged += Modules_CollectionChanged;
			searchBox.TextChanged += SearchBox_TextChanged;
			searchBox.GotFocus += (_, __) => _currentSpoofer = null;

			var verifySpoofDirectionToggle = new Action<ToggleButton>(tb =>
			{
				if (clientPoisoningToggle.Checked == routerPoisoningToggle.Checked && clientPoisoningToggle.Checked == false)
					tb.Checked = true;
			});
			routerPoisoningToggle.CheckedChanged += (_, __) => verifySpoofDirectionToggle(clientPoisoningToggle);
			clientPoisoningToggle.CheckedChanged += (_, __) => verifySpoofDirectionToggle(routerPoisoningToggle);

			routerPoisoningToggle.CheckedChanged += (_, __) => ApplySpooferSettings(_currentSpoofer);
			clientPoisoningToggle.CheckedChanged += (_, __) => ApplySpooferSettings(_currentSpoofer);
			tbUseLocalMac.CheckedChanged += (_, __) => ApplySpooferSettings(_currentSpoofer);
			broadcastToggle.CheckedChanged += (_, __) => ApplySpooferSettings(_currentSpoofer);
			routingToggle.CheckedChanged += (_, __) => ApplySpooferSettings(_currentSpoofer);
		}

		public void SetStyling(ColorScheme colorScheme)
		{
			ColorizeRecursevly(panel2, colorScheme.PrimaryColors, false);
			ColorizeRecursevly(optionPanel, colorScheme.PrimaryColors, true);

			_dropShadow.ColorEnd = colorScheme.SecondaryColors.BackColor;
			_dropShadow.ColorBegin = ControlPaint.Dark(_dropShadow.ColorEnd, 0.01f);

			_dropShadow.ShadowSize = 8;
			actionButton.Radius = new CornerRadius(5);
			actionButton.BorderWidth = 1;

			actionButton.Text = "Spoof";
			searchBox.Radius = new CornerRadius(5);
			searchBox.MouseEffect = true;

			actionButton.Height = searchBox.Height;

			routerPoisoningToggle.Checked = true;
			clientPoisoningToggle.Checked = true;
			broadcastToggle.Checked = true;
			tbUseLocalMac.Checked = true;

			foreach (Control c in panel2.Controls)
			{
				c.Margin = new Padding(0, (panel2.Height - c.Height) / 2, 15, 0);
			}
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			_dropShadow.AddControl(optionPanel);
			HookEvents();
		}

		private void ColorizeRecursevly(Control parent, IColorTable colorTable, bool includeParent)
		{
			if (includeParent)
			{
				if (parent is INetStormControl isc)
					isc.ColorTable = colorTable;
				else
				{
					parent.BackColor = colorTable.BackColor;
					parent.ForeColor = colorTable.ForeColor;
				}
			}

			foreach (Control c in parent.Controls)
			{
				ColorizeRecursevly(c, colorTable, true);
			}
		}
	}
}
