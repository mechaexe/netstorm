﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using NetStorm.CoreNG;
using NetStorm.CoreNG.Modules.RessourceExhauster;
using NetStorm.UILib.Coloring;
using NetStorm.UILib.Features;

namespace NetStorm.GUI.Vectors
{
	public partial class MiscVectors : UserControl, INetStormPage
	{
		private readonly SetInfoText setInfoText;
		private readonly DhcpStarvation dhcpStarvation;

		public MiscVectors(SetInfoText setInfoText, DhcpStarvation dhcpStarvation)
		{
			InitializeComponent();
			this.setInfoText = setInfoText;
			this.dhcpStarvation = dhcpStarvation;
		}

		private void HookEvents()
		{
			acquireIpButton.Click += AcquireIpButton_Click;
			dhcpStarvation.CollectionChanged += DhcpStarvation_CollectionChanged;
			dhcpStarvation.Stopped += (_,__) => acquireIpButton.Invoke(new Action(() => 
			{
				SetActionButtonText(acquireIpButton, "Occupy available addresses");
				acquireIpButton.Enabled = true;
			}));
		}

		private void DhcpStarvation_CollectionChanged(object sender, EventArgs e)
		{
			if (InvokeRequired)
				dhcpExhaustionBoard.Invoke(new Action(() => dhcpExhaustionBoard.Invalidate()));
			else
				dhcpExhaustionBoard.Invalidate();
		}

		private void AcquireIpButton_Click(object sender, EventArgs e)
		{
			if (dhcpStarvation.Running)
			{
				dhcpStarvation.Abort();
				SetActionButtonText(acquireIpButton, "Aborting");
				acquireIpButton.Enabled = false;
			}
			else
			{
				dhcpStarvation.SetNIC(NicManager.Instance.PrimaryDevice);
				if (dhcpStarvation.Start())
					SetActionButtonText(acquireIpButton, "Abort");
			}
		}

		private void SetActionButtonText(NetStorm.UILib.Controls.Button button, string text)
		{
			button.Text = text;
			acquireIpButton.Location = new Point(button.Parent.Width - button.Width - button.Margin.Right, button.Parent.Height - button.Height - button.Margin.Bottom);
		}

		protected override void OnLoad(EventArgs e)
		{
			HookEvents();
			base.OnLoad(e);
		}

		public void SetStyling(ColorScheme colorScheme)
		{
			foreach (Control c in panel.Controls)
			{
				if (c is UILib.Controls.Board b)
				{
					b.Radius = new CornerRadius(10);
					b.ColorTable = colorScheme.PrimaryColors;
					foreach (Control cc in b.Controls)
						if (cc is UILib.Controls.INetStormControl isc)
							isc.ColorTable = colorScheme.PrimaryColors;
				}
			}

			dhcpExhaustionBoard.Font = acquireIpButton.Font;
			dhcpExhaustionBoard.Padding = new Padding(5);
			dhcpExhaustionBoard.SetTitle(() => "DHCP-Exhaustion");
			dhcpExhaustionBoard.SetContent(() =>
			{
				var res = "Status: " + (dhcpStarvation.Running ? "Running" : "Inactive") + "\n";
				res += $"Acquired IPs: {dhcpStarvation.AcquiredAddresses.Count}\n";
				if (dhcpStarvation.AcquiredAddresses.Count > 0)
					res += $"Last acquired IP: {dhcpStarvation.AcquiredAddresses.Last()}\n";
				return res;
			});
			acquireIpButton.Radius = new CornerRadius(5);
			acquireIpButton.AutoSize = true;
			SetActionButtonText(acquireIpButton, "Occupy available addresses");

			foreach (Control c in panel2.Controls)
			{
				c.Margin = new Padding(0, (panel2.Height - c.Height) / 2, 15, 0);
			}
		}
	}
}
