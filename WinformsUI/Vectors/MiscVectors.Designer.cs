﻿namespace NetStorm.GUI.Vectors
{
	partial class MiscVectors
	{
		/// <summary> 
		/// Erforderliche Designervariable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Verwendete Ressourcen bereinigen.
		/// </summary>
		/// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Vom Komponenten-Designer generierter Code

		/// <summary> 
		/// Erforderliche Methode für die Designerunterstützung. 
		/// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
		/// </summary>
		private void InitializeComponent()
		{
			this.panel2 = new System.Windows.Forms.FlowLayoutPanel();
			this.acquireIpButton = new NetStorm.UILib.Controls.Button();
			this.dhcpExhaustionBoard = new NetStorm.UILib.Controls.Board();
			this.panel = new System.Windows.Forms.FlowLayoutPanel();
			this.dhcpExhaustionBoard.SuspendLayout();
			this.panel.SuspendLayout();
			this.SuspendLayout();
			// 
			// panel2
			// 
			this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel2.Location = new System.Drawing.Point(0, 0);
			this.panel2.Margin = new System.Windows.Forms.Padding(0);
			this.panel2.Name = "panel2";
			this.panel2.Padding = new System.Windows.Forms.Padding(350, 0, 0, 0);
			this.panel2.Size = new System.Drawing.Size(1340, 70);
			this.panel2.TabIndex = 14;
			// 
			// acquireIpButton
			// 
			this.acquireIpButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
			this.acquireIpButton.BorderWidth = 1F;
			this.acquireIpButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(239)))), ((int)(((byte)(241)))));
			this.acquireIpButton.Location = new System.Drawing.Point(85, 109);
			this.acquireIpButton.Margin = new System.Windows.Forms.Padding(12, 12, 12, 12);
			this.acquireIpButton.Name = "acquireIpButton";
			this.acquireIpButton.Padding = new System.Windows.Forms.Padding(5, 5, 5, 5);
			this.acquireIpButton.Size = new System.Drawing.Size(183, 29);
			this.acquireIpButton.TabIndex = 27;
			this.acquireIpButton.Text = "Occupy available addresses";
			this.acquireIpButton.TextAlignment = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// dhcpExhaustionBoard
			// 
			this.dhcpExhaustionBoard.Controls.Add(this.acquireIpButton);
			this.dhcpExhaustionBoard.MouseEffect = false;
			this.dhcpExhaustionBoard.Location = new System.Drawing.Point(12, 12);
			this.dhcpExhaustionBoard.Margin = new System.Windows.Forms.Padding(12, 12, 0, 0);
			this.dhcpExhaustionBoard.Name = "dhcpExhaustionBoard";
			this.dhcpExhaustionBoard.Size = new System.Drawing.Size(280, 150);
			this.dhcpExhaustionBoard.TabIndex = 28;
			this.dhcpExhaustionBoard.WordWrap = false;
			// 
			// panel
			// 
			this.panel.Controls.Add(this.dhcpExhaustionBoard);
			this.panel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel.Location = new System.Drawing.Point(0, 69);
			this.panel.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
			this.panel.Name = "panel";
			this.panel.Size = new System.Drawing.Size(1340, 624);
			this.panel.TabIndex = 29;
			// 
			// MiscVectors
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.panel);
			this.Controls.Add(this.panel2);
			this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
			this.Name = "MiscVectors";
			this.Size = new System.Drawing.Size(1340, 693);
			this.dhcpExhaustionBoard.ResumeLayout(false);
			this.panel.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.FlowLayoutPanel panel2;
		private UILib.Controls.Button acquireIpButton;
		private UILib.Controls.Board dhcpExhaustionBoard;
		private System.Windows.Forms.FlowLayoutPanel panel;
	}
}
