﻿using NetStorm.Common.Devices;
using NetStorm.Common.Worker;
using NetStorm.CoreNG;
using NetStorm.CoreNG.Modules.Spoofer;
using NetStorm.CoreNG.Tools;
using NetStorm.CoreNG.Worker;
using NetStorm.UILib.Coloring;
using NetStorm.UILib.Controls;
using NetStorm.UILib.Features;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.NetworkInformation;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NetStorm.GUI.Vectors
{
	public partial class ArpSpoofer : UserControl, INetStormPage
	{
		private readonly IDropShadow dropShadow;
		private readonly ObservableCollection<INetStormWorker<ISpoofer>> workers;
		private readonly SetInfoText setInfoText;
		private INetStormWorker<ISpoofer> selectedSpoofer;

		public ArpSpoofer(ObservableCollection<INetStormWorker<ISpoofer>> workers, SetInfoText setInfoText)
		{
			InitializeComponent();

			this.workers = workers;
			this.setInfoText = setInfoText;
			dropShadow = FeatureFactory.CreateDropShadow(optionWrapper);
		}

		#region Listview item creation & display
		private ListViewItem CreateListviewItem(INetStormWorker<ISpoofer> worker)
		{
			var spoofer = worker.Module;
			var updateItems = new[]
			{
				FeatureFactory.GetAutoUpdateField("ARP".ToString),
				FeatureFactory.GetAutoUpdateField(spoofer, () => spoofer.Target1.GetPreferredStack(IpStackMode.IPv4).ToString(), nameof(spoofer.Target1)),
				FeatureFactory.GetAutoUpdateField(spoofer, () => spoofer.Target1.Mac.ToCannonicalString(), nameof(spoofer.Target1)),
				FeatureFactory.GetAutoUpdateField(spoofer, () =>
				{
					var res = "";
					res += (int)(spoofer.SpoofDirection & SpoofDirection.Target1) == (int)SpoofDirection.Target1 ? "←" : "";
					res += spoofer.SpoofDirection == SpoofDirection.Both ? " | " : "";
					res += (int)(spoofer.SpoofDirection & SpoofDirection.Target2) == (int)SpoofDirection.Target2 ? "→" : "";
					return res;
				} , nameof(spoofer.SpoofDirection)),
				FeatureFactory.GetAutoUpdateField(spoofer, () => spoofer.Target2.GetPreferredStack(IpStackMode.IPv4).ToString(), nameof(spoofer.Target2)),
				FeatureFactory.GetAutoUpdateField(spoofer, () => spoofer.Target2.Mac.ToCannonicalString(), nameof(spoofer.Target2))
			};
			var item = new AutoUpdateListviewItem<ISpoofer>(spoofer)
			{
				Group = deviceView.Groups[0],
				Tag = worker
			};
			foreach (var ui in updateItems)
			{
				item.AddField(ui);
			}

			return item;
		}

		private void Modules_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
		{
			if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Add)
				foreach (INetStormWorker<ISpoofer> d in e.NewItems)
					DisplayListviewItem(CreateListviewItem(d));
			else if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Remove)
				foreach (INetStormWorker<ISpoofer> d in e.OldItems)
					DeleteListviewItem(d);
		}

		private void DisplayListviewItem(ListViewItem item)
		{
			if (deviceView.InvokeRequired)
				deviceView.Invoke(new Action<ListViewItem>(item => deviceView.Items.Add(item)));
			else
				deviceView.Items.Add(item);
		}

		private void DeleteListviewItem(INetStormWorker<ISpoofer> spoofer)
		{
			var removeFromList = new Action(() =>
			{
				foreach (AutoUpdateListviewItem<ISpoofer> lv in deviceView.Items)
				{
					if (lv.Object.Identifier == spoofer.Module.Identifier)
					{
						deviceView.Items.Remove(lv);
						break;
					}
				}
			});
			if (deviceView.InvokeRequired)
				deviceView.Invoke(removeFromList);
			else
				removeFromList();
		}
		#endregion

		#region Spoofer creation / deletion
		private async Task<bool> StartSpoofer(string text)
		{
			var successfull = false;
			if (NicManager.Instance.PrimaryDeviceIndex >= 0)
			{
				var device = await Task.Run(() => DeviceInitialiser.Initialise(text));
				var intf = NicManager.Instance.PrimaryDevice;
				if (device.NetworkInterface.Supports(IpStackMode.IPv4) &&
					device.NetworkInterface.Mac != PhysicalAddress.None &&
					intf.IPv4Stack.Network.Contains(device.NetworkInterface.IPv4Stack.IPAddress))
				{
					var s = CreateSpoofer($"spoofer {Guid.NewGuid()}", device.NetworkInterface, intf);
					s.SetNic(intf);

					var worker = new TimerWorker<ISpoofer>(s.Identifier, s) { CycleDurationSeconds = 3 };
					if ((successfull = worker.Start()))
						workers.Add(worker);
				}
			}
			return successfull;
		}

		private ISpoofer CreateSpoofer(string identifier, INetworkInterface target, ILocalNetworkInterface nic)
		{
			var s = new CoreNG.Modules.Spoofer.ArpSpoofer(identifier)
			{
				Target1 = target,
				Target2 = nic.Gateway.NetworkInterface,
				Broadcast = broadcastToggle.Checked,
				SpoofedMac = tbUseLocalMac.Checked ? nic.Mac : PhysicalAddressExtension.GenerateRandom(),
				SpoofDirection = 0,
				Router = routingToggle.Checked ? WorkerFactory.GetNicRouter($"router for {identifier}") : null
			};
			s.SpoofDirection |= routerPoisoningToggle.Checked ? SpoofDirection.Target2 : 0;
			s.SpoofDirection |= clientPoisoningToggle.Checked ? SpoofDirection.Target1 : 0;
			return s;
		}

		private bool RemoveTarget(string ip)
		{
			foreach (AutoUpdateListviewItem<ISpoofer> lv in deviceView.Items)
				if (lv.Object.Target1.IPv4Stack.IPAddress.ToString() == ip)
					return RemoveSpoofer((INetStormWorker<ISpoofer>)lv.Tag);

			return false;
		}

		private bool RemoveSpoofer(INetStormWorker<ISpoofer> spooferWorker)
		{
			if (workers.Remove(spooferWorker))
			{
				spooferWorker.Stop();
				return true;
			}
			else return false;
		}

		private bool IsSpoofed(string ip)
			=> workers.Any(x => x.Module.Target1.IPv4Stack.IPAddress.ToString() == ip);
		#endregion

		private void DeviceView_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
		{
			if (e.IsSelected && e.Item is AutoUpdateListviewItem<ISpoofer> lv)
			{
				selectedSpoofer = null;
				DisplaySpooferSettings(((INetStormWorker<ISpoofer>)lv.Tag).Module);
				selectedSpoofer = (INetStormWorker<ISpoofer>)lv.Tag;
				actionButton.Text = "Pardon";
				searchBox.Text = selectedSpoofer.Module.Target1.IPv4Stack.IPAddress.ToString();
			}
		}

		private SpoofDirection GetSpoofDirectionFromOptions()
		{
			SpoofDirection sd = 0;
			sd |= clientPoisoningToggle.Checked ? SpoofDirection.Target1 : 0;
			sd |= routerPoisoningToggle.Checked ? SpoofDirection.Target2 : 0;
			return sd;
		}

		private void ApplySpooferSettings(ISpoofer spoofer)
		{
			if(spoofer != null)
			{
				if (spoofer is IArpSpoofer arpS)
				{
					arpS.SpoofedMac = tbUseLocalMac.Checked ? spoofer.LocalNetworkInterface.Mac : PhysicalAddressExtension.GenerateRandom();
					arpS.Broadcast = broadcastToggle.Checked;
				}
				spoofer.SpoofDirection = GetSpoofDirectionFromOptions();
				if (routingToggle.Checked && spoofer.Router == null)
					spoofer.Router = WorkerFactory.GetNicRouter($"router for {spoofer.Identifier}");
				else if (!routingToggle.Checked)
					spoofer.Router = null;
			}
		}

		private void DisplaySpooferSettings(ISpoofer spoofer)
		{
			routerPoisoningToggle.Checked = (int)(spoofer.SpoofDirection & SpoofDirection.Target2) == (int)SpoofDirection.Target2;
			clientPoisoningToggle.Checked = (int)(spoofer.SpoofDirection & SpoofDirection.Target1) == (int)SpoofDirection.Target1;
			routingToggle.Checked = spoofer.Router != null;
			if (spoofer is IArpSpoofer asp)
			{
				broadcastToggle.Enabled = tbUseLocalMac.Enabled = true;
				broadcastToggle.Checked = asp.Broadcast;
				tbUseLocalMac.Checked = asp.SpoofedMac.Equals(asp.LocalNetworkInterface.Mac);
			}
			else broadcastToggle.Enabled = tbUseLocalMac.Enabled = false;
		}

		private async void ActionButton_Click(object sender, EventArgs e)
		{
			if (NicManager.Instance.PrimaryDevice != null && searchBox.Text != "")
			{
				if (RemoveTarget(searchBox.Text))
					actionButton.Text = "Spoof";
				else
				{
					actionButton.Enabled = false;
					if (await StartSpoofer(searchBox.Text))
						searchBox.Text = "";
					actionButton.Enabled = true;
				}
			}
		}

		private void SearchBox_TextChanged(object sender, EventArgs e)
			=> actionButton.Text = IsSpoofed(searchBox.Text) ? "Pardon" : "Spoof";

		private void HookEvents()
		{
			actionButton.Click += ActionButton_Click;
			deviceView.ItemSelectionChanged += DeviceView_ItemSelectionChanged;
			workers.CollectionChanged += Modules_CollectionChanged;
			searchBox.TextChanged += SearchBox_TextChanged;
			searchBox.GotFocus += (_, __) => selectedSpoofer = null;

			var verifySpoofDirectionToggle = new Action<ToggleButton>(tb =>
			{
				if (clientPoisoningToggle.Checked == routerPoisoningToggle.Checked && clientPoisoningToggle.Checked == false)
					tb.Checked = true;
			});
			routerPoisoningToggle.CheckedChanged += (_, __) => verifySpoofDirectionToggle(clientPoisoningToggle);
			clientPoisoningToggle.CheckedChanged += (_, __) => verifySpoofDirectionToggle(routerPoisoningToggle);

			routerPoisoningToggle.CheckedChanged += (_, __) => ApplySpooferSettings(selectedSpoofer?.Module);
			clientPoisoningToggle.CheckedChanged += (_, __) => ApplySpooferSettings(selectedSpoofer?.Module);
			tbUseLocalMac.CheckedChanged += (_, __) => ApplySpooferSettings(selectedSpoofer?.Module);
			broadcastToggle.CheckedChanged += (_, __) => ApplySpooferSettings(selectedSpoofer?.Module);
			routingToggle.CheckedChanged += (_, __) => ApplySpooferSettings(selectedSpoofer?.Module);
		}

		public void SetStyling(ColorScheme colorScheme)
		{
			ColorizeRecursevly(panel2, colorScheme.PrimaryColors, false);
			ColorizeRecursevly(optionPanel, colorScheme.PrimaryColors, true);

			dropShadow.ColorEnd = colorScheme.SecondaryColors.BackColor;

			dropShadow.ColorBegin = ControlPaint.Dark(dropShadow.ColorEnd, 0.01f);
			dropShadow.ShadowSize = 8;
			actionButton.Radius = new CornerRadius(5);
			actionButton.BorderWidth = 1;

			actionButton.Text = "Spoof";
			searchBox.Radius = new CornerRadius(5);
			searchBox.MouseEffect = true;

			actionButton.Height = searchBox.Height;

			routerPoisoningToggle.Checked = true;
			clientPoisoningToggle.Checked = true;
			broadcastToggle.Checked = true;
			tbUseLocalMac.Checked = true;

			foreach (Control c in panel2.Controls)
			{
				c.Margin = new Padding(0, (panel2.Height - c.Height) / 2, 15, 0);
			}
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			dropShadow.AddControl(optionPanel);
			HookEvents();
		}

		private void ColorizeRecursevly(Control parent, IColorTable colorTable, bool includeParent)
		{
			if (includeParent)
			{
				if (parent is INetStormControl isc)
					isc.ColorTable = colorTable;
				else
				{
					parent.BackColor = colorTable.BackColor;
					parent.ForeColor = colorTable.ForeColor;
				}
			}

			foreach (Control c in parent.Controls)
			{
				ColorizeRecursevly(c, colorTable, true);
			}
		}
	}
}
