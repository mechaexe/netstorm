﻿namespace NetStorm.GUI.IntrusionDetection
{
	partial class ArpDetector
	{
		/// <summary> 
		/// Erforderliche Designervariable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Verwendete Ressourcen bereinigen.
		/// </summary>
		/// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Vom Komponenten-Designer generierter Code

		/// <summary> 
		/// Erforderliche Methode für die Designerunterstützung. 
		/// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
		/// </summary>
		private void InitializeComponent()
		{
			System.Windows.Forms.ListViewGroup listViewGroup1 = new System.Windows.Forms.ListViewGroup("", System.Windows.Forms.HorizontalAlignment.Left);
			System.Windows.Forms.ListViewGroup listViewGroup2 = new System.Windows.Forms.ListViewGroup("", System.Windows.Forms.HorizontalAlignment.Left);
			System.Windows.Forms.ListViewGroup listViewGroup3 = new System.Windows.Forms.ListViewGroup("", System.Windows.Forms.HorizontalAlignment.Left);
			System.Windows.Forms.ListViewGroup listViewGroup4 = new System.Windows.Forms.ListViewGroup("", System.Windows.Forms.HorizontalAlignment.Left);
			this.panel2 = new System.Windows.Forms.FlowLayoutPanel();
			this.searchBox = new NetStorm.UILib.Controls.NSTextBox();
			this.actionButton = new NetStorm.UILib.Controls.Button();
			this.optionWrapper = new System.Windows.Forms.Panel();
			this.optionPanel = new System.Windows.Forms.FlowLayoutPanel();
			this.optionHeader = new System.Windows.Forms.Label();
			this.defensesEnabled = new NetStorm.UILib.Controls.ToggleButton();
			this.deviceView = new NetStorm.UILib.Controls.ListViewEx();
			this.ipAddress = new System.Windows.Forms.ColumnHeader();
			this.macAddress = new System.Windows.Forms.ColumnHeader();
			this.manufacturer = new System.Windows.Forms.ColumnHeader();
			this.target = new System.Windows.Forms.ColumnHeader();
			this.panel2.SuspendLayout();
			this.optionWrapper.SuspendLayout();
			this.optionPanel.SuspendLayout();
			this.SuspendLayout();
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.searchBox);
			this.panel2.Controls.Add(this.actionButton);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel2.Location = new System.Drawing.Point(0, 0);
			this.panel2.Margin = new System.Windows.Forms.Padding(0);
			this.panel2.Name = "panel2";
			this.panel2.Padding = new System.Windows.Forms.Padding(320, 0, 0, 0);
			this.panel2.Size = new System.Drawing.Size(785, 70);
			this.panel2.TabIndex = 20;
			// 
			// searchBox
			// 
			this.searchBox.CueText = "Search";
			this.searchBox.Location = new System.Drawing.Point(324, 20);
			this.searchBox.Margin = new System.Windows.Forms.Padding(4, 20, 4, 3);
			this.searchBox.Name = "searchBox";
			this.searchBox.Size = new System.Drawing.Size(300, 28);
			this.searchBox.TabIndex = 18;
			this.searchBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// actionButton
			// 
			this.actionButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
			this.actionButton.BorderWidth = 1F;
			this.actionButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(239)))), ((int)(((byte)(241)))));
			this.actionButton.Location = new System.Drawing.Point(632, 20);
			this.actionButton.Margin = new System.Windows.Forms.Padding(4, 20, 4, 3);
			this.actionButton.Name = "actionButton";
			this.actionButton.Padding = new System.Windows.Forms.Padding(7);
			this.actionButton.Size = new System.Drawing.Size(80, 28);
			this.actionButton.TabIndex = 17;
			this.actionButton.Text = "Start/Stop";
			this.actionButton.TextAlignment = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// optionWrapper
			// 
			this.optionWrapper.Controls.Add(this.optionPanel);
			this.optionWrapper.Dock = System.Windows.Forms.DockStyle.Right;
			this.optionWrapper.Location = new System.Drawing.Point(785, 0);
			this.optionWrapper.Margin = new System.Windows.Forms.Padding(0);
			this.optionWrapper.Name = "optionWrapper";
			this.optionWrapper.Padding = new System.Windows.Forms.Padding(12, 0, 0, 0);
			this.optionWrapper.Size = new System.Drawing.Size(215, 500);
			this.optionWrapper.TabIndex = 23;
			// 
			// optionPanel
			// 
			this.optionPanel.BackColor = System.Drawing.Color.White;
			this.optionPanel.Controls.Add(this.optionHeader);
			this.optionPanel.Controls.Add(this.defensesEnabled);
			this.optionPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.optionPanel.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
			this.optionPanel.Location = new System.Drawing.Point(12, 0);
			this.optionPanel.Margin = new System.Windows.Forms.Padding(9, 0, 0, 0);
			this.optionPanel.Name = "optionPanel";
			this.optionPanel.Size = new System.Drawing.Size(203, 500);
			this.optionPanel.TabIndex = 2;
			// 
			// optionHeader
			// 
			this.optionHeader.AutoSize = true;
			this.optionHeader.Location = new System.Drawing.Point(6, 6);
			this.optionHeader.Margin = new System.Windows.Forms.Padding(6, 6, 6, 0);
			this.optionHeader.Name = "optionHeader";
			this.optionHeader.Size = new System.Drawing.Size(49, 15);
			this.optionHeader.TabIndex = 0;
			this.optionHeader.Text = "Options";
			// 
			// defensesEnabled
			// 
			this.defensesEnabled.Checked = false;
			this.defensesEnabled.Location = new System.Drawing.Point(12, 27);
			this.defensesEnabled.Margin = new System.Windows.Forms.Padding(12, 6, 0, 0);
			this.defensesEnabled.Name = "defensesEnabled";
			this.defensesEnabled.Size = new System.Drawing.Size(190, 25);
			this.defensesEnabled.TabIndex = 1;
			this.defensesEnabled.Text = "Enable Defenses";
			// 
			// deviceView
			// 
			this.deviceView.AutoSizeLastColumn = true;
			this.deviceView.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.deviceView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.ipAddress,
            this.macAddress,
            this.manufacturer,
            this.target});
			this.deviceView.Dock = System.Windows.Forms.DockStyle.Fill;
			this.deviceView.FullRowSelect = true;
			listViewGroup1.Header = "";
			listViewGroup1.Name = "spoofer";
			listViewGroup2.Header = "";
			listViewGroup2.Name = "validated";
			listViewGroup3.Header = "";
			listViewGroup3.Name = "notValidated";
			listViewGroup4.Header = "";
			listViewGroup4.Name = "validationPending";
			this.deviceView.Groups.AddRange(new System.Windows.Forms.ListViewGroup[] {
            listViewGroup1,
            listViewGroup2,
            listViewGroup3,
            listViewGroup4});
			this.deviceView.HideSelection = false;
			this.deviceView.Location = new System.Drawing.Point(0, 69);
			this.deviceView.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
			this.deviceView.MultiSelect = false;
			this.deviceView.Name = "deviceView";
			this.deviceView.Size = new System.Drawing.Size(785, 431);
			this.deviceView.TabIndex = 24;
			this.deviceView.UseCompatibleStateImageBehavior = false;
			this.deviceView.View = System.Windows.Forms.View.Details;
			// 
			// ipAddress
			// 
			this.ipAddress.Text = "IP Address";
			this.ipAddress.Width = 100;
			// 
			// macAddress
			// 
			this.macAddress.Text = "MAC Address";
			this.macAddress.Width = 150;
			// 
			// manufacturer
			// 
			this.manufacturer.Text = "Manufacturer";
			this.manufacturer.Width = 300;
			// 
			// target
			// 
			this.target.Text = "Target";
			// 
			// ArpDetector
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.deviceView);
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.optionWrapper);
			this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
			this.Name = "ArpDetector";
			this.Size = new System.Drawing.Size(1000, 500);
			this.panel2.ResumeLayout(false);
			this.panel2.PerformLayout();
			this.optionWrapper.ResumeLayout(false);
			this.optionPanel.ResumeLayout(false);
			this.optionPanel.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.FlowLayoutPanel panel2;
		private UILib.Controls.NSTextBox searchBox;
		private UILib.Controls.Button actionButton;
		private System.Windows.Forms.Panel optionWrapper;
		private System.Windows.Forms.FlowLayoutPanel optionPanel;
		private System.Windows.Forms.Label optionHeader;
		private UILib.Controls.ToggleButton defensesEnabled;
		private System.Windows.Forms.ColumnHeader ipAddress;
		private System.Windows.Forms.ColumnHeader macAddress;
		private System.Windows.Forms.ColumnHeader manufacturer;
		private UILib.Controls.ListViewEx deviceView;
		private System.Windows.Forms.ColumnHeader target;
	}
}
