﻿using NetStorm.Common.Devices;
using NetStorm.CoreNG;
using NetStorm.CoreNG.Modules.IntrusionDetection;
using NetStorm.UILib;
using NetStorm.UILib.Coloring;
using NetStorm.UILib.Controls;
using NetStorm.UILib.Features;
using System;
using System.Drawing;
using System.Net.NetworkInformation;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NetStorm.GUI.IntrusionDetection
{
	public partial class ArpDetector : UserControl, INetStormPage
	{
		public delegate void SetActivePage(int index);

		private readonly IDropShadow dropShadow;
		private readonly IArpDetector arpDetector;
		private readonly SetInfoText setInfoText;

		public ArpDetector(IArpDetector arpDetector, SetInfoText setInfoText)
		{
			InitializeComponent();
			dropShadow = FeatureFactory.CreateDropShadow(optionWrapper);

			this.arpDetector = arpDetector;

			this.setInfoText = setInfoText;
		}

		private void ActionButton_Click(object sender, EventArgs e)
		{
			if (arpDetector.Running)
			{
				arpDetector.Abort();
			}
			else if (NicManager.Instance.Count > 0 && NicManager.Instance.PrimaryDeviceIndex >= 0)
			{
				arpDetector.SetNIC(NicManager.Instance.PrimaryDevice);
				var items = deviceView.Items;
				deviceView.Items.Clear();
				if (!arpDetector.Start())
				{
					deviceView.Items.AddRange(items);
				}
				else
				{
					setInfoText($"Validating network devices");
				}
			}
		}

		#region Device printing
		private void PrintDeviceToListview(string search, IArpEntry device, IArpEntry target, Color forecolor, Color backcolor, ListViewGroup group, int paintPriority)
		{
			var add = new Action(() =>
			{
				if (
					search == "" ||
					device.IPv4Stack.IPAddress.ToString().Contains(search) ||
					device.Mac.ToString().ToLower().Contains(search.Replace(":", ".").Replace(".", "-").Replace("-", "").ToLower()))
				{
					var item = new AutoUpdateListviewItem<IArpEntry>(device);
					item = UpdateListviewItem(item, device, target, forecolor, backcolor, group, paintPriority);
					lock (deviceView.Items)
					{
						deviceView.Items.Add(item);
					}
				}
			});
			var replace = new Action<AutoUpdateListviewItem<IArpEntry>>(x => x = UpdateListviewItem(x, device, target, forecolor, backcolor, group, paintPriority));
			OperateOnListView(device, replace, add, paintPriority);
		}

		private void OperateOnListView(IArpEntry device, Action<AutoUpdateListviewItem<IArpEntry>> itemExists, Action itemMissing, int actionPriority)
		{
			var func = new Action(() =>
			{
				var exists = false;
				foreach (AutoUpdateListviewItem<IArpEntry> lv in deviceView.Items)
					if (lv.Object.Mac.Equals(device.Mac))
					{
						if((int)lv.Tag <= actionPriority)
							itemExists?.Invoke(lv);
						exists = true;
						break;
					}
				if(!exists)
					itemMissing?.Invoke();
			});

			if (InvokeRequired)
				deviceView.Invoke(func);
			else
				func();
		}
		#endregion

		#region Device filtering
		private void FilterDevices(string text)
		{
			deviceView.Items.Clear();
			//foreach (var d in deviceCache)
			//{
			//	PrintDeviceToListview(text, d);
			//}
		}

		private async void SearchBox_TextChanged(object sender, EventArgs e)
		{
			var changed = false;
			var oldText = searchBox.Text;
			await Task.Run(() =>
			{
				Thread.Sleep(100);
				changed = oldText != searchBox.Text;
			});
			if (!changed)
			{
				FilterDevices(oldText);
			}
		}
		#endregion

		private AutoUpdateListviewItem<IArpEntry> UpdateListviewItem(
			AutoUpdateListviewItem<IArpEntry> item,
			IArpEntry host, 
			IArpEntry target, 
			Color forecolor, 
			Color backcolor, 
			ListViewGroup group, 
			int paintPriority)
		{
			var updateItems = new[]
			{
				FeatureFactory.GetAutoUpdateField(host, () => host.IPv4Stack.IPAddress.ToString(), nameof(host.IPv4Stack)),
				FeatureFactory.GetAutoUpdateField(host, () => host.Mac?.ToCannonicalString(), nameof(host.Mac)),
				FeatureFactory.GetAutoUpdateField(host, () => host.MacVendor.Manufacturer, nameof(host.MacVendor)),
				FeatureFactory.GetAutoUpdateField(() =>
				{
					if(target != null)
						return $"{target.IPv4Stack.IPAddress} ({target.Mac.ToCannonicalString()})";
					else return "";
				})
			};
			item.SubItems.Clear();
			item.RemoveFields();
			foreach (var ui in updateItems)
			{
				item.AddField(ui);
			}
			item.ForeColor = forecolor;
			item.BackColor = backcolor;
			item.Group = group;
			item.Tag = paintPriority;
			return item;
		}

		private void HookEvents()
		{
			actionButton.Click += ActionButton_Click;
			arpDetector.Started += (_, __) => SetActionButtonText("Stop");
			arpDetector.Stopped += (_, __) => SetActionButtonText("Start");
			arpDetector.HostDetected += (_, __) => PrintDeviceToListview(searchBox.Text, __.Argument, null, Colors.Light, Colors.Brown, deviceView.Groups[3], 0);
			arpDetector.ValidationFailed += (_, __) => PrintDeviceToListview(searchBox.Text, __.Argument, null, Colors.Light, Colors.Brown, deviceView.Groups[2], 1);
			arpDetector.ValidHostFound += (_, __) => PrintDeviceToListview(searchBox.Text, __.Argument, null, Colors.Light, Colors.Green, deviceView.Groups[1], 2);
			arpDetector.AttackDetected += (_, __) => PrintDeviceToListview(searchBox.Text, __.Attacker, __.Victim, Colors.Light, Colors.Red, deviceView.Groups[0], 2);
		}

		private void SetActionButtonText(string text)
		{
			if (InvokeRequired)
			{
				Invoke(new Action(() => actionButton.Text = text));
			}
			else
			{
				actionButton.Text = text;
			}
		}

		public void SetStyling(ColorScheme colorScheme)
		{
			ConfigureUI();
			ColorizeRecursevly(panel2, colorScheme.PrimaryColors, false);
			ColorizeRecursevly(optionPanel, colorScheme.PrimaryColors, true);

			dropShadow.ColorEnd = colorScheme.SecondaryColors.BackColor;
			dropShadow.ColorBegin = ControlPaint.Dark(dropShadow.ColorEnd, 0.01f);
		}

		private void ConfigureUI()
		{
			dropShadow.ShadowSize = 8;
			actionButton.Radius = new CornerRadius(5);
			actionButton.BorderWidth = 1;

			actionButton.Text = arpDetector.Running ? "Stop" : "Start";
			searchBox.CueText = "Search";
			searchBox.Radius = new CornerRadius(5);
			searchBox.MouseEffect = true;
			actionButton.Height = searchBox.Height;
			defensesEnabled.Enabled = false;

			foreach (Control c in panel2.Controls)
			{
				c.Margin = new Padding(0, (panel2.Height - c.Height) / 2, 15, 0);
			}
		}

		private void ColorizeRecursevly(Control parent, IColorTable colorTable, bool includeParent)
		{
			if (includeParent)
			{
				if (parent is INetStormControl isc)
					isc.ColorTable = colorTable;
				else
				{
					parent.BackColor = colorTable.BackColor;
					parent.ForeColor = colorTable.ForeColor;
				}
			}

			foreach (Control c in parent.Controls)
			{
				ColorizeRecursevly(c, colorTable, true);
			}
		}

		protected override void OnLoad(EventArgs e)
		{
			dropShadow.AddControl(optionPanel);
			HookEvents();
			base.OnLoad(e);
		}
	}
}
