﻿using NetStorm.UILib.Coloring;

namespace NetStorm.GUI
{
	interface INetStormPage
	{
		void Hide();
		void Show();
		void BringToFront();
		void SetStyling(ColorScheme colorScheme);
	}
}
