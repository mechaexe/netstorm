﻿namespace NetStorm.GUI
{
	partial class TabGUI
	{
		/// <summary> 
		/// Erforderliche Designervariable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Verwendete Ressourcen bereinigen.
		/// </summary>
		/// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Vom Komponenten-Designer generierter Code

		/// <summary> 
		/// Erforderliche Methode für die Designerunterstützung. 
		/// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
		/// </summary>
		private void InitializeComponent()
		{
			this.navigationPanel = new System.Windows.Forms.Panel();
			this.navigation = new NetStorm.UILib.Controls.MultiButton();
			this.navigationPanel.SuspendLayout();
			this.SuspendLayout();
			// 
			// navigationPanel
			// 
			this.navigationPanel.BackColor = System.Drawing.SystemColors.ControlLight;
			this.navigationPanel.Controls.Add(this.navigation);
			this.navigationPanel.Location = new System.Drawing.Point(0, 0);
			this.navigationPanel.Margin = new System.Windows.Forms.Padding(0);
			this.navigationPanel.Name = "navigationPanel";
			this.navigationPanel.Size = new System.Drawing.Size(319, 70);
			this.navigationPanel.TabIndex = 7;
			// 
			// navigation
			// 
			this.navigation.Location = new System.Drawing.Point(43, 10);
			this.navigation.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
			this.navigation.Name = "navigation";
			this.navigation.Padding = new System.Windows.Forms.Padding(0, 5, 0, 5);
			this.navigation.Radius = new UILib.Features.CornerRadius(5);
			this.navigation.SelectedIndex = -1;
			this.navigation.Size = new System.Drawing.Size(0, 0);
			this.navigation.TabIndex = 1;
			this.navigation.Text = "multiButton1";
			this.navigation.TextSpacing = 10;
			// 
			// TabGUI
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.navigationPanel);
			this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
			this.Name = "TabGUI";
			this.Size = new System.Drawing.Size(1340, 693);
			this.navigationPanel.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion
		private System.Windows.Forms.Panel navigationPanel;
		private UILib.Controls.MultiButton navigation;
	}
}
