﻿using System.Threading.Tasks;
using System.Timers;
using NetStorm.CoreNG.Modules;

namespace NetStorm.CoreNG.Worker
{
	public class TimerWorker<T> : NetStormBaseWorker<T> where T : IModule
	{
		private Timer timer;
		private uint cycles;

		public TimerWorker(string name) : base(name)
		{
		}

		public TimerWorker(string name, T module) : base(name, module)
		{
		}

		~TimerWorker() => timer?.Dispose();

		private void DoWork()
		{
			// Beware of multithreading, everything can be set to NULL anytime
			if (timer != null &&
				Module != null &&
				cycles <= Cycles &&
				Module.IsReady)
			{
				Module?.DoWork();
				if (Cycles > 0)
				{
					cycles++;
				}

				timer?.Start();
			}
			else
				Abort();
		}

		public override bool Start()
		{
			if (!Running && Module != null && Module.Prepare())
			{
				cycles = 0;
				timer = new Timer
				{
					Interval = CycleDurationSeconds * 1000,
					AutoReset = false
				};
				timer.Elapsed += (_, __) => DoWork();
				Task.Run(DoWork);
				return base.Start();
			}
			else return false;
		}

		public override void Abort()
		{
			if (Running && timer != null)
			{
				timer.Stop();
				timer.Dispose();
				timer = null;
			}
			base.Abort();
		}
	}

	public class TimerWorker : TimerWorker<IModule>
	{
		public TimerWorker(string name) : base(name)
		{
		}

		public TimerWorker(string name, IModule module) : base(name, module)
		{
		}
	}
}
