﻿using NetStorm.CoreNG.Modules;
using System;

namespace NetStorm.CoreNG.Worker
{
	public interface IWorkerNG
	{
		bool Running { get; }
		double Interval { get; set; }
		event EventHandler<StateArgs> WorkerStateChanged;
		void Register(BackgroundWorkerDelegate workerDelegate);
		void Revoke(BackgroundWorkerDelegate workerDelegate);
	}

	public delegate void BackgroundWorkerDelegate(bool autoExit);
}
