﻿using NetStorm.CoreNG.Modules;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Timers;

namespace NetStorm.CoreNG.Worker
{
	public class TimerWorkerNg : IWorkerNG
	{
		public bool Running
		{
			get => _running;
			private set
			{
				if (_running != value)
				{
					_running = value;
					WorkerStateChanged?.Invoke(this, new StateArgs(_running));
				}
			}
		}

		public double Interval { get => _timer.Interval; set => _timer.Interval = value; }

		public event EventHandler<StateArgs> WorkerStateChanged;
		private bool _running;
		private readonly Timer _timer;
		private readonly List<BackgroundWorkerDelegate> _workerDelegates;
		private readonly object _workerDelegatesLock;

		public TimerWorkerNg()
		{
			_timer = new Timer
			{
				AutoReset = true,
				Interval = 30000
			};
			_workerDelegates = new List<BackgroundWorkerDelegate>();
			_workerDelegatesLock = new object();

			_timer.Elapsed += Timer_Elapsed;
		}

		private void Start()
		{
			_timer.Enabled = Running = true;
		}

		private void Stop()
		{
			_timer.Enabled = Running = false;
		}

		public void Register(BackgroundWorkerDelegate workerDelegate)
		{
			lock (_workerDelegatesLock)
				_workerDelegates.Add(workerDelegate);
			Task.Run(() => workerDelegate(false));
			if (!_running)
				Start();
		}

		public void Revoke(BackgroundWorkerDelegate workerDelegate)
		{
			lock (_workerDelegatesLock)
				_workerDelegates.Remove(workerDelegate);
			if (_workerDelegates.Count == 0)
				Stop();
		}

		private void Timer_Elapsed(object sender, ElapsedEventArgs e)
		{
			_timer.Enabled = false;
			lock(_workerDelegatesLock)
				_workerDelegates.ForEach(x => x(false));
			if (_workerDelegates.Count != 0)
				_timer.Enabled = true;
			else Stop();
		}
	}
}
