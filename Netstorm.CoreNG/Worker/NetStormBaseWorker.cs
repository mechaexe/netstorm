﻿using NetStorm.CoreNG.Modules;

namespace NetStorm.CoreNG.Worker
{
	public abstract class NetStormBaseWorker<T> : Common.Worker.BaseWorker, INetStormWorker<T> where T : IModule
	{
		public override string Name { get; }
		IModule INetStormWorker.Module { get => Module; set => Module = (T)value; }
		public T Module
		{
			get => module;
			set
			{
				if (!Running && (module == null || !module.Equals(value)))
				{
					module = value;
				}
			}
		}

		public uint CycleDurationSeconds { get; set; }
		public uint Cycles { get; set; }

		private T module;

		public NetStormBaseWorker(string name)
		{
			Name = name;
		}

		public NetStormBaseWorker(string name, T module) : this(name)
		{
			Module = module;
		}

		protected void OnExit()
		{
			if (Running)
			{				
				Module.Cleanup();
				Running = false;
			}
		}

		public override bool Start()
			=> Running = !Running && Module != null;

		public override void Abort()
			=> OnExit();

		public override void WaitForExit()
			=> Module.GetCancellationToken().WaitForExit();
	}
}
