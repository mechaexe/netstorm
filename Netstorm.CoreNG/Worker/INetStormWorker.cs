﻿using NetStorm.Common.Worker;
using NetStorm.CoreNG.Modules;

namespace NetStorm.CoreNG.Worker
{
	public interface INetStormWorker : IWorker
	{
		uint CycleDurationSeconds { get; set; }
		uint Cycles { get; set; }
		IModule Module { get; set; }
	}

	public interface INetStormWorker<T> : INetStormWorker where T : IModule
	{
		new T Module { get; set; }
	}
}