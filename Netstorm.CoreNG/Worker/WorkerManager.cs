﻿using NetStorm.CoreNG.Modules;
using System;
using System.Collections.Generic;
using System.Text;

namespace NetStorm.CoreNG.Worker
{
	class WorkerManager
	{
		public static WorkerManager Global { get; } = new WorkerManager();

		private WorkerManager() { }

		public void Register(IWorkerNG worker) { }
		public void StopAll() { }
	}
}
