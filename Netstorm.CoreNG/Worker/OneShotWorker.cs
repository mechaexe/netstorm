﻿using NetStorm.CoreNG.Modules;
using System;
using System.Threading;

namespace NetStorm.CoreNG.Worker
{
	public class OneShotWorker : IWorkerNG
	{
		public bool Running => _modules > 0;

		public double Interval { get; set; }

		public event EventHandler<StateArgs> WorkerStateChanged;

		private uint _modules;

		public void Register(BackgroundWorkerDelegate workerDelegate)
		{
			SetActiveModules(GetActiveModules() + 1);
			ThreadPool.QueueUserWorkItem(x => 
			{
				workerDelegate(true);
				SetActiveModules(GetActiveModules() - 1);
			});
		}

		private uint GetActiveModules()
		{
			return _modules;
		}

		private void SetActiveModules(uint value)
		{
			if (value == 1 || value == 0)
			{
				_modules = value;
				WorkerStateChanged?.Invoke(this, new StateArgs(Running));
			}
		}

		public void Revoke(BackgroundWorkerDelegate workerDelegate) { }
	}
}
