﻿namespace NetStorm.CoreNG
{
	public enum Subsystem
	{
		Posix = 0,
		Windows = 1
	}

	public enum PrivilegeLevel
	{
		Root = 0,
		User = 1
	}
}
