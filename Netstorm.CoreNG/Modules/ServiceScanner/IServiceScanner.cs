﻿using NetStorm.Common.Services;
using NetStorm.CoreNG.Modules.ServiceScanner.Bannergrabber;
using NetStorm.CoreNG.Modules.ServiceScanner.ServiceDetector;
using System.Collections.Generic;
using System.Net;

namespace NetStorm.CoreNG.Modules.ServiceScanner
{
	public interface IServiceScanner
	{
		List<IBannergrabber> Bannergrabbers { get; }
		List<IServiceDetector> ServiceDetectors { get; }

		IService ScanPort(IPEndPoint localEndpoint, IPEndPoint remoteEndPoint, IService service = null);
	}
}