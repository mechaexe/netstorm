﻿using NetStorm.Common.Services;
using NetStorm.CoreNG.Modules.ServiceScanner.Bannergrabber;
using NetStorm.CoreNG.Modules.ServiceScanner.ServiceDetector;
using System.Collections.Generic;
using System.Net;

namespace NetStorm.CoreNG.Modules.ServiceScanner
{
	public class GenericServiceScanner : IServiceScanner
	{
		public List<IBannergrabber> Bannergrabbers { get; }
		public List<IServiceDetector> ServiceDetectors { get; }

		public GenericServiceScanner()
		{
			Bannergrabbers = new List<IBannergrabber>();
			ServiceDetectors = new List<IServiceDetector>()
			{
				new GenericServiceDetector()
			};
		}

		public IService ScanPort(IPEndPoint localEndpoint, IPEndPoint remoteEndPoint, IService service = null)
		{
			var res = service;
			foreach (var bgrabber in Bannergrabbers.ToArray())
			{
				var banner = bgrabber.GrabBanner(localEndpoint, remoteEndPoint);
				if (banner != null)
					foreach (var detector in ServiceDetectors.ToArray())
						res = detector.DetectService(banner, res);
			}
			return res;
		}
	}
}
