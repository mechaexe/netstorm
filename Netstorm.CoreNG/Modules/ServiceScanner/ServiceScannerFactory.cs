﻿using NetStorm.CoreNG.Modules.ServiceScanner.Bannergrabber;
using NetStorm.CoreNG.Modules.ServiceScanner.ServiceDetector;

namespace NetStorm.CoreNG.Modules.ServiceScanner
{
	public static class ServiceScannerFactory
	{
		public static IServiceScanner GetFullServiceScanner()
		{
			var serviceScanner = new GenericServiceScanner();
			serviceScanner.Bannergrabbers.AddRange(new IBannergrabber[]
			{
					new TcpBannergrabber(),
					new WebBannergrabber()
			});
			serviceScanner.ServiceDetectors.AddRange(new IServiceDetector[]
			{
					new HttpServiceDetector(),
					new SshServiceDetector(),
					new UPnPServiceDetector(),
					new SmtpServiceDetector()
			});
			return serviceScanner;
		}
	}
}
