﻿using NetStorm.Common.Services;

namespace NetStorm.CoreNG.Modules.ServiceScanner.ServiceDetector
{
	class SmtpServiceDetector : IServiceDetector
	{
		public IService DetectService(string banner, IService baseService)
		{
			if(banner.StartsWith("220") && banner.Contains("SMTP"))
			{
				var svc = new GenericService()
				{
					Service = new Application("SMTP")
				};
				svc.Banners.Add(banner);
				return svc;
			}
			return baseService;
		}
	}
}
