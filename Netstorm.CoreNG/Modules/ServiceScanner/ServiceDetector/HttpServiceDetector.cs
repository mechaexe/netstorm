﻿using NetStorm.Common.Services;
using Protocols.HTTP;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace NetStorm.CoreNG.Modules.ServiceScanner.ServiceDetector
{
	public class HttpServiceDetector : IServiceDetector
	{
		public static HashSet<string> KnownHeaders { get; } = new HashSet<string>(new[]
		{
			"server",
			"www-authenticate",
			"location"
		});
		public static List<Regex> KnownContentFields { get; } = new List<Regex>(new[]
		{
			new Regex("<title>(.*)</title>", RegexOptions.IgnoreCase | RegexOptions.Multiline),
			new Regex("<meta name=\"description\" content=\"(.*)\"", RegexOptions.IgnoreCase | RegexOptions.Multiline)
		});
		public static List<char> HeaderApplicationSeparators { get; } = new List<char>(new[]
		{
			',',
			' '
		});

		public IEnumerable<string> GrabBannerFromResponse(HttpResponse website)
		{
			//Process HTTP-Headers
			foreach (var b in website.Headers)
			{
				if (KnownHeaders.Contains(b.Key))
					yield return b.Value;
			}

			//Process Content
			var escaped = Regex.Escape(website.Content);
			foreach (var regex in KnownContentFields)
			{
				var matches = regex.Match(escaped);
				if (matches.Success && matches.Groups.Count >= 2)
					yield return Regex.Unescape(matches.Groups[1].Value);
			}
		}

		public virtual IService DetectService(string banner, IService baseService)
		{
			var parsed = HttpResponse.Parse(banner);
			if(parsed != null)
			{
				var service = new BasicApplicationService()
				{
					Service = new Application("HTTP", parsed.HttpVersion.ToString())
				};
				if (parsed.Headers.ContainsKey("server"))
					service.Applications.AddRange(GetApplications(parsed.Headers["server"]));

				var matches = KnownContentFields[1].Match(Regex.Escape(parsed.Content));
				if (matches.Success && matches.Groups.Count >= 2)
					service.AdditionalInformation = Regex.Unescape(matches.Groups[1].Value);

				foreach (var webBanner in GrabBannerFromResponse(parsed))
					service.Banners.Add(webBanner);
				return service;
			}
			return baseService;
		}

		private List<Application> GetApplications(string header)
		{
			var res = new List<Application>();
			var normalised = header.Replace("(", "").Replace(")", "").Trim();
			var split = new string[0];

			foreach(var c in HeaderApplicationSeparators)
			{
				split = normalised.Split(c);
				if (split.Length > 1)
					break;
			}

			foreach (var s in split)
			{
				var appSplit = s.Trim().Split(new char[] { '/' });
				if (appSplit.Length >= 3)
					res.Add(new Application(appSplit[0], appSplit[1], string.Join(" ", appSplit[2..])));
				else if (appSplit.Length == 2)
					res.Add(new Application(appSplit[0], appSplit[1]));
				else
					res.Add(new Application(appSplit[0]));
			}

			return res;
		}
	}
}
