﻿using NetStorm.Common.Services;
using Protocols.HTTP;
using System;

namespace NetStorm.CoreNG.Modules.ServiceScanner.ServiceDetector
{
	public class UPnPServiceDetector : IServiceDetector
	{
		public IService DetectService(string banner, IService baseService)
		{
			var parsed = HttpResponse.Parse(banner);
			if (parsed != null && parsed.Headers.ContainsKey("st") && parsed.Headers.ContainsKey("usn") && parsed.Headers.ContainsKey("location"))
			{
				baseService = new HttpServiceDetector().DetectService(banner, baseService);
				baseService = new UPnPService((BasicApplicationService)baseService)
				{
					Service = new Application("HTTP/UPnP")
				};
				(baseService as UPnPService).Services.TryAdd(new Uri(parsed.Headers["location"]), null);
			}
			return baseService;
		}
	}
}
