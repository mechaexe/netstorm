﻿using NetStorm.Common.Services;

namespace NetStorm.CoreNG.Modules.ServiceScanner.ServiceDetector
{
	public interface IServiceDetector
	{
		IService DetectService(string banner, IService baseService);
	}
}
