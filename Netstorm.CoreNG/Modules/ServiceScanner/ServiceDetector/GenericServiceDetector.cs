﻿using NetStorm.Common.Services;

namespace NetStorm.CoreNG.Modules.ServiceScanner.ServiceDetector
{
	class GenericServiceDetector : IServiceDetector
	{
		public IService DetectService(string banner, IService baseService)
		{
			var result = baseService ?? new GenericService();
			result.Banners.Add(banner);
			return result;
		}
	}
}
