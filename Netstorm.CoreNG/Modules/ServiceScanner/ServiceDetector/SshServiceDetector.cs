﻿using NetStorm.Common.Services;
using System;

namespace NetStorm.CoreNG.Modules.ServiceScanner.ServiceDetector
{
	public class SshServiceDetector : IServiceDetector
	{
		/// <summary>
		/// Processing of SSH banner as defined in rfc 4253
		/// SSH-protoversion-softwareversion SP comments CR LF
		/// </summary>
		/// <param name="banners"></param>
		/// <param name="baseService"></param>
		/// <returns></returns>
		public IService DetectService(string banner, IService baseService)
		{
			if (banner.StartsWith("SSH-"))
			{
				var identificators = banner.Split((char)32);

				// Index 0 contains SSH-protoversion-softwareversion_SP
				var splitted = identificators[0].RemoveEmptyLines().Trim().Split('-');
				if (splitted.Length >= 3)
				{
					const char applicationSeperator = '_';
					var service = new BasicApplicationService()
					{
						Service = new Application("SSH", splitted[1])
					};
					service.Applications.Add(new Application(splitted[2].ReadUntil(0, applicationSeperator), splitted[2].Substring(splitted[2].IndexOf(applicationSeperator) + 1)));

					if (identificators.Length >= 2)
						service.AdditionalInformation += string.Join(" ", identificators[1..]);

					service.Banners.Add(banner);

					return service;
				}
			}
			return baseService;
		}
	}
}
