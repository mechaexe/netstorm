﻿using Protocols.HTTP;
using System;
using System.Net;

namespace NetStorm.CoreNG.Modules.ServiceScanner.Bannergrabber
{
	public class WebBannergrabber : IBannergrabber
	{
		public string GrabBanner(IPEndPoint localEndpoint, IPEndPoint remoteEndpoint)
			=> GrabBanner(localEndpoint, new Uri($"http{ (remoteEndpoint.Port == 443 ? "s" : "") }://{remoteEndpoint}"), remoteEndpoint.Port == 443);

		public string GrabBanner(IPEndPoint localEndpoint, Uri remoteUri, bool ssl)
			=> HttpClient.Get(localEndpoint, remoteUri)?.ToString();
	}
}
