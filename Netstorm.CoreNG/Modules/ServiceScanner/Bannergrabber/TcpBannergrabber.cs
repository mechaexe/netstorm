﻿using NetStorm.Common.Extensions;
using Protocols.TCP;
using System;
using System.Net;

namespace NetStorm.CoreNG.Modules.ServiceScanner.Bannergrabber
{
	public class TcpBannergrabber : IBannergrabber
	{
		public string GrabBanner(IPEndPoint localEndpoint, Uri remoteUri, bool ssl)
		{
			var requester = new TcpRequester();
			var bytes = requester.GetResponse(localEndpoint, remoteUri, ssl, null);
			if (bytes != null)
				return ToReadableChars(bytes).RemoveEmptyLines().Trim();
			else return null;
		}

		public string GrabBanner(IPEndPoint localEndpoint, IPEndPoint remoteEndpoint)
			=> GrabBanner(localEndpoint, new Uri($"tcp://{remoteEndpoint}"), false);

		private string ToReadableChars(byte[] data)
		{
			string res = "";
			foreach (char b in data)
				if(b.IsPrintable())
					res += b;
			return res;
 		}
	}
}
