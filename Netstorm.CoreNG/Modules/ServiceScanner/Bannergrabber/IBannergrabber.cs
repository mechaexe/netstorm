﻿using System;
using System.Net;

namespace NetStorm.CoreNG.Modules.ServiceScanner.Bannergrabber
{
	public interface IBannergrabber
	{
		string GrabBanner(IPEndPoint localEndpoint, IPEndPoint remoteEndpoint);
		string GrabBanner(IPEndPoint localEndpoint, Uri remoteUri, bool ssl);
	}
}
