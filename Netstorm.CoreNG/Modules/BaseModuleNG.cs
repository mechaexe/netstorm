﻿using NetStorm.Common.Devices;
using NetStorm.Common.Worker.PacketReceiver;
using NetStorm.CoreNG.Worker;
using PacketDotNet;
using System;
using System.Threading;

namespace NetStorm.CoreNG.Modules
{
	public abstract class BaseModuleNG
	{
		public string Identifier { get; }

		public bool Running
		{
			get => _running;
			private set
			{
				if (_running != value)
				{
					_running = value;
					ModuleStateChanged?.Invoke(this, new StateArgs(_running));
				}
			}
		}
		public event EventHandler<StateArgs> ModuleStateChanged;

		protected IPacketTranceiver Tranceiver { get; private set; }
		protected DateTime ModuleStarted { get; private set; }
		protected DateTime ModuleStopped { get; private set; }
		protected double WorkerInterval => _worker.Interval;

		private bool _running;
		private IWorkerNG _worker;
		private CancellationTokenSource _cancellationTokenSource;

		protected BaseModuleNG(string identifier)
		{
			Identifier = identifier;
		}

		public virtual void Start(IPacketTranceiver tranceiver, IWorkerNG worker)
		{
			if (Running) throw new InvalidOperationException($"Module {Identifier} is already running");

			_cancellationTokenSource = new CancellationTokenSource();

			_worker = worker;
			Tranceiver = tranceiver;

			Tranceiver?.Register(ProcessPacket);
			_worker?.Register(DoWork);

			ModuleStarted = DateTime.Now;
			Running = true;
		}

		public virtual void Stop()
		{
			_worker?.Revoke(DoWork);
			Tranceiver?.Revoke(ProcessPacket);
			_cancellationTokenSource?.Cancel();
			ModuleStopped = DateTime.Now;
			Running = false;
		}

		private void DoWork(bool autoExit)
			=> DoWork(autoExit, _cancellationTokenSource);

		protected abstract void ProcessPacket(Packet packet, ILocalNetworkInterface nic);
		protected abstract void DoWork(bool autoExit, CancellationTokenSource tokenSource);
	}
}
