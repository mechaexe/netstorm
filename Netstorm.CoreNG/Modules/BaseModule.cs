﻿using NetStorm.Common.Devices;
using NetStorm.Common.Worker;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace NetStorm.CoreNG.Modules
{
	public abstract class BaseModule : IModule
	{
		public string Identifier { get; }
		public bool IsReady => GetCancellationToken().CanContinue();
		protected ILocalNetworkInterface Nic { get; private set; }

		public event EventHandler ModuleStarted, ModuleFinished;
		public event PropertyChangedEventHandler PropertyChanged;

		private readonly ICancellationToken _cancellationToken;

		public BaseModule(string name)
		{
			Identifier = name;
			_cancellationToken = WorkerFactory.GetCancellationToken();
		}

		protected void OnPropertyChanged([CallerMemberName] string str = "")
			=> PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(str));

		public virtual void SetNic(ILocalNetworkInterface nic)
		{
			if (!IsReady)
				Nic = nic;
		}

		public virtual bool Prepare()
		{
			if (!IsReady)
			{
				Common.Logging.LogEngine.GlobalLog.Log($"Preparing {Identifier}");
				Nic.CaptureStopped += Nic_CaptureStopped;
				_cancellationToken.Set();
				ModuleStarted?.Invoke(this, new EventArgs());
				return true;
			}
			else return false;
		}

		private void Nic_CaptureStopped(object sender, EventArgs e)
			=> Cleanup();

		public virtual void Cleanup()
		{
			if (IsReady)
			{
				Common.Logging.LogEngine.GlobalLog.Log($"Resetting {Identifier}");
				_cancellationToken.Reset();
				Nic.CaptureStopped -= Nic_CaptureStopped;
				ModuleFinished?.Invoke(this, new EventArgs());
			}
		}

		public ICancellationToken GetCancellationToken()
			=> _cancellationToken;

		public abstract void DoWork();
	}
}
