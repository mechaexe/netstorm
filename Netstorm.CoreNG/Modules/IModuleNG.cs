﻿using NetStorm.Common.Worker.PacketReceiver;
using NetStorm.CoreNG.Worker;
using System;

namespace NetStorm.CoreNG.Modules
{
	public interface IModuleNG
	{
		string Identifier { get; }
		bool Running { get; }
		event EventHandler<StateArgs> ModuleStateChanged;

		void Start(IPacketTranceiver tranceiver, IWorkerNG worker);
		void Stop();
		IModuleResult GetResult();
	}

	public class StateArgs : EventArgs
	{
		public StateArgs(bool running)
		{
			Running = running;
		}

		public bool Running { get; }
	}
}
