﻿using NetStorm.Common.Devices;
using NetStorm.Common.Worker.PacketReceiver;
using NetStorm.Common.Worker.Routing;
using NetStorm.CoreNG.Worker;
using PacketDotNet;

namespace NetStorm.CoreNG.Modules.Spoofer
{
	public abstract class SpooferNG : BaseModuleNG, ISpooferNG
	{
		public bool IsRouting => _router != null;

		private INicRouter _router;

		public SpooferNG(string identifier) : base(identifier) { }

		public override void Start(IPacketTranceiver tranceiver, IWorkerNG worker)
		{
			if (_router != null)
				StartRouter();
			base.Start(tranceiver, worker);
		}

		public override void Stop()
		{
			_router?.StopAndClearRoutes();
			base.Stop();
		}

		private void StartRouter()
		{
			var ops = GetOptions();
			_router.SetRoute(ops.Target1, ops.Target2);
			_router.SetNIC(ops.LocalInterface);
			_router.Start();
		}

		public void SetRouter(INicRouter router)
		{
			_router?.StopAndClearRoutes();
			_router = router;
			if (Running && _router != null)
				StartRouter();
		}

		public virtual IModuleResult GetResult()
			=> null;

		protected override void ProcessPacket(Packet packet, ILocalNetworkInterface nic) { }

		protected abstract SpoofOptions GetOptions();
	}
}
