﻿using NetStorm.Common.Devices;
using NetStorm.Common.Worker.Routing;

namespace NetStorm.CoreNG.Modules.Spoofer
{
	public abstract class BaseSpoofer : BaseModule, ISpoofer
	{
		public INetworkInterface Target1
		{
			get => t1;
			set
			{
				if (t1 == null || !t1.Equals(value))
				{
					t1 = value;
					_router?.SetRoute(t1, t2);
					OnPropertyChanged();
				}
			}
		}
		public INetworkInterface Target2
		{
			get => t2;
			set
			{
				if (t2 == null || !t2.Equals(value))
				{
					t2 = value;
					_router?.SetRoute(t1, t2);
					OnPropertyChanged();
				}
			}
		}
		public SpoofDirection SpoofDirection
		{
			get => sd;
			set
			{
				if (sd != value)
				{
					sd = value;
					OnPropertyChanged();
				}
			}
		}

		public ILocalNetworkInterface LocalNetworkInterface => Nic;

		public INicRouter Router
		{
			get => _router;
			set
			{
				_router?.StopAndClearRoutes();
				_router = value;
				if (IsReady && _router != null)
					PrepareRouter();
				OnPropertyChanged();
			}
		}

		private INetworkInterface t1, t2;
		private SpoofDirection sd;
		private INicRouter _router;

		public BaseSpoofer(string name) : base(name) { }

		public override void Cleanup()
		{
			Nic.StopCapture();
			_router?.StopAndClearRoutes();
			base.Cleanup();
		}

		public override bool Prepare()
		{
			if (!IsReady)
			{
				Nic.StartCapture();
				if(_router != null)
					PrepareRouter();
			}
			return base.Prepare();
		}

		private void PrepareRouter()
		{
			_router.SetRoute(t1, t2);
			_router.SetNIC(Nic);
			_router.Start();
		}

		public override void DoWork()
			=> SendPackets(Nic, Target1, Target2, SpoofDirection);

		protected abstract void SendPackets(
			ILocalNetworkInterface nic,
			INetworkInterface t1,
			INetworkInterface t2,
			SpoofDirection sd);
	}
}
