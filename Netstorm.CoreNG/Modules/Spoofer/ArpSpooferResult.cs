﻿using System;

namespace NetStorm.CoreNG.Modules.Spoofer
{
	public class ArpSpooferResult : IModuleResult
	{
		public ArpSpooferResult(DateTime moduleStarted, DateTime moduleStopped, ArpSpoofOptions spoofOptions)
		{
			ModuleStarted = moduleStarted;
			ModuleStopped = moduleStopped;
			SpoofOptions = spoofOptions ?? throw new ArgumentNullException(nameof(spoofOptions));
		}

		public DateTime ModuleStarted { get; }

		public DateTime ModuleStopped { get; }
		public ArpSpoofOptions SpoofOptions { get; }
	}
}
