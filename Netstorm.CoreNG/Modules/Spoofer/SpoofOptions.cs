﻿using NetStorm.Common.Devices;
using System;

namespace NetStorm.CoreNG.Modules.Spoofer
{
	public class SpoofOptions
	{
		public SpoofOptions(INetworkInterface target1, INetworkInterface target2, SpoofDirection spoofDirection, ILocalNetworkInterface localInterface)
		{
			Target1 = target1 ?? throw new ArgumentNullException(nameof(target1));
			Target2 = target2 ?? throw new ArgumentNullException(nameof(target2));
			SpoofDirection = spoofDirection;
			LocalInterface = localInterface ?? throw new ArgumentNullException(nameof(localInterface));
		}

		public INetworkInterface Target1 { get; }
		public INetworkInterface Target2 { get; }
		public SpoofDirection SpoofDirection { get; }
		public ILocalNetworkInterface LocalInterface { get; }
	}
}
