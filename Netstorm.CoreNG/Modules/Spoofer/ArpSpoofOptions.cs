﻿using NetStorm.Common.Devices;
using System;
using System.Net.NetworkInformation;

namespace NetStorm.CoreNG.Modules.Spoofer
{
	public class ArpSpoofOptions : SpoofOptions
	{
		public ArpSpoofOptions(PhysicalAddress spoofedAddress, bool broadcast, INetworkInterface target1, INetworkInterface target2, SpoofDirection spoofDirection, ILocalNetworkInterface localInterface) : 
			base(target1, target2, spoofDirection, localInterface)
		{
			SpoofedAddress = spoofedAddress ?? throw new ArgumentNullException(nameof(spoofedAddress));
			Broadcast = broadcast;
		}

		public PhysicalAddress SpoofedAddress { get; }
		public bool Broadcast { get; }
	}
}
