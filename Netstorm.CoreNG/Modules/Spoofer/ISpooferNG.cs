﻿using NetStorm.Common.Worker.Routing;

namespace NetStorm.CoreNG.Modules.Spoofer
{
	public interface ISpooferNG : IModuleNG
	{
		bool IsRouting { get; }
		void SetRouter(INicRouter router);
	}
}
