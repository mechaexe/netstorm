﻿using System.Net.NetworkInformation;

namespace NetStorm.CoreNG.Modules.Spoofer
{
	public interface IArpSpoofer : ISpoofer
	{
		bool Broadcast { get; set; }
		PhysicalAddress SpoofedMac { get; set; }
	}
}
