﻿using System;
using System.ComponentModel;
using NetStorm.Common.Devices;
using NetStorm.Common.Worker.Routing;

namespace NetStorm.CoreNG.Modules.Spoofer
{
	[Flags]
	public enum SpoofDirection
	{
		Target1 = 1,
		Target2 = 2,
		Both = 3
	}

	public interface ISpoofer : IModule, INotifyPropertyChanged
	{
		ILocalNetworkInterface LocalNetworkInterface { get; }
		INetworkInterface Target1 { get; set; }
		INetworkInterface Target2 { get; set; }
		SpoofDirection SpoofDirection { get; set; }
		INicRouter Router { get; set; }
	}
}
