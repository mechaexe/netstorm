﻿using NetStorm.Common.Devices;
using System;

namespace NetStorm.CoreNG.Modules.IntrusionDetection
{
	public class AttackerEventArgs : EventArgs
	{
		public AttackerEventArgs(IArpEntry attacker, IArpEntry victim, AttackType type, string description)
		{
			Attacker = attacker;
			Victim = victim;
			Type = type;
			Description = description;
		}

		public IArpEntry Attacker { get; }
		public IArpEntry Victim { get; }
		public AttackType Type { get; }
		public string Description { get; }

	}
}
