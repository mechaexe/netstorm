﻿using NetStorm.Common.AccessControl;
using NetStorm.Common.Devices;
using NetStorm.Common.Logging;
using NetStorm.Common.Worker;
using NetStorm.Common.Worker.PacketReceiver;
using PacketDotNet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Threading.Tasks;

namespace NetStorm.CoreNG.Modules.IntrusionDetection
{
	public class ArpDetector : BaseNicWorker, IArpDetector
	{
		enum ValidationState
		{
			Pending = 0,
			Validated = 1,
			ValidationFailed = 2,
			Spoofer = 3,
			Unknown = -1
		}

		private const ushort checkPort = 18763;
		private const uint tcpGraceTime_MS = 2000;

		public List<IArpEntry> Validated => validated.Values.ToList();
		public List<IArpEntry> Spoofer => spoofer.Values.ToList();

		public override string Name { get; }

		public event EventHandler<EventArgs<IArpEntry>> ValidHostFound, ValidationFailed, HostDetected;
		public event EventHandler<AttackerEventArgs> AttackDetected;

		private readonly Dictionary<string, PhysicalAddress> validationsPending, notValidated;
		private readonly Dictionary<PhysicalAddress, IArpEntry> spoofer;
		private readonly Dictionary<string, IArpEntry> validated;
		private IPacketTranceiver packetReceiver;
		private readonly TcpFilter tcpFilter;
		private readonly ICancellationToken cancellationToken;

		public ArpDetector(string name)
		{
			Name = name;
			spoofer = new Dictionary<PhysicalAddress, IArpEntry>();
			validationsPending = new Dictionary<string, PhysicalAddress>();
			notValidated = new Dictionary<string, PhysicalAddress>();
			validated = new Dictionary<string, IArpEntry>();
			cancellationToken = WorkerFactory.GetCancellationToken();
			tcpFilter = new TcpFilter()
			{
				DstPort = checkPort
			};
		}

		public void ProcessPacket(Packet packet, ILocalNetworkInterface nic)
		{
			if (packet is EthernetPacket eth)
			{
				if (eth.PayloadPacket is ArpPacket)
					ValidateArp(eth, nic);
				else if (tcpFilter.IsValid(eth))
					ValidateTCP(eth);
			}
		}


		/// <summary>
		/// Validates ARP-Packets, if a packet is invalid, there is probably an attack going on
		/// </summary>
		/// <param name="eth"></param>
		private void ValidateArp(EthernetPacket eth, ILocalNetworkInterface nic)
		{
			var arp = eth.PayloadPacket as ArpPacket;
			
			if ((eth.SourceHardwareAddress.Equals(arp.SenderHardwareAddress) &&
				arp.SenderHardwareAddress.Equals(nic.Mac)) ||					// Discard own packets
				!nic.IPv4Stack.Network.Contains(arp.SenderProtocolAddress))		// Discard packtes outside network scope
				return;

			if (!eth.SourceHardwareAddress.Equals(arp.SenderHardwareAddress))	// If eth and arp hw addresses do not match, raise awareness
				OnAttackerFound(arp.SenderProtocolAddress, eth.SourceHardwareAddress, "ETH/ARP-Mismatch");
			else 
				ValidateHost(arp.SenderProtocolAddress, arp.SenderHardwareAddress, nic);
		}

		/// <summary>
		/// Checks if a host is valid or invalid and raises alarm if invalid (assuming non-modified network stack)
		/// </summary>
		/// <param name="eth"></param>
		private void ValidateTCP(EthernetPacket eth)
		{
			var ip = eth.PayloadPacket as IPPacket;
			var tcp = ip.PayloadPacket as TcpPacket;

			if (tcp.DestinationPort == checkPort && (tcp.Acknowledgment || tcp.Reset) && 
				!validated.ContainsKey(ip.SourceAddress.ToString()))
				OnValidated(DeviceFactory.GetArpEntry(ip.SourceAddress, eth.SourceHardwareAddress));
		}

		/// <summary>
		/// Validate a host by sending a TCP SYN packet
		/// </summary>
		/// <param name="ip"></param>
		/// <param name="pmac"></param>
		/// <param name="nic"></param>
		private void ValidateHost(IPAddress ip, PhysicalAddress pmac, ILocalNetworkInterface nic)
		{
			switch(ValidateFromCache(ip, pmac))
			{
				// This function gets called on every arp packet, so we need to make sure we only execute it once for every ip - mac
				case ValidationState.ValidationFailed:
				case ValidationState.Unknown:
					LogEngine.GlobalLog.Log($"Validating host: {ip} at {pmac.ToCannonicalString()}");
					lock (validationsPending)
						if (!validationsPending.TryAdd(ip.ToString(), pmac))
							break;

					lock (validated)
						if (validated.ContainsKey(ip.ToString()))
							break;

					HostDetected?.Invoke(this, new EventArgs<IArpEntry>(DeviceFactory.GetArpEntry(ip, pmac)));
					var validationPacket = GenerateSynPacket(nic.Mac,
						pmac,
						nic.IPv4Stack.IPAddress,
						ip,
						checkPort,
						checkPort);
					nic.Transmit(validationPacket);

					// Wait until tcp may be answered
					Task.Run(() => RevalidateHost(ip, pmac, tcpGraceTime_MS));
					break;

				case ValidationState.Spoofer:
					OnAttackerFound(ip, pmac, $"ARP-Cache-Mismatch");
					break;
			}
		}

		private void RevalidateHost(IPAddress ip, PhysicalAddress pmac, uint gracePeriod_MS)
		{
			// Grace period for answering TCP packets
			if (cancellationToken.CanContinue(gracePeriod_MS))	
			{
				lock (validated)
					if (validated.ContainsKey(ip.ToString()))
						return;

				lock (notValidated)
					if (notValidated.ContainsKey(ip.ToString()))
						return;

				lock (spoofer)
					if (spoofer.ContainsKey(pmac))
						return;

				OnValidationFailed(DeviceFactory.GetArpEntry(ip, pmac));
			}
		}

		/// <summary>
		/// Check if current ip - mac combination matches previous ip - mac findings
		/// </summary>
		/// <param name="ip"></param>
		/// <param name="pmac"></param>
		/// <returns></returns>
		private ValidationState ValidateFromCache(IPAddress ip, PhysicalAddress pmac)
		{
			var mac = PhysicalAddress.None;
			var device = GetArpEntry(ip);

			if (device.Item2 != null)
				mac = device.Item2.Mac;

			return mac != PhysicalAddress.None && !pmac.Equals(mac) ? ValidationState.Spoofer : device.Item1;
		}

		private (ValidationState, IArpEntry) GetArpEntry(IPAddress ipAddress)
		{
			if (validated.ContainsKey(ipAddress.ToString()))
				return (ValidationState.Validated, validated[ipAddress.ToString()]);

			if (validationsPending.ContainsKey(ipAddress.ToString()))
				return (ValidationState.Pending, DeviceFactory.GetArpEntry(ipAddress, validationsPending[ipAddress.ToString()]));

			if (notValidated.ContainsKey(ipAddress.ToString()))
				return (ValidationState.ValidationFailed, DeviceFactory.GetArpEntry(ipAddress, notValidated[ipAddress.ToString()]));

			return (ValidationState.Unknown, null);
		}

		private void OnValidated(IArpEntry dev)
		{
			lock (validationsPending)
				validationsPending.Remove(dev.IPv4Stack.IPAddress.ToString());

			lock (spoofer)
				spoofer.Remove(dev.Mac);

			lock (notValidated)
				notValidated.Remove(dev.IPv4Stack.IPAddress.ToString());

			lock (validated)
				if (!validated.TryAdd(dev.IPv4Stack.IPAddress.ToString(), dev))	//Already validated
					return;

			LogEngine.GlobalLog.Log($"Validated host {dev.IPv4Stack.IPAddress} at { dev.Mac.ToCannonicalString() }");
			ValidHostFound?.Invoke(this, new EventArgs<IArpEntry>(dev));
		}

		private void OnValidationFailed(IArpEntry dev)
		{
			lock (validationsPending)
				validationsPending.Remove(dev.IPv4Stack.IPAddress.ToString());

			lock (notValidated)
				notValidated.TryAdd(dev.IPv4Stack.IPAddress.ToString(), dev.Mac);

			lock (spoofer)
				spoofer.Remove(dev.Mac);

			LogEngine.GlobalLog.Log($"Validation failed {dev.IPv4Stack.IPAddress} at { dev.Mac.ToCannonicalString() }", LogLevel.Error);
			ValidationFailed?.Invoke(this, new EventArgs<IArpEntry>(dev));
		}

		private IPAddress GetIPFromMAC(PhysicalAddress address)
		{
			lock (notValidated)
				foreach (var kv in notValidated)
					if (kv.Value.Equals(address))
						return IPAddress.Parse(kv.Key);

			lock (validationsPending)
				foreach (var kv in validationsPending)
					if (kv.Value.Equals(address))
						return IPAddress.Parse(kv.Key);

			lock (validated)
				foreach (var kv in validated)
					if (kv.Value.Mac.Equals(address))
						return kv.Value.IPv4Stack.IPAddress;

			return IPAddress.None;
		}

		private void OnAttackerFound(IPAddress senderIP, PhysicalAddress ethSource, string description)
		{
			var attacker = DeviceFactory.GetArpEntry(GetIPFromMAC(ethSource), ethSource);

			lock (validated)
				validated.Remove(attacker.IPv4Stack.IPAddress.ToString());

			lock (validationsPending)
				validationsPending.Remove(attacker.IPv4Stack.IPAddress.ToString());

			lock (notValidated)
				notValidated.Remove(attacker.IPv4Stack.IPAddress.ToString());

			lock (spoofer)
				if (!spoofer.TryAdd(ethSource, attacker))
				{
					spoofer[ethSource] = attacker;
					return;
				}

			LogEngine.GlobalLog.Log($"Attacker found: { ethSource.ToCannonicalString() } spoofing {senderIP}: Reason ({description})", LogLevel.Critical);
			AttackDetected?.Invoke(this, new AttackerEventArgs(attacker, GetArpEntry(senderIP).Item2, AttackType.Arp, description));
		}

		private EthernetPacket GenerateSynPacket(
				PhysicalAddress srcMac, PhysicalAddress dstMac,
				IPAddress srcAddr, IPAddress dstAddr,
				ushort srcPort, ushort dstPort
			)
		{
			var eth = new EthernetPacket(srcMac, dstMac, EthernetType.IPv4);
			var ip = new IPv4Packet(srcAddr, dstAddr);
			var tcp = new TcpPacket(srcPort, dstPort)
			{
				Synchronize = true,
			};

			ip.PayloadPacket = tcp;
			eth.PayloadPacket = ip;

			tcp.UpdateTcpChecksum();
			ip.UpdateIPChecksum();
			eth.UpdateCalculatedValues();
			return eth;
		}

		public override void Abort()
		{
			packetReceiver?.Abort();
			cancellationToken.Reset();
			Running = false;
		}

		public override void WaitForExit()
		{
			packetReceiver?.WaitForExit();
			cancellationToken.WaitForExit();
		}

		public override void SetNIC(ILocalNetworkInterface nic)
		{
			base.SetNIC(nic);
			packetReceiver = nic.GetDedicatedPacketTranceiver();
		}

		public override bool Start()
		{
			if (!Running && Nic != null && Nic.Supports(IpStackMode.IPv4))
			{
				validated.Clear();
				validationsPending.Clear();
				notValidated.Clear();
				spoofer.Clear();

				cancellationToken.Set();
				packetReceiver.Register(ProcessPacket);
				
				tcpFilter.ChildFilter = new IPPacketFilter()
				{
					DstAddress = Nic.IPv4Stack.IPAddress
				};
				return Running = true;
			}
			else return Running;
		}
	}
}
