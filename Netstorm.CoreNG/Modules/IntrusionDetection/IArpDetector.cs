﻿using NetStorm.Common.Devices;
using NetStorm.Common.Worker;
using System;
using System.Collections.Generic;

namespace NetStorm.CoreNG.Modules.IntrusionDetection
{
	public interface IArpDetector : INicWorker
	{
		List<IArpEntry> Spoofer { get; }
		List<IArpEntry> Validated { get; }

		event EventHandler<EventArgs<IArpEntry>> ValidationFailed, ValidHostFound, HostDetected;
		event EventHandler<AttackerEventArgs> AttackDetected;
	}
}