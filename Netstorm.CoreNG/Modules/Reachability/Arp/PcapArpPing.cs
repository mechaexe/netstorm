﻿using System;
using System.Net;
using System.Net.NetworkInformation;
using System.Threading;
using System.Threading.Tasks;
using NetStorm.Common.Devices;
using PacketDotNet;

namespace NetStorm.CoreNG.Modules.Reachability.Arp
{
	class PcapArpPing : IArpPing
	{
		private const int defaultTimeout = 5000;
		private const int defaultSleep = 10;
		private ArpReply result;
		private readonly object mutex = new object();
		private readonly ILocalNetworkInterface nic;

		public PcapArpPing(ILocalNetworkInterface nic)
		{
			this.nic = nic;
		}

		private void OnPacketArrived(Packet e, IPAddress target, DateTime startTime)
		{
			if (e is EthernetPacket eth &&
					eth.PayloadPacket is ArpPacket arp &&
					arp.SenderProtocolAddress.Equals(target))
			{
				result = new ArpReply(target, arp.SenderHardwareAddress, startTime, DateTime.Now, ArpStatus.Success);
			}
		}

		private EthernetPacket GenerateRequest(IPAddress target)
		{
			var eth = new EthernetPacket(nic.Mac, PhysicalAddressExtension.Broadcast, EthernetType.Arp);
			var arp = new ArpPacket(ArpOperation.Request, PhysicalAddressExtension.Broadcast, target, nic.Mac, nic.IPv4Stack.IPAddress);
			eth.PayloadPacket = arp;
			eth.UpdateCalculatedValues();
			return eth;
		}

		public IArpReply Send(string ipv4)
		{
			return Send(ipv4, defaultTimeout);
		}

		public IArpReply Send(string ipv4, int timeout_MS)
		{
			lock (mutex)
			{
				var startTime = DateTime.Now;
				var target = IPAddress.Parse(ipv4);
				result = new ArpReply(target, PhysicalAddress.None, startTime, DateTime.Now, ArpStatus.Failed);
				try
				{
					var handler = new EventHandler<Packet>((_, __) => OnPacketArrived(__, target, startTime));
					nic.PacketCaptured += handler;
					nic.Transmit(GenerateRequest(target));
					int slept = 0;
					while (timeout_MS >= slept && result.ArpStatus != ArpStatus.Success)
					{
						Thread.Sleep(defaultSleep);
						slept += defaultSleep;
					}
					nic.PacketCaptured -= handler;

					if (result.ArpStatus != ArpStatus.Success)
					{
						result = new ArpReply(target, PhysicalAddress.None, startTime, DateTime.Now, ArpStatus.DestinationUnreachable);
					}
				}
				catch { }
				return result;
			}
		}

		public Task<IArpReply> SendAsync(string ipv4)
		{
			return Task.Run(() => Send(ipv4, defaultTimeout));
		}

		public Task<IArpReply> SendAsync(string ipv4, int timeout_MS)
		{
			return Task.Run(() => Send(ipv4, timeout_MS));
		}
	}
}
