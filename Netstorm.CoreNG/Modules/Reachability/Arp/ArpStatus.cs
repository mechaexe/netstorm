﻿namespace NetStorm.CoreNG.Modules.Reachability.Arp
{
	public enum ArpStatus
	{
		Success = 0,
		DestinationUnreachable = 0x43,
		Failed = -1
	}
}
