﻿using NetStorm.Common.Logging;
using NetStorm.CoreNG.Tools.Caching;
using System;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace NetStorm.CoreNG.Modules.Reachability.Arp
{
	class FallbackArpPing : IArpPing
	{
		public IArpReply Send(string ipv4)
		{
			var res = new ArpReply();
			if (IPAddress.TryParse(ipv4, out var ip) &&
				ip.AddressFamily == AddressFamily.InterNetwork)
			{
				var arpCache = new ArpCache();
				var mac = arpCache.GetEntry(ip.ToString())?.Mac;
				if (mac != PhysicalAddress.None)
				{
					res = new ArpReply(ip, mac, DateTime.Now, DateTime.Now, ArpStatus.Success);
				}
				else
				{
					try
					{
						using (var p = new Ping())
						{
							var startTime = DateTime.Now;
							var pingRes = p.Send(ip.ToString());
							if (pingRes.Status == IPStatus.Success)
							{
								var device = arpCache.GetEntry(ipv4);
								var status = device.Mac == PhysicalAddress.None ? ArpStatus.Failed : ArpStatus.Success;
								res = new ArpReply(ip, device.Mac, startTime, new TimeSpan(pingRes.RoundtripTime * TimeSpan.TicksPerMillisecond), status);
							}
						}
					}
					catch (PingException) { }
					catch (Exception e) { LogEngine.GlobalLog.Log($"Ping to {ipv4} failed: { e.Message }", LogLevel.Normal); }
				}
			}
			return res;
		}

		public Task<IArpReply> SendAsync(string ipv4) => Task.Run(() => Send(ipv4));
	}
}
