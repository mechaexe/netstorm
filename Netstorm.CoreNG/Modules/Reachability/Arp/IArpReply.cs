﻿using System;
using System.Net;
using System.Net.NetworkInformation;
using NetStorm.Common.Devices;

namespace NetStorm.CoreNG.Modules.Reachability.Arp
{
	public interface IArpReply
	{
		ArpStatus ArpStatus { get; }
		IPAddress IPAddress { get; }
		PhysicalAddress PhysicalAddress { get; }
		DateTime Sent { get; }
		DateTime Received { get; }

		IArpEntry ToArpEntry();
	}
}