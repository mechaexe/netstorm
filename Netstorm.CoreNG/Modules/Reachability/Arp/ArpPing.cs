﻿using System;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Threading.Tasks;
using NetStorm.Common.Logging;

namespace NetStorm.CoreNG.Modules.Reachability.Arp
{
	class ArpPing : IArpPing
	{
		private IArpReply InternalSend(string ipv4)
		{
			var res = new ArpReply();

			if(IPAddress.TryParse(ipv4, out var ip) &&
				ip.AddressFamily == AddressFamily.InterNetwork &&
				LocalEnvironment.Subsystem == Subsystem.Windows)
			{
				var sent = DateTime.Now;
				var ipAddr = BitConverter.ToUInt32(ip.GetAddressBytes(), 0);

				try
				{
					byte[] macByte = new byte[6];
					int length = macByte.Length;
					int errorCode = NativeMethods.SendARP(ipAddr, 0, macByte, ref length);
					var mac = errorCode == 0 ?
						PhysicalAddress.Parse(BitConverter.ToString(macByte, 0, length)) :
						PhysicalAddress.None;
					res = new ArpReply(ip, mac, sent, DateTime.Now, (ArpStatus)errorCode);
				}
				catch (Exception e) { LogEngine.GlobalLog.Log("SendArp failed: " + e.Message, LogLevel.Normal); }
			}
			return res;
		}

		public IArpReply Send(string ipv4) => Send(ipv4, false);

		public IArpReply Send(string ipv4, bool pingOnFailed)
		{
			var res = InternalSend(ipv4);

			if (pingOnFailed && res.ArpStatus != ArpStatus.Success)
			{
				var ping = new FallbackArpPing();
				res = ping.Send(ipv4);
			}

			return res;
		}

		public Task<IArpReply> SendAsync(string ipv4) => SendAsync(ipv4, false);

		public async Task<IArpReply> SendAsync(string ipv4, bool pingOnFailed) => await Task.Run(() => Send(ipv4, pingOnFailed));
	}
}