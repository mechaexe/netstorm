﻿using System.Threading.Tasks;

namespace NetStorm.CoreNG.Modules.Reachability.Arp
{
	public interface IArpPing
	{
		IArpReply Send(string ipv4);
		Task<IArpReply> SendAsync(string ipv4);
	}
}
