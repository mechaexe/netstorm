﻿using System;
using System.Net;
using System.Net.NetworkInformation;
using NetStorm.Common.Devices;

namespace NetStorm.CoreNG.Modules.Reachability.Arp
{
	class ArpReply : IArpReply
	{
		public PhysicalAddress PhysicalAddress { get; }
		public IPAddress IPAddress { get; }
		public ArpStatus ArpStatus { get; }
		public DateTime Sent { get; }
		public DateTime Received { get; }

		public ArpReply()
		{
			IPAddress = IPAddress.None;
			ArpStatus = ArpStatus.Failed;
			Sent = DateTime.Now;
			Received = DateTime.Now;
		}

		public ArpReply(IPAddress ipAddress, PhysicalAddress physicalAddress, DateTime pingSent, DateTime pingReceived, ArpStatus status)
		{
			IPAddress = ipAddress;
			PhysicalAddress = physicalAddress;
			Sent = pingSent;
			ArpStatus = status;
		}

		public ArpReply(IPAddress ipAddress, PhysicalAddress physicalAddress, DateTime pingSent, TimeSpan responseTime, ArpStatus status)
		{
			IPAddress = ipAddress;
			PhysicalAddress = physicalAddress;
			Sent = pingSent;
			ArpStatus = status;
			Received = pingSent + responseTime;
		}

		public IArpEntry ToArpEntry()
		{
			return DeviceFactory.GetArpEntry(IPAddress.ToString(), PhysicalAddress);
		}

		public override string ToString()
		{
			return "[ArpResult] Status: " + ArpStatus + ", IPv4: " + IPAddress.ToString() + ", MAC: " + PhysicalAddress.ToCannonicalString() + ", Milliseconds: " + (Received - Sent).TotalMilliseconds;
		}
	}
}
