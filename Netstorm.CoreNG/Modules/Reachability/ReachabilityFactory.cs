﻿using NetStorm.Common.Devices;
using NetStorm.CoreNG.Modules.Reachability.Arp;

namespace NetStorm.CoreNG.Modules.Reachability
{
	public static class ReachabilityFactory
	{
		public static bool ArpAvailable => LocalEnvironment.Api != NetworkAPI.Native || LocalEnvironment.Subsystem == Subsystem.Windows;

		public static IArpPing GetArpPing(ILocalNetworkInterface nic = null)
		{
			if (LocalEnvironment.Api != NetworkAPI.Native && nic != null)
			{
				return new PcapArpPing(nic);
			}

			if (LocalEnvironment.Subsystem == Subsystem.Windows)
			{
				return new ArpPing();
			}

			return new FallbackArpPing();
		}
	}
}
