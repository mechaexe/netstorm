﻿using NetStorm.Common.Devices;
using NetStorm.CoreNG.Modules.NetworkScanner.Detection;
using NetStorm.CoreNG.Modules.Transformation;
using System;

namespace NetStorm.CoreNG.Modules.NetworkScanner
{
	public interface INetworkScanner : IModuleNG
	{
		IDetector Detector { get; set; }
		ITransformator Transformator { get; set; }
		event EventHandler<INetworkDevice> DeviceDetected;
		void SetOptions(NetworkOptions options);
		new ScanResult GetResult();
	}
}