﻿using NetStorm.Common.Devices;
using NetStorm.Common.Worker.PacketReceiver;
using NetStorm.CoreNG.Modules.NetworkScanner.Detection;
using NetStorm.CoreNG.Modules.Transformation;
using NetStorm.CoreNG.Worker;
using PacketDotNet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Threading;
using System.Threading.Tasks;

namespace NetStorm.CoreNG.Modules.NetworkScanner
{
	public class NetworkScanner : BaseModuleNG, INetworkScanner
	{
		public IDetector Detector
		{
			get => _detector;
			set
			{
				if (Running)
					throw new InvalidOperationException("Detector cannot be modified during execution");
				if(value == null || !value.Equals(_detector))
				{
					if (_detector != null)
						_detector.DeviceDetected -= Detector_DeviceDetected;
					_detector = value;
					if (_detector != null)
						_detector.DeviceDetected += Detector_DeviceDetected;
				}
			}
		}
		public ITransformator Transformator { get; set; }

		public event EventHandler<INetworkDevice> DeviceDetected;

		private NetworkOptions _options;
		private IDetector _detector;
		private readonly Dictionary<PhysicalAddress, INetworkDevice> _detectedDevices;
		private readonly object _detectedDevicesLock;

		public NetworkScanner(string identifier) : base(identifier)
		{
			_detectedDevicesLock = new object();
			_detectedDevices = new Dictionary<PhysicalAddress, INetworkDevice>();
			_options = new NetworkOptions(IPNetwork.Parse("255.255.255.255", 0), null);
		}

		private void Detector_DeviceDetected(object sender, EventArgs<INetworkDevice> e)
		{
			var device = e.Argument;
			if (device.NetworkInterface.Mac != PhysicalAddress.None &&
				device.NetworkInterface.Mac != null &&
				((device.NetworkInterface.Supports(IpStackMode.IPv4) && _options.NetworkV4.Contains(device.NetworkInterface.IPv4Stack.IPAddress)) ||
				(device.NetworkInterface.Supports(IpStackMode.IPv6))))
			{
				var isNew = true;
				device.Discovered();
				device.Statistics.FirstSeen = DateTime.Now;
				lock (_detectedDevicesLock)
				{
					if (!(isNew = _detectedDevices.TryAdd(device.NetworkInterface.Mac, device)))
					{
						_detectedDevices[device.NetworkInterface.Mac].Merge(device);
						device = _detectedDevices[device.NetworkInterface.Mac];
					}
				}
				if (Transformator != null)
					ThreadPool.QueueUserWorkItem(arg => Transformator.TransformDevice(device, false));

				if (isNew)
					DeviceDetected?.Invoke(this, device);
			}
		}

		protected override void ProcessPacket(Packet packet, ILocalNetworkInterface nic)
			=> Detector?.ProcessPackets(_options, Tranceiver, packet);

		protected async override void DoWork(bool autoExit, CancellationTokenSource tokenSource)
		{
			try
			{
				Detector?.SendPackets(_options, Tranceiver, tokenSource.Token);
				if(autoExit)
				{
					await Task.Delay((int)WorkerInterval, tokenSource.Token);
					Stop();
				}
			}
			catch (SharpPcap.DeviceNotReadyException) { Stop(); }
			catch (TaskCanceledException) { Stop(); }
		}

		public override void Start(IPacketTranceiver tranceiver, IWorkerNG worker)
		{
			if (worker == null) throw new ArgumentNullException(nameof(worker));
			if (_options == null) throw new InvalidOperationException("Options are required");
			_detectedDevices.Clear();
			base.Start(tranceiver, worker);
		}

		public void SetOptions(NetworkOptions options)
		{
			if (Running) throw new InvalidOperationException();
			_options = options ?? throw new ArgumentNullException(nameof(options));
		}

		public ScanResult GetResult()
			=> new ScanResult(_options, _detectedDevices.Values.ToList(), ModuleStarted, Running ? DateTime.Now : ModuleStopped);

		IModuleResult IModuleNG.GetResult()
			=> GetResult();
	}
}
