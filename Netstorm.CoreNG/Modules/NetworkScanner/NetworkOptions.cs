﻿using NetStorm.Common.Devices;
using System;
using System.Net;

namespace NetStorm.CoreNG.Modules.NetworkScanner
{
	public class NetworkOptions
	{
		public NetworkOptions(IPNetwork networkV4, INetworkInterface localInterface)
		{
			NetworkV4 = networkV4 ?? throw new ArgumentNullException(nameof(networkV4));
			NetworkV6 = IPAddressExtensions.PrivateNetworksV6[1];
			LocalInterface = localInterface;
		}

		public NetworkOptions(IPNetwork networkV4, IPNetwork networkV6, INetworkInterface localInterface) : this(networkV4, localInterface)
		{
			NetworkV6 = networkV6 ?? throw new ArgumentNullException(nameof(networkV6));
		}

		public IPNetwork NetworkV4 { get; }
		public IPNetwork NetworkV6 { get; }

		public INetworkInterface LocalInterface { get; }
	}
}
