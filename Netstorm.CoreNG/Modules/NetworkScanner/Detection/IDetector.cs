﻿using NetStorm.Common.Devices;
using NetStorm.Common.Worker.PacketReceiver;
using PacketDotNet;
using System;
using System.Threading;

namespace NetStorm.CoreNG.Modules.NetworkScanner.Detection
{
	public interface IDetector
	{
		event EventHandler<EventArgs<INetworkDevice>> DeviceDetected;

		void SendPackets(NetworkOptions options, IPacketTranceiver tranceiver, CancellationToken cancellationToken);
		void ProcessPackets(NetworkOptions options, IPacketTranceiver tranceiver, Packet packet);
	}
}
