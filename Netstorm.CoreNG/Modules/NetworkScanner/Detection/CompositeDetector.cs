﻿using NetStorm.Common.Devices;
using NetStorm.Common.Worker.PacketReceiver;
using PacketDotNet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace NetStorm.CoreNG.Modules.NetworkScanner.Detection
{
	public class CompositeDetector : IDetector
	{
		public event EventHandler<EventArgs<INetworkDevice>> DeviceDetected;

		private readonly List<IDetector> _detectors = new List<IDetector>();

		private void OnDeviceDetected(object sender, EventArgs<INetworkDevice> eventArgs)
			=> DeviceDetected?.Invoke(sender, eventArgs);

		public void ProcessPackets(NetworkOptions options, IPacketTranceiver tranceiver, Packet packet)
			=> _detectors.ToList().ForEach(x => x.ProcessPackets(options, tranceiver, packet));

		public void SendPackets(NetworkOptions options, IPacketTranceiver tranceiver, CancellationToken cancellationToken)
			=> _detectors.ToList().ForEach(x => x.SendPackets(options, tranceiver, cancellationToken));

		public void AddDetector(IDetector detector)
		{
			detector.DeviceDetected += OnDeviceDetected;
			_detectors.Add(detector);
		}

		public bool RemoveDetector(Type detector)
			=> _detectors.RemoveType(detector);

		public void RemoveAllDetectors()
			=> _detectors.Clear();
	}
}
