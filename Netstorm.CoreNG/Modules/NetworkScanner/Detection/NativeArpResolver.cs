﻿using NetStorm.Common.Classes;
using NetStorm.Common.Devices;
using NetStorm.Common.Worker.PacketReceiver;
using NetStorm.CoreNG.Modules.Reachability.Arp;
using PacketDotNet;
using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace NetStorm.CoreNG.Modules.NetworkScanner.Detection
{
	public class NativeArpResolver : IDetector
	{
		public event EventHandler<EventArgs<INetworkDevice>> DeviceDetected;

		private void OnDeviceDetected(INetworkInterface entry) => DeviceDetected?.Invoke(this, new EventArgs<INetworkDevice>(DeviceFactory.GetNetworkDevice(entry)));

		public void SendPackets(NetworkOptions options, IPacketTranceiver tranceiver, CancellationToken cancellationToken)
		{
			var ping = Reachability.ReachabilityFactory.GetArpPing(null);
			var ops = new ParallelOptions() { CancellationToken = cancellationToken };
			try
			{
				Parallel.ForEach(options.NetworkV4.ListIPAddress(FilterEnum.Usable), ops, (address, asdf) =>
				{
					if (!ops.CancellationToken.IsCancellationRequested)
					{
						var res = ping.Send(address.ToString());
						if (res.ArpStatus == ArpStatus.Success)
							OnDeviceDetected(DeviceFactory.GetInterface(new IPStack(res.IPAddress, options.NetworkV4), res.PhysicalAddress));
					}
					else
						asdf.Stop();
				});
			}
			catch (OperationCanceledException) { }
		}

		public void ProcessPackets(NetworkOptions options, IPacketTranceiver tranceiver, Packet packet) { }
	}
}
