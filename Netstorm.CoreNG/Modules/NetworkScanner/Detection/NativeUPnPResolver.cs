﻿using NetStorm.Common.Classes;
using NetStorm.Common.Devices;
using NetStorm.Common.Worker.PacketReceiver;
using NetStorm.CoreNG.Modules.ServiceScanner.ServiceDetector;
using NetStorm.CoreNG.Tools.Caching;
using PacketDotNet;
using Protocols.UPnP;
using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace NetStorm.CoreNG.Modules.NetworkScanner.Detection
{
	public class NativeUPnPResolver : IDetector
	{
		public event EventHandler<EventArgs<INetworkDevice>> DeviceDetected;

		public void ProcessPackets(NetworkOptions options, IPacketTranceiver tranceiver, Packet packet) { }

		public async void SendPackets(NetworkOptions options, IPacketTranceiver tranceiver, CancellationToken cancellationToken)
			=> await Discover(options.LocalInterface.IPv4Stack.IPAddress);

		private async Task Discover(IPAddress localBind)
		{
			var client = new UPnPDiscovery();
			var responses = client.DiscoverRootDevices(localBind);
			var arpCache = new ArpCache();
			var upnpDetector = new UPnPServiceDetector();
			foreach (var response in await responses)
			{
				var entry = arpCache.GetEntry(response.Item1.ToString());
				var device = DeviceFactory.GetNetworkDevice(entry);
				device.Ports.Add(new Port(UPnPDiscovery.UPnPPort)
				{
					Status = PortStatus.Open,
					Protocol = Protocol.Udp,
					Service = upnpDetector.DetectService(response.Item2.ToString(), null)
				});
				DeviceDetected?.Invoke(this, new EventArgs<INetworkDevice>(device));
			}
		}
	}
}
