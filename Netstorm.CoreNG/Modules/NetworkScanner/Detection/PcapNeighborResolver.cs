﻿using NetStorm.Common.Classes;
using NetStorm.Common.Devices;
using NetStorm.Common.Worker.PacketReceiver;
using PacketDotNet;
using PacketDotNet.Utils;
using Protocols.NDP;
using System;
using System.Text;
using System.Threading;

namespace NetStorm.CoreNG.Modules.NetworkScanner.Detection
{
	public class PcapNeighborResolver : IDetector
	{
		public event EventHandler<EventArgs<INetworkDevice>> DeviceDetected;

		private void OnDeviceDetected(INetworkInterface entry) => DeviceDetected?.Invoke(this, new EventArgs<INetworkDevice>(DeviceFactory.GetNetworkDevice(entry)));

		public void ProcessPackets(NetworkOptions options, IPacketTranceiver tranceiver, Packet packet)
		{
			if (packet is EthernetPacket eth &&
				eth.PayloadPacket is IPv6Packet ipv6 &&
				ipv6.PayloadPacket is IcmpV6Packet icmpv6 &&
					(icmpv6.Type == IcmpV6Type.RouterAdvertisement ||
					icmpv6.Type == IcmpV6Type.EchoReply ||
					icmpv6.Type == IcmpV6Type.NeighborAdvertisement))
			{
				OnDeviceDetected(DeviceFactory.GetInterface(new IPStack(ipv6.SourceAddress, options.NetworkV6), eth.SourceHardwareAddress));
			}
		}

		public void SendPackets(NetworkOptions options, IPacketTranceiver tranceiver, CancellationToken cancellationToken)
		{
			var packets = new[]
			{
				PrepareICMPv6Ping(options.LocalInterface),
				PrepareNeighborSolicitation(options.LocalInterface)
			};
			try { tranceiver.SendPackets(packets); }
			catch (SharpPcap.DeviceNotReadyException) { }
		}

		private EthernetPacket PrepareICMPv6Ping(INetworkInterface nic)
		{
			var mcast = NeighborDiscoveryFactory.MulticastAddresses["all-nodes-link-local"];
			var eth = new EthernetPacket(nic.Mac, NeighborDiscoveryFactory.CreateMulticastMac(mcast), EthernetType.IPv6);
			var ip = new IPv6Packet(nic.IPv6Stack.IPAddress, mcast);
			var ns = new IcmpV6Packet(new ByteArraySegment(new byte[40]))
			{
				Type = IcmpV6Type.EchoRequest,
				PayloadData = Encoding.ASCII.GetBytes("abcdefghijklmnopqrstuvwabcdefghi")
			};

			ip.PayloadPacket = ns;
			eth.PayloadPacket = ip;

			eth.UpdateCalculatedValues();
			ip.UpdateCalculatedValues();
			ns.UpdateCalculatedValues();

			return eth;
		}

		//Visit https://tools.ietf.org/html/rfc4861#section-7.1.1 for reference
		private EthernetPacket PrepareNeighborSolicitation(INetworkInterface nic)
		{
			var mcast = NeighborDiscoveryFactory.MulticastAddresses["all-nodes-link-local"];
			var eth = new EthernetPacket(nic.Mac, NeighborDiscoveryFactory.CreateMulticastMac(mcast), EthernetType.IPv6);
			var ip = new IPv6Packet(nic.IPv6Stack.IPAddress, mcast)
			{
				HopLimit = 255  //Defined in rfc 4861
			};
			var ns = NeighborDiscoveryFactory.CreateNeighborSolicitationPacket(nic.Mac, nic.IPv6Stack.IPAddress);

			ip.PayloadPacket = ns;
			eth.PayloadPacket = ip;

			eth.UpdateCalculatedValues();
			ip.UpdateCalculatedValues();
			ns.UpdateCalculatedValues();

			return eth;
		}
	}
}
