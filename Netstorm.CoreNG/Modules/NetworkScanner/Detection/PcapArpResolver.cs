﻿using NetStorm.Common.Classes;
using NetStorm.Common.Devices;
using NetStorm.Common.Worker.PacketReceiver;
using PacketDotNet;
using System;
using System.Net;
using System.Net.NetworkInformation;
using System.Threading;
using System.Threading.Tasks;

namespace NetStorm.CoreNG.Modules.NetworkScanner.Detection
{
	public class PcapArpResolver : IDetector
	{
		public int ArtificialSlowDownMax { get; set; } = 1000;
		public event EventHandler<EventArgs<INetworkDevice>> DeviceDetected;

		private void OnDeviceDetected(INetworkInterface entry) => DeviceDetected?.Invoke(this, new EventArgs<INetworkDevice>(DeviceFactory.GetNetworkDevice(entry)));

		public void ProcessPackets(NetworkOptions options, IPacketTranceiver tranceiver, Packet packet)
		{
			if ((packet is EthernetPacket eth && eth.PayloadPacket is ArpPacket arp && arp.Operation == ArpOperation.Response))
			{
				OnDeviceDetected(DeviceFactory.GetInterface(new IPStack(arp.SenderProtocolAddress, options.NetworkV4), arp.SenderHardwareAddress));
			}
		}
		public void SendPackets(NetworkOptions options, IPacketTranceiver tranceiver, CancellationToken cancellationToken)
		{
			try
			{
				var rnd = new Random();
				var ops = new ParallelOptions() { CancellationToken = cancellationToken };
				Parallel.ForEach(
					options.NetworkV4.ListIPAddress(FilterEnum.Usable),
					ops,
					(ip, state) =>
					{
						Thread.Sleep(rnd.Next(ArtificialSlowDownMax));
						if (ops.CancellationToken.IsCancellationRequested)
							state.Stop();
						else
							tranceiver.SendPackets(CreatePacket(options.LocalInterface, ip));
					}
				);
			}
			catch (OperationCanceledException) { }
			catch (SharpPcap.DeviceNotReadyException) { }
		}

		private EthernetPacket CreatePacket(INetworkInterface nic, IPAddress address)
		{
			var eth = new EthernetPacket(nic.Mac, PhysicalAddressExtension.Broadcast, EthernetType.Arp);
			var arp = new ArpPacket(ArpOperation.Request, PhysicalAddressExtension.Broadcast, address, nic.Mac, nic.IPv4Stack.IPAddress);
			eth.PayloadPacket = arp;
			eth.UpdateCalculatedValues();
			return eth;
		}
	}
}
