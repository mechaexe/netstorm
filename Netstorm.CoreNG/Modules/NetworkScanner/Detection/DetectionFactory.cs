﻿using NetStorm.Common.Devices;

namespace NetStorm.CoreNG.Modules.NetworkScanner.Detection
{
	public static class DetectionFactory
	{
		public static IDetector GetArpResolver(NetworkAPI networkAPI)
			=> networkAPI == NetworkAPI.Native ? (IDetector)new NativeArpResolver() : new PcapArpResolver();

		public static IDetector GetNeighborResolver() => new PcapNeighborResolver();
		public static IDetector GetPassiveResolver() => new PcapPassiveResolver();
		public static IDetector GetUPnPResolver() => new NativeUPnPResolver();
	}
}
