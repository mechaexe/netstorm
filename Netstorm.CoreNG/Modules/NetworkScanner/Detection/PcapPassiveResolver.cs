﻿using NetStorm.Common.Classes;
using NetStorm.Common.Devices;
using NetStorm.Common.Worker.PacketReceiver;
using PacketDotNet;
using System;
using System.Threading;

namespace NetStorm.CoreNG.Modules.NetworkScanner.Detection
{
	public class PcapPassiveResolver : IDetector
	{
		public event EventHandler<EventArgs<INetworkDevice>> DeviceDetected;

		private void OnDeviceDetected(INetworkInterface entry) => DeviceDetected?.Invoke(this, new EventArgs<INetworkDevice>(DeviceFactory.GetNetworkDevice(entry)));

		public void ProcessPackets(NetworkOptions options, IPacketTranceiver tranceiver, Packet packet)
		{
			if(packet is EthernetPacket eth)
			{
				if (eth.PayloadPacket is IPPacket ipPack)
					ProcessIpPacket(options, eth, ipPack);
				else if (eth.PayloadPacket is ArpPacket arp && 
					arp.Operation == ArpOperation.Response)
					OnDeviceDetected(DeviceFactory.GetInterface(new IPStack(arp.SenderProtocolAddress, options.NetworkV4), arp.SenderHardwareAddress));
			}
		}

		private void ProcessIpPacket(NetworkOptions options, EthernetPacket eth, IPPacket ipPack)
		{
			var dev = DeviceFactory.GetInterface();
			if (ipPack is IPv4Packet)
			{
				dev.IPv4Stack = new IPStack(ipPack.SourceAddress, options.NetworkV4);
			}
			else
			{
				dev.IPv6Stack = new IPStack(ipPack.SourceAddress, options.NetworkV6);
			}

			dev.Mac = eth.SourceHardwareAddress;
			OnDeviceDetected(dev);
		}

		public void SendPackets(NetworkOptions options, IPacketTranceiver tranceiver, CancellationToken cancellationToken) { }
	}
}
