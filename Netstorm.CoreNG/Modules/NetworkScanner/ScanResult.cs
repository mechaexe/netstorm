﻿using NetStorm.Common.Devices;
using System;
using System.Collections.Generic;

namespace NetStorm.CoreNG.Modules.NetworkScanner
{
	public class ScanResult : IModuleResult
	{
		public ScanResult(NetworkOptions options, List<INetworkDevice> devices, DateTime moduleStarted, DateTime moduleStopped)
		{
			Options = options ?? throw new ArgumentNullException(nameof(options));
			Devices = devices ?? throw new ArgumentNullException(nameof(devices));
			ModuleStarted = moduleStarted;
			ModuleStopped = moduleStopped;
		}

		public NetworkOptions Options { get; }

		public List<INetworkDevice> Devices { get; }

		public DateTime ModuleStarted { get; }

		public DateTime ModuleStopped { get; }
	}
}
