﻿using NetStorm.Common.Collections;
using NetStorm.Common.Devices;
using System.Net;
using System.Net.NetworkInformation;

namespace NetStorm.CoreNG.Modules.Transformation
{
	public class PingFingerprintTransform : ITransformator
	{
		public INetworkDevice TransformDevice(INetworkDevice device, bool overrideCheck)
		{
			if(device.OSMatrix == null || overrideCheck)
			{
				var res = GetFingerprint(device.GetHostIdentifier(IpStackMode.IPv4));
				if (res.Item1)
				{
					res.Item2.Merge(device.OSMatrix);
					device.OSMatrix = res.Item2;
					device.Discovered();
				}
			}
			return device;
		}

		public (bool, OsMatrix) GetFingerprint(string hostname)
		{
			var res = new OsMatrix();
			bool successful = false;
			
			try
			{
				using var p = new Ping();
				var pingres = p.Send(hostname);
				if (pingres.Status == IPStatus.Success && pingres.Options != null)
				{
					Evaluate(pingres.Options.Ttl, res, !IPAddress.TryParse(hostname, out var _));
					successful = true;
				}
			}
			catch { }
			return (successful, res);
		}

		private void Evaluate(int ttl, OsMatrix matrix, bool hasHostname)
		{
			switch (ttl)
			{
				case 255:
					matrix["bsd"].Match++;
					matrix["linux"].Match++;
					matrix["macos"].Match++;
					break;
				case 254:
					matrix["solaris"].Match++;
					matrix["aix"].Match++;
					matrix["cisco"].Match++;
					break;
				case 128:
					matrix["win10"].Match++;
					break;
				case 64:
					matrix["bsd"].Match++;
					matrix["linux"].Match++;
					matrix["macos"].Match++;
					matrix["android"].Match++;
					break;
				case 60:
					matrix["macos"].Match++;
					break;
				case 32:
					matrix["winwork"].Match++;
					matrix["win95"].Match++;
					matrix["winnt3"].Match++;
					matrix["winnt4"].Match++;
					break;
			}

			if (!hasHostname)
			{
				matrix["android"].Match++;
				matrix["cisco"].Match++;
				matrix["linux"].Match++;
				matrix["aix"].Match++;
				matrix["ios"].Match++;
				matrix["win10"].Match++;
			}
		}
	}
}
