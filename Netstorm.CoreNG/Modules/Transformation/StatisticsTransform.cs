﻿using System;
using System.Linq;
using System.Net;
using NetStorm.Common.Devices;

namespace NetStorm.CoreNG.Modules.Transformation
{
	public class StatisticsTransform : ITransformator
	{
		public INetworkDevice TransformDevice(INetworkDevice device, bool overrideCheck)
		{
			device.Statistics.DeviceState = DateTime.Now - device.Statistics.LastSeen < new TimeSpan(0, 1, 0) ? DeviceState.Online : DeviceState.Offline;
			device.DeviceType = device.NetworkInterface.GetPreferredStack(IpStackMode.IPv4).IPAddress.IsPrivate() ? DeviceType.Private : DeviceType.Public;
			if (NicManager.Instance.Where(x => x.Gateway != null).Select(x => x.Gateway.NetworkInterface.Mac.ToString()).Contains(device.NetworkInterface.Mac.ToString()))
			{
				device.DeviceType = DeviceType.Gateway;
			}

			if (NicManager.Instance.Select(x => x.Mac.ToString()).Contains(device.NetworkInterface.Mac.ToString()))
			{
				device.DeviceType = DeviceType.LocalMachine;
			}

			return device;
		}
	}
}
