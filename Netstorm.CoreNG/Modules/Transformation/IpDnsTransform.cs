﻿using NetStorm.Common.Classes;
using NetStorm.Common.Devices;
using System.Net;
using System.Net.Sockets;

namespace NetStorm.CoreNG.Modules.Transformation
{
	public class IpDnsTransform : ITransformator
	{
		public INetworkDevice TransformDevice(INetworkDevice device, bool overrideCheck)
		{
			try
			{
				var id = device.GetHostIdentifier(IpStackMode.IPv4);
				var entry = Dns.GetHostEntry(id);
				if (!string.IsNullOrEmpty(entry.HostName) && entry.HostName != "." && (device.Hostname == null || overrideCheck))
					device.Hostname = entry.HostName;
				foreach (var a in entry.AddressList)
				{
					if (a.AddressFamily == AddressFamily.InterNetwork && (overrideCheck || !device.NetworkInterface.Supports(IpStackMode.IPv4)))
					{
						device.NetworkInterface.IPv4Stack = new IPStack(a, device.NetworkInterface.IPv4Stack.Network);
					}
					else if (a.AddressFamily == AddressFamily.InterNetworkV6 && (overrideCheck || !device.NetworkInterface.Supports(IpStackMode.IPv6)))
					{
						device.NetworkInterface.IPv6Stack = new IPStack(a, device.NetworkInterface.IPv6Stack.Network);
					}
				}
				device.Discovered();
			}
			catch { }
			return device;
		}
	}
}
