﻿namespace NetStorm.CoreNG.Modules.Transformation
{
	public static class TransformationFactory
	{
		public static ITransformator GetDnsTransform() => new DnsTransform();
		public static ITransformator GetIpDnsTransform() => new IpDnsTransform();
		public static ITransformator GetFingerprintTransform() => new PingFingerprintTransform();
		public static ITransformator GetIpInfoTransform() => new IPInfoTransform();
		public static ITransformator GetStatisticsTransform() => new StatisticsTransform();
		public static ITransformator GetArpTransform() => new ArpTransform();
		public static ITransformator GetUPnPTransform() => new UPnPTransform();
	}
}
