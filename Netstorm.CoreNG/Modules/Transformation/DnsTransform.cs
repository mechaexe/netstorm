﻿using NetStorm.Common.Devices;
using System.Net;
using System.Net.Sockets;

namespace NetStorm.CoreNG.Modules.Transformation
{
	public class DnsTransform : ITransformator
	{
		public INetworkDevice TransformDevice(INetworkDevice device, bool overrideCheck)
		{
			if (device.Hostname == null || overrideCheck)
			{
				try
				{
					var id = device.GetHostIdentifier(IpStackMode.IPv4);
					var entry = Dns.GetHostEntry(id);
					if (!string.IsNullOrEmpty(entry.HostName) && entry.HostName != ".")
						device.Hostname = entry.HostName;
					device.Discovered();
					return device;
				}
				catch (SocketException) 
				{
					if (device.Hostname == null)
						device.Hostname = ""; 
				}
			}
			return device;
		}
	}
}
