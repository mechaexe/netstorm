﻿using System.Net.NetworkInformation;
using NetStorm.Common.Devices;
using NetStorm.CoreNG.Modules.Reachability;
using NetStorm.CoreNG.Modules.Reachability.Arp;

namespace NetStorm.CoreNG.Modules.Transformation
{
	public class ArpTransform : ITransformator
	{
		public INetworkDevice TransformDevice(INetworkDevice device, bool overrideCheck)
		{
			var nic = device.NetworkInterface;
			if (nic.Supports(IpStackMode.IPv4) && (device.NetworkInterface.Mac == PhysicalAddress.None || overrideCheck))
			{
				var arp = ReachabilityFactory.GetArpPing(null);
				var res = arp.Send(nic.IPv4Stack.IPAddress.ToString());

				if (res.ArpStatus == ArpStatus.Success)
				{
					nic.Mac = res.PhysicalAddress;
					device.Discovered();
				}
			}
			return device;
		}
	}
}
