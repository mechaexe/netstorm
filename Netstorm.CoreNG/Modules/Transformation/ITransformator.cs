﻿using NetStorm.Common.Devices;

namespace NetStorm.CoreNG.Modules.Transformation
{
	public interface ITransformator
	{
		INetworkDevice TransformDevice(INetworkDevice device, bool overrideCheck);
	}
}
