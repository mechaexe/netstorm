﻿using NetStorm.Common.Devices;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NetStorm.CoreNG.Modules.Transformation
{
	public class CompositeTransformator : ITransformator
	{
		private readonly List<ITransformator> _transformators = new List<ITransformator>();
		public INetworkDevice TransformDevice(INetworkDevice device, bool overrideCheck)
		{
			foreach (var t in _transformators.ToArray())
				device = t.TransformDevice(device, overrideCheck);
			return device;
		}

		public void AddTransformation(ITransformator transformator)
			=> _transformators.Add(transformator);

		public bool RemoveTransformation(Type transformator)
			=> _transformators.RemoveType(transformator);

		public void RemoveAllTransformations()
			=> _transformators.Clear();
	}
}
