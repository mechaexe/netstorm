﻿using NetStorm.Common.Classes;
using NetStorm.Common.Devices;
using NetStorm.Common.Services;
using NetStorm.CoreNG.Modules.ServiceScanner.ServiceDetector;
using Protocols.HTTP;
using System.Xml.Linq;

namespace NetStorm.CoreNG.Modules.Transformation
{
	class UPnPTransform : ITransformator
	{
		public const ushort UPnPPort = 1900;
		public INetworkDevice TransformDevice(INetworkDevice device, bool overrideCheck)
		{
			try
			{
				if (device.Ports.Contains(UPnPPort, Protocol.Udp) && 
					device.Ports[UPnPPort, Protocol.Udp].Service is UPnPService upnp)
				{
					foreach(var loc in upnp.Services)
					{
						if(loc.Value == null || overrideCheck)
						{
							var httpPacket = HttpClient.Get(loc.Key);
							if (httpPacket != null)
							{
								upnp.Services[loc.Key] = XElement.Parse(httpPacket.Content);
								if (string.IsNullOrEmpty(device.Hostname))
									device.Hostname = upnp.Services[loc.Key].GetElementFromPath("device/friendlyName")?.Value;
								var serviceDetector = new HttpServiceDetector();
								var upnpServicePort = new Port((ushort)loc.Key.Port)
								{
									Service = serviceDetector.DetectService(httpPacket.ToString(), null),
									Protocol = Protocol.Tcp,
									Status = PortStatus.Open
								};
								device.Ports.Add(upnpServicePort);
							}
						}
					}
				}
			}
			catch { }
			return device;
		}
	}
}
