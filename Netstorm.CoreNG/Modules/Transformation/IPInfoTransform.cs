﻿using NetStorm.Common.Devices;
using NetStorm.CoreNG.Tools.IpInfo;
using System.Net;

namespace NetStorm.CoreNG.Modules.Transformation
{
	public class IPInfoTransform : ITransformator
	{
		public INetworkDevice TransformDevice(INetworkDevice device, bool overrideCheck)
		{
			if (!device.NetworkInterface.GetPreferredStack(IpStackMode.IPv4).IPAddress.IsPrivate() || overrideCheck)
			{
				device.IPInfo = IpInfoFactory.GetIpApiResolver().ResolveISP(device.NetworkInterface.GetPreferredStack(IpStackMode.IPv4).IPAddress)?.IPInfo;
			}
			return device;
		}
	}
}
