﻿using System;

namespace NetStorm.CoreNG.Modules
{
	public interface IModuleResult
	{
		DateTime ModuleStarted { get; }
		DateTime ModuleStopped { get; }
		TimeSpan ScanDuration => ModuleStopped - ModuleStarted;
	}
}
