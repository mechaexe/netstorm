﻿using NetStorm.Common.Devices;
using NetStorm.Common.Worker;
using System;

namespace NetStorm.CoreNG.Modules
{
	public interface IModule
	{
		string Identifier { get; }
		event EventHandler ModuleStarted, ModuleFinished;
		bool IsReady { get; }
		void SetNic(ILocalNetworkInterface nic);
		ICancellationToken GetCancellationToken();

		bool Prepare();
		void Cleanup();
		void DoWork();
	}
}
