﻿using NetStorm.Common.Classes;
using System;

namespace NetStorm.CoreNG.Modules.Portscanner
{
	public interface IPortscanner : IModuleNG
	{
		event EventHandler<EventArgs<IPort>> PortDetected;
	}
}
