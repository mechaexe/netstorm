﻿using NetStorm.Common.Classes;
using PacketDotNet;
using System;

namespace NetStorm.CoreNG.Modules.Portscanner.Tcp
{
	public abstract class PcapTcpScan : PcapScan
	{
		public bool Synchronize { get; set; }
		public bool Acknowledge { get; set; }
		public bool Urgent { get; set; }
		public bool Push { get; set; }
		public bool Finished { get; set; }
		public bool Reset { get; set; }

		protected readonly PortStatus UnprocessedPortStatus;
		private readonly Random _random;

		protected PcapTcpScan(string identifier, PortStatus assumeUnprocessedPortsAs) : base(identifier)
		{
			_random = new Random();
			UnprocessedPortStatus = assumeUnprocessedPortsAs;
		}

		protected override void CleanUp(PortscanOptions options)
		{
			foreach (var p in options.Ports)
			{
				if (!DetectedPorts.ContainsKey(p))
				{
					OnPortDetected(Port.GetPort(p, Protocol.Tcp, UnprocessedPortStatus));
				}
			}
		}

		protected override TransportPacket CreateL3Packet(ushort lPort, ushort rPort)
		{
			return new TcpPacket(lPort, rPort)
			{
				Synchronize = Synchronize,
				Urgent = Urgent,
				Push = Push,
				Finished = Finished,
				Acknowledgment = Acknowledge,
				Reset = Reset,
				SequenceNumber = (ushort)_random.Next(ushort.MinValue, ushort.MaxValue)
			};
		}

		protected override bool IsValidPacket(Packet p)
			=> base.IsValidPacket(p) &&
				p.PayloadPacket.PayloadPacket is TcpPacket tcp &&
				tcp.DestinationPort == RX_PORT;
	}
}
