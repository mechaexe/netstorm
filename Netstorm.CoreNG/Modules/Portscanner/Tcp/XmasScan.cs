﻿using NetStorm.Common.Classes;

namespace NetStorm.CoreNG.Modules.Portscanner.Tcp

{
	public class XmasScan : NullScan
	{
		public XmasScan(string identifier) : base(identifier, PortStatus.Open | PortStatus.Filtered)
		{
			Urgent = true;
			Push = true;
			Finished = true;
		}
	}
}
