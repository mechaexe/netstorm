﻿using NetStorm.Common.Classes;
using NetStorm.Common.Devices;
using PacketDotNet;
using System;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;

namespace NetStorm.CoreNG.Modules.Portscanner.Tcp
{
	public class ConnectScan : Portscanner
	{
		public ConnectScan(string identifier) : base(identifier) { }

		protected override void ProcessPacket(Packet packet, ILocalNetworkInterface incomingInterface, PortscanOptions options) { }

		protected override void SendPackets(PortscanOptions options, CancellationTokenSource tokenSource)
		{
			try
			{
				var ops = new ParallelOptions() { CancellationToken = tokenSource.Token };
				Parallel.ForEach(options.Ports, ops, (p, state) =>
				{
					if (!ops.CancellationToken.IsCancellationRequested)
					{
						using var socket = new Socket(SocketType.Stream, System.Net.Sockets.ProtocolType.Tcp);
						try
						{
							socket.Connect(options.TargetIp, p);
							var ps = socket.Connected ? PortStatus.Open : PortStatus.Closed;
							socket.Close();
							OnPortDetected(Port.GetPort(p, Protocol.Tcp, ps));
						}
						catch (SocketException) { OnPortDetected(Port.GetPort(p, Protocol.Tcp, PortStatus.Closed)); }
					}

					if (ops.CancellationToken.IsCancellationRequested)
						state.Stop();
				});
			}
			catch(OperationCanceledException) { }
		}
	}
}
