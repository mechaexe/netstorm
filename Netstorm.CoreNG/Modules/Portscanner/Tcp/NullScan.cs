﻿using NetStorm.Common.Classes;
using NetStorm.Common.Devices;
using PacketDotNet;

namespace NetStorm.CoreNG.Modules.Portscanner.Tcp

{
	public class NullScan : PcapTcpScan
	{
		public NullScan(string identifier) : base(identifier, PortStatus.Open | PortStatus.Filtered) { }
		protected NullScan(string identifier, PortStatus assumeUnprocessedPortsAs) : base(identifier, assumeUnprocessedPortsAs) { }

		protected override void ProcessPacket(Packet packet, ILocalNetworkInterface incomingInterface, PortscanOptions options)
		{
			if (IsValidPacket(packet))
			{
				var eth = packet as EthernetPacket;
				var ip = eth.PayloadPacket as IPPacket;
				var tcp = ip.PayloadPacket as TcpPacket;

				if (options.TargetIp.Equals(ip.SourceAddress) && !DetectedPorts.ContainsKey(tcp.SourcePort))
				{
					OnPortDetected(Port.GetPort(tcp.SourcePort, Protocol.Tcp, tcp.Reset ? PortStatus.Closed : UnprocessedPortStatus));
				}
			}
		}
	}
}
