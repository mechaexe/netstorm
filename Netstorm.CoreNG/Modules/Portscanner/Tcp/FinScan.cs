﻿using NetStorm.Common.Classes;

namespace NetStorm.CoreNG.Modules.Portscanner.Tcp
{
	public class FinScan : NullScan
	{
		public FinScan(string identifier) : base(identifier, PortStatus.Open | PortStatus.Filtered)
		{
			Finished = true;
		}
	}
}
