﻿using NetStorm.Common.Classes;
using NetStorm.Common.Devices;
using PacketDotNet;

namespace NetStorm.CoreNG.Modules.Portscanner.Tcp
{
	public class SynScan : PcapTcpScan
	{
		public SynScan(string identifier) : base(identifier, PortStatus.Closed)
		{
			Synchronize = true;
		}

		protected override void ProcessPacket(Packet packet, ILocalNetworkInterface incomingInterface, PortscanOptions options)
		{
			if (IsValidPacket(packet))
			{
				var eth = packet as EthernetPacket;
				var ip = eth.PayloadPacket as IPPacket;
				var tcp = ip.PayloadPacket as TcpPacket;

				if (options.TargetIp.Equals(ip.SourceAddress) && !DetectedPorts.ContainsKey(tcp.SourcePort))
				{
					var portStatus = tcp.Synchronize && tcp.Acknowledgment ? PortStatus.Open : PortStatus.Closed;
					if (portStatus == PortStatus.Open)
					{
						var rstPacket = new TcpPacket(tcp.SourcePort, tcp.DestinationPort)
						{
							Acknowledgment = true,
							Reset = true,
							SequenceNumber = tcp.AcknowledgmentNumber,
							AcknowledgmentNumber = tcp.SequenceNumber + 1
						};

						var transportPacket = AssemblePacket(
							eth.DestinationHardwareAddress,
							eth.SourceHardwareAddress,
							ip.DestinationAddress,
							ip.SourceAddress,
							rstPacket
						);

						transportPacket.UpdateCalculatedValues();
						try { incomingInterface.Transmit(transportPacket); }
						catch { }
					}

					OnPortDetected(Port.GetPort(tcp.SourcePort, Protocol.Tcp, portStatus));
				}
			}
		}
	}
}
