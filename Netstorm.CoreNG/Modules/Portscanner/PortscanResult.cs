﻿using NetStorm.Common.Devices;
using System;

namespace NetStorm.CoreNG.Modules.Portscanner
{
	public class PortscanResult : IModuleResult
	{
		public PortscanResult(DateTime moduleStarted, DateTime moduleStopped, PortscanOptions options, INetworkDevice target)
		{
			ModuleStarted = moduleStarted;
			ModuleStopped = moduleStopped;
			Options = options ?? throw new ArgumentNullException(nameof(options));
			Target = target ?? throw new ArgumentNullException(nameof(target));
		}

		public PortscanOptions Options { get; }
		public DateTime ModuleStarted { get; }
		public DateTime ModuleStopped { get; }
		public INetworkDevice Target { get; }
	}
}
