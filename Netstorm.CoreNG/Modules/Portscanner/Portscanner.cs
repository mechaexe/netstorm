﻿using NetStorm.Common.Classes;
using NetStorm.Common.Devices;
using NetStorm.Common.Worker.PacketReceiver;
using NetStorm.CoreNG.Worker;
using PacketDotNet;
using System;
using System.Collections.Generic;
using System.Threading;

namespace NetStorm.CoreNG.Modules.Portscanner
{
	public abstract class Portscanner : BaseModuleNG, IPortscanner
	{
		public event EventHandler<EventArgs<IPort>> PortDetected;

		protected Dictionary<ushort, IPort> DetectedPorts { get; }
		private PortscanOptions _options;
		private INetworkDevice _scannedDevice;
		private readonly object _portsLock;

		public Portscanner(string identifier) : base(identifier)
		{
			DetectedPorts = new Dictionary<ushort, IPort>();
			_portsLock = new object();
		}

		public void SetOptions(PortscanOptions options)
		{
			if (Running) throw new InvalidOperationException();
			_options = options ?? throw new ArgumentNullException(nameof(options));
			_scannedDevice = _options.DeviceTemplate ?? DeviceFactory.GetNetworkDevice(DeviceFactory.GetInterface(options.TargetIp, options.NextHopMac));
		}

		protected virtual void OnPortDetected(IPort port)
		{
			bool isNew = false;
			lock (_portsLock)
				isNew = DetectedPorts.TryAdd(port.Number, port);

			_scannedDevice.Ports.AddOrReplace(port);

			ThreadPool.QueueUserWorkItem(x =>
			{
				if (_options.ServiceScanner != null && isNew && port.Status != PortStatus.Closed)
					port.Service = _options.ServiceScanner.ScanPort(
						_options.LocalInterface.ToIpEndPoint(_options.StackMode, 0), 
						_scannedDevice.NetworkInterface.ToIpEndPoint(_options.StackMode, port.Number), 
						port.Service);

				if(port.Number == 8080)
					try { }
					catch { }

				if (isNew)
					PortDetected?.Invoke(this, new EventArgs<IPort>(port));
			});
		}

		protected override void ProcessPacket(Packet packet, ILocalNetworkInterface nic)
			=> ProcessPacket(packet, nic, _options);

		protected override void DoWork(bool autoExit, CancellationTokenSource tokenSource)
		{
			try { SendPackets(_options, tokenSource); }
			catch (SharpPcap.DeviceNotReadyException) { Stop(); }
		}

		public PortscanResult GetResult()
			=> new PortscanResult(ModuleStarted, Running ? DateTime.Now : ModuleStopped, _options, _scannedDevice);

		IModuleResult IModuleNG.GetResult()
			=> GetResult();

		protected abstract void SendPackets(PortscanOptions options, CancellationTokenSource tokenSource);
		protected abstract void ProcessPacket(Packet packet, ILocalNetworkInterface incomingInterface, PortscanOptions options);

		public override void Start(IPacketTranceiver tranceiver, IWorkerNG worker)
		{
			if (_options == null) throw new InvalidOperationException("Options are required");
			base.Start(tranceiver, worker);
		}
	}
}
