﻿using NetStorm.Common.Devices;
using NetStorm.CoreNG.Modules.ServiceScanner;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.NetworkInformation;

namespace NetStorm.CoreNG.Modules.Portscanner
{
	public class PortscanOptions
	{
		public INetworkInterface LocalInterface { get; }
		public IServiceScanner ServiceScanner { get; }
		public INetworkDevice DeviceTemplate { get; }
		public IPAddress TargetIp { get; }
		public IpStackMode StackMode => TargetIp.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork ?
			IpStackMode.IPv4 : IpStackMode.IPv6;
		public PhysicalAddress NextHopMac { get; }
		public ICollection<ushort> Ports { get; }

		public PortscanOptions(IPAddress ipAddress, PhysicalAddress macAddress, ILocalNetworkInterface localInterface, ICollection<ushort> ports, IServiceScanner serviceScanner)
		{
			TargetIp = ipAddress ?? throw new ArgumentNullException(nameof(ipAddress));
			NextHopMac = macAddress == null || macAddress == PhysicalAddress.None ? localInterface?.Gateway?.NetworkInterface?.Mac : macAddress;
			Ports = ports ?? throw new ArgumentNullException(nameof(ports));
			ServiceScanner = serviceScanner;
			LocalInterface = localInterface;
		}

		public PortscanOptions(INetworkDevice target, ILocalNetworkInterface localInterface, ICollection<ushort> ports, IServiceScanner serviceScanner, IpStackMode stackMode = IpStackMode.IPv4) : 
			this(target.NetworkInterface.GetPreferredStack(stackMode).IPAddress, target.NetworkInterface.Mac, localInterface, ports, serviceScanner)
		{
			DeviceTemplate = target;
		}
	}
}
