﻿using PacketDotNet;
using System.Net;
using System.Net.NetworkInformation;
using System.Threading;
using System.Threading.Tasks;

namespace NetStorm.CoreNG.Modules.Portscanner
{
	public abstract class PcapScan : Portscanner
	{
		protected const ushort RX_PORT = 61513;

		public PcapScan(string identifier) : base(identifier) { }

		protected async override void SendPackets(PortscanOptions options, CancellationTokenSource tokenSource)
		{
			const int timeoutAfter = 50;
			const int timeout_ms = 250;

			int count = 0;
			try
			{
				var srcMac = options.LocalInterface.Mac;
				foreach (var p in options.Ports)
				{
					var l3 = CreateL3Packet(RX_PORT, p);
					var res = AssemblePacket(
						srcMac,
						options.NextHopMac,
						options.LocalInterface.GetPreferredStack(options.StackMode).IPAddress,
						options.TargetIp,
						l3);

					res.UpdateCalculatedValues();
					Tranceiver.SendPackets(res);
					count = (count + 1) % timeoutAfter;
					if (count == 0)
						await Task.Delay(timeout_ms, tokenSource.Token);
				}
				await Task.Delay(15000, tokenSource.Token);
			}
			catch { }
			finally
			{
				CleanUp(options);
				Stop();
			}
		}

		protected EthernetPacket AssemblePacket(
			PhysicalAddress srcMac,
			PhysicalAddress dstMac,
			IPAddress srcIP,
			IPAddress dstIP,
			TransportPacket l3Packet)
		{
			var eth = new EthernetPacket(srcMac, dstMac, dstIP.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork ? EthernetType.IPv4 : EthernetType.IPv6);

			if (dstIP.AddressFamily == System.Net.Sockets.AddressFamily.InterNetworkV6)
			{
				var ip = new IPv6Packet(srcIP, dstIP)
				{
					PayloadPacket = l3Packet
				};
				UpdateL3Checksum(l3Packet);
				eth.PayloadPacket = ip;
			}
			else
			{
				var ip = new IPv4Packet(srcIP, dstIP)
				{
					PayloadPacket = l3Packet
				};
				UpdateL3Checksum(l3Packet);
				ip.UpdateIPChecksum();
				eth.PayloadPacket = ip;
			}
			eth.UpdateCalculatedValues();
			return eth;
		}

		protected virtual bool IsValidPacket(Packet p)
			=> p is EthernetPacket eth &&
				eth.PayloadPacket is IPPacket ip &&
				(ip.PayloadPacket is TcpPacket || ip.PayloadPacket is UdpPacket);

		private void UpdateL3Checksum(TransportPacket transportPacket)
		{
			var tcp = transportPacket as TcpPacket;
			tcp?.UpdateTcpChecksum();

			var udp = transportPacket as UdpPacket;
			udp?.UpdateUdpChecksum();
		}

		protected abstract void CleanUp(PortscanOptions options);
		protected abstract TransportPacket CreateL3Packet(ushort lPort, ushort rPort);
	}
}
