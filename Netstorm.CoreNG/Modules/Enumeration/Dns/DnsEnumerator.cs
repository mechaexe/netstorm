﻿using DNS.Client;
using DNS.Protocol;
using NetStorm.Common.Worker;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Net;

namespace NetStorm.CoreNG.Modules.Enumeration.Dns
{
	public abstract class DnsEnumerator : BaseModule
	{
		public string Domain { get; set; }
		public IPEndPoint TargetDnsServer { get; set; }
		public INotifyCollectionChanged Responses { get; }
		protected readonly ObservableCollection<Response> responses;

		public DnsEnumerator(string name) : base(name)
		{
			responses = new ObservableCollection<Response>();
			Responses = new ReadOnlyObservableCollection<Response>(responses);
		}

		public override void DoWork()
			=> GetResponses().ToList();

		public IEnumerable<IResponse> GetResponses()
		{
			Prepare();

			responses.Clear();
			ClientRequest request = new ClientRequest(TargetDnsServer)
			{
				RecursionDesired = true,
				OperationCode = OperationCode.Query
			};
			foreach (var nxtDomain in GetResponses(request, Domain))
				yield return nxtDomain;

			Cleanup();
		}

		protected abstract IEnumerable<IResponse> GetResponses(ClientRequest request, string query);
	}
}
