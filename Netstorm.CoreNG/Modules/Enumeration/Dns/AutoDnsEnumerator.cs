﻿using DNS.Client;
using DNS.Protocol;
using NetStorm.CoreNG.PackageExtensions.DNS;
using System.Collections.Generic;

namespace NetStorm.CoreNG.Modules.Enumeration.Dns
{
	public class AutoDnsEnumerator : DnsEnumerator
	{
		public AutoDnsEnumerator(string name) : base(name) { }
		protected override IEnumerable<IResponse> GetResponses(ClientRequest request, string query)
		{
			var scraper = new DnsScraper($"{Identifier} scraper", true)
			{
				Domain = query,
				TargetDnsServer = TargetDnsServer
			};
			scraper.SetNic(Nic);
			foreach (var res in scraper.GetResponses())
				yield return res;

			var walker = new DnsZoneWalker($"{Identifier} walker")
			{
				Domain = query,
				TargetDnsServer = TargetDnsServer
			};
			walker.SetNic(Nic);
			foreach (var domain in walker.GetResponses())
			{
				if(domain.AnswerRecords.Count > 0)
				{
					scraper.Domain = ((NsecResourceRecord)domain.AnswerRecords[0]).NextDomainName;
					if (scraper.Domain.StartsWith("\0")) // \0... indicates white lies, so this loop would go on forever
						break;
					yield return domain;
					foreach (var res in scraper.GetResponses())
						yield return res;
				}
			}
		}
	}
}
