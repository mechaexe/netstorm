﻿using DNS.Client;
using DNS.Protocol;
using System;
using System.Collections.Generic;

namespace NetStorm.CoreNG.Modules.Enumeration.Dns
{
	public class DnsScraper : DnsEnumerator
	{
		public HashSet<RecordType> Types { get; }
		
		public DnsScraper(string name, bool autofill) : base(name)
		{
			if (autofill)
				Types = new HashSet<RecordType>() { RecordType.SOA, RecordType.A, RecordType.AAAA, RecordType.CNAME, RecordType.NS, RecordType.MX };
			else
				Types = new HashSet<RecordType>();
		}
		protected override IEnumerable<IResponse> GetResponses(ClientRequest request, string query)
		{
			request.Id = DateTime.Now.Millisecond;
			foreach(var type in Types)
			{
				request.Questions.Add(new Question(DNS.Protocol.Domain.FromString(query), type));
				var response = GetResponse(request);
				if (response != null)
					yield return response;
				request.Questions.Clear();
			}
		}

		private IResponse GetResponse(ClientRequest request)
		{
			var responseTask = request.Resolve();
			try 
			{ 
				responseTask.Wait();
				return responseTask.Result;
			}
			catch { }
			return null;
		}
	}
}
