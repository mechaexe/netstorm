﻿using DNS.Client;
using DNS.Protocol;
using NetStorm.CoreNG.PackageExtensions.DNS;
using System;
using System.Collections.Generic;

namespace NetStorm.CoreNG.Modules.Enumeration.Dns
{
	public class DnsZoneWalker : DnsEnumerator
	{
		public DnsZoneWalker(string name) : base(name) { }

		private Response GetEntry(ClientRequest request, string query, int tries)
		{
			Response result = null;
			request.Id = DateTime.Now.Millisecond;
			request.Questions.Clear();
			request.Questions.Add(new Question(DNS.Protocol.Domain.FromString(query), (RecordType)47));  //47 NSEC-Type
			for (int i = 0; i < tries && result == null && IsReady; i++)
			{
				var s = request.Resolve();
				try
				{
					s.Wait();
					result = new Response(s.Result);
					if (result.AnswerRecords.Count > 0)
						result.AnswerRecords[0] = new NsecResourceRecord(result.AnswerRecords[0]);
					if (result.ResponseCode == ResponseCode.NoError && 
						result.AnswerRecords.Count > 0 && 
						!string.IsNullOrEmpty(((NsecResourceRecord)result.AnswerRecords[0]).NextDomainName))
						responses.Add(result);
					else result = null;
				}
				catch { GetCancellationToken().CanContinue(100); }
			}
			return result;
		}

		protected override IEnumerable<IResponse> GetResponses(ClientRequest request, string query)
		{
			var initialQuery = query;
			do
			{
				var result = GetEntry(request, query, 3);
				if (result != null && result.AnswerRecords.Count > 0)
				{
					yield return result;
					// Prevent dns zone walk loop as z points back to a
					if (((NsecResourceRecord)result.AnswerRecords[0]).NextDomainName == initialQuery)
						query = null;
					else
						query = ((NsecResourceRecord)result.AnswerRecords[0]).NextDomainName;
				}
			} while (query != null && IsReady);
		}
	}
}
