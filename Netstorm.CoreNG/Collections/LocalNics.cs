﻿using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using NetStorm.Common.Classes;
using NetStorm.Common.Devices;

namespace NetStorm.CoreNG.Collections
{
	public static class LocalNics
	{
		public static IEnumerable<ILocalNetworkInterface> GetActiveNics(IEnumerable<IArpEntry> arpCache)
		{
			string[] excludedIPs = new string[] { "255.255.255.255", "127.0.0.1", "0.0.0.0" };
			foreach (var nic in NetworkInterface.GetAllNetworkInterfaces())
			{
				if (IsOperable(nic))
				{
					var localNic = ParseNic(nic);
					if (!excludedIPs.Contains(localNic.IPv4Stack.IPAddress.ToString()))
					{
						localNic.Gateway = ParseGateway(nic, localNic, arpCache);
						yield return localNic;
					}
				}
			}
		}

		private static INetworkDevice ParseGateway(NetworkInterface lnic, ILocalNetworkInterface parsed, IEnumerable<IArpEntry> arpCache)
		{
			var gws = lnic.GetIPProperties().GatewayAddresses;

			if (gws.Count > 0)
			{
				var nic = DeviceFactory.GetInterface();
				foreach (var addr in gws)
				{
					if (addr.Address.AddressFamily == AddressFamily.InterNetwork)
					{
						nic.IPv4Stack = new IPStack(addr.Address, parsed.IPv4Stack.Network);
					}
					else if (addr.Address.AddressFamily == AddressFamily.InterNetworkV6)
					{
						nic.IPv6Stack = new IPStack(addr.Address, parsed.IPv6Stack.Network);
					}
				}

				foreach (var entry in arpCache)
				{
					if (entry.IPv4Stack.IPAddress.Equals(nic.IPv4Stack.IPAddress))
					{
						nic.Mac = entry.Mac;
						break;
					}
				}
				return DeviceFactory.GetNetworkDevice(nic);
			}
			else return null;
		}

		private static ILocalNetworkInterface ParseNic(NetworkInterface nic)
		{
			var localNic = DeviceFactory.GetLocalNetworkInterface();

			foreach (var unicastAddr in nic.GetIPProperties().UnicastAddresses)
			{
				if (unicastAddr.Address.AddressFamily == AddressFamily.InterNetwork)
				{
					localNic.IPv4Stack = new IPStack(unicastAddr.Address, (byte)unicastAddr.PrefixLength);
				}
				else if (unicastAddr.Address.AddressFamily == AddressFamily.InterNetworkV6)
				{
					localNic.IPv6Stack = new IPStack(unicastAddr.Address, (byte)unicastAddr.PrefixLength);
				}
			}

			localNic.Mac = nic.GetPhysicalAddress();
			localNic.Id = nic.Id;
			localNic.InterfaceName = nic.Name;
			return localNic;
		}

		public static bool IsOperable(NetworkInterface nic)
		{
			return nic.OperationalStatus == OperationalStatus.Up &&
					//nic.GetIPProperties().GatewayAddresses.Count > 0 &&
					nic.Supports(NetworkInterfaceComponent.IPv4);
		}
	}
}
