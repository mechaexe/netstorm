﻿using System.Runtime.InteropServices;

namespace NetStorm.CoreNG
{
	static class NativeMethods
	{
		[DllImport("iphlpapi.dll", ExactSpelling = true)]
		public static extern int SendARP(uint DestIP, uint SrcIP, byte[] pMacAddr, ref int PhyAddrLen);
	}
}
