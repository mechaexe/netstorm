﻿using NetStorm.Common.Devices;
using NetStorm.Common.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace NetStorm.CoreNG.Tools.Caching
{
	public abstract class DeviceEntryCache
	{
		protected abstract string CacheName { get; }
		private readonly List<INetworkInterface> _cache;
		private readonly TimeSpan _maxCacheAge;
		private DateTime _lastCache;

		public DeviceEntryCache()
		{
			_cache = new List<INetworkInterface>();
			_maxCacheAge = new TimeSpan(0, 2, 0);
			Recache();
		}

		public List<INetworkInterface> GetEntries()
		{
			if (_lastCache + _maxCacheAge < DateTime.Now)
				Recache();
			return _cache.ToList();
		}

		public List<INetworkInterface> GetEntries(IPNetwork network)
			=> GetEntries().Where(x => network.Contains(x.IPv4Stack?.IPAddress) || network.Contains(x.IPv6Stack?.IPAddress)).ToList();

		public INetworkInterface GetEntry(string ip)
		{
			var devs = GetEntries().Where(x => x.IPv4Stack != null && x.IPv4Stack.IPAddress.ToString() == ip || x.IPv6Stack != null && x.IPv6Stack.IPAddress.Equals(IPAddress.Parse(ip))).ToArray();
			return devs.Length > 0 ? devs[0] : default;
		}

		public void Recache()
		{
			var cache = ReadCache().ToArray();
			LogEngine.GlobalLog.Log($"[{CacheName}] cached {cache.Length} entities");
			if (cache != null)
			{
				lock (_cache)
				{
					_cache.Clear();
					_cache.AddRange(cache);
				}
				_lastCache = DateTime.Now;
			}
		}

		protected abstract IEnumerable<INetworkInterface> ReadCache();
	}
}
