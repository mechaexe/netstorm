﻿using NetStorm.Common.Devices;
using System.Collections.Generic;

namespace NetStorm.CoreNG.Tools.Caching
{
	public class NdCache : DeviceEntryCache
	{
		protected override string CacheName => "nd cache";

		protected override IEnumerable<INetworkInterface> ReadCache()
		{
			var cache = Cmd.ExecuteNew("netsh", "interface ipv6 show neighbors");
			return cache.Item1 == 0 ?
				CacheParserFactory.GetWindowsNetshIPv6Parser().ParseNdCache(cache.Item2) :
				null;
		}
	}
}
