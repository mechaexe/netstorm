﻿using NetStorm.Common.Devices;
using System.Collections.Generic;

namespace NetStorm.CoreNG.Tools.Caching
{
	public class ArpCache : DeviceEntryCache
	{
		protected override string CacheName => "arp cache";

		protected override IEnumerable<INetworkInterface> ReadCache()
		{
			var cache = ReadArpCache();
			if (cache == null)
				cache = ReadNdCache();
			return cache;
		}

		private IEnumerable<INetworkInterface> ReadArpCache()
		{
			var cache = Cmd.ExecuteNew("arp", "-a");
			return cache.Item1 == 0 ?
				CacheParserFactory.GetWindowsArpParser().ParseArpCache(cache.Item2) :
				null;
		}

		private IEnumerable<INetworkInterface> ReadNdCache()
		{
			var cache = Cmd.ExecuteNew("netsh", "interface ipv4 show neighbors");
			return cache.Item1 == 0 ?
				CacheParserFactory.GetWindowsNetshIPv4Parser().ParseArpCache(cache.Item2) :
				null;
		}
	}
}
