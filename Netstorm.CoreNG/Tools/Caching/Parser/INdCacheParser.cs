﻿using NetStorm.Common.Devices;
using System.Collections.Generic;

namespace NetStorm.CoreNG.Tools.Caching
{
	public interface INdCacheParser
	{
		IEnumerable<INetworkInterface> ParseNdCache(string ndCache);
	}
}