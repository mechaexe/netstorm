﻿using NetStorm.Common.Devices;
using System;
using System.Collections.Generic;
using System.Net.NetworkInformation;

namespace NetStorm.CoreNG.Tools.Caching.Parser.Windows
{
	class WindowsArpCacheParser : IArpCacheParser
	{
		public IEnumerable<INetworkInterface> ParseArpCache(string arpCache)
		{
			var arpEntries = arpCache.ToLower().RemoveEmptyLines().Split('\n');
			foreach (var line in arpEntries)
			{
				var entry = line.RemoveWhitespace(" ").Split(' ');
				if (!entry[0].StartsWith("Int") && entry.Length == 3) //excludes internet as well as interface
				{
					yield return DeviceFactory.GetInterface(entry[0], PhysicalAddressExtension.Parse(entry[1]));
				}
			}
		}
	}
}
