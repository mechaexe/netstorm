﻿using NetStorm.Common.Devices;
using System;
using System.Collections.Generic;
using System.Net.NetworkInformation;

namespace NetStorm.CoreNG.Tools.Caching.Parser.Windows
{
	class WindowsNetshNeighborParser : INdCacheParser, IArpCacheParser
	{
		public IEnumerable<INetworkInterface> ParseArpCache(string arpCache)
		{
			foreach (var p in InternalParse(arpCache))
				yield return DeviceFactory.GetInterface(p.Item1, p.Item2);
		}

		public IEnumerable<INetworkInterface> ParseNdCache(string ndCache)
		{
			foreach (var p in InternalParse(ndCache))
				yield return DeviceFactory.GetInterface(p.Item1, p.Item2);
		}

		private IEnumerable<(string, PhysicalAddress, string)> InternalParse(string cache)
		{
			var arpEntries = cache.RemoveEmptyLines().Split('\n');
			var excludedBeginnings = new[]
			{
				"Int",	//short for int* (eg internet, interface)
				"-"
			};
			foreach (var line in arpEntries)
			{
				var entry = line.RemoveWhitespace(" ").Split(' ');
				if (!entry[0].StartsWithAny(excludedBeginnings) && entry.Length == 3)   // [0] - IP, [1] - Physical Addr, [3] - Type
					yield return (entry[0], PhysicalAddressExtension.Parse(entry[1]), entry[2]);
			}
		}
	}
}
