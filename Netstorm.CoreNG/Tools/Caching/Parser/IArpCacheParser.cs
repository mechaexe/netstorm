﻿using NetStorm.Common.Devices;
using System.Collections.Generic;

namespace NetStorm.CoreNG.Tools.Caching
{
	public interface IArpCacheParser
	{
		IEnumerable<INetworkInterface> ParseArpCache(string arpCache);
	}
}