﻿namespace NetStorm.CoreNG.Tools.Caching
{
	static class CacheParserFactory
	{
		public static IArpCacheParser GetWindowsNetshIPv4Parser() => new Parser.Windows.WindowsNetshNeighborParser();
		public static INdCacheParser GetWindowsNetshIPv6Parser() => new Parser.Windows.WindowsNetshNeighborParser();
		public static IArpCacheParser GetWindowsArpParser() => new Parser.Windows.WindowsArpCacheParser();
	}
}
