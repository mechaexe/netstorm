﻿using System;
using System.Diagnostics;
using NetStorm.Common.Logging;

namespace NetStorm.CoreNG.Tools
{
	public static class Cmd
	{
		public static string Execute(string filename, string args)
			=> ExecuteNew(filename, args).Item2;

		public static (int, string) ExecuteNew(string filename, string args)
		{
			string output = "";
			int exitCode = -1;
			using (Process p = new Process())
			{
				p.StartInfo = new ProcessStartInfo()
				{
					Arguments = args,
					CreateNoWindow = true,
					RedirectStandardOutput = true,
					UseShellExecute = false,
					FileName = filename,
				};
				try
				{
					p.Start();
					while (!p.StandardOutput.EndOfStream)
					{
						output += p.StandardOutput.ReadLine() + "\n";
					}
				}
				catch (Exception e) { LogEngine.GlobalLog.Log("Process execute failed: " + e.Message, LogLevel.Error); }
				exitCode = p.ExitCode;
				p.Dispose();
			}
			return (exitCode, output);
		}
	}
}
