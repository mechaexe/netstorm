﻿using NetStorm.Common.Classes;
using NetStorm.Common.Devices;
using NetStorm.CoreNG.Modules.Transformation;
using System;
using System.Net;
using System.Net.Sockets;

namespace NetStorm.CoreNG.Tools
{
	public static class DeviceInitialiser
	{
		public static INetworkInterface Initialise(IPAddress ip)
		{
			var nic = DeviceFactory.GetInterface();
			switch (ip.AddressFamily)
			{
				case AddressFamily.InterNetwork:
					nic.IPv4Stack = new IPStack(ip);
					break;
				case AddressFamily.InterNetworkV6:
					nic.IPv6Stack = new IPStack(ip);
					break;
			}
			return nic;
		}

		public static INetworkDevice Initialise(string hostOrIp)
		{
			INetworkDevice device;
			if (IPAddress.TryParse(hostOrIp, out var ip))
				device = DeviceFactory.GetNetworkDevice(Initialise(ip));
			else
			{
				device = DeviceFactory.GetNetworkDevice();
				device.Hostname = hostOrIp;
				TransformationFactory.GetIpDnsTransform().TransformDevice(device, true);
			}
			device.Statistics.FirstSeen = DateTime.Now;
			device = TransformationFactory.GetStatisticsTransform().TransformDevice(device, true);
			if (device.NetworkInterface.StackMode != IpStackMode.Disabled)
				device.Discovered();
			return device;
		}
	}
}
