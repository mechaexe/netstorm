﻿using NetStorm.Common.Devices;
using NetStorm.Common.Location;
using Newtonsoft.Json.Linq;
using Protocols.HTTP;
using Protocols.ICMP;
using System;
using System.Linq;
using System.Net;

namespace NetStorm.CoreNG.Tools.IpInfo
{
	class IpApiResolver : IIpInfoResolver
	{
		public InternetServiceProvider ResolveISP(IPAddress bindIP, IPAddress target)
		{
			Uri uri = GetUri(target);
			var data = HttpClient.Get(new IPEndPoint(bindIP, 0), uri);
			return data == null ? null : GetIsp(data);
		}

		public InternetServiceProvider ResolveISP(IPAddress target)
		{
			Uri uri = GetUri(target);
			var data = HttpClient.Get(uri);
			return data == null ? null : GetIsp(data);
		}

		private Uri GetUri(IPAddress address)
			=> new Uri($"http://ip-api.com/json/{address}?lang={LocalEnvironment.Language}");

		private InternetServiceProvider GetIsp(HttpResponse response)
		{
			InternetServiceProvider res = null;
			if(response.StatusCode == 200)
			{
				try
				{
					var json = JObject.Parse(response.Content);
					var ip = IPAddress.Parse(json["query"].ToString());
					var coords = new Coordinates((decimal)json["lon"], (decimal)json["lat"]);
					var loc = new Location(coords, json["country"].ToString(), json["countryCode"].ToString(), json["regionName"].ToString(), json["city"].ToString(), int.Parse(json["zip"].ToString().RemoveWhitespace()), json["timezone"].ToString());
					var usesNAT = TraceRoute.TraceIcmp(ip.ToString(), 2, 500).ToArray().Length > 1;
					var info = new IPInfo(loc, json["isp"].ToString(), json["as"].ToString(), json["org"].ToString());
					res = new InternetServiceProvider(ip, usesNAT, info);
				}
				catch { }
			}
			return res;
		}
	}
}
