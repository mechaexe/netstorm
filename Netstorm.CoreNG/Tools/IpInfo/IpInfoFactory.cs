﻿namespace NetStorm.CoreNG.Tools.IpInfo
{
	public static class IpInfoFactory
	{
		public static IIpInfoResolver GetIpApiResolver() => new IpApiResolver();
		public static IIpInfoResolver GetIpInfoIsResolver() => new IpInfoIsResolver();
	}
}
