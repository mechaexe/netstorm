﻿using NetStorm.Common.Devices;
using System.Net;

namespace NetStorm.CoreNG.Tools.IpInfo
{
	public interface IIpInfoResolver
	{
		InternetServiceProvider ResolveISP(IPAddress target);
		InternetServiceProvider ResolveISP(IPAddress bindIP, IPAddress target);
	}
}