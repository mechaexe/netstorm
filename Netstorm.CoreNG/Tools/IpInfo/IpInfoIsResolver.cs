﻿using NetStorm.Common.Devices;
using NetStorm.Common.Location;
using Newtonsoft.Json.Linq;
using Protocols.HTTP;
using Protocols.ICMP;
using System;
using System.Linq;
using System.Net;

namespace NetStorm.CoreNG.Tools.IpInfo
{
	class IpInfoIsResolver : IIpInfoResolver
	{
		public InternetServiceProvider ResolveISP(IPAddress bindIP, IPAddress target)
		{
			Uri uri = GetUri(target);
			var data = HttpClient.Get(new IPEndPoint(bindIP, 0), uri);
			return data == null ? null : GetIsp(data);
		}

		public InternetServiceProvider ResolveISP(IPAddress target)
		{
			Uri uri = GetUri(target);
			var data = HttpClient.Get(uri);
			return data == null ? null : GetIsp(data);
		}

		private Uri GetUri(IPAddress address)
			=> new Uri($"https://ipinfo.is/{address}");

		private InternetServiceProvider GetIsp(HttpResponse response)
		{
			InternetServiceProvider res = null;
			if(response.StatusCode == 200)
			{
				try
				{
					var json = JObject.Parse(response.Content);
					var ip = IPAddress.Parse(json["ip"].ToString());
					var coords = new Coordinates((decimal)json["longitude"], (decimal)json["latitude"]);
					var loc = new Location(coords, json["country"]["long_name"].ToString(), json["country"]["short_name"].ToString(), json["region"].ToString(), json["city"].ToString(), int.Parse(json["zip_code"].ToString()), json["time_zone"].ToString());
					var usesNAT = TraceRoute.TraceIcmp(ip.ToString(), 2, 500).ToArray().Length > 1;
					var info = new IPInfo(loc, json["domain"].ToString(), "", json["isp"].ToString());
					res = new InternetServiceProvider(ip, usesNAT, info);
				}
				catch { }
			}
			return res;
		}
	}
}
