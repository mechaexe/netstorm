﻿using NetStorm.Common.Devices;
using NetStorm.Common.Logging;
using NetStorm.CoreNG.Collections;
using SharpPcap.LibPcap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;

namespace NetStorm.CoreNG
{
    public sealed class NicManager : IReadOnlyList<ILocalNetworkInterface>
	{
		private const string deviceSeperator = @"\Device\NPF_";

		public static NicManager Instance { get; } = new NicManager();

		public int PrimaryDeviceIndex { get; set; }
		public int Count => interfaces.Count;
		public ILocalNetworkInterface PrimaryDevice => this[PrimaryDeviceIndex];
		public ILocalNetworkInterface this[int index] => interfaces[index];

		public event EventHandler InterfacesChanged;
		public event EventHandler<ILocalNetworkInterface> InterfaceStopped, InterfaceStarted;

		private readonly List<ILocalNetworkInterface> interfaces;

		private NicManager()
		{
			interfaces = new List<ILocalNetworkInterface>();
			PrimaryDeviceIndex = -1;
			NetworkChange.NetworkAddressChanged += NetworkChange_NetworkAddressChanged;
		}

		~NicManager()
		{
			NetworkChange.NetworkAddressChanged -= NetworkChange_NetworkAddressChanged;
			CloseAll();
		}

		public void CloseAll()
		{
			LogEngine.GlobalLog.Log("Closing all nics", LogLevel.Normal);
			lock (this)
			{
				foreach (var nic in this)
				{
					nic.ForceStop();
					nic.CaptureStopped -= CurrentInterface_CaptureStopped;
					nic.CaptureStarted -= CurrentInterface_CaptureStarted;
				}
			}
		}

		public void RefreshNicList()
		{
			LogEngine.GlobalLog.Log("Updating nic list", LogLevel.Normal);

			lock (this)
			{
				CloseAll();

				interfaces.Clear();
				interfaces.AddRange(GetNetworkInterfaces());
				foreach (var nic in this)
				{
					nic.CaptureStopped += CurrentInterface_CaptureStopped;
					nic.CaptureStarted += CurrentInterface_CaptureStarted;
				}
			}
			PrimaryDeviceIndex = Count == 0 ? -1 : 0;
			LogInterfaces();
			InterfacesChanged?.Invoke(this, new EventArgs());
		}

		private void LogInterfaces()
		{
			lock (this)
			{
				foreach (var nic in this)
				{
					LogEngine.GlobalLog.Log($"{nic.InterfaceName};" +
						$" IPv4: {nic.IPv4Stack};" +
						$" IPv6: {nic.IPv6Stack};" +
						$" MAC: {nic.Mac.ToCannonicalString()} ({nic.MacVendor.Manufacturer}); " +
						$" Gateway: {nic.Gateway?.NetworkInterface?.GetPreferredStack(IpStackMode.IPv4)}",
						LogLevel.Normal
					);
				}
			}
		}

		private List<ILocalNetworkInterface> GetNetworkInterfaces()
		{
			var cache = new Tools.Caching.ArpCache().GetEntries();
			var list = LocalNics.GetActiveNics(cache);
			//If exceptions in the following code occurs, its because of our lib :(
			switch (LocalEnvironment.Api)
			{
				case NetworkAPI.LibPcap:
					list = ListPcapNics(list, LibPcapLiveDeviceList.New());
					break;
				default:
					break;
			}
			return list.ToList();
		}

		private IEnumerable<ILocalNetworkInterface> ListPcapNics(IEnumerable<ILocalNetworkInterface> nicList, IEnumerable<PcapDevice> pcapDevices)
		{
			foreach (var pcapNic in pcapDevices)
			{
				foreach (var nic in nicList)
				{
					if (pcapNic.Interface.Name.Split(deviceSeperator)[1] == nic.Id && pcapNic is LibPcapLiveDevice libDev)
					{
						yield return DeviceFactory.GetLocalNetworkInterface(libDev, nic);
						break;
					}
				}
			}
		}

		private void NetworkChange_NetworkAddressChanged(object sender, EventArgs e)
		{
			lock (this)
			{
				NetworkChange.NetworkAddressChanged -= NetworkChange_NetworkAddressChanged;
				LogEngine.GlobalLog.Log("NIC change registered...");

				//Check the need for a refresh
				var refresh = false;
				var newNics = GetNetworkInterfaces();
				if (Count == newNics.Count)
				{
					foreach (var nic in this)
					{
						if (!newNics.Contains(nic))
						{
							refresh = true;
							break;
						}
					}
				}
				else 
					refresh = true;

				//Update nic list
				if (refresh)
					RefreshNicList();

				NetworkChange.NetworkAddressChanged += NetworkChange_NetworkAddressChanged;
			}
		}

		private void CurrentInterface_CaptureStarted(object sender, EventArgs e) => InterfaceStarted?.Invoke(this, ((ILocalNetworkInterface)sender));

		private void CurrentInterface_CaptureStopped(object sender, EventArgs e) => InterfaceStopped?.Invoke(this, ((ILocalNetworkInterface)sender));

		public IEnumerator<ILocalNetworkInterface> GetEnumerator()
			=> interfaces.GetEnumerator();

		System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
			=> GetEnumerator();
	}
}
