﻿using DNS.Protocol;
using DNS.Protocol.ResourceRecords;
using System;

namespace NetStorm.CoreNG.PackageExtensions.DNS
{
	public class NsecResourceRecord : ResourceRecord
	{
		public string NextDomainName { get; }

		public NsecResourceRecord(IResourceRecord record) :
			this(record.Name, record.Data, record.Type, record.Class, record.TimeToLive)
		{ }

		public NsecResourceRecord(Domain domain, byte[] data, RecordType type, RecordClass klass = RecordClass.IN, TimeSpan ttl = default) : 
			base(domain, data, type, klass, ttl)
		{
			if(type == (RecordType)47)
			{
				NextDomainName = "";
				for (int i = 0; i < data.Length && data[i] != 0; i++)
				{
					var domainLen = data[i];
					for (int k = 0; k < domainLen; k++, i++)
						NextDomainName += (char)data[i + 1];
					NextDomainName += ".";
				}
				NextDomainName = NextDomainName.Substring(0, NextDomainName.Length - 1);
			}
		}

		public override string ToString()
		{
			return base.ToString().Replace("}", $", {nameof(NextDomainName)}={NextDomainName}}}");
		}
	}
}
