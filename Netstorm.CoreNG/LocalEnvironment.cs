﻿using System;
using System.Globalization;
using System.Threading;
using NetStorm.Common.Devices;
using NetStorm.Common.Logging;
using SharpPcap;
using SharpPcap.LibPcap;

namespace NetStorm.CoreNG
{
	public static class LocalEnvironment
	{
		public static Subsystem Subsystem => _subsystem;
		public static NetworkAPI Api => _api;
		public static PrivilegeLevel PrivilegeLevel => _privilegeLevel;
		public static string Language => Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName;

		private static Subsystem _subsystem = Subsystem.Posix;
		private static NetworkAPI _api = NetworkAPI.Native;
		private static PrivilegeLevel _privilegeLevel = PrivilegeLevel.User;

		private static bool _alreadyChecked;

		public static void Analyze(bool overrideCheck = false)
		{
			if (_alreadyChecked && !overrideCheck)
			{
				return;
			}

			_alreadyChecked = true;
			_subsystem = GetSubsystem();
			_privilegeLevel = GetAccessLevel();
			_api = GetNetworkApi();

			SetCultureInfo();
			PrintToLog();
		}

		private static void PrintToLog()
		{
			LogEngine.GlobalLog.Log(
				$"System: {_subsystem}; " +
				$"Privilege: {_privilegeLevel}; " +
				$"Api: {_api}; " +
				$"Language: {Language}", LogLevel.Normal);
		}

		/// <summary>
		/// Set cultureinfo for the whole application
		/// </summary>
		private static void SetCultureInfo()
		{
			var lang = Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName.ToLower();
			if (!(lang == "de" || lang == "en"))
			{
				Thread.CurrentThread.CurrentCulture = CultureInfo.GetCultureInfo("en-UK");
				Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo("en-UK");
			}
		}

		/// <summary>
		/// Get the network api (WinPcap/LibPcap)
		/// </summary>
		/// <returns></returns>
		private static NetworkAPI GetNetworkApi()
		{
			//try
			//{
			//	var pcapList = NpcapDeviceList.New();
			//	if (pcapList != null)
			//	{
			//		if (pcapList.Count == 0 || InterfaceAvailable(pcapList[0]))
			//			return NetworkAPI.NPcap;
			//	}
			//}
			//catch { }
			try
			{
				var pcapList = LibPcapLiveDeviceList.New();
				if (pcapList != null)
				{
					if (pcapList.Count == 0 || InterfaceAvailable(pcapList[0]))
					{
						return NetworkAPI.LibPcap;
					}
				}
			}
			catch { }
			return NetworkAPI.Native;
		}

		/// <summary>
		/// Check if a pcap interface is working
		/// </summary>
		/// <param name="device"></param>
		/// <returns></returns>
		private static bool InterfaceAvailable(LibPcapLiveDevice device)
		{
			LogEngine.GlobalLog.Log("Running nic availability check...");
			try
			{
				device.Open(SharpPcap.DeviceModes.Promiscuous);
				device.OnPacketArrival += Device_OnPacketArrival;
				device.StartCapture();
				device.StopCapture();
				device.OnPacketArrival -= Device_OnPacketArrival;
				device.Close();
				LogEngine.GlobalLog.Log("NIC is working properly");
				return true;
			}
			catch (Exception e)
			{
				LogEngine.GlobalLog.Log($"NIC is not available: {e.Message}", LogLevel.Error);
				return false;
			}
		}

		/// <summary>
		/// Get the privilege level (User/Root)
		/// </summary>
		/// <returns></returns>
		private static PrivilegeLevel GetAccessLevel()
		{
			return PrivilegeLevel.User;
		}

		/// <summary>
		/// Get subsystem (WIN/POSIX)
		/// </summary>
		/// <returns></returns>
		private static Subsystem GetSubsystem()
		{
			int p = (int)Environment.OSVersion.Platform;
			return (p == 4) || (p == 6) || (p == 128) ? Subsystem.Posix : Subsystem.Windows;
		}

		/// <summary>
		/// Stub function, to test whether PCAP is working properly
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private static void Device_OnPacketArrival(object sender, SharpPcap.PacketCapture e) { }
	}
}
