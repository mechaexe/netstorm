﻿using System;
using System.Runtime.InteropServices;

namespace NetStorm.UILib
{
	class NativeMethods
	{
		[DllImport("user32.dll", EntryPoint = "ReleaseCapture")]
		public static extern void ReleaseCapture();

		[DllImport("user32.dll", EntryPoint = "SendMessage")]
		public static extern void SendMessage(IntPtr hWnd, int wMsg, int wParam, int lParam);
	}
}
