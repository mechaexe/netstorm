﻿using NetStorm.UILib.Coloring;
using System.Drawing;
using System.Windows.Forms;

namespace NetStorm.UILib.ColorTables
{
	public class MenuItemColorTable : ProfessionalColorTable
	{
		private static ColorScheme ColorScheme { get; } = Colors.GetColorScheme();
		public override Color MenuItemSelected => ColorScheme.PrimaryColors.ToHighlight(ColorScheme.PrimaryColors.BackColor);
		public override Color MenuItemSelectedGradientBegin => MenuItemSelected;
		public override Color MenuItemSelectedGradientEnd => MenuItemSelected;
		public override Color MenuItemBorder => ColorScheme.PrimaryColors.Accent;

		public override Color MenuItemPressedGradientMiddle => ColorScheme.PrimaryColors.ToPressed(ColorScheme.PrimaryColors.BackColor);
		public override Color MenuItemPressedGradientBegin => MenuItemPressedGradientMiddle;
		public override Color MenuItemPressedGradientEnd => MenuItemPressedGradientMiddle;

		public MenuItemColorTable()
		{
			UseSystemColors = false;
		}
	}
}
