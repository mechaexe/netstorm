﻿namespace NetStorm.UILib.Coloring
{
	public class ColorScheme
	{
		public ColorScheme(IColorTable primaryColors, IColorTable secondaryColors, IColorTable accentColors)
		{
			PrimaryColors = primaryColors;
			SecondaryColors = secondaryColors;
			AccentColors = accentColors;
		}

		public IColorTable PrimaryColors { get; }

		public IColorTable SecondaryColors { get; }

		public IColorTable AccentColors { get; }
	}
}

