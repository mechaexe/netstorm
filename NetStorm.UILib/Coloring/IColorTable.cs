﻿using System.Drawing;

namespace NetStorm.UILib.Coloring
{
	public interface IColorTable
	{
		Color Accent { get; }
		Color BackColor { get; }
		Color ForeColor { get; }

		Color ToHighlight(Color color);
		Color ToPressed(Color color);
	}
}