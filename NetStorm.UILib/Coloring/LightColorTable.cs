﻿using System.Drawing;
using System.Windows.Forms;

namespace NetStorm.UILib.Coloring
{
	public class LightColorTable : IColorTable
	{
		public LightColorTable(Color foreColor, Color backColor, Color accent)
		{
			ForeColor = foreColor;
			BackColor = backColor;
			Accent = accent;
		}

		public Color ForeColor { get; }
		public Color BackColor { get; }
		public Color Accent { get; }

		public Color ToHighlight(Color color)
			=> ControlPaint.Dark(ControlPaint.Light(color, 0.2f), 0.0001f);

		public Color ToPressed(Color color)
			=> ControlPaint.Light(ToHighlight(color), 0.2f);
	}
}
