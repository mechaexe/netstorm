﻿using System.Drawing;

namespace NetStorm.UILib
{
	public static class Fonts
	{
		static readonly string DefaultFontLight = "Segoe UI Light";
		static readonly string DefaultFont = "Segoe UI";
		static readonly float TitleSize = 18f;
		static readonly float BigSize = 12f;
		static readonly float DefaultSize = 9.75f;
		static readonly float SmallSize = 8.75f;

		public static readonly Font Title = new Font(DefaultFontLight, TitleSize, FontStyle.Regular, GraphicsUnit.Point, 0, false);

		public static readonly Font Big = new Font(DefaultFont, BigSize, FontStyle.Regular, GraphicsUnit.Point, 0, false);
		public static readonly Font Default = new Font(DefaultFont, DefaultSize, FontStyle.Regular, GraphicsUnit.Point, 0, false);
		public static readonly Font Small = new Font(DefaultFont, SmallSize, FontStyle.Regular, GraphicsUnit.Point, 0, false);

		public static readonly Font Bold = new Font(Small, FontStyle.Bold);
		public static readonly Font Italic = new Font(Small, FontStyle.Italic);
	}
}
