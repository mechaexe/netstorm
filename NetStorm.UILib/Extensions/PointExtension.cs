﻿namespace System.Drawing
{
	static class PointExtension
	{
		public static Point ToPoint(this PointF point)
			=> new Point((int)point.X, (int)point.Y);
	}
}
