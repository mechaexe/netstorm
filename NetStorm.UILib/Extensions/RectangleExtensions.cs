﻿namespace System.Drawing
{
	static class RectangleExtensions
	{
		public static Rectangle ToRectangle(this RectangleF rectangle)
			=> new Rectangle((int)rectangle.X, (int)rectangle.Y, (int)rectangle.Width, (int)rectangle.Height);
	}
}
