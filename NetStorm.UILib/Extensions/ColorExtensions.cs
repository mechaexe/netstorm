﻿using System.Collections.Generic;

namespace System.Drawing
{
	public static class ColorExtensions
	{
		public static Color GetBlendColor(this ICollection<Color> colors)
		{
			byte alpha = 0;
			byte red = 0;
			byte green = 0;
			byte blue = 0;

			foreach (var c in colors)
			{
				alpha += (byte)(c.A / colors.Count);
				red += (byte)(c.R / colors.Count);
				green += (byte)(c.G / colors.Count);
				blue += (byte)(c.B / colors.Count);
			}

			return Color.FromArgb(alpha, red, green, blue);
		}
	}
}
