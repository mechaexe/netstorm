﻿using NetStorm.UILib.Coloring;
using NetStorm.UILib.Features;
using System.Drawing;

namespace NetStorm.UILib.Controls
{
	public interface INetStormControl
	{
		IColorTable ColorTable { get; set; }
		bool MouseEffect { get; set; }
		CornerRadius Radius { get; set; }
		float BorderWidth { get; set; }
		Rectangle ClientRectangle { get; }
	}
}