﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Runtime.Serialization;
using System.Windows.Forms;
using NetStorm.UILib.Features.AutoUpdate;

namespace NetStorm.UILib.Controls
{
	public class AutoUpdateListviewItem<T> : ListViewItem, ISerializable where T : INotifyPropertyChanged
	{
		public delegate Color ColorFormat(T @object);

		public ColorFormat ForeColorFormatter
		{
			get => foreColorFormatter;
			set
			{
				if (!value.Equals(foreColorFormatter))
				{
					foreColorFormatter = value;
					Paint();
				}
			}
		}
		public ColorFormat BackColorFormatter
		{
			get => backColorFormatter;
			set
			{
				if (!value.Equals(backColorFormatter))
				{
					backColorFormatter = value;
					Paint();
				}
			}
		}

		public T Object { get; }

		private bool InvokeRequired => ListView != null && ListView.InvokeRequired;
		private readonly List<IAutoUpdateField> autoUpdateFields = new List<IAutoUpdateField>();
		private ColorFormat foreColorFormatter, backColorFormatter;
		public event EventHandler<T> OnPaint;

		public AutoUpdateListviewItem(T @object)
		{
			Object = @object;
			Object.PropertyChanged += Object_PropertyChanged;
			Render();
		}

		public AutoUpdateListviewItem<T> AddField(IAutoUpdateField field)
		{
			lock (autoUpdateFields)
			{
				autoUpdateFields.Add(field);
			}

			Render();
			field.PropertyChanged += Object_PropertyChanged;
			return this;
		}

		public AutoUpdateListviewItem<T> RemoveField(IAutoUpdateField field)
		{
			var successful = false;
			lock (autoUpdateFields)
			{
				successful = autoUpdateFields.Remove(field);
			}

			if (successful)
			{
				field.PropertyChanged -= Object_PropertyChanged;
			}

			Render();
			return this;
		}

		public void RemoveFields()
		{
			lock (autoUpdateFields)
				autoUpdateFields.Clear();
		}

		protected virtual void Object_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			Render();
			Paint();
			OnPaint?.Invoke(this, Object);
		}

		private void Render()
		{
			try
			{
				if (InvokeRequired)
				{
					ListView.Invoke(new Action(Render));
				}
				else
				{
					DrawContent();
					Paint();
					ListView?.Sort();
					OnPaint?.Invoke(this, Object);
				}
			}
			catch { }
		}

		private void Paint()
		{
			try
			{
				if (InvokeRequired)
				{
					ListView?.Invoke(new Action(Paint));
				}
				else
				{
					if (foreColorFormatter != null)
					{
						ForeColor = foreColorFormatter(Object);
					}

					if (backColorFormatter != null)
					{
						BackColor = backColorFormatter(Object);
					}
				}
			}
			catch { }
		}

		private void DrawContent()
		{
			if (InvokeRequired)
			{
				ListView?.Invoke(new Action(DrawContent));
			}
			else
			{
				SubItems.Clear();

				if (autoUpdateFields.Count > 0)
				{
					Text = autoUpdateFields[0].Value;
					for (int i = 1; i < autoUpdateFields.Count; i++)
					{
						SubItems.Add(autoUpdateFields[i].Value);
					}
				}
			}
		}
	}
}
