﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace NetStorm.UILib.Controls
{
	public class Button : NetStormControl
	{
		public HorizontalAlignment TextAlignment
		{
			get => _textAlignment;
			set
			{
				if (_textAlignment != value)
				{
					_textAlignment = value;
					Invalidate();
				}
			}
		}

		public new string Text
		{
			get => base.Text;
			set
			{
				if(base.Text != value)
				{
					base.Text = value;
					if(autoSize)
						UpdateSize();
				}
			}
		}

		[Browsable(true)]
		public override bool AutoSize
		{
			get => autoSize;
			set
			{
				if (autoSize != value)
				{
					autoSize = value;
					UpdateSize();
				}
			}
		}

		private HorizontalAlignment _textAlignment;
		private bool autoSize;

		public Button()
		{
			MouseEffect = true;
		}

		public void PerformClick(MouseButtons mouseButtons)
		{
			OnMouseClick(new MouseEventArgs(mouseButtons, 1, 0, 0, 0));
		}

		private PointF GetTextOrigin(Graphics g, HorizontalAlignment alignment)
		{
			var measuredText = g.MeasureString(Text, Font);
			var location = new PointF(0, (Height - measuredText.Height) / 2);
			switch (alignment)
			{
				case HorizontalAlignment.Left:
					location.X = Padding.Left;
					break;
				case HorizontalAlignment.Center:
					location.X = (Width - measuredText.Width) / 2;
					break;
				case HorizontalAlignment.Right:
					location.X = Width - measuredText.Width - Padding.Right;
					break;
			}
			return location;
		}

		private Size GetSize(Graphics g)
		{
			if (autoSize)
			{
				var ts = g.MeasureString(Text, Font);
				ts.Width += Padding.Left + Padding.Right;
				ts.Height += Padding.Top + Padding.Bottom;
				return ts.ToSize();
			}
			else
			{
				return Size;
			}
		}

		private void UpdateSize()
		{
			Size = GetSize(CreateGraphics());
			Invalidate();
		}

		protected override void OnFontChanged(EventArgs e)
		{
			UpdateSize();
			base.OnFontChanged(e);
		}

		protected override void OnPaint(PaintEventArgs e)
		{
			var location = GetTextOrigin(e.Graphics, _textAlignment);

			using (var b = new SolidBrush(GetColor(ActiveColorTable.ForeColor)))
			{
				e.Graphics.DrawString(Text, Font, b, location);
			}

			base.OnPaint(e);
		}

		protected override void OnTextChanged(EventArgs e)
		{
			base.OnTextChanged(e);
			Invalidate();
		}
	}
}
