﻿using NetStorm.UILib.Coloring;
using NetStorm.UILib.Features;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace NetStorm.UILib.Controls
{
	public class MultiButton : NetStormControl
	{
		public event EventHandler SelectedIndexChanged;

		public int SelectedIndex
		{
			get => activeIndex;
			set
			{
				if (value >= 0 && value != activeIndex)
				{
					var graphics = CreateGraphics();
					PaintAffectedCells(graphics, activeIndex, value);
					activeIndex = value;
					SelectedIndexChanged?.Invoke(this, new EventArgs());
				}
			}
		}
		public int TextSpacing
		{
			get => textSpacing;
			set
			{
				if (textSpacing != value)
				{
					textSpacing = value;
					Size = CalculateControlSize(CreateGraphics()).ToSize();
					Invalidate();
				}
			}
		}

		public IColorTable CheckedColorTable
		{
			get => checkedColorTable;
			set
			{
				if(value == null || !value.Equals(checkedColorTable))
				{
					checkedColorTable = value;
					Invalidate();
				}
			}
		}

		public IList<string> Buttons { get; }

		public event EventHandler ButtonAdded, ButtonRemoved;

		private int activeIndex, radius, textSpacing;
		private IColorTable checkedColorTable;

		public MultiButton()
		{
			Buttons = new ObservableCollection<string>();
			((ObservableCollection<string>)Buttons).CollectionChanged += MultiButton_CollectionChanged;
			checkedColorTable = new DarkColorTable(Colors.Light, Colors.Dark, Colors.DarkBlue);
			activeIndex = -1;
			radius = 5;
			textSpacing = 10;
			MouseEffect = true;
		}

		private void MultiButton_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
		{
			UpdateSize();
			if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Add)
				ButtonAdded?.Invoke(this, new EventArgs());
			else if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Remove)
				ButtonRemoved?.Invoke(this, new EventArgs());
		}

		protected override void OnMouseClick(MouseEventArgs e)
		{
			var graphics = CreateGraphics();
			var newIndex = GetButtonIndex(e.Location, graphics);

			if (newIndex >= 0)
			{
				SelectedIndex = newIndex;
			}

			base.OnMouseClick(e);
		}

		private SizeF CalculateControlSize(Graphics graphics)
		{
			var res = new SizeF();
			for (int i = 0; i < Buttons.Count; i++)
			{
				var btn = GetButtonRectangle(i, graphics);
				res.Height = Math.Max(res.Height, btn.Height);
				res.Width += btn.Width;
			}
			return res;
		}

		private int GetButtonIndex(Point location, Graphics graphics)
		{
			float currentWidth = 0;

			for (int i = 0; i < Buttons.Count; i++)
			{
				currentWidth += GetButtonRectangle(i, graphics).Width;
				if (location.X <= currentWidth)
				{
					return i;
				}
			}
			return -1;
		}

		private PointF GetTextLocation(int index, RectangleF buttonRect, Graphics graphics)
		{
			var textSize = graphics.MeasureString(Buttons[index], Font);
			var res = new PointF(buttonRect.X + textSpacing, (buttonRect.Height - textSize.Height) / 2);
			if (index == 0)
			{
				res.X += Padding.Left;
			}

			return res;
		}

		private RectangleF GetButtonRectangle(int index, Graphics graphics)
		{
			var rect = new RectangleF();
			if (index == -1)
			{
				return rect;
			}

			if (index == 0)
			{
				// Add padding to most left button width
				rect.Width = Padding.Left;
			}
			if (index == Buttons.Count - 1)
			{
				// Add padding to most right button width
				rect.Width += Padding.Right;
			}
			if (index > 0)
			{
				// Add padding to location from left
				rect.X = Padding.Left;
			}

			for (int i = 0; i < index; i++)
			{
				rect.X += graphics.MeasureString(Buttons[i], Font).Width + textSpacing * 2;
			}

			var textSize = graphics.MeasureString(Buttons[index], Font);
			rect.Width += textSize.Width + textSpacing * 2;
			rect.Height = Math.Max(textSize.Height + Padding.Top + Padding.Bottom, Height);
			return rect;
		}

		#region Drawing
		protected override void OnPaintBackground(PaintEventArgs pevent)
		{
			base.OnPaintBackground(pevent);
			PaintBackgroundInternal(pevent.Graphics);
		}

		protected override void OnPaint(PaintEventArgs e)
		{
			base.OnPaint(e);
			PaintInternal(e.Graphics);
		}

		private void UpdateSize()
		{
			Size = CalculateControlSize(CreateGraphics()).ToSize();
			Invalidate();
		}

		protected override void OnFontChanged(EventArgs e)
		{
			UpdateSize();
			base.OnFontChanged(e);
		}

		private void PaintBackgroundInternal(Graphics g)
		{
			g.CompositingQuality = CompositingQuality.HighQuality;
			g.SmoothingMode = SmoothingMode.AntiAlias;
			using (var gp = RoundedCorners.GetGraphicsPath(ClientRectangle, radius, true))
			{
				if (Parent != null)
				{
					g.Clear(Parent.BackColor);
				}

				using (var b = new SolidBrush(BackColor))
				{
					g.FillPath(b, gp);
				}

				RectangleF buttonRect;
				for (int i = 0; i < Buttons.Count; i++)
				{
					buttonRect = GetButtonRectangle(i, g);
					PaintButtonBG(g, i, i == activeIndex ? CheckedColorTable : ColorTable);
				}
			}
		}

		private void PaintInternal(Graphics g)
		{
			using (var gp = RoundedCorners.GetGraphicsPath(ClientRectangle, radius, true))
			{
				RectangleF buttonRect;
				for (int i = 0; i < Buttons.Count; i++)
				{
					buttonRect = GetButtonRectangle(i, g);
					DrawButtonString(g, buttonRect, i, i == activeIndex ? CheckedColorTable.ForeColor : ColorTable.ForeColor);
				}
			}
		}

		private void PaintButtonBG(Graphics g, int index, IColorTable colorTable)
		{
			var rect = GetButtonRectangle(index, g);
			GraphicsPath gp;
			if(Buttons.Count > 1)
			{
				if (index == 0)
				{
					gp = RoundedCorners.GetGraphicsPath(rect.ToRectangle(), radius, 0, 0, radius, false, true);
				}
				else if (index == Buttons.Count - 1)
				{
					gp = RoundedCorners.GetGraphicsPath(rect.ToRectangle(), 0, radius, radius, 0, true, true);
				}
				else
				{
					gp = RoundedCorners.GetGraphicsPath(rect.ToRectangle(), 0, false, true);
				}
			}
			else gp = RoundedCorners.GetGraphicsPath(rect.ToRectangle(), radius, true, true);

			using (var b = new SolidBrush(GetColor(colorTable, colorTable.BackColor)))
			{
				g.FillPath(b, gp);
				b.Color = colorTable.Accent;
				using (var p = new Pen(b))
				{
					g.DrawPath(p, gp);
				}
			}
			gp.Dispose();
		}

		private void DrawButtonString(Graphics g, RectangleF buttonRect, int index, Color foreColor)
		{
			using (var b = new SolidBrush(foreColor))
			{
				g.DrawString(Buttons[index], Font, b, GetTextLocation(index, buttonRect, g));
			}
		}

		private void PaintButton(Graphics g, int index, IColorTable colorTable)
		{
			if (index >= 0)
			{
				var buttonRect = GetButtonRectangle(index, g);
				PaintButtonBG(g, index, colorTable);
				DrawButtonString(g, buttonRect, index, GetColor(colorTable, colorTable.ForeColor));
			}
		}

		private void PaintAffectedCells(Graphics g, int oldIndex, int newIndex)
		{
			g.CompositingQuality = CompositingQuality.HighQuality;
			g.SmoothingMode = SmoothingMode.AntiAlias;
			using (var gp = RoundedCorners.GetGraphicsPath(ClientRectangle, radius, true))
			{
				PaintButton(g, oldIndex, ColorTable);
				PaintButton(g, newIndex, CheckedColorTable);
			}
		}

		#endregion

	}
}
