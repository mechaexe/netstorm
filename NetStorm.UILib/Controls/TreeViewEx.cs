﻿using System.Drawing;
using System.Windows.Forms;

namespace NetStorm.UILib.Controls
{
	public class TreeViewEx : TreeView
	{
		protected override void OnDrawNode(DrawTreeNodeEventArgs e)
		{
			var textSize = e.Graphics.MeasureString(e.Node.Text, e.Node.NodeFont);

			if (textSize.Width > Width)
			{
				e.Node.Text = TrimText(e.Graphics, e.Node.NodeFont, e.Node.Text);
			}
		}

		private string TrimText(Graphics g, Font f, string text)
		{
			const string ellipsis = "...";

			text = text.Remove(text.Length - 4);
			var ellipsisSize = g.MeasureString(ellipsis, f);
			var textSize = g.MeasureString(text, f);

			if (Width < textSize.Width + ellipsisSize.Width)
			{
				return TrimText(g, f, text);
			}
			else
			{
				return text + ellipsis;
			}
		}
	}
}
