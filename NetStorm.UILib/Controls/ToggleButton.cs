﻿using NetStorm.UILib.Coloring;
using NetStorm.UILib.Features;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace NetStorm.UILib.Controls
{
	[DefaultEvent("CheckedChanged")]
	public class ToggleButton : NetStormControl, ICheckbox
	{
		private const int EM = 16;
		private static readonly Size toggleSize = new Size(2 * EM, EM);
		private static readonly int gripDiameter = 9;

		public bool Checked
		{
			get => _checked;
			set
			{
				if (_checked != value)
				{
					_checked = value;
					_gripRect.Location = GetGripLocation(_gripRect, _toggleRect);
					Invalidate();
					CheckedChanged?.Invoke(this, new EventArgs());
				}
			}
		}

		public IColorTable CheckedColorTable { get; set; }

		private bool _checked;
		private Rectangle _toggleRect, _gripRect;

		public event EventHandler CheckedChanged;

		public ToggleButton()
		{
			CheckedColorTable = new DarkColorTable(Colors.Light, Colors.Green, Colors.Light);
			_toggleRect = new Rectangle(new Point(), toggleSize);
			_gripRect = new Rectangle(0, 0, gripDiameter, gripDiameter);
			Padding = new Padding(0, 0, 10, 0);
		}

		private void DrawGrip(Graphics g)
		{
			var color = Enabled ? CheckedColorTable.ForeColor : CheckedColorTable.ToPressed(CheckedColorTable.ForeColor);

			using (var brush = new SolidBrush(color))
				g.FillEllipse(brush, _gripRect);

			using (var pen = new Pen(CheckedColorTable.Accent, 0.1f))
				g.DrawEllipse(pen, _gripRect);
		}

		private void DrawToggler(Graphics g)
		{
			var backColor = _checked ? CheckedColorTable.BackColor : BackColor;
			backColor = Enabled ? backColor : CheckedColorTable.ToPressed(backColor);
			using (var gp = RoundedCorners.GetGraphicsPath(_toggleRect)) 
			{
				using (var brush = new SolidBrush(backColor))
					g.FillPath(brush, gp);

				using (var pen = new Pen(CheckedColorTable.Accent, 0.1f))
					g.DrawPath(pen, gp);
			}
		}

		private void DrawText(Graphics g)
		{
			var textSize = g.MeasureString(Text, Font);
			var location = new PointF(Padding.Left, (ClientRectangle.Height - textSize.Height) / 2);

			using (var brush = new SolidBrush(ForeColor))
				g.DrawString(Text, Font, brush, location);
		}

		private Point GetGripLocation(Rectangle gripRect, Rectangle toggleRect)
		{
			var location = _toggleRect.Location;
			// Center point to toggle radius center
			location.X += (Checked ? toggleRect.Width - toggleRect.Height / 2 : toggleRect.Height / 2) - 1;

			// Grip offset
			location.X -= gripRect.Width / 2;
			location.Y += (toggleRect.Height - gripRect.Height) / 2;
			return location;
		}

		private Point GetToggleLocation(Rectangle toggleRect, Rectangle controlRect)
			=> new Point(controlRect.Width - toggleRect.Width - Padding.Right,
				(controlRect.Height - toggleRect.Height) / 2);

		private void UpdateToggleLocation()
		{
			_toggleRect.Location = GetToggleLocation(_toggleRect, ClientRectangle);
			_gripRect.Location = GetGripLocation(_gripRect, _toggleRect);
		}

		#region Overrides
		protected override void OnInvalidated(InvalidateEventArgs e)
		{
			UpdateToggleLocation();
			base.OnInvalidated(e);
		}

		protected override void OnClick(EventArgs e)
		{
			Focus();
			Checked = !Checked;
			base.OnClick(e);
		}

		protected override void OnSizeChanged(EventArgs e)
		{
			UpdateToggleLocation();
			base.OnSizeChanged(e);
		}

		protected override void OnPaint(PaintEventArgs e)
		{
			base.OnPaint(e);
			DrawText(e.Graphics);
			e.Graphics.SmoothingMode = SmoothingMode.AntiAlias;
			DrawToggler(e.Graphics);
			DrawGrip(e.Graphics);
		}
		#endregion
	}
}
