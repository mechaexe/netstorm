﻿using NetStorm.UILib.Coloring;
using NetStorm.UILib.Features;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace NetStorm.UILib.Controls
{
	public class Board : Panel, INetStormControl
	{
		public CornerRadius Radius
		{
			get => _radius;
			set
			{
				if (!_radius.Equals(value))
				{
					_radius = value;
					Invalidate();
				}
			}
		}
		public bool WordWrap
		{
			get => _wrapWords;
			set
			{
				if (_wrapWords != value)
				{
					_wrapWords = value;
					Invalidate();
				}
			}
		}
		public IColorTable ColorTable
		{
			get => _colorScheme;
			set
			{
				if(value == null || !value.Equals(_colorScheme))
				{
					_colorScheme = value;
					BackColor = _colorScheme.BackColor;
					ForeColor = _colorScheme.ForeColor;
				}
			}
		}
		public bool MouseEffect { get; set; }
		public float BorderWidth
		{
			get => _borderWidth;
			set
			{
				if (_borderWidth != value)
				{
					_borderWidth = value;
					Invalidate();
				}
			}
		}

		private INotifyPropertyChanged _observable;
		private StringDelegate _getTitle, _getContent;
		private bool _wrapWords, _hasFocus, _isMouseDown;
		private CornerRadius _radius;
		private int _textHash;
		private float _borderWidth;
		private IColorTable _colorScheme;

		public Board()
		{
			_getTitle = () => "";
			_getContent = () => "";
			_colorScheme = new DarkColorTable(Colors.Light, Colors.Dark, Colors.DarkBlue);
		}

		public void SetObservable(INotifyPropertyChanged observable)
		{
			if (this._observable != null)
				this._observable.PropertyChanged -= Observable_PropertyChanged;

			this._observable = observable;
			this._observable.PropertyChanged += Observable_PropertyChanged;
			Invalidate();
		}

		private void Observable_PropertyChanged(object sender, PropertyChangedEventArgs e)
			=> UpdateInternal();

		public void SetTitle(StringDelegate title)
		{
			_getTitle = title;
			UpdateInternal();
		}

		public void SetContent(StringDelegate content)
		{
			_getContent = content;
			UpdateInternal();
		}

		private void UpdateInternal()
		{
			var newHash = (_getTitle?.Invoke() + _getContent?.Invoke()).GetHashCode();
			if (_textHash != newHash)
			{
				_textHash = newHash;
				Invalidate();
			}
		}

		protected virtual Color GetColor(IColorTable colorTable, Color baseColor)
		{
			if (Enabled && MouseEffect)
			{
				if (_isMouseDown)
					return colorTable.ToPressed(baseColor);
				else if (_hasFocus)
					return colorTable.ToHighlight(baseColor);
			}
			return baseColor;
		}

		protected virtual Color GetColor(Color baseColor)
			=> GetColor(_colorScheme, baseColor);

		protected override void OnPaintBackground(PaintEventArgs pevent)
		{
			base.OnPaintBackground(pevent);

			if (Radius.HasRadius && Parent != null)
			{
				using (var b = new SolidBrush(Parent.BackColor))
				{
					pevent.Graphics.FillRectangle(b, ClientRectangle);
				}
			}

			using (var b = new SolidBrush(GetColor(BackColor)))
			{
				if (Radius.HasRadius)
				{
					using (var gp = RoundedCorners.GetGraphicsPath(ClientRectangle, Radius, true, true))
					{
						pevent.Graphics.SmoothingMode = SmoothingMode.AntiAlias;
						pevent.Graphics.FillPath(b, gp);
						if (BorderWidth > 0)
							using (var pen = new Pen(ColorTable.Accent, BorderWidth))
								pevent.Graphics.DrawPath(pen, gp);
						pevent.Graphics.SmoothingMode = SmoothingMode.Default;
					}
				}
				else
				{
					pevent.Graphics.FillRectangle(b, ClientRectangle);
					if (BorderWidth > 0)
						using (var pen = new Pen(ColorTable.Accent, BorderWidth))
							pevent.Graphics.DrawRectangle(pen, ClientRectangle);
				}
			}
		}

		protected override void OnPaint(PaintEventArgs e)
		{
			base.OnPaint(e);

			var title = WordWrap ? WrapString(e.Graphics, Text + _getTitle(), Font) : Text + _getTitle();
			var titleSize = e.Graphics.MeasureString(title, Font);
			var titleLocation = new PointF(Padding.Left, Padding.Top);

			var content = WordWrap ? WrapString(e.Graphics, _getContent(), Font) : _getContent();
			var contentLocation = new PointF(Padding.Left, titleSize.Height + Padding.Top + Font.Height / 2);

			e.Graphics.SmoothingMode = SmoothingMode.AntiAlias;
			using (var b = new SolidBrush(GetColor(ForeColor)))
			{
				e.Graphics.DrawString(title, Font, b, titleLocation);
				e.Graphics.DrawString(content, Font, b, contentLocation);
				if(!string.IsNullOrEmpty(title))
				{
					using (var p = new Pen(b))
					{
						e.Graphics.DrawLine(p,
							new PointF(Padding.Left, Padding.Top + titleSize.Height),
							new PointF(Width - Padding.Right, Padding.Top + titleSize.Height));
					}
				}
			}
			e.Graphics.SmoothingMode = SmoothingMode.None;
		}

		private string WrapString(Graphics g, string text, Font f)
		{
			var desiredWidth = Width - Padding.Right - Padding.Left;
			string res = text;
			if (g.MeasureString(text, f).Width > desiredWidth)
			{
				foreach (char c in text)
				{
					if (g.MeasureString(res, f).Width > desiredWidth)
					{
						var words = res.Split(' ');
						res = "";
						for (int i = 0; i < words.Length - 1; i++)
						{
							res += words[i] + " ";
						}

						res += "\n" + words[words.Length - 1];
					}
					res += c;
				}
			}
			return res;
		}

		#region Overrides
		protected override void OnMouseUp(MouseEventArgs e)
		{
			CheckSetClickEffect(false);
			base.OnMouseUp(e);
		}
		protected override void OnMouseDown(MouseEventArgs e)
		{
			CheckSetClickEffect(true);
			base.OnMouseDown(e);
		}

		protected override void OnGotFocus(EventArgs e)
		{
			CheckSetFocusEffect(true);
			base.OnGotFocus(e);
		}

		protected override void OnMouseEnter(EventArgs e)
		{
			CheckSetFocusEffect(true);
			base.OnMouseEnter(e);
		}

		protected override void OnLostFocus(EventArgs e)
		{
			CheckSetFocusEffect(false);
			base.OnLostFocus(e);
		}

		protected override void OnMouseLeave(EventArgs e)
		{
			CheckSetFocusEffect(false);
			base.OnMouseLeave(e);
		}

		private void CheckSetFocusEffect(bool b)
		{
			if (MouseEffect)
			{
				_hasFocus = b;
				Invalidate();
			}
		}
		private void CheckSetClickEffect(bool b)
		{
			if (MouseEffect)
			{
				_isMouseDown = b;
				Invalidate();
			}
		}
		#endregion
	}
}
