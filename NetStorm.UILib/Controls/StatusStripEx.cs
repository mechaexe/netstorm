﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace NetStorm.UILib.Controls
{
	public class StatusStripEx : StatusStrip
	{
		private readonly int gripSize = 16;
		private Rectangle gripRect;

		public StatusStripEx()
		{
			DoubleBuffered = true;
			SetStyle(ControlStyles.OptimizedDoubleBuffer | ControlStyles.ResizeRedraw | ControlStyles.UserPaint | ControlStyles.UserMouse, true);
		}

		protected override void OnClientSizeChanged(EventArgs e)
		{
			base.OnClientSizeChanged(e);
			if (Parent != null && Parent is Form f)
			{
				if (f.WindowState == FormWindowState.Normal)
				{
					gripRect = new Rectangle(Width - gripSize, Height - gripSize, gripSize, gripSize);
				}
				else
				{
					gripRect = new Rectangle();
				}
			}
		}

		protected override void WndProc(ref Message m)
		{
			const int WM_NCHITTEST = 0x0084;
			const int HTTRANSPARENT = -1;

			if (m.Msg == WM_NCHITTEST )
			{
				m.Result = (IntPtr)HTTRANSPARENT;
			}
			else
			{
				base.WndProc(ref m);
			}
		}
	}
}
