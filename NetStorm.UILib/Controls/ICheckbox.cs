﻿using NetStorm.UILib.Coloring;

namespace NetStorm.UILib.Controls
{
	public interface ICheckbox : INetStormControl
	{
		IColorTable CheckedColorTable { get; set; }
		bool Checked { get; set; }
	}
}