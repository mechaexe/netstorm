﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace NetStorm.UILib.Controls
{
	public class CueTextbox : TextBox
	{
		public event EventHandler CueTextChanged;
		private string _cueText;

		public string CueText
		{
			get { return _cueText; }
			set
			{
				value = value ?? string.Empty;
				if (value != _cueText)
				{
					_cueText = value;
					OnCueTextChanged(EventArgs.Empty);
				}
			}
		}
		public int CueTextY { get; set; } = 2;

		public CueTextbox()
			: base()
		{
			_cueText = string.Empty;
			BorderStyle = BorderStyle.FixedSingle;
			SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
		}

		protected virtual void OnCueTextChanged(EventArgs e)
		{
			Invalidate(true);
			CueTextChanged?.Invoke(this, new EventArgs());
		}

		protected override void OnPaint(PaintEventArgs e)
		{
			base.OnPaint(e);
			e.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;

			if (string.IsNullOrEmpty(Text.Trim()) && !string.IsNullOrEmpty(CueText) && !Focused)
			{
				var startingPoint = GetStartingPoint(e.Graphics);
				var format = new System.Drawing.StringFormat();
				Font font = new Font(Font.FontFamily.Name, Font.Size, FontStyle.Italic);
				if (RightToLeft == RightToLeft.Yes)
				{
					format.LineAlignment = StringAlignment.Far;
					format.FormatFlags = StringFormatFlags.DirectionRightToLeft;
				}

				using(var b = new SolidBrush(ControlPaint.Dark(ForeColor, 0.0001f)))
					e.Graphics.DrawString(CueText, font, b, startingPoint, format);
			}
		}

		private PointF GetStartingPoint(Graphics g)
		{
			var stringSize = g.MeasureString(CueText, Font);

			var pad = 3;

			switch (TextAlign)
			{
				case HorizontalAlignment.Center:
					return new PointF(Width / 2 - stringSize.Width / 2, CueTextY);
				case HorizontalAlignment.Left:
					return new PointF(pad, CueTextY);
				case HorizontalAlignment.Right:
					return new PointF(Width - pad - stringSize.Width, CueTextY);
			}
			return new PointF();
		}

		const int WM_PAINT = 0x000F;
		protected override void WndProc(ref Message m)
		{
			base.WndProc(ref m);
			if (m.Msg == WM_PAINT)
			{
				OnPaint(new PaintEventArgs(Graphics.FromHwnd(m.HWnd), ClientRectangle));
			}
		}
	}
}
