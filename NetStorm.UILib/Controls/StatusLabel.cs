﻿using NetStorm.UILib.Features;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace NetStorm.UILib.Controls
{
	public class StatusLabel : NetStormControl
	{
		public int IndicatorSize
		{
			get => _indicator.Width;
			set
			{
				if (_indicator.Width != value)
				{
					_indicator.Size = new Size(value, value);
					UpdateLabelSizeAndLocation();
					Invalidate();
				}
			}
		}
		public Color IndicatorColor
		{
			get => _indicatorColor;
			set
			{
				if (_indicatorColor != value)
				{
					_indicatorColor = value;
					Invalidate(_indicator);
				}
			}
		}
		
		[Browsable(true)]
		public new string Text
		{
			get => _label.Text;
			set
			{
				if(Text != value)
				{
					_label.Text = value;
					UpdateLabelSizeAndLocation();
				}
			}
		}
		public bool AutoEllipsis
		{
			get => _label.AutoEllipsis;
			set => _label.AutoEllipsis = value;
		}
		private Color _indicatorColor;
		private Rectangle _indicator;
		private readonly Label _label;

		public StatusLabel()
		{
			_indicator = new Rectangle();
			_indicatorColor = Color.White;
			_label = new Label() { AutoSize = true, AutoEllipsis = true, Padding = new Padding(0, 0, 5, 0), Margin = new Padding(0), TextAlign = ContentAlignment.MiddleLeft };
			Controls.Add(_label);
		}

		protected override void OnForeColorChanged(EventArgs e)
		{
			base.OnForeColorChanged(e);
			_label.ForeColor = ForeColor;
		}

		protected override void OnBackColorChanged(EventArgs e)
		{
			base.OnBackColorChanged(e);
			_label.BackColor = BackColor;
		}

		protected override void OnFontChanged(EventArgs e)
		{
			base.OnFontChanged(e);
			_label.Font = Font;
			UpdateLabelSizeAndLocation();
		}

		protected override void OnClientSizeChanged(EventArgs e)
		{
			base.OnClientSizeChanged(e);
			UpdateLabelSizeAndLocation();
		}

		protected override void OnSizeChanged(EventArgs e)
		{
			base.OnSizeChanged(e);
			UpdateLabelSizeAndLocation();
		}

		protected override void OnPaintBackground(PaintEventArgs pevent)
		{
			base.OnPaintBackground(pevent);

			if(IndicatorSize > 0)
			{
				_indicator.Location = GetIndicatorLocation();
				pevent.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
				using (var gp = RoundedCorners.GetGraphicsPath(_indicator))
				using (var b = new SolidBrush(_indicatorColor))
					pevent.Graphics.FillPath(b, gp);

				pevent.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.Default;
			}
		}

		private void UpdateLabelSizeAndLocation()
		{
			_label.Location = GetLabelLocation();
			_label.MaximumSize = new Size(
				Width - _label.Location.X - Padding.Right,
				Font.Height);
		}

		private Point GetLabelLocation()
		{
			var res = GetIndicatorLocation();
			res.X += IndicatorSize > 0 ? IndicatorSize + 5 : 0;
			res.Y = (Height - _label.Height) / 2 - 1;
			return res;
		}

		private Point GetIndicatorLocation()
			=> new Point()
			{
				X = Padding.Left,
				Y = (Height - _indicator.Height) / 2
			};
	}
}
