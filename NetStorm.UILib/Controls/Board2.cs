﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace NetStorm.UILib.Controls
{
	public class Board2 : NSPanel
	{
		public new string Text
		{
			get => _contentLabel.Text;
			set
			{
				_contentLabel.Text = value;
				_contentLabel.Visible = !string.IsNullOrEmpty(value);
				UpdateLabelSize();
				if (_autoHeight)
					UpdateHeight();
			}
		}
		public string TitleText
		{
			get => _titleLabel.Text;
			set
			{
				_titleLabel.Text = value;
				UpdateLabels();
			}
		}
		public bool AutoHeight
		{
			get => _autoHeight;
			set
			{
				if(AutoHeight != value)
				{
					_autoHeight = value;
					UpdateLabelSize();
					if (value)
						UpdateHeight();
				}
			}
		}
		public bool ShowTitle
		{
			get => _showTitle;
			set
			{
				if(_showTitle != value)
				{
					_separator.Visible = _titleLabel.Visible = _showTitle = value;
					UpdateLabels();
				}
			}
		}

		private bool _autoHeight, _showTitle;
		private readonly Label _titleLabel, _contentLabel;
		private readonly Control _separator;

		public Board2()
		{
			_autoHeight = false;
			_showTitle = true;
			DoubleBuffered = true;
			
			SetStyle(ControlStyles.OptimizedDoubleBuffer, true);

			_separator = new Control() { Height = 1 };
			_titleLabel = new Label() { AutoSize = false, AutoEllipsis = true };
			_contentLabel = new Label() { AutoSize = true, AutoEllipsis = true };

			Controls.Add(_titleLabel);
			Controls.Add(_separator);
			Controls.Add(_contentLabel);

			foreach(Control c in Controls)
			{
				c.MouseHover += (_, __) => OnMouseHover(__);
				c.MouseEnter += (_, __) => OnMouseEnter(__);
				c.MouseLeave += (_, __) => OnMouseLeave(__);
				c.MouseDown += (_, __) => OnMouseDown(new MouseEventArgs(__.Button, __.Clicks, PointToClient(Cursor.Position).X, PointToClient(Cursor.Position).Y, __.Delta));
				c.MouseUp += (_, __) => OnMouseUp(new MouseEventArgs(__.Button, __.Clicks, PointToClient(Cursor.Position).X, PointToClient(Cursor.Position).Y, __.Delta));
				c.MouseClick += (_, __) => OnMouseClick(new MouseEventArgs(__.Button, __.Clicks, PointToClient(Cursor.Position).X, PointToClient(Cursor.Position).Y, __.Delta));
				c.MouseDoubleClick += (_, __) => OnMouseDoubleClick(new MouseEventArgs(__.Button, __.Clicks, PointToClient(Cursor.Position).X, PointToClient(Cursor.Position).Y, __.Delta));
				c.Click += (_, __) => OnClick(__); 
			}
		}

		private void UpdateLabels()
		{
			UpdateLabelLocation();
			UpdateLabelSize();
			if (_autoHeight)
				UpdateHeight();
		}

		private void UpdateLabelSize()
		{
			_titleLabel.Size = new Size(Width - Padding.Left - Padding.Right, _titleLabel.Height);
			_separator.Width = _titleLabel.Width;
			var maxContentSize = new Size(_titleLabel.Size.Width, MaximumSize.Height - Padding.Top - Padding.Bottom - (ShowTitle ? _titleLabel.Height : 0));
			if(Controls.Count > 2)
				foreach (Control c in Controls)
					if(c != _titleLabel && c != _contentLabel)
						maxContentSize.Height -= c.Height - c.Margin.Top - c.Margin.Bottom;
			_contentLabel.MaximumSize = maxContentSize;
		}

		private void UpdateHeight()
		{
			var accHeight = Padding.Bottom + (ShowTitle ? _titleLabel.Height + Padding.Top : 0);
			foreach (Control c in Controls)
				if(c != _titleLabel)
					accHeight += c.Height + Padding.Top;
			Height = accHeight;
		}

		private void UpdateLabelLocation()
		{
			if (!ShowTitle)
				_contentLabel.Location = new Point(Padding.Left, Padding.Top);
			else
				_contentLabel.Location = new Point(Padding.Left, Padding.Top * 2 + _titleLabel.Height);

			_titleLabel.Location = new Point(Padding.Left, Padding.Top);
			_separator.Location = new Point(Padding.Left, _titleLabel.Location.Y + _titleLabel.Height);
		}

		#region Overrides
		protected override void OnParentChanged(EventArgs e)
		{
			base.OnParentChanged(e);
			UpdateLabelSize();
			if (AutoHeight)
				UpdateHeight();
		}

		protected override void OnPaddingChanged(EventArgs e)
		{
			base.OnPaddingChanged(e);
			UpdateLabelLocation();
		}

		protected override void OnClientSizeChanged(EventArgs e)
		{
			base.OnClientSizeChanged(e);
			UpdateLabelSize();
		}

		protected override void OnSizeChanged(EventArgs e)
		{
			base.OnSizeChanged(e);
			UpdateLabelSize();
		}

		protected override void OnBackColorChanged(EventArgs e)
		{
			base.OnBackColorChanged(e);
			_titleLabel.BackColor =
				_contentLabel.BackColor = BackColor;
			_separator.BackColor = ForeColor;
		}

		protected override void OnForeColorChanged(EventArgs e)
		{
			base.OnForeColorChanged(e);
			_titleLabel.ForeColor =
				_contentLabel.ForeColor =
				_separator.BackColor = ForeColor;
		}

		protected override void OnFontChanged(EventArgs e)
		{
			base.OnFontChanged(e);
			UpdateLabelLocation();
			UpdateLabelSize();
		}
		#endregion
	}
}
