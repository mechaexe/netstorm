﻿using NetStorm.UILib.Coloring;
using NetStorm.UILib.Features;
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace NetStorm.UILib.Controls
{
	public class NSFlowLayoutPanel : FlowLayoutPanel, INetStormControl
	{
		public float BorderWidth
		{
			get => _borderWidth;
			set
			{
				if (_borderWidth != value)
				{
					_borderWidth = value;
					Invalidate();
				}
			}
		}
		public CornerRadius Radius
		{
			get => _radius;
			set
			{
				if (!_radius.Equals(value))
				{
					_radius = value;
					Invalidate();
				}
			}
		}
		public IColorTable ColorTable
		{
			get => _originalColorTable;
			set
			{
				if (value == null || !value.Equals(_originalColorTable))
				{
					_originalColorTable = value;
					SetActiveColorTable(value);
				}
			}
		}
		public bool MouseEffect { get; set; }

		protected bool HasFocus { get; private set; }
		protected bool IsMouseOver { get; private set; }
		protected bool IsMouseDown { get; private set; }
		protected IColorTable ActiveColorTable { get; private set; }

		private float _borderWidth;
		private CornerRadius _radius;
		private IColorTable _originalColorTable;

		public NSFlowLayoutPanel()
		{
			ActiveColorTable = _originalColorTable = new DarkColorTable(Colors.Light, Colors.Dark, Colors.DarkBlue);
			DoubleBuffered = true;
			SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
		}

		protected virtual Color GetColor(IColorTable colorTable, Color baseColor)
		{
			if (Enabled && MouseEffect && (IsMouseOver || HasFocus))
				return colorTable.ToHighlight(baseColor);
			return baseColor;
		}

		protected virtual Color GetColor(Color baseColor)
			=> GetColor(ActiveColorTable, baseColor);

		protected void SetActiveColorTable(IColorTable colorTable)
		{
			ActiveColorTable = colorTable;
			UpdateColors();
		}

		protected virtual void UpdateColors()
		{
			BackColor = GetColor(ActiveColorTable.BackColor);
			ForeColor = GetColor(ActiveColorTable.ForeColor);
		}

		protected override void OnMouseEnter(EventArgs e)
		{
			IsMouseOver = true;
			base.OnMouseEnter(e);
			UpdateColors();
		}

		protected override void OnMouseLeave(EventArgs e)
		{
			IsMouseOver = false;
			base.OnMouseLeave(e);
			UpdateColors();
		}

		protected override void OnMouseDown(MouseEventArgs e)
		{
			IsMouseDown = true;
			base.OnMouseDown(e);
			UpdateColors();
		}

		protected override void OnMouseUp(MouseEventArgs e)
		{
			IsMouseDown = false;
			base.OnMouseUp(e);
			UpdateColors();
		}

		protected override void OnGotFocus(EventArgs e)
		{
			HasFocus = true;
			base.OnGotFocus(e);
			UpdateColors();
		}

		protected override void OnLostFocus(EventArgs e)
		{
			HasFocus = false;
			base.OnLostFocus(e);
			UpdateColors();
		}

		protected override void OnPaintBackground(PaintEventArgs pevent)
		{
			base.OnPaintBackground(pevent);

			if (Radius.HasRadius && Parent != null)
			{
				using (var b = new SolidBrush(Parent.BackColor))
				{
					pevent.Graphics.FillRectangle(b, ClientRectangle);
				}
			}

			using (var b = new SolidBrush(BackColor))
			{
				if (Radius.HasRadius)
				{
					using (var gp = RoundedCorners.GetGraphicsPath(ClientRectangle, Radius, Radius.MaxRightRadius != 0, Radius.BottomLeft != 0 || Radius.BottomRight != 0))
					{
						pevent.Graphics.SmoothingMode = SmoothingMode.AntiAlias;
						pevent.Graphics.FillPath(b, gp);
						if (BorderWidth > 0)
							using (var pen = new Pen(ActiveColorTable.Accent, BorderWidth))
								pevent.Graphics.DrawPath(pen, gp);
						pevent.Graphics.SmoothingMode = SmoothingMode.Default;
					}
				}
				else
				{
					pevent.Graphics.FillRectangle(b, ClientRectangle);
					if (BorderWidth > 0)
						using (var pen = new Pen(ActiveColorTable.Accent, BorderWidth))
							pevent.Graphics.DrawRectangle(pen, ClientRectangle);
				}
			}
		}
	}
}
