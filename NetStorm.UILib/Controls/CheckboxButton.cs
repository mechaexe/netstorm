﻿using NetStorm.UILib.Coloring;
using System;
using System.ComponentModel;

namespace NetStorm.UILib.Controls
{
	[DefaultEvent("CheckedChanged")]
	public class CheckboxButton : Button, ICheckbox
	{
		public bool Checked
		{
			get => _isChecked;
			set
			{
				if (_isChecked != value)
				{
					_isChecked = value;
					if (_isChecked)
						SetActiveColorTable(CheckedColorTable);
					else
						SetActiveColorTable(ColorTable);
					CheckedChanged?.Invoke(this, new EventArgs());
				}
			}
		}

		public IColorTable CheckedColorTable { get; set; }
		private bool _isChecked;

		public event EventHandler CheckedChanged;

		public CheckboxButton()
		{
			CheckedColorTable = new DarkColorTable(Colors.Light, Colors.Dark, Colors.DarkBlue);
		}
	}
}
