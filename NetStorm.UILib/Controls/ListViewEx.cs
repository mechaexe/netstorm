﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace NetStorm.UILib.Controls
{
	public class ListViewEx : ListView
	{
		public bool AutoSizeLastColumn { get; set; }

		public ListViewEx()
		{
			AutoSizeLastColumn = true;
			DoubleBuffered = true;
			SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
		}

		//protected override void OnDrawColumnHeader(DrawListViewColumnHeaderEventArgs e)
		//{
		//	using (var b = new SolidBrush(BackColor))
		//	{
		//		e.Graphics.FillRectangle(b, e.Bounds);
		//	}

		//	using (var b = new SolidBrush(ForeColor))
		//	{
		//		e.Graphics.DrawString(e.Header.Text, Font, b, e.Bounds);
		//	}
		//}

		private void ResizeLastColumn()
		{
			if (View == View.Details &&
				AutoSizeLastColumn &&
				Columns.Count > 0)
			{
				int width = 0;
				for (int i = 0; i < Columns.Count - 1; ++i)
					width += Columns[i].Width;

				Columns[Columns.Count - 1].Width = Width - width - SystemInformation.VerticalScrollBarWidth;
			}
		}

		protected override void OnColumnWidthChanging(ColumnWidthChangingEventArgs e)
		{
			base.OnColumnWidthChanging(e);

			if (e.ColumnIndex != Columns.Count - 1)
			{
				ResizeLastColumn();
			}
		}

		//protected override void OnColumnWidthChanged(ColumnWidthChangedEventArgs e)
		//{
		//	base.OnColumnWidthChanged(e);

		//	if (e.ColumnIndex != Columns.Count - 1)
		//	{
		//		ResizeLastColumn();
		//	}
		//}

		protected override void OnResize(EventArgs e)
		{
			ResizeLastColumn();
			base.OnResize(e);
		}

		protected override void OnDrawItem(DrawListViewItemEventArgs e) => e.DrawDefault = true;
	}
}
