﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace NetStorm.UILib.Controls
{
	public class NSTextBox : NetStormControl
	{
		public string CueText
		{
			get => _textBox.CueText;
			set => _textBox.CueText = value;
		}
		public bool Multiline
		{
			get => _textBox.Multiline;
			set => _textBox.Multiline = value;
		}
		public bool ReadOnly
		{
			get => _textBox.ReadOnly;
			set => _textBox.ReadOnly = value;
		}
		public new string Text
		{
			get => _textBox.Text;
			set => _textBox.Text = value;
		}
		public HorizontalAlignment TextAlign
		{
			get => _textBox.TextAlign;
			set => _textBox.TextAlign = value;
		}
		public Icon Icon
		{
			get => _icon;
			set
			{
				_icon = value;
				IconChanged?.Invoke(this, new EventArgs());
			}
		}
		public override ContextMenuStrip ContextMenuStrip
		{
			get => _textBox.ContextMenuStrip;
			set => _textBox.ContextMenuStrip = value;
		}

		public event EventHandler CueTextChanged;
		public event EventHandler TextAlignChanged;
		public event EventHandler IconChanged;

		private readonly CueTextbox _textBox;
		private Icon _icon;

		public NSTextBox()
		{
			MouseEffect = true;
			_textBox = new CueTextbox
			{
				BorderStyle = BorderStyle.None,
				CueTextY = 0,
				ContextMenuStrip = new ContextMenuStrip()	// Removes auto right click context menu
			};
			Controls.Add(_textBox);

			_textBox.TextChanged += (_,__) => OnTextChanged(__);
			_textBox.CueTextChanged += (_, __) => OnCueTextChanged(__);
			_textBox.TextAlignChanged += (_, __) => TextAlignChanged?.Invoke(this, __);
			_textBox.MouseEnter += (_, __) => OnMouseEnter(__);
			_textBox.MouseLeave += (_, __) => OnMouseLeave(__);
			_textBox.GotFocus += (_, __) => OnGotFocus(__);
			_textBox.LostFocus += (_, __) => OnLostFocus(__);
		}

		protected virtual void OnCueTextChanged(EventArgs e)
			=> CueTextChanged?.Invoke(this, e);

		private void UpdateTextboxSizeAndLocation()
		{
			const int iconPad = 5;
			_textBox.Size = new Size(
				Size.Width - Padding.Left - Padding.Right - Radius.MaxRadius - (_icon == null ? Radius.MaxRadius : _icon.Width - iconPad),
				Size.Height - Padding.Top - Padding.Bottom);
			_textBox.Location = new Point(Padding.Left + Radius.MaxLeftRadius + (_icon == null ? 0 : Math.Min(_textBox.Width, _textBox.Height) + iconPad),
				(Height - _textBox.Height) / 2);
		}

		protected override void OnBackColorChanged(EventArgs e)
		{
			base.OnBackColorChanged(e);
			_textBox.BackColor = BackColor;
		}

		protected override void OnForeColorChanged(EventArgs e)
		{
			base.OnForeColorChanged(e);
			_textBox.ForeColor = ForeColor;
		}

		protected override void OnGotFocus(EventArgs e)
		{
			base.OnGotFocus(e);
			_textBox.Focus();
		}

		protected override void OnSizeChanged(EventArgs e)
		{
			base.OnSizeChanged(e);
			UpdateTextboxSizeAndLocation();
		}

		protected override void OnClientSizeChanged(EventArgs e)
		{
			base.OnClientSizeChanged(e);
			UpdateTextboxSizeAndLocation();
		}

		protected override void OnPaddingChanged(EventArgs e)
		{
			base.OnPaddingChanged(e);
			UpdateTextboxSizeAndLocation();
		}

		protected override void OnPaintBackground(PaintEventArgs pevent)
		{
			base.OnPaintBackground(pevent);
			if(_icon != null)
			{
				var rectDims = Math.Min(_textBox.Width, _textBox.Height);
				var rect = new Rectangle(
					new Point(Padding.Left + Math.Max(Radius.BottomLeft, Radius.TopLeft), (Height - _textBox.Height) / 2), 
					new Size(rectDims, rectDims));
				pevent.Graphics.DrawIcon(_icon, rect);
			}
		}
	}
}
