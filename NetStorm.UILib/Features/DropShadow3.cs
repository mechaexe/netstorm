﻿using NetStorm.UILib.Controls;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace NetStorm.UILib.Features
{
	class DropShadow3 : IDropShadow
	{
		public int ShadowSize
		{
			get => size;
			set
			{
				if (size != value)
				{
					size = value;
					RedrawAll(parent.CreateGraphics());
				}
			}
		}
		public Color ColorBegin
		{
			get => colorBegin;
			set
			{
				if (!colorBegin.Equals(value))
				{
					colorBegin = value;
					RedrawAll(parent.CreateGraphics());
				}
			}
		}
		public Color ColorEnd
		{
			get => colorEnd;
			set
			{
				if (!colorEnd.Equals(value))
				{
					colorEnd = value;
					RedrawAll(parent.CreateGraphics());
				}
			}
		}
		private readonly Control parent;
		private readonly List<Control> childs;
		private Color colorBegin, colorEnd;
		private int size;

		public DropShadow3(Control container)
		{
			size = 8;
			colorEnd = Color.FromArgb(0x313131);
			colorBegin = Color.FromArgb(0x323232);
			parent = container;
			parent.Paint += (_, __) => RedrawAll(__.Graphics);
			childs = new List<Control>();
		}

		private void RedrawAll(Graphics graphics)
		{
			foreach (var child in childs)
			{
				RedrawChild(child, graphics);
			}
		}

		private void RedrawChild(Control child, Graphics graphics)
		{
			if (child is INetStormControl nsControl)
				DrawShadow(graphics, ColorBegin, ColorEnd, RoundedCorners.GetGraphicsPath(nsControl.ClientRectangle, nsControl.Radius), ShadowSize);
			else
				DrawShadow(graphics, ColorBegin, ColorEnd, RoundedCorners.GetGraphicsPath(child.ClientRectangle, 0), ShadowSize);
		}

		private void DrawShadow(Graphics G, Color startColor, Color endColor, GraphicsPath GP, int d)
		{
			using(var pen = new Pen(Color.Black, 1.75f))	// <== pen width (*)
			{
				foreach (var color in GetColorVector(startColor, endColor, d))
				{
					G.ScaleTransform(1f, 1f);        // <== shadow vector!
					pen.Color = color;
					G.DrawPath(pen, GP);
				}
			}
			G.ResetTransform();
		}


		private Color[] GetColorVector(Color fc, Color bc, int depth)
		{
			var cv = new Color[depth];
			float dRed = 1f * (bc.R - fc.R) / depth;
			float dGreen = 1f * (bc.G - fc.G) / depth;
			float dBlue = 1f * (bc.B - fc.B) / depth;
			for (int d = 1; d <= depth; d++)
			{
				cv[d - 1] = Color.FromArgb(
					255, 
					(int)(fc.R + dRed * d),
					(int)(fc.G + dGreen * d),
					(int)(fc.B + dBlue * d)
				);
			}
			return cv;
		}

		public void AddControl(Control c)
		{
			childs.Add(c);
			RedrawChild(c, parent.CreateGraphics());
			c.VisibleChanged += (_, __) => RedrawChild(c.Parent, parent.CreateGraphics());
			c.Paint += (_, __) => RedrawChild(c, parent.CreateGraphics());
		}

		public bool RemoveControl(Control c)
		{
			var res = childs.Remove(c);
			if (res)
			{
				c.VisibleChanged -= (_, __) => RedrawChild(c.Parent, parent.CreateGraphics());
				c.Paint -= (_, __) => RedrawChild(c, parent.CreateGraphics());
				RedrawAll(parent.CreateGraphics());
			}
			return true;
		}
	}
}
