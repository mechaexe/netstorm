﻿using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace NetStorm.UILib.Features
{
	class DropShadow2 : IDropShadow
	{
		public int ShadowSize
		{
			get => size;
			set
			{
				if (size == value)
				{
					return;
				}

				size = value;
				RedrawComplete(parent.CreateGraphics());
			}
		}
		public Color ColorBegin
		{
			get => colorBegin;
			set
			{
				if (colorBegin == value)
				{
					return;
				}

				colorBegin = value;
				RedrawComplete(parent.CreateGraphics());
			}
		}
		public Color ColorEnd
		{
			get => colorEnd;
			set
			{
				if (colorEnd == value)
				{
					return;
				}

				colorEnd = value;
				RedrawComplete(parent.CreateGraphics());
			}
		}
		private readonly Control parent;
		private readonly List<Control> childs;
		private Color colorBegin, colorEnd;
		private int size;

		public DropShadow2(Control container)
		{
			size = 8;
			colorEnd = Color.FromArgb(0x313131);
			colorBegin = Color.FromArgb(0x323232);
			parent = container;
			parent.Paint += (_, __) => RedrawComplete(__.Graphics);
			childs = new List<Control>();
		}

		private void RedrawComplete(Graphics graphics)
		{
			foreach (var child in childs)
			{
				RedrawChild(child, graphics);
			}
		}

		private void RedrawChild(Control child, Graphics graphics)
		{
			if (!child.Visible)
			{
				return;
			}

			using (var gp = RoundedCorners.GetGraphicsPath(child.ClientRectangle, 10))
			{
				DrawPathWithFuzzyLine(gp, graphics, Color.Black, 100, 10, 1, parent.BackColor);
			}
		}

		public void AddControl(Control c)
		{
			childs.Add(c);
			RedrawChild(c, parent.CreateGraphics());
			c.VisibleChanged += (_, __) => RedrawChild(c.Parent, parent.CreateGraphics());
			c.Paint += (_, __) => RedrawChild(c, parent.CreateGraphics());
		}

		public bool RemoveControl(Control c)
		{
			var res = childs.Remove(c);
			if (!res)
			{
				return false;
			}

			c.VisibleChanged -= (_, __) => RedrawChild(c.Parent, parent.CreateGraphics());
			c.Paint -= (_, __) => RedrawChild(c, parent.CreateGraphics());
			RedrawComplete(parent.CreateGraphics());
			return true;
		}

		private void DrawPathWithFuzzyLine(GraphicsPath path, Graphics gr, Color base_color, int max_opacity, int width, int opaque_width, Color parentBG)
		{
			// Number of pens we will use.
			int num_steps = width - opaque_width + 1;

			// Change in alpha between pens.
			float delta = (float)max_opacity / num_steps / num_steps;

			// Initial alpha.
			float alpha = 100;

			using (Pen pen = new Pen(base_color)
			{
				EndCap = LineCap.Round,
				StartCap = LineCap.Round
			})
			{
				for (pen.Width = width; pen.Width >= opaque_width; pen.Width--)
				{
					gr.DrawPath(pen, path);
					alpha -= delta;
					//pen.Color = Color.FromArgb(
					//	(int)alpha,
					//	base_color.R,
					//	base_color.G,
					//	base_color.B);
					pen.Color = ColorExtensions.GetBlendColor(new Color[]
					{
						Color.FromArgb(
							(int)alpha,
							base_color.R,
							base_color.G,
							base_color.B),
						parentBG
					});
				}
			}
		}
	}
}
