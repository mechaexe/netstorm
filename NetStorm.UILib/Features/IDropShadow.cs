﻿using System.Drawing;
using System.Windows.Forms;

namespace NetStorm.UILib.Features
{
	public interface IDropShadow
	{
		Color ColorBegin { get; set; }
		Color ColorEnd { get; set; }
		int ShadowSize { get; set; }

		void AddControl(Control c);
		bool RemoveControl(Control c);
	}
}