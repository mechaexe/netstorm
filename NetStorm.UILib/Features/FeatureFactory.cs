﻿using System.Collections.Specialized;
using System.ComponentModel;
using System.Windows.Forms;
using NetStorm.UILib.Features.AutoUpdate;

namespace NetStorm.UILib.Features
{
	public static class FeatureFactory
	{
		public static IDragger CreateDragger(Control actor, Form target) => new Dragger(actor, target);

		public static IDropShadow CreateDropShadow(Control parent) => new DropShadow(parent);

		public static IDropShadow CreateDropShadow3(Control parent) => new DropShadow3(parent);

		public static IAutoUpdateField GetAutoUpdateField<T>(T observable, string field)
			=> new AutoUpdateField<T>(observable, field);

		public static IAutoUpdateField GetAutoUpdateField(StringDelegate getText)
			=> new UpdateField(getText);
		public static IAutoUpdateField GetAutoUpdateField(INotifyPropertyChanged observable, StringDelegate getText)
			=> new UpdateField(observable, getText);
		public static IAutoUpdateField GetAutoUpdateField(INotifyPropertyChanged observable, StringDelegate getText, string fieldName)
			=> new UpdateField(observable, getText, fieldName);
		public static IAutoUpdateField GetAutoUpdateField(INotifyCollectionChanged observable, StringDelegate getText)
			=> new UpdateField(observable, getText);
	}
}
