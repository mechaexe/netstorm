﻿using System.Drawing;
using System.Windows.Forms;

namespace NetStorm.UILib.Features
{
	class Dragger : IDragger
	{
		public bool Enabled { get; set; }
		private readonly Form target;

		private bool isMouseDown;
		private Point lastLocation;

		public Dragger(Control actor, Form target)
		{
			this.target = target;

			lastLocation = target.Location;

			actor.MouseDoubleClick += Actor_MouseDoubleClick;
			actor.MouseMove += Actor_MouseMove;
			actor.MouseDown += Actor_MouseDown;
			actor.MouseUp += (_, __) => isMouseDown = false;
		}

		private void Actor_MouseDown(object sender, MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Left)
			{
				isMouseDown = true;
				lastLocation = e.Location;
			}
		}

		private void Actor_MouseMove(object sender, MouseEventArgs e)
		{
			if (Enabled && isMouseDown)
			{
				target.Location = new Point((target.Location.X - lastLocation.X) + e.X, (target.Location.Y - lastLocation.Y) + e.Y);
			}
		}

		private void Actor_MouseDoubleClick(object sender, MouseEventArgs e)
		{
			if (Enabled)
			{
				target.WindowState = target.WindowState == FormWindowState.Maximized ? FormWindowState.Normal : FormWindowState.Maximized;
			}
		}
	}
}
