﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace NetStorm.UILib.Features
{
	static class RoundedCorners
	{
		private static Point[] GetTopLine(Rectangle rectangle, int leftRadius, int rightRadius)
		{
			return new Point[]
			{
				new Point(rectangle.X + leftRadius, rectangle.Y),
				new Point(rectangle.X + rectangle.Width - rightRadius, rectangle.Y)
			};
		}

		private static Point[] GetBottomLine(Rectangle rectangle, int leftRadius, int rightRadius)
		{
			var top = GetTopLine(rectangle, leftRadius, rightRadius);
			for (int i = 0; i < top.Length; i++)
			{
				top[i].Y += rectangle.Height;
			}

			return top;
		}

		private static Point[] GetRightLine(Rectangle rectangle, int topRadius, int bottomRadius)
		{
			return new Point[]
			{
				new Point(rectangle.X + rectangle.Width, rectangle.Y + topRadius),
				new Point(rectangle.X + rectangle.Width, rectangle.Y + rectangle.Height - bottomRadius)
			};
		}

		private static Point[] GetLeftLine(Rectangle rectangle, int topRadius, int bottomRadius)
		{
			var top = GetRightLine(rectangle, topRadius, bottomRadius);
			for (int i = 0; i < top.Length; i++)
			{
				top[i].X = rectangle.X;
			}

			return top;
		}

		private static GraphicsPath AddTopLine(this GraphicsPath gp, Rectangle rectangle, int leftRadius, int rightRadius)
		{
			var diameter = 2 * leftRadius;
			var top = GetTopLine(rectangle, leftRadius, rightRadius);

			if (leftRadius > 0)
			{
				gp.AddArc(rectangle.X, rectangle.Y, diameter, diameter, 180, 90);
			}

			gp.AddLine(top[0], top[1]);

			return gp;
		}

		private static GraphicsPath AddRightLine(this GraphicsPath gp, Rectangle rectangle, int topRadius, int bottomRadius)
		{
			var diameter = 2 * topRadius;

			var right = GetRightLine(rectangle, topRadius, bottomRadius);

			if (topRadius > 0)
			{
				gp.AddArc(right[0].X - diameter, rectangle.Y, diameter, diameter, -90, 90);
			}

			gp.AddLine(right[0], right[1]);
			return gp;
		}

		private static GraphicsPath AddBottomLine(this GraphicsPath gp, Rectangle rectangle, int leftRadius, int rightRadius)
		{
			var diameter = 2 * rightRadius;
			var bottomLine = GetBottomLine(rectangle, leftRadius, rightRadius);

			if (rightRadius > 0)
			{
				gp.AddArc(bottomLine[1].X - rightRadius, bottomLine[1].Y - diameter, diameter, diameter, 0, 90);
			}

			gp.AddLine(bottomLine[1], bottomLine[0]);
			return gp;
		}

		private static GraphicsPath AddLeftLine(this GraphicsPath gp, Rectangle rectangle, int topRadius, int bottomRadius)
		{
			var diameter = 2 * bottomRadius;
			var leftLine = GetLeftLine(rectangle, topRadius, bottomRadius);

			if (bottomRadius > 0)
			{
				gp.AddArc(leftLine[1].X, leftLine[1].Y - bottomRadius, diameter, diameter, 90, 90);
			}

			gp.AddLine(leftLine[1], leftLine[0]);

			return gp;
		}

		public static GraphicsPath GetGraphicsPath(Rectangle rectangle, CornerRadius radius, bool oobCorrectionRight = false, bool oobCorrectionBottom = false)
		{
			var gp = new GraphicsPath();

			rectangle.Width -= oobCorrectionRight ? 1 : 0;
			rectangle.Height -= oobCorrectionBottom ? 1 : 0;

			gp.AddTopLine(rectangle, radius.TopLeft, radius.TopRight)
				.AddRightLine(rectangle, radius.TopRight, radius.BottomRight)
				.AddBottomLine(rectangle, radius.BottomLeft, radius.BottomRight)
				.AddLeftLine(rectangle, radius.TopLeft, radius.BottomLeft)
				.CloseFigure();
			return gp;
		}

		public static GraphicsPath GetGraphicsPath(Rectangle rectangle, int topLeft, int topRight, int bottomRight, int bottomLeft, bool oobCorrectionRight = false, bool oobCorrectionBottom = false)
		{
			var gp = new GraphicsPath();

			rectangle.Width -= oobCorrectionRight ? 1 : 0;
			rectangle.Height -= oobCorrectionBottom ? 1 : 0;

			gp.AddTopLine(rectangle, topLeft, topRight)
				.AddRightLine(rectangle, topRight, bottomRight)
				.AddBottomLine(rectangle, bottomLeft, bottomRight)
				.AddLeftLine(rectangle, topLeft, bottomLeft)
				.CloseFigure();
			return gp;
		}

		public static GraphicsPath GetGraphicsPath(Rectangle rectangle)
			=> GetGraphicsPath(rectangle, Math.Min(rectangle.Height, rectangle.Width) / 2, true, true);

		public static GraphicsPath GetGraphicsPath(Rectangle rectangle, int radius, bool oobCorrectionRight = true, bool oobCorrectionBottom = true)
			=> GetGraphicsPath(rectangle, radius, radius, radius, radius, oobCorrectionRight, oobCorrectionBottom);
	}
}
