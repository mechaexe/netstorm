﻿using System;
using System.ComponentModel;

namespace NetStorm.UILib.Features.AutoUpdate
{
	class AutoUpdateField<T> : INotifyPropertyChanged, IAutoUpdateField
	{
		public string Value => prefix + GetFieldValue();
		private readonly StringFormat stringFormatter;
		private readonly string field;
		private readonly T observable;
		private readonly string prefix;

		public event PropertyChangedEventHandler PropertyChanged;

		public AutoUpdateField(T observable, string field) :
			this("", observable, field, (_) => _?.ToString())
		{ }

		public AutoUpdateField(T observable, string field, StringFormat stringFormatter) :
			this("", observable, field, stringFormatter)
		{ }

		public AutoUpdateField(string prefix, T observable, string field, StringFormat stringFormatter)
		{
			this.field = field;
			this.stringFormatter = stringFormatter;
			this.observable = observable;
			this.prefix = prefix;
			if (observable is INotifyPropertyChanged inf)
			{
				inf.PropertyChanged += Observable_PropertyChanged;
			}
		}

		private void Observable_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			if (e.PropertyName == field)
			{
				Update();
			}
		}

		private string GetFieldValue()
		{
			try
			{
				Type t = observable.GetType();
				var val = t.GetProperty(field).GetValue(observable);
				return stringFormatter(val);
			}
			catch { return ""; }
		}

		public void Update()
			=> PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Value)));
	}
}
