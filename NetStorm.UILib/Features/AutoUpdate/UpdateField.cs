﻿using System.Collections.Specialized;
using System.ComponentModel;

namespace NetStorm.UILib.Features.AutoUpdate
{
	class UpdateField : IAutoUpdateField
	{
		public string Value => getText();

		public event PropertyChangedEventHandler PropertyChanged;

		private readonly StringDelegate getText;

		public UpdateField(StringDelegate getText)
			=> this.getText = getText;

		public UpdateField(INotifyPropertyChanged observable, StringDelegate getText) : this(getText)
		{
			observable.PropertyChanged += (_, __) => Update();
		}

		public UpdateField(INotifyCollectionChanged observable, StringDelegate getText) : this(getText)
		{
			observable.CollectionChanged += (_, __) => Update();
		}

		public UpdateField(INotifyPropertyChanged observable, StringDelegate getText, string fieldName) : this(getText)
		{
			observable.PropertyChanged += (_, __) =>
			{
				if (__.PropertyName == fieldName)
				{
					Update();
				}
			};
		}

		public void Update()
			=> PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Value)));
	}
}
