﻿using System.ComponentModel;

namespace NetStorm.UILib.Features.AutoUpdate
{
	public interface IAutoUpdateField : INotifyPropertyChanged
	{
		string Value { get; }

		void Update();
	}
}