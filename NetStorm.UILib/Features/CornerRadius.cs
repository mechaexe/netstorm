﻿using System;

namespace NetStorm.UILib.Features
{
	public struct CornerRadius
	{
		public int MaxRadius => Math.Max(MaxLeftRadius, MaxRightRadius);
		public int MaxLeftRadius => Math.Max(TopLeft, BottomLeft);
		public int MaxRightRadius => Math.Max(TopRight, BottomRight);
		public bool HasRadius => !(TopRight == 0 && TopLeft == 0 && BottomRight == 0 && BottomLeft == 0);
		public int TopLeft { get; set; }
		public int TopRight { get; set; }
		public int BottomLeft { get; set; }
		public int BottomRight { get; set; }

		public CornerRadius(int all)
		{
			TopLeft = all;
			TopRight = all;
			BottomLeft = all;
			BottomRight = all;
		}

		public CornerRadius(int topLeft, int topRight, int bottomLeft, int bottomRight) : this(topLeft)
		{
			TopRight = topRight;
			BottomLeft = bottomLeft;
			BottomRight = bottomRight;
		}

		public override bool Equals(object obj)
		{
			return obj is CornerRadius cr &&
				cr.BottomLeft == BottomLeft &&
				cr.BottomRight == BottomRight &&
				cr.TopLeft == TopLeft &&
				cr.TopRight == TopRight;
		}

		public override int GetHashCode()
		{
			return $"{TopLeft}|{TopRight}|{BottomLeft}|{BottomRight}".GetHashCode();
		}
	}
}
