﻿using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace NetStorm.UILib.Features
{
	class DropShadow : IDropShadow
	{
		public int ShadowSize
		{
			get => size;
			set
			{
				if (size == value)
				{
					return;
				}

				size = value;
				RedrawComplete(parent.CreateGraphics());
			}
		}
		public Color ColorBegin
		{
			get => colorBegin;
			set
			{
				if (colorBegin == value)
				{
					return;
				}

				colorBegin = value;
				RedrawComplete(parent.CreateGraphics());
			}
		}
		public Color ColorEnd
		{
			get => colorEnd;
			set
			{
				if (colorEnd == value)
				{
					return;
				}

				colorEnd = value;
				RedrawComplete(parent.CreateGraphics());
			}
		}
		private readonly Control parent;
		private readonly List<Control> childs;
		private Color colorBegin, colorEnd;
		private int size;
		private readonly int shrink_X = 0;
		private readonly int shrink_Y = 0;

		public DropShadow(Control container)
		{
			size = 8;
			colorEnd = Color.FromArgb(0x313131);
			colorBegin = Color.FromArgb(0x323232);
			parent = container;
			parent.Paint += (_, __) => RedrawComplete(__.Graphics);
			childs = new List<Control>();
		}

		private void RedrawComplete(Graphics graphics)
		{
			foreach (var child in childs)
			{
				RedrawChild(child, graphics);
			}
		}

		private void RedrawChild(Control child, Graphics graphics)
		{
			if (!child.Visible)
			{
				return;
			}

			var colors = GetColorVector(colorBegin, colorEnd, size).ToArray();
			var loc = child.Location;

			using (var pen = new Pen(Color.Wheat, 1f))
			{
				for (int i = 0; i < size; i++)
				{
					pen.Color = colors[i];
					graphics.DrawRectangle(pen, loc.X - i + shrink_X / 2, loc.Y - i + shrink_Y / 2, child.Width + 2 * i - +shrink_X / 2, child.Height + 2 * i - shrink_Y / 2);
				}
			}
		}

		public void AddControl(Control c)
		{
			childs.Add(c);
			RedrawChild(c, parent.CreateGraphics());
			c.VisibleChanged += (_, __) => RedrawChild(c.Parent, parent.CreateGraphics());
			c.Paint += (_, __) => RedrawChild(c, parent.CreateGraphics());
		}

		public bool RemoveControl(Control c)
		{
			var res = childs.Remove(c);
			if (!res)
			{
				return false;
			}

			c.VisibleChanged -= (_, __) => RedrawChild(c.Parent, parent.CreateGraphics());
			c.Paint -= (_, __) => RedrawChild(c, parent.CreateGraphics());
			RedrawComplete(parent.CreateGraphics());
			return true;
		}

		private List<Color> GetColorVector(Color fc, Color bc, int depth)
		{
			List<Color> cv = new List<Color>();
			float dRed = 1f * (bc.R - fc.R) / depth;
			float dGreen = 1f * (bc.G - fc.G) / depth;
			float dBlue = 1f * (bc.B - fc.B) / depth;
			for (int d = 1; d <= depth; d++)
			{
				cv.Add(Color.FromArgb(255, (int)(fc.R + dRed * d),
				  (int)(fc.G + dGreen * d), (int)(fc.B + dBlue * d)));
			}

			return cv;
		}
	}
}
