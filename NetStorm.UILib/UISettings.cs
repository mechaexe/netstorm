﻿using System.Drawing;
using System.Windows.Forms;

namespace NetStorm.UILib
{
	public static class UISettings
	{
		public const int ButtonRadius = 10;
		public static Padding ButtonMargin = new Padding(10, 10, 10, 0);
		public static Size TabButtonSize = new Size(100, 30);
	}
}
