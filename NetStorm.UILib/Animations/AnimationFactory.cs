﻿using System.Windows.Forms;

namespace NetStorm.UILib.Animations
{
	public static class AnimationFactory
	{
		public static IAnimation<float> CreateFloatAnimator(Control control, string field)
		{
			return new FloatAnimation(control, field);
		}
	}
}
