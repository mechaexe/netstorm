﻿using System;

namespace NetStorm.UILib.Animations
{
	public interface IAnimation<T>
	{
		event EventHandler AnimationFinished;
		void Animate(T start, T end);
	}
}
