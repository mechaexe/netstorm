﻿using System;
using System.Windows.Forms;

namespace NetStorm.UILib.Animations
{
	class FloatAnimation : IAnimation<float>, IDisposable
	{
		public event EventHandler AnimationFinished;

		private readonly Timer animTimer;
		private readonly Control c;
		private float increment, end;

		private readonly string field;
		private bool isDisposed;

		public FloatAnimation(Control control, string field)
		{
			animTimer = new Timer();
			animTimer.Tick += AnimTimer_Tick;

			this.field = field;
			c = control;
			isDisposed = false;
		}

		~FloatAnimation()
		{
			if (isDisposed)
			{
				return;
			}

			animTimer?.Dispose();
		}

		private void AnimTimer_Tick(object sender, EventArgs e)
		{
			var pi = c.GetType().GetProperty(field);

			if (!float.TryParse(pi.GetValue(c).ToString(), out var f))
			{
				OnEnd();
				return;
			}

			f += increment;

			if ((increment > 0 && f > end) || (increment < 0 && f < end))
			{
				f = end;
			}

			pi.SetValue(c, Convert.ChangeType(f, pi.PropertyType));

			if (f == end)
			{
				OnEnd();
			}
		}

		public void Animate(float incrementBy, float endValue)
		{
			if (animTimer.Enabled ||
				incrementBy == 0)
			{
				return;
			}

			animTimer.Interval = 1;

			increment = incrementBy;
			end = endValue;

			animTimer.Start();
		}

		private void OnEnd()
		{
			animTimer.Stop();
			AnimationFinished?.Invoke(this, new EventArgs());
		}

		public void Dispose()
		{
			animTimer?.Dispose();
			isDisposed = true;
		}
	}
}
