﻿using NetStorm.UILib.Coloring;
using NetStorm.UILib.Properties;
using System.Drawing;

namespace NetStorm.UILib
{
	public static class Colors
	{
		public static Color Brown => Color.FromArgb(109, 76, 65);
		public static Color DarkBlue => Color.FromArgb(96, 125, 139);
		public static Color Red => Color.FromArgb(204, 92, 57);
		public static Color Blue => Color.FromArgb(33, 122, 206);
		public static Color Green => Color.FromArgb(77, 135, 92);
		public static Color Orange => Color.FromArgb(209, 119, 17);
		public static Color Dark => Color.FromArgb(32, 34, 37);
		public static Color Light => Color.FromArgb(248, 248, 248);
		public static Color Yellow => Color.FromArgb(245, 205, 8);

		public static ColorScheme GetColorScheme()
		{
			var settings = Settings.Default;
			if (settings.UseDarkTheme)
				return new ColorScheme(
					new DarkColorTable(settings.PrimaryDarkForecolor, settings.PrimaryDarkBackcolor, settings.AccentColorDark),
					new DarkColorTable(settings.SecondaryDarkForecolor, settings.SecondaryDarkBackcolor, settings.AccentColorDark),
					new DarkColorTable(settings.SecondaryDarkForecolor, settings.AccentColorDark, settings.AccentColorDark));
			else
				return new ColorScheme(
					new LightColorTable(settings.PrimaryLightForecolor, settings.PrimaryLightBackcolor, settings.AccentColorLight),
					new LightColorTable(settings.SecondaryLightForecolor, settings.SecondaryLightBackcolor, settings.AccentColorLight),
					new LightColorTable(settings.SecondaryLightForecolor, settings.AccentColorLight, settings.AccentColorLight));
		}
	}
}
