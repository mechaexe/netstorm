﻿using Protocols.Extensions;
using Protocols.TCP;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;

namespace Protocols.WhoIs
{
	public class WhoisCrawler
	{
		private static readonly Regex _hasReferralRegex = new Regex(
				@"(^ReferralServer:\W+whois://(?<refsvr>[^:\r\n]+)(:(?<port>\d+))?)|" +
				@"(^\s*(Registrar\s+)?Whois Server:\s*(?<refsvr>[^:\r\n]+)(:(?<port>\d+))?)|" +
				@"(^\s*refer:\s*(?<refsvr>[^:\r\n]+)(:(?<port>\d+))?)|" +
				@"(^remarks:\W+.*(?<refsvr>whois\.[0-9a-z\-\.]+\.[a-z]{2,})(:(?<port>\d+))?)",
				RegexOptions.Compiled | RegexOptions.Multiline | RegexOptions.IgnoreCase);
		private static readonly Uri initialLookup = new Uri("whois://whois.iana.org:43");

		private static string QueryRegistrar(IPEndPoint localEndpoint, Uri whoisServer, string ipOrHostname)
		{
			string registrar = null;
			var requester = new TcpRequester();
			var resultBytes = requester.GetResponse(localEndpoint, whoisServer, false, Encoding.ASCII.GetBytes(ipOrHostname));
			if (resultBytes.Length != 0)
				registrar = Encoding.ASCII.GetString(resultBytes).ForceLF();
			return registrar;
		}

		public static List<WhoisResponse> Query(IPEndPoint localEndpoint, string ipOrHostname)
			=> Query(localEndpoint, initialLookup, ipOrHostname);

		public static List<WhoisResponse> Query(IPEndPoint localEndpoint, Uri whoisServer, string ipOrHostname)
		{
			var res = new List<WhoisResponse>();
			ipOrHostname = ipOrHostname.EndsWith("\r\n") ? ipOrHostname : ipOrHostname + "\r\n";
			QueryRecursive(localEndpoint, whoisServer, ipOrHostname, res);
			return res;
		}

		private static void QueryRecursive(IPEndPoint localEndpoint, Uri whoisServer, string ipOrHostname, List<WhoisResponse> responses)
		{
			var reg = QueryRegistrar(localEndpoint, whoisServer, ipOrHostname);
			if (reg != null)
			{
				responses.Add(new WhoisResponse(whoisServer.DnsSafeHost, reg));
				if (HasReferral(reg, whoisServer.DnsSafeHost, out whoisServer))
					QueryRecursive(localEndpoint, whoisServer, ipOrHostname, responses);
			}
		}

		private static bool HasReferral(string rawResponse, string currentServer, out Uri whoisServer)
		{
			whoisServer = null;
			var success = false;

			var m2 = _hasReferralRegex.Match(rawResponse);
			try
			{
				if (m2.Success)
				{
					var port = m2.Groups["port"].Success ? int.Parse(m2.Groups["port"].Value) : 43;
					whoisServer = new Uri($"whois://{m2.Groups[@"refsvr"].Value.Trim()}:{port}");
					success = currentServer.ToLower() != whoisServer.DnsSafeHost.ToLower();
				}
			}
			catch { }

			return success;
		}
	}
}
