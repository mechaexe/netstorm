﻿using System.Net;
using System.Text.RegularExpressions;

namespace Protocols.WhoIs
{
	public class WhoisResponse
	{
		public string ResponseFrom { get; set; }
		public string Organisation { get; set; }
		public string Raw { get; set; }
		public IPAddress StartAddress { get; set; }
		public IPAddress EndAddress { get; set; }

		public WhoisResponse(string responseFrom, string response)
		{
            this.ResponseFrom = responseFrom;
            this.Raw = response;
            this.Organisation = ResolveOrganisation(response);

            var range = ResolveRange(response);
            StartAddress = range.Item1;
            EndAddress = range.Item2;

            if (responseFrom == "whois.arin.net")
			{
                var arin = ResolveArinRange(response);
                Organisation = arin.Item1;
                StartAddress = arin.Item2;
                EndAddress = arin.Item3;
			}
        }

        private string ResolveOrganisation(string raw)
		{
            var orgMatch = Regex.Match(raw,
               @"(^f\.\W*\[組織名\]\W+(?<orgName>[^\r\n]+))|" +
               @"(^\s*(OrgName|descr|[Rr]egistrant [Oo]rganization|owner|[Oo]rgani[sz]ation):\W+(?<orgName>[^\r\n]+))",
               RegexOptions.Multiline);
            return orgMatch.Success ? orgMatch.Groups["orgName"].Value.Trim() : "";
        }

        private (IPAddress, IPAddress) ResolveRange(string raw)
        {
            var rangeMatch = Regex.Match(raw,
               @"(^a.\W*\[IPネットワークアドレス\]\W+(?<adr>[^\r\n]+))|" +
               @"(^(NetRange|CIDR|inetnum):\W+(?<adr>[^\r\n]+))",
               RegexOptions.Multiline);

            (IPAddress, IPAddress) result = (null, null);
			if (rangeMatch.Success)
			{
                var split = rangeMatch.Groups["adr"].Value.Split('-');
                result = (IPAddress.Parse(split[0].Trim()), IPAddress.Parse(split[1].Trim()));
            }

            return result;
        }

        private (string, IPAddress, IPAddress) ResolveArinRange(string raw)
        {
            var rangeMatch = Regex.Matches(raw,
                    @"(?<org>^.*) (?<adr>\d+\.\d+\.\d+\.\d+ - \d+\.\d+\.\d+\.\d+)",
                    RegexOptions.Multiline);

            (string, IPAddress, IPAddress) result = ("", null, null);
            if (rangeMatch.Count > 0)
            {
                var mymatch = rangeMatch[rangeMatch.Count - 1];
                // Test to see if the information was already picked up from above
                if (mymatch.Groups["org"].Value.Trim() != "NetRange:" &&
                    mymatch.Groups["org"].Value.Trim() != "CIDR:" &&
                    mymatch.Groups["org"].Value.Trim() != "inetnum:")
                {
                    var split = mymatch.Groups["adr"].Value.Split('-');
                    result = (mymatch.Groups["org"].Value.Trim(), IPAddress.Parse(split[0].Trim()), IPAddress.Parse(split[1].Trim()));
                }
            }

            return result;
        }
    }
}
