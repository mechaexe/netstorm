﻿using PacketDotNet;
using PacketDotNet.Utils;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.NetworkInformation;

namespace Protocols.NDP
{
	/// <summary>
	/// See https://tools.ietf.org/html/rfc4861 for reference
	/// </summary>
	public static class NeighborDiscoveryFactory
	{
		private static readonly PhysicalAddress NDSolicitationMac = PhysicalAddress.Parse("333300000002");

		public static Dictionary<string, IPAddress> MulticastAddresses { get; }

		public static PhysicalAddress CreateMulticastMac(IPAddress ipv6Multicast)
		{
			var mcast = NDSolicitationMac.GetAddressBytes();
			mcast[mcast.Length - 1] = ipv6Multicast.GetAddressBytes()[ipv6Multicast.GetAddressBytes().Length - 1];
			return new PhysicalAddress(mcast);
		}

		static NeighborDiscoveryFactory()
		{
			MulticastAddresses = new Dictionary<string, IPAddress>
			{
				{ "all-nodes-link-local", IPAddress.Parse("ff02::1") },
				{ "all-routers-link-local", IPAddress.Parse("ff02::1") },
				{ "all-routers-site-local", IPAddress.Parse("ff05::1") },
			};
		}

		/* ----- ICMPv6 Neighbor solicitation packet -----
		 
		  0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
		 +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
		 |     Type      |     Code      |          Checksum             |
		 +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
		 |                           Reserved                            |
		 +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
		 |                                                               |
		 +                                                               +
		 |                                                               |
		 +                       Target Address                          +
		 |                                                               |
		 +                                                               +
		 |                                                               |
		 +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
		 |   Options ...
		 +-+-+-+-+-+-+-+-+-+-+-+-
		*/
		public static IcmpV6Packet CreateNeighborSolicitationPacket(PhysicalAddress sourceMac, IPAddress sourceIP)
		{
			var ipAddressBytes = sourceIP.GetAddressBytes();
			var macAddressBytes = sourceMac.GetAddressBytes();

			var header = new ByteArraySegment(new byte[8]); //NS-Packet has 8 byte header
			var nsPayload = new byte[ipAddressBytes.Length + 2 + macAddressBytes.Length];
			
			Array.Copy(ipAddressBytes, nsPayload, ipAddressBytes.Length);   //Set target ipv6 address to local address

			nsPayload[ipAddressBytes.Length] = 1;      //Set "Type Field [16]" to 1 (Source link layer-address)
			nsPayload[ipAddressBytes.Length + 1] = (byte)(macAddressBytes.Length / 6);  //Set "Lenght Field [17]" to 1 (6 Bytes - MAC lenght)

			Array.Copy(macAddressBytes, 0, nsPayload, 2 + ipAddressBytes.Length, macAddressBytes.Length);   //Set target mac to local address

			var packet = new IcmpV6Packet(header)
			{
				Type = IcmpV6Type.NeighborSolicitation,
				PayloadData = nsPayload
			};
			return packet;
		}

		public static IcmpV6Packet CreateRouterSolicitationPacket(PhysicalAddress sourceMac)
		{
			var macAddressBytes = sourceMac.GetAddressBytes();
			var header = new ByteArraySegment(new byte[8]); //NS-Packet has 8 byte header
			var nsPayload = new byte[2 + macAddressBytes.Length];

			nsPayload[0] = 1;   //Set "Type" to 1 (Source link layer-address)
			nsPayload[1] = (byte)(macAddressBytes.Length / 6);  //Set "Lenght" to 1 (6 Bytes - MAC lenght)

			Array.Copy(macAddressBytes, 0, nsPayload, 2, macAddressBytes.Length);   //Set target mac to local address

			var packet = new IcmpV6Packet(header)
			{
				Type = IcmpV6Type.RouterSolicitation,
				PayloadData = nsPayload
			};
			return packet;
		}
	}
}
