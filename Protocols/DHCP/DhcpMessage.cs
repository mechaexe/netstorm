﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using Protocols.DHCP.Protocol;
using Protocols.DHCP.Protocol.DhcpOptions;

namespace Protocols.DHCP
{
	public class DhcpMessage : IMessage
	{
		private const uint DhcpMagicCookie = 1669485411;

		public DhcpOperation DhcpOperation { get; set; }
		public HardwareType HardwareType { get; set; }
		public byte Hops { get; set; }
		public uint TransactionID { get; set; }
		public uint Elapsed { get; set; }
		public BootpFlag BootpFlags { get; set; }
		public IPAddress ClientIpAddress { get; set; }
		public IPAddress YourIpAddress { get; set; }
		public IPAddress NextServerIpAddress { get; set; }
		public IPAddress RelayAgentIpAddress { get; set; }
		public PhysicalAddress ClientMacAddress { get; set; }
		public string ServerName { get; set; }
		public string BootfileName { get; set; }
		public DhcpOptionList DhcpOptions { get; }

		public DhcpMessage()
		{
			ServerName = "";
			BootfileName = "";
			DhcpOptions = new DhcpOptionList();
			BootpFlags = BootpFlag.Broadcast;
			DhcpOperation = DhcpOperation.BootRequest;
			HardwareType = HardwareType.Ethernet;
			ClientIpAddress = IPAddress.Any;
			YourIpAddress = IPAddress.Any;
			NextServerIpAddress = IPAddress.Any;
			RelayAgentIpAddress = IPAddress.Any;
			ClientMacAddress = PhysicalAddress.None;
		}

		public static DhcpMessage Parse(byte[] packet)
		{
			var res = new DhcpMessage()
			{
				DhcpOperation = (DhcpOperation)packet[0],
				HardwareType = (HardwareType)packet[1],
				Hops = packet[3],
				TransactionID = BitConverter.ToUInt32(packet, 4),
				BootpFlags = (BootpFlag)BitConverter.ToUInt16(packet, 10),
				ClientIpAddress = new IPAddress(packet.Skip(12).Take(4).ToArray()),
				YourIpAddress = new IPAddress(packet.Skip(16).Take(4).ToArray()),
				NextServerIpAddress = new IPAddress(packet.Skip(20).Take(4).ToArray()),
				RelayAgentIpAddress = new IPAddress(packet.Skip(24).Take(4).ToArray()),
				ClientMacAddress = new PhysicalAddress(packet.Skip(28).Take(6).ToArray()),
				//ServerName = Encoding.ASCII.GetString(packet.Skip(44).Take(64).ToArray()),
				//BootfileName = Encoding.ASCII.GetString(packet.Skip(98).Take(128).ToArray())
			};
			for (int i = 240; i < packet.Length && ((DhcpOption)packet[i] != DhcpOption.End);)  //240 to skip magic cookie
			{
				int len = 2 + packet[i + 1];    // Add 2 to for op-code and lenght + add lenght
				var op = DhcpOptionParser.ParseOption(packet.Skip(i).Take(len).ToArray());
				res.DhcpOptions.Add(op.Option, op);
				i += len;
			}
			return res;
		}

		public override string ToString()
		{
			var res = "";
			res += $"OP: {DhcpOperation} ({(int)DhcpOperation})" +
				$"HW: {HardwareType} ({(int)HardwareType})";
			var op = 0;
			foreach (var kv in DhcpOptions)
				res += $"Option {op++}: {kv.Value}";
			return res;
		}

		public byte[] GetBytes()
		{
			var initialPacket = new byte[236];
			int offset = 0;

			initialPacket[offset++] = (byte)DhcpOperation;  //OP
			initialPacket[offset++] = (byte)HardwareType;   //HTYPE Ethernet = 0x1
			initialPacket[offset++] = 6;    //HW-Len 6 Octets
			initialPacket[offset++] = Hops; //HOPS

			BitConverter.GetBytes(TransactionID).CopyTo(initialPacket, offset); //XID (4 bytes)
			offset += 4;

			BitConverter.GetBytes(Elapsed).CopyTo(initialPacket, offset);       //Elapsed Seconds (2 bytes)
			offset += 2;

			BitConverter.GetBytes((int)BootpFlags).CopyTo(initialPacket, offset);   //Flags (2 bytes)
			offset += 2;

			ClientIpAddress.GetAddressBytes().CopyTo(initialPacket, offset+=4);    //CIADDR (4 bytes)
			offset += 4;

			YourIpAddress.GetAddressBytes().CopyTo(initialPacket, offset);      //YIADDR (4 bytes)
			offset += 4;

			NextServerIpAddress.GetAddressBytes().CopyTo(initialPacket, offset);    //SIADDR (4 bytes)
			offset += 4;

			RelayAgentIpAddress.GetAddressBytes().CopyTo(initialPacket, offset);    //GIADDR (4 bytes)
			offset += 4;

			ClientMacAddress.GetAddressBytes().CopyTo(initialPacket, offset);       //CHADDR (6 octets)
			offset += 4;

			//Ignore SNAME & BootFileName

			//Encoding.ASCII.GetBytes(ServerName).Take(64).ToArray().CopyTo(initialPacket, offset);  //SNAME (64 bytes)
			//offset += 64;

			//Encoding.ASCII.GetBytes(BootfileName).Take(128).ToArray().CopyTo(initialPacket, offset);  //BootFile (128 bytes)

			var packet = new List<byte>(initialPacket);
			if (DhcpOptions.Count > 0)
			{
				packet.AddRange(BitConverter.GetBytes(DhcpMagicCookie).Reverse());
				foreach (var option in DhcpOptions.Values)
				{
					packet.AddRange(option.GetBytes());
				}
			}
			return packet.ToArray();
		}
	}
}
