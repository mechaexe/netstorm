﻿using Protocols.DHCP.Client.RequestResolver;
using Protocols.DHCP.Protocol;
using Protocols.DHCP.Protocol.DhcpOptions;
using System;
using System.Net;
using System.Net.NetworkInformation;
using System.Threading.Tasks;
using System.Timers;

namespace Protocols.DHCP.Client
{
	public class DhcpClient
	{
		public const ushort DefaultRemotePort = 67;
		public const ushort DefaultLocalPort = 68;

		public PhysicalAddress PhysicalAddress { get; }
		public string Hostname { get; }
		public IPAddress ClientIP
		{
			get => _clientIP;
			private set
			{
				if (_clientIP != value)
				{
					_clientIP = value;
				}
			}
		}
		public IPAddress NextServerIpAddress { get; private set; }
		public IPAddress RelayAgentIpAddress { get; private set; }
		public string ServerName { get; private set; }
		public string BootfileName { get; private set; }
		public DhcpOptionList Options { get; }

		private IPAddress _clientIP;
		private readonly IRequestResolver resolver;
		private Timer renewTimer;
		private uint failedRenew;

		public DhcpClient(PhysicalAddress physicalAddress, string hostname, IPAddress bindIP) :
			this(physicalAddress, hostname, new UdpRequestResolver(new IPEndPoint(bindIP, DefaultLocalPort), new IPEndPoint(IPAddress.Broadcast, DefaultRemotePort)))
		{ }

		public DhcpClient(PhysicalAddress physicalAddress, string hostname, IRequestResolver resolver)
		{
			this.resolver = resolver;
			Hostname = hostname;
			PhysicalAddress = physicalAddress;
			ClientIP = IPAddress.None;
			NextServerIpAddress = IPAddress.None;
			RelayAgentIpAddress = IPAddress.None;
			ServerName = "";
			BootfileName = "";
			Options = new DhcpOptionList();
		}

		public async Task<bool> AcquireIp(IPAddress preferredIP, Progress<DhcpMessage> progress = null, bool autoRenew = false)
		{
			var successful = false;
			var offer = await Discover(preferredIP, progress);
			if (offer != null)
			{
				var ack = await Request(
					((IpOption)offer.DhcpOptions[DhcpOption.DhcpAddress]).Addresses[0],
					offer.YourIpAddress,
					progress, 
					offer.TransactionID, 
					autoRenew);
				successful = ack != null;
			}
			return successful;
		}

		public async Task<DhcpMessage> Discover(IPAddress preferredIP, Progress<DhcpMessage> progress, bool greedy = false)
		{
			var request = DhcpRequestFactory.CreateDiscover(preferredIP, PhysicalAddress);
			var offers = await resolver.Resolve(request, progress, IPAddress.Any, greedy);
			DhcpMessage result = null;
			foreach (var offer in offers)
			{
				if (offer.DhcpOptions[DhcpOption.DhcpMessageType][2] == (byte)DhcpMessageType.Offer)    // [0] -> DhcpOption, [1] -> Lenght, [2] -> MessageType
				{
					result = offer;
					break;
				}
			}
			return result;
		}

		public async Task<DhcpMessage> Request(IPAddress serverIP, IPAddress offeredIP, Progress<DhcpMessage> progress, uint transaction, bool autoRenew)
		{
			var request = DhcpRequestFactory.CreateRequest(offeredIP, PhysicalAddress, transaction);
			var acks = await resolver.Resolve(request, progress, serverIP);
			DhcpMessage result = null;
			foreach (var ack in acks)
			{
				if (ack.TransactionID == request.TransactionID &&
					ack.DhcpOperation == DhcpOperation.BootReply &&
					ack.DhcpOptions[DhcpOption.DhcpMessageType][2] == (byte)DhcpMessageType.Ack)
				{
					result = ack;
					Apply(result);
					break;
				}
			}
			if (result == null)
				failedRenew++;
			ConfigureAddressTime(autoRenew);
			return result;
		}

		public void Release()
		{
			if(ClientIP != IPAddress.None)
				Release(ClientIP);
		}

		public async Task<DhcpMessage[]> RequestInfo(Progress<DhcpMessage> progress)
			=> await resolver.Resolve(DhcpRequestFactory.CreateInform(ClientIP, PhysicalAddress), progress, IPAddress.Any, true);

		private void Release(IPAddress dhcpIP)
		{
			var request = DhcpRequestFactory.CreateRelease(dhcpIP, PhysicalAddress);
			resolver.Announce(request);
			ClientIP = IPAddress.None;
			NextServerIpAddress = IPAddress.None;
			RelayAgentIpAddress = IPAddress.None;
			ServerName = "";
			BootfileName = "";
			Options.Clear();
			renewTimer?.Dispose();
		}

		private void Apply(DhcpMessage acknowledgement)
		{
			failedRenew = 0;
			ClientIP = acknowledgement.YourIpAddress;
			NextServerIpAddress = acknowledgement.NextServerIpAddress;
			RelayAgentIpAddress = acknowledgement.RelayAgentIpAddress;
			ServerName = acknowledgement.ServerName;
			BootfileName = acknowledgement.BootfileName;
			
			Options.Clear();
			foreach (var option in acknowledgement.DhcpOptions)
			{
				if (option.Key != DhcpOption.DhcpMessageType &&
					option.Key != DhcpOption.End &&
					option.Key != DhcpOption.Pad)
				{
					Options.Add(option.Value);
				}
			}
		}

		private void ConfigureAddressTime(bool renew)
		{
			renewTimer?.Dispose();
			if(Options.ContainsKey(DhcpOption.AddressTime))
			{
				renewTimer = new Timer((Options[DhcpOption.AddressTime] as TimeOption).Time.TotalMilliseconds / 2 * (1 + failedRenew))
				{
					AutoReset = false,
					Enabled = true
				};
				if (renewTimer.Interval > 1000) //Release if interval is less than a minute
				{
					if (renew)
						renewTimer.Elapsed += async (_, __) => await Request(
							((IpOption)Options[DhcpOption.DhcpAddress]).Addresses[0],
							ClientIP,
							null,
							(uint)DateTime.UtcNow.Millisecond, true);
					else
						renewTimer.Elapsed += (_, __) => Release();
				}
				else if (renew)
					renewTimer.Elapsed += async (_, __) =>
					{
						Release();
						await AcquireIp(IPAddress.Any, null, true);
					};
			}
		}
	}
}
