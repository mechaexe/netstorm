﻿using Protocols.DHCP.Protocol;
using Protocols.DHCP.Protocol.DhcpOptions;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;

namespace Protocols.DHCP.Client.RequestResolver
{
	public class UdpRequestResolver : IRequestResolver
	{
		private class UdpState
		{
			public bool Greedy { get; set; }
			public uint TransactionID { get; set; }
			public CancellationTokenSource CancellationToken { get; set; }
			public IProgress<DhcpMessage> Progress { get; set; }
			public UdpClient UdpClient { get; set; }
			public IPEndPoint RemoteEP { get; set; }
			public IPAddress ExpectedServer { get; set; }
			public List<DhcpMessage> Messages { get; } = new List<DhcpMessage>();
		}

		private readonly IPEndPoint remoteEndpoint, localEndpoint;

		public UdpRequestResolver(IPEndPoint localEndpoint, IPEndPoint remoteEndpoint)
		{
			this.localEndpoint = localEndpoint;
			this.remoteEndpoint = remoteEndpoint;
		}

		public void Announce(DhcpMessage request)
		{
			using (var udpClient = new UdpClient(localEndpoint))
			{
				var data = request.GetBytes();
				udpClient.Send(data, data.Length, remoteEndpoint);
				udpClient.Close();
			}
		}

		public async Task<DhcpMessage[]> Resolve(DhcpMessage request, Progress<DhcpMessage> progress, IPAddress expectedAddress, bool greedy = false)
		{
			var result = new UdpState()
			{
				RemoteEP = new IPEndPoint(remoteEndpoint.Address, remoteEndpoint.Port),
				Progress = progress,
				TransactionID = request.TransactionID,
				ExpectedServer = expectedAddress,
				Greedy = greedy,
				CancellationToken = new CancellationTokenSource()
			};
			const int timeout_MS = 60000;
			const int ppM = 3;

			using (var udpClient = new UdpClient(localEndpoint))
			{
				result.UdpClient = udpClient;
				var data = request.GetBytes();
				udpClient.BeginReceive(new AsyncCallback(OnUdpPacketReceived), result);
				for (int i = 0; i < ppM && !result.CancellationToken.IsCancellationRequested; i++)
				{
					udpClient.Send(data, data.Length, remoteEndpoint);
					try { await Task.Delay(timeout_MS / ppM, result.CancellationToken.Token); }
					catch { }
				}

				udpClient.Close();
			}
			return result.Messages.ToArray();
		}

		private void OnUdpPacketReceived(IAsyncResult ar)
		{
			UdpState state = (UdpState)ar.AsyncState;
			var ep = new IPEndPoint(state.RemoteEP.Address, state.RemoteEP.Port);
			try
			{
				var raw = state.UdpClient.EndReceive(ar, ref ep);
				try
				{
					var packet = DhcpMessage.Parse(raw);
					if ((state.ExpectedServer == IPAddress.Any || 
						((IpOption)packet.DhcpOptions[DhcpOption.DhcpAddress]).Addresses[0].Equals(state.ExpectedServer)) &&
						state.TransactionID == packet.TransactionID)
					{
						state.Progress.Report(packet);
						state.Messages.Add(packet);
					}
				}
				catch { }
				if (state.Greedy || state.Messages.Count == 0)
					state.UdpClient.BeginReceive(new AsyncCallback(OnUdpPacketReceived), state);
				else
					state.CancellationToken.Cancel();
			}
			catch { }
		}
	}
}
