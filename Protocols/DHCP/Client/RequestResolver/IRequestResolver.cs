﻿using System;
using System.Net;
using System.Threading.Tasks;

namespace Protocols.DHCP.Client.RequestResolver
{
	public interface IRequestResolver
	{
		Task<DhcpMessage[]> Resolve(DhcpMessage request, Progress<DhcpMessage> progress, IPAddress expectedAddress, bool greedy = false);
		void Announce(DhcpMessage request);
	}
}
