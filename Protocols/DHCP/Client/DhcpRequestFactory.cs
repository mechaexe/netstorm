﻿using System;
using System.Net;
using System.Net.NetworkInformation;
using Protocols.DHCP.Protocol;

namespace Protocols.DHCP.Client
{
	public static class DhcpRequestFactory
	{
		private static DhcpMessage CreateDiscoverInternal(PhysicalAddress physicalAddress, IPAddress clientIP)
		{
			var res = new DhcpMessage()
			{
				DhcpOperation = DhcpOperation.BootRequest,
				TransactionID = (uint)DateTime.UtcNow.Ticks,
				ClientMacAddress = physicalAddress,
				ClientIpAddress = clientIP
			};
			res.DhcpOptions.Add(DhcpOption.DhcpMessageType, (byte)DhcpMessageType.Discover);
			return res;
		}

		public static DhcpMessage CreateInform(IPAddress clientIP, PhysicalAddress physicalAddress)
		{
			var res = CreateDiscoverInternal(physicalAddress, clientIP);
			res.DhcpOptions.SetOption(DhcpOption.DhcpMessageType, (byte)DhcpMessageType.Inform);
			res.DhcpOptions.Add(DhcpOption.ParameterList, new byte[]
			{
				(byte)DhcpOption.SubnetMask,
				(byte)DhcpOption.Router,
				(byte)DhcpOption.TimeServer,
				(byte)DhcpOption.NameServer,
				(byte)DhcpOption.DomainNameServer,
				(byte)DhcpOption.DomainNameSuffix,
				(byte)DhcpOption.BroadcastAddress,
				(byte)DhcpOption.StaticRoute,
				(byte)DhcpOption.ClassId,
				(byte)DhcpOption.VendorInformation,
				(byte)DhcpOption.FullQualifiedDomainName,
			});
			res.DhcpOptions.Add(DhcpOption.End);
			return res;
		}

		public static DhcpMessage CreateDiscover(PhysicalAddress physicalAddress)
		{
			var res = CreateDiscoverInternal(physicalAddress, IPAddress.Any);
			res.DhcpOptions.Add(DhcpOption.End);
			return res;
		}

		public static DhcpMessage CreateDiscover(IPAddress preferredIP, PhysicalAddress physicalAddress)
		{
			var res = CreateDiscoverInternal(physicalAddress, IPAddress.Any);
			res.DhcpOptions.Add(DhcpOption.AddressRequest, preferredIP.GetAddressBytes());
			res.DhcpOptions.Add(DhcpOption.End);
			return res;
		}

		public static DhcpMessage CreateRelease(IPAddress dhcpIP, PhysicalAddress physicalAddress)
		{
			var res = new DhcpMessage()
			{
				DhcpOperation = DhcpOperation.BootRequest,
				TransactionID = (uint)DateTime.UtcNow.Ticks,
				ClientMacAddress = physicalAddress,
				ClientIpAddress = dhcpIP
			};
			res.DhcpOptions.Add(DhcpOption.DhcpMessageType, (byte)DhcpMessageType.Release);
			res.DhcpOptions.Add(DhcpOption.End);
			return res;
		}

		public static DhcpMessage CreateRequest(IPAddress offeredIP, PhysicalAddress physicalAddress, uint transaction)
		{
			var res = new DhcpMessage()
			{
				DhcpOperation = DhcpOperation.BootRequest,
				ClientMacAddress = physicalAddress,
				TransactionID = transaction
			};
			res.DhcpOptions.Add(DhcpOption.DhcpMessageType, (byte)DhcpMessageType.Request);
			res.DhcpOptions.Add(DhcpOption.AddressRequest, offeredIP.GetAddressBytes());
			res.DhcpOptions.Add(DhcpOption.End);
			return res;
		}
	}
}
