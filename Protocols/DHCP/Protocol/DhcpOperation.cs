﻿namespace Protocols.DHCP.Protocol
{
	public enum DhcpOperation : byte
	{
		BootRequest = 0x01,
		BootReply
	}
}
