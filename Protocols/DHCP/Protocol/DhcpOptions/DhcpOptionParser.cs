﻿namespace Protocols.DHCP.Protocol.DhcpOptions
{
	public static class DhcpOptionParser
	{
		public static IDhcpOption ParseOption(byte[] data)
		{
			switch ((DhcpOption)data[0])
			{
				case DhcpOption.BroadcastAddress:
				case DhcpOption.DhcpAddress:
				case DhcpOption.DomainNameServer:
				case DhcpOption.Router:
				case DhcpOption.SubnetMask:
					return new IpOption(data);

				case DhcpOption.AddressTime:
				case DhcpOption.AddressRenewal:
				case DhcpOption.AddressRebind:
					return new TimeOption(data);

				case DhcpOption.DomainNameSuffix:
					return new StringOption(data);

				default:
					return new BaseDhcpOption(data);
			}
		}
	}
}
