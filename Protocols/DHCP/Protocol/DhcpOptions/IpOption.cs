﻿using System;
using System.Linq;
using System.Net;

namespace Protocols.DHCP.Protocol.DhcpOptions
{
	public class IpOption : BaseDhcpOption
	{
		public IPAddress[] Addresses { get; }

		public override byte[] Contents
		{
			get
			{
				var res = new byte[4 * Addresses.Length];
				for (int i = 0; i < Addresses.Length; i++)
				{
					Array.Copy(Addresses[i].GetAddressBytes(), 0, res, 4 * i, 4);
				}

				return res;
			}
		}

		public IpOption(byte[] data)
		{
			Option = (DhcpOption)data[0];
			Addresses = new IPAddress[(data.Length - 2) / 4];
			for (int i = 2; i < data.Length; i += 4)
				Addresses[(i - 2) / 4] = new IPAddress(data.Skip(i).Take(4).ToArray());
		}

		public IpOption(DhcpOption option, IPAddress address)
		{
			Option = option;
			Addresses = new IPAddress[] { address };
		}

		public IpOption(DhcpOption option, IPAddress[] addresses)
		{
			Option = option;
			Addresses = addresses;
		}

		public override string ToString()
		{
			var res = Option.ToString() + ": ";
			foreach (var a in Addresses)
			{
				res += a.ToString() + ", ";
			}

			return res;
		}
	}
}
