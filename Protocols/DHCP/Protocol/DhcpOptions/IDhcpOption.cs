﻿namespace Protocols.DHCP.Protocol.DhcpOptions
{
	public interface IDhcpOption
	{
		byte this[int index] { get; }
		DhcpOption Option { get; }

		byte[] Contents { get; }

		byte[] GetBytes();
	}
}