﻿using System.Collections.Generic;

namespace Protocols.DHCP.Protocol.DhcpOptions
{
	public class DhcpOptionList : Dictionary<DhcpOption, IDhcpOption>
	{
		private byte[] PrepareData(DhcpOption option, params byte[] data)
		{
			var content = new byte[data.Length + 2];
			content[0] = (byte)option;
			content[1] = (byte)data.Length;
			data.CopyTo(content, 2);
			return content;
		}

		public void SetOption(DhcpOption option, params byte[] data)
		{
			if (!ContainsKey(option))
			{
				Add(option, data);
			}
			else
			{
				this[option] = new BaseDhcpOption(option, data);
			}
		}

		public void Add(DhcpOption option, params byte[] data)
			=> Add(option, new BaseDhcpOption(option, data));

		public void Add(IDhcpOption option)
			=> Add(option.Option, option);
	}
}
