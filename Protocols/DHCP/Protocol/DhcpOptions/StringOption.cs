﻿using System.Text;

namespace Protocols.DHCP.Protocol.DhcpOptions
{
	public class StringOption : BaseDhcpOption
	{
		public string String { get; }

		public override byte[] Contents => Encoding.ASCII.GetBytes(String);

		public StringOption(DhcpOption option, string str)
		{
			Option = option;
			String = str;
		}

		public StringOption(byte[] data) : this((DhcpOption)data[0], Encoding.ASCII.GetString(data, 2, data[1]))
		{
		}

		public override string ToString()
			=> $"{Option}: {String}";
	}
}
