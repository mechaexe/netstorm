﻿using System;

namespace Protocols.DHCP.Protocol.DhcpOptions
{
	public class BaseDhcpOption : IDhcpOption
	{
		public DhcpOption Option { get; protected set; }

		public virtual byte[] Contents { get; protected set; }

		public byte this[int index]
		{
			get
			{
				if (index == 0)
					return (byte)Option;
				else if (index == 1)
					return (byte)Contents.Length;
				else
					return Contents[index - 2];
			}
		}

		public BaseDhcpOption()
		{
			Option = DhcpOption.End;
			Contents = new byte[0];
		}

		public BaseDhcpOption(DhcpOption option, params byte[] contents)
		{
			Option = option;
			Contents = contents;
		}

		// Parse DHCP accordingly
		public BaseDhcpOption(byte[] data)
		{
			Option = (DhcpOption)data[0];
			if (data.Length > 1)
			{
				Contents = new byte[data.Length - 2];
				if (Contents.Length > 0)
					Array.Copy(data, 2, Contents, 0, Contents.Length);
			}
			else Contents = new byte[0];
		}

		public byte[] GetBytes()
		{
			var bytes = new byte[2 + Contents.Length];
			bytes[0] = (byte)Option;
			bytes[1] = (byte)(Contents.Length);
			Array.Copy(Contents, 0, bytes, 2, Contents.Length);
			return bytes;
		}

		public override string ToString()
		{
			var res = "";
			foreach (var d in Contents)
			{
				res += d.ToString() + ", ";
			}

			return $"{Option} [{Contents.Length}] - {res}";
		}
	}
}
