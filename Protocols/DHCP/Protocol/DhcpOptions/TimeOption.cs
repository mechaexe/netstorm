﻿using System;
using System.Linq;

namespace Protocols.DHCP.Protocol.DhcpOptions
{
	public class TimeOption : BaseDhcpOption
	{
		public TimeSpan Time { get; }

		public override byte[] Contents => BitConverter.GetBytes(Time.TotalSeconds);

		public TimeOption(byte[] bytes)
		{
			Option = (DhcpOption)bytes[0];

			if (bytes.Length > 1)
			{
				var b = bytes.Skip(2).Take(bytes[1]).Reverse().ToArray();
				var t = BitConverter.ToUInt32(b, 0);
				Time = new TimeSpan(0, 0, (int)t);
			}
		}

		public TimeOption(DhcpOption option, TimeSpan time)
		{
			Option = option;
			Time = time;
		}

		public override string ToString()
			=> $"{Option}: {Time}";
	}
}
