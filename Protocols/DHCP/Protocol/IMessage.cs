﻿using Protocols.DHCP.Protocol.DhcpOptions;
using System.Net;
using System.Net.NetworkInformation;

namespace Protocols.DHCP.Protocol
{
	public interface IMessage
	{
		DhcpOperation DhcpOperation { get; set; }
		HardwareType HardwareType { get; set; }
		byte Hops { get; set; }
		uint TransactionID { get; set; }
		uint Elapsed { get; set; }
		BootpFlag BootpFlags { get; set; }
		IPAddress ClientIpAddress { get; set; }
		IPAddress YourIpAddress { get; set; }
		IPAddress NextServerIpAddress { get; set; }
		IPAddress RelayAgentIpAddress { get; set; }
		PhysicalAddress ClientMacAddress { get; set; }
		string ServerName { get; set; }
		string BootfileName { get; set; }
		DhcpOptionList DhcpOptions { get; }
		byte[] GetBytes();
	}
}