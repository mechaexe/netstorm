﻿namespace Protocols.DHCP.Protocol
{
	public enum HardwareType
	{
		Ethernet = 0x01,
		ExperimentalEthernet,
		AmateurRadio,
		ProteonTokenRing,
		Chaos,
		IEEE802Networks,
		ArcNet,
		Hyperchnnel,
		Lanstar
	}
}
