﻿namespace Protocols.DHCP.Protocol
{
	public enum DhcpMessageType
	{
		Discover = 0x01,
		Offer,
		Request,
		Decline,
		Ack,
		Nak,
		Release,
		Inform,
		ForceRenew,
		LeaseQuery,
		LeaseUnassigned,
		LeaseUnknown,
		LeaseActive
	}
}
