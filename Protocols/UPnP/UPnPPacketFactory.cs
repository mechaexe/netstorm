﻿using System;
using Protocols.HTTP;

namespace Protocols.UPnP
{
	public static class UPnPPacketFactory
	{
		public static HttpRequest CreateDiscoveryPacket(string searchTarget, int timeout_S)
		{
			var req = new HttpRequest()
			{
				HttpVersion = new Version(1, 1),
				Method = "M-SEARCH",
				URI = "*"
			};
			req.Headers.Add("HOST", "239.255.255.250:1900");
			req.Headers.Add("MAN", "ssdp:discover");
			req.Headers.Add("MX", timeout_S.ToString());
			req.Headers.Add("ST", searchTarget);
			return req;
		}
		public static HttpRequest CreateDiscoveryPacketAny(int timeout_S)
			=> CreateDiscoveryPacket("ssdp:all", timeout_S);
		public static HttpRequest CreateDiscoveryPacketRoot(int timeout_S)
			=> CreateDiscoveryPacket("upnp:rootdevice", timeout_S);
		public static HttpRequest CreateDiscoveryPacketAny()
			=> CreateDiscoveryPacketAny(5);
		public static HttpRequest CreateDiscoveryPacketRoot()
			=> CreateDiscoveryPacketRoot(5);
	}
}
