﻿using Protocols.HTTP;
using Protocols.UPnP.RequestResolver;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace Protocols.UPnP
{
	public class UPnPDiscovery
	{
		public const ushort UPnPPort = 1900;
		public static readonly IPAddress UPnPMcast = IPAddress.Parse("239.255.255.250");
		
		public async Task<List<(IPAddress, HttpResponse)>> Discover(IPAddress bindAddress, string searchTarget, int timeout_S)
		{
			//UPnP listens on MCAST 239.255.255.250:1900 UDP
			var remoteEndpoint = new IPEndPoint(UPnPMcast, UPnPPort);
			var req = new HttpRequest()
			{
				HttpVersion = new Version(1, 1),
				Method = "M-SEARCH",
				URI = "*"
			};
			req.Headers.Add("HOST", $"{remoteEndpoint.Address}:{remoteEndpoint.Port}");
			req.Headers.Add("MAN", "\"ssdp:discover\"");
			req.Headers.Add("MX", timeout_S.ToString());
			req.Headers.Add("ST", searchTarget);
			var res = new UdpRequestResolver(new IPEndPoint(bindAddress, 0), remoteEndpoint, timeout_S * 1000);
			return await res.Resolve(req);
		}
		public async Task<List<(IPAddress, HttpResponse)>> DiscoverAnyDevices(IPAddress bindAddress)
			=> await Discover(bindAddress, "ssdp:all", 5);
		public async Task<List<(IPAddress, HttpResponse)>> DiscoverRootDevices(IPAddress bindAddress)
			=> await Discover(bindAddress, "upnp:rootdevice", 5);
	}
}
