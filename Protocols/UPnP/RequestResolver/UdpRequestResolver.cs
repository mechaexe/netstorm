﻿using Protocols.HTTP;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace Protocols.UPnP.RequestResolver
{
	public class UdpRequestResolver : IRequestResolver
	{
		private readonly int timeout;
		private readonly IPEndPoint remoteEndpoint, localEndpoint;

		public UdpRequestResolver(IPEndPoint localEndpoint, IPEndPoint remoteEndpoint, int timeout = 60000)
		{
			this.localEndpoint = localEndpoint;
			this.remoteEndpoint = remoteEndpoint;
			this.timeout = timeout;
		}

		public async Task<List<(IPAddress, HttpResponse)>> Resolve(HttpRequest request)
		{
			var packets = new List<(IPAddress, HttpResponse)>();
			Task rx;
			using (var udpClient = new UdpClient(localEndpoint))
			{
				rx = Task.Run(() => RxAsync(udpClient, remoteEndpoint, ref packets));
				var data = request.ToBytes();
				udpClient.Send(data, data.Length, remoteEndpoint);
				for (int i = 0; i < timeout / 100; i++)
					await Task.Delay(100);

				udpClient.Close();
			}
			await rx;
			return packets;
		}

		private void RxAsync(UdpClient udpClient, IPEndPoint destEndPoint, ref List<(IPAddress, HttpResponse)> packets)
		{
			try
			{
				var raw = new byte[0];
				while (true)
				{
					var epCpy = new IPEndPoint(destEndPoint.Address, destEndPoint.Port);
					raw = udpClient.Receive(ref epCpy);
					var packet = HttpResponse.Parse(raw);
					packets.Add((epCpy.Address, packet));
				}
			}
			catch { }
		}
	}
}
