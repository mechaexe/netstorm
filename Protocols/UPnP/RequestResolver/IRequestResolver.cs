﻿using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Protocols.HTTP;

namespace Protocols.UPnP.RequestResolver
{
	public interface IRequestResolver
	{
		Task<List<(IPAddress, HttpResponse)>> Resolve(HttpRequest request);
	}
}
