﻿using System.Collections.Generic;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading;

namespace Protocols.ICMP
{
	public class TraceRoute
	{
		public static IEnumerable<PingReply> TraceIcmp(string destination, int maxHops, int delayMs)
		{
			using var p = new Ping();
			for (int hops = 0, ttl = 1; hops < maxHops; ttl++)
			{
				var reply = p.Send(destination, 10000, Encoding.ASCII.GetBytes("abcdefghi"), new PingOptions(ttl, true));

				if (reply.Status == IPStatus.TtlExpired || reply.Status == IPStatus.Success)
				{
					hops++;
					yield return reply;
				}

				if (reply.Status != IPStatus.TtlExpired && reply.Status != IPStatus.TimedOut)
					break;

				if (delayMs > 0)
					Thread.Sleep(delayMs);
			}
		}
	}
}
