﻿using System;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;

namespace Protocols.TCP
{
	public class TcpPing
	{
		private PingState ParseSocketError(SocketError socketError)
		{
			switch (socketError)
			{
				case SocketError.ConnectionRefused:
				case SocketError.ConnectionReset:
					return PingState.PortUnreachable;

				case SocketError.TimedOut:
				case SocketError.NetworkReset:
					return PingState.TimedOut;

				default:
					return PingState.DeviceUnreachable;
			}
		}

		public TcpPingResult Send(IPEndPoint localEndpoint, string hostOrIp, ushort destinationPort, short ttl, int timeout)
		{
			var res = new TcpPingResult()
			{
				DestinationPort = destinationPort,
				Source = localEndpoint.Address,
				SourcePort = (ushort)localEndpoint.Port
			};

			using var sock = new Socket(localEndpoint.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
			sock.Ttl = ttl;
			var sw = new Stopwatch();
			try
			{
				sw.Start();
				sock.Bind(localEndpoint);
				res.RequestSent = DateTime.Now;
				var connInfo = sock.BeginConnect(hostOrIp, destinationPort, null, null);
				var signalled = connInfo.AsyncWaitHandle.WaitOne(timeout);
				sw.Stop();

				if (signalled || sock.Connected)
				{
					sock.EndConnect(connInfo);

					res.State = PingState.PortUnreachable;
					res.Ttl = sock.Ttl;

					if (sock.Connected)
					{
						res.Destination = (sock.RemoteEndPoint as IPEndPoint).Address;
						var lIpEp = sock.LocalEndPoint as IPEndPoint;
						res.Source = lIpEp.Address;
						res.SourcePort = (ushort)lIpEp.Port;
						res.State = PingState.Success;
					}
				}
				else throw new SocketException(10060);  // timed out
			}
			catch (SocketException se)
			{
				res.Ttl = ttl;
				res.State = ParseSocketError(se.SocketErrorCode);
			}
			finally
			{
				res.RoundTripTime = sw.Elapsed;
				sock.Dispose();
			}

			return res;
		}

		public TcpPingResult Send(string hostOrIP, ushort destinationPort, AddressFamily prefferedAddressFamily, short ttl, int timeout)
			=> Send(new IPEndPoint(prefferedAddressFamily == AddressFamily.InterNetwork ? IPAddress.Any : IPAddress.IPv6Any, 0), hostOrIP, destinationPort, ttl, timeout);
	}
}
