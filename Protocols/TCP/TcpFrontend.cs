﻿using NetStorm.Common.Logging;
using Protocols.TCP.Streams;
using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Security.Authentication;

namespace Protocols.TCP
{
	public class TcpFrontend
	{
		public bool ConnectTo(IPEndPoint localEndpoint, Uri remoteUri, bool ssl, InteractiveStream interactiveStream)
		{
			IStreamFactory streamFactory;
			if (ssl)
				streamFactory = new SecureSocketStream()
				{
					IgnoreSslError = true,
					AuthorisedHost = remoteUri.IdnHost
				};
			else streamFactory = new DefaultNetworkStream();
			return ConnectTo(localEndpoint, remoteUri, streamFactory, interactiveStream);
		}

		public bool ConnectTo(IPEndPoint localEndpoint, Uri remoteUri, IStreamFactory streamFactory, InteractiveStream interactiveStream)
		{
			var successful = false;
			try
			{
				using var client = new TcpClient(localEndpoint)
				{
					ReceiveTimeout = 10000
				};
				client.Connect(remoteUri.DnsSafeHost, remoteUri.Port);

				if (successful = client.Connected)
				{
					using var stream = streamFactory.GetStream(client.GetStream());
					interactiveStream(stream, client);
					stream.Dispose();
				}

				client.Dispose();
			}
			catch (SocketException se) { LogEngine.GlobalLog.Log($"TCP request to {remoteUri.Host}:{remoteUri.Port} failed (SocketException): {se.Message}"); }
			catch (IOException ioe) { LogEngine.GlobalLog.Log($"TCP request to {remoteUri.Host}:{remoteUri.Port} failed (IOException): {ioe.Message}"); }
			catch (AuthenticationException ae) { LogEngine.GlobalLog.Log($"TCP request to {remoteUri.Host}:{remoteUri.Port} failed (AuthenticationException): {ae.Message}"); }
			return successful;
		}
	}

	public delegate void InteractiveStream(Stream stream, TcpClient client); 
}
