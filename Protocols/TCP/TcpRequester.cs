﻿using Protocols.Extensions;
using System;
using System.Net;

namespace Protocols.TCP
{
	public class TcpRequester
	{
		public byte[] GetResponse(IPEndPoint localEndpoint, Uri remoteUri, bool ssl, byte[] initialRequest = null)
		{
			var result = new byte[0];
			var func = new InteractiveStream((stream, client) =>
			{
				if (stream.CanWrite && initialRequest != null)
				{
					stream.Write(initialRequest, 0, initialRequest.Length);
					stream.Flush();
				}

				result = stream.ReadToEnd();
			});
			var frontend = new TcpFrontend();
			frontend.ConnectTo(localEndpoint, remoteUri, ssl, func);
			return result;
		}
	}
}
