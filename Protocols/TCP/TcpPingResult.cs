﻿using System;
using System.Net;

namespace Protocols.TCP
{
	public enum PingState
	{
		DeviceUnreachable,
		PortUnreachable,
		TimedOut,
		Success,
	}

	public class TcpPingResult
	{
		public ushort DestinationPort { get; set; }
		public ushort SourcePort { get; set; }
		public IPAddress Destination { get; set; }
		public IPAddress Source { get; set; }
		public short Ttl { get; set; }
		public PingState State { get; set; }
		public DateTime RequestSent { get; set; }
		public TimeSpan RoundTripTime { get; set; }
	}
}
