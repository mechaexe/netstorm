﻿using System.IO;
using System.Net.Security;

namespace Protocols.TCP.Streams
{
	class SecureSocketStream : IStreamFactory
	{
		public bool IgnoreSslError { get; set; }
		public string AuthorisedHost { get; set; }

		public Stream GetStream(Stream stream)
		{
			SslStream sslStream = IgnoreSslError ?
				sslStream = new SslStream(stream, false, AcceptAllCertifications) :
				new SslStream(stream, false);
			sslStream.AuthenticateAsClient(AuthorisedHost);
			return sslStream;
		}

		private static bool AcceptAllCertifications(object sender,
			System.Security.Cryptography.X509Certificates.X509Certificate certification,
			System.Security.Cryptography.X509Certificates.X509Chain chain,
			SslPolicyErrors sslPolicyErrors)
			=> true;
	}
}
