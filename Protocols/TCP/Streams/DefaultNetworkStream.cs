﻿using System.IO;

namespace Protocols.TCP.Streams
{
	class DefaultNetworkStream : IStreamFactory
	{
		public Stream GetStream(Stream stream)
			=> stream;
	}
}
