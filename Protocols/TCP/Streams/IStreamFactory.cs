﻿using System.IO;

namespace Protocols.TCP.Streams
{
	public interface IStreamFactory
	{
		Stream GetStream(Stream stream);
	}
}