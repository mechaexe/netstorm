﻿using System;
using System.Collections.Generic;

namespace Protocols.HTTP
{
	public class HttpRequest : HttpPacket
	{
		public string Method { get; set; } = "";
		public string URI { get; set; } = "";

		public IEnumerable<KeyValuePair<string, string>> GetParams()
		{
			var parameters = URI.Split('?');
			if(parameters.Length > 1)
				foreach(var param in parameters[1].Split('&'))
				{
					var action = param.Split('=')[0];
					KeyValuePair<string, string> res;
					try
                    {
						res = new KeyValuePair<string, string>(action, param[(action.Length + 1)..]);
                    }
                    catch (ArgumentOutOfRangeException)
                    {
						res = new KeyValuePair<string, string>(action, string.Empty);
                    }
					yield return res;
				}
		}

		public override string ToString()
		{
			var res = $"{Method} {URI} HTTP/{HttpVersion.Major}.{HttpVersion.Minor}{Separator}";

			foreach (var header in Headers)
				res += $"{header.Key}:{header.Value}{Separator}";

			res += $"{Separator}{Content}";
			return res;
		}
	}
}
