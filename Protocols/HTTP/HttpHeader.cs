﻿using System.Collections;
using System.Collections.Generic;

namespace Protocols.HTTP
{
	class HttpHeader : IDictionary<string, string>
	{
		private readonly Dictionary<string, string> dict = new Dictionary<string, string>();

		public ICollection<string> Keys => ((IDictionary<string, string>)dict).Keys;

		public ICollection<string> Values => ((IDictionary<string, string>)dict).Values;

		public int Count => ((ICollection<KeyValuePair<string, string>>)dict).Count;

		public bool IsReadOnly => ((ICollection<KeyValuePair<string, string>>)dict).IsReadOnly;

		public string this[string key] { get => dict[key.ToLower()]; set => dict[key.ToLower()] = value; }

		public void Add(string key, string value)
			=> dict.Add(key.ToLower(), value);

		public bool Remove(string key)
			=> dict.Remove(key.ToLower());

		public bool TryGetValue(string key, out string value)
			=> dict.TryGetValue(key.ToLower(), out value);

		public bool ContainsKey(string key)
			=> dict.ContainsKey(key.ToLower());

		public void Add(KeyValuePair<string, string> item)
		{
			((ICollection<KeyValuePair<string, string>>)dict).Add(item);
		}

		public void Clear()
		{
			((ICollection<KeyValuePair<string, string>>)dict).Clear();
		}

		public bool Contains(KeyValuePair<string, string> item)
		{
			return ((ICollection<KeyValuePair<string, string>>)dict).Contains(item);
		}

		public void CopyTo(KeyValuePair<string, string>[] array, int arrayIndex)
		{
			((ICollection<KeyValuePair<string, string>>)dict).CopyTo(array, arrayIndex);
		}

		public bool Remove(KeyValuePair<string, string> item)
		{
			return ((ICollection<KeyValuePair<string, string>>)dict).Remove(item);
		}

		public IEnumerator<KeyValuePair<string, string>> GetEnumerator()
		{
			return ((IEnumerable<KeyValuePair<string, string>>)dict).GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return ((IEnumerable)dict).GetEnumerator();
		}
	}
}
