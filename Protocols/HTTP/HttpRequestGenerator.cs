﻿using System;

namespace Protocols.HTTP
{
	public static class HttpRequestGenerator
	{
		public static HttpRequest CreateSimpleRequest(Uri uri)
		{
			var request = new HttpRequest()
			{
				Method = "GET",
				URI = uri.LocalPath,
				HttpVersion = new Version(1, 1)
			};
			request.Headers.Add("Host", uri.Host);
			request.Headers.Add("User-Agent", "Mozilla/5.0 (X11; Linux x86_64; rv:12.0) Gecko/20100101 Firefox/12.0");
			request.Headers.Add("Connection", "close");
			return request;
		}
	}
}
