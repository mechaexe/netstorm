﻿using Protocols.TCP;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Text;

namespace Protocols.HTTP
{
	public class HttpClient
	{
        public static string CreateRequest(string method, Uri uri, string content)
            => $"{method} {uri.PathAndQuery} HTTP/1.1\r\n" +
            $"Host: {uri.IdnHost}:{uri.Port}\r\n" +
            $"User-Agent: NetStorm Crawler\r\n" +
			//$"Accept-Encoding: gzip\r\n" +
			(!string.IsNullOrEmpty(content) ? $"Content-Lenght: {Encoding.ASCII.GetBytes(content).Length}\r\n" : "") +
            $"Connection: Close\r\n\r\n";

        public static List<HttpResponse> Get(IPEndPoint localEndpoint, Uri location, int maxRedirects)
        {
            var res = new List<HttpResponse>();
            for(int i = 0; i <= maxRedirects || maxRedirects < 0; i++)
			{
                var packet = Get(localEndpoint, location);
                if (packet != null)
                {
                    res.Add(packet);
                    if (packet.StatusCode >= 300 &&
                        packet.StatusCode < 400 &&
                        packet.Headers.ContainsKey("location"))
                        location = new Uri(packet.Headers["location"]);
                    else break;
                }
                else break;
			}
            return res;
        }

        public static HttpResponse Get(Uri location)
        {
            HttpResponse res = null;
            try
            {
                using var wclient = new WebClient
                {
                    Encoding = Encoding.UTF8
                };
                var content = wclient.DownloadString(location);
                res = new HttpResponse
                {
                    Content = content,
                    StatusCode = 200,
                    StatusMessage = "OK",
                    HttpVersion = new Version(1, 1)
                };
                for (int i = 0; i < wclient.ResponseHeaders.Count; i++)
                    res.Headers.Add(wclient.ResponseHeaders.Keys[i], wclient.ResponseHeaders.GetValues(i)[0]);
            }
            catch(WebException we) 
            {
                HttpWebResponse response = (HttpWebResponse)we.Response;
                res = new HttpResponse()
                {
                    StatusCode = (int)response.StatusCode,
                    StatusMessage = response.StatusDescription,
                    HttpVersion = response.ProtocolVersion
                };
                for (int i = 0; i < response.Headers.Count; i++)
                    res.Headers.Add(response.Headers.Keys[i], response.Headers.GetValues(i)[0]);
            }
			catch { }
            return res;
        }

        // Inspired by https://stackoverflow.com/a/35533927
        public static HttpResponse Get(IPEndPoint localEndpoint, Uri location)
        {
            HttpResponse result = null;
            if (localEndpoint.Address == IPAddress.Any && localEndpoint.Port == 0)
                result = Get(location);
			else
			{
                var tcp = new TcpFrontend();
                var logic = new InteractiveStream((stream, client) =>
                {
                    var request = CreateRequest("GET", location, null);
                    var requestBytes = Encoding.ASCII.GetBytes(request);
                    stream.Write(requestBytes, 0, requestBytes.Length);

                    result = Receive(stream);
                });
                tcp.ConnectTo(localEndpoint, location, location.Scheme == "https", logic);
            }
            return result;
        }

        private static HttpResponse Receive(Stream stream)
		{
            using (var memory = new MemoryStream())
            {
                stream.CopyTo(memory);
                memory.Position = 0;
                var data = memory.ToArray();

                if (data.Length > 0)
                {
                    var index = BinaryMatch(data, Encoding.ASCII.GetBytes("\r\n\r\n")) + 4;
                    var headers = Encoding.ASCII.GetString(data, 0, index);
                    memory.Position = index;

                    if (headers.IndexOf("Content-Encoding: gzip") > 0)
                    {
                        using GZipStream decompressionStream = new GZipStream(memory, CompressionMode.Decompress);
                        using var decompressedMemory = new MemoryStream();
                        decompressionStream.CopyTo(decompressedMemory);
                        decompressedMemory.Position = 0;
                        return HttpResponse.Parse(headers, Encoding.UTF8.GetString(decompressedMemory.ToArray()));
                    }
                    else
                        return HttpResponse.Parse(headers, Encoding.UTF8.GetString(data, index, data.Length - index));
                }
                else return null;
            }
        }

        private static int BinaryMatch(byte[] input, byte[] pattern)
        {
            int sLen = input.Length - pattern.Length + 1;
            for (int i = 0; i < sLen; ++i)
            {
                bool match = true;
                for (int j = 0; j < pattern.Length; ++j)
                {
                    if (input[i + j] != pattern[j])
                    {
                        match = false;
                        break;
                    }
                }
                if (match)
                {
                    return i;
                }
            }
            return -1;
        }
	}
}
