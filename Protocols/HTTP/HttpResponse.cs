﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Protocols.HTTP
{
	public class HttpResponse : HttpPacket
	{
		//public HttpRequest Request { get; set; }

		public int StatusCode { get; set; } = 0;
		public string StatusMessage { get; set; } = "";

		public override string ToString()
		{
			var res = $"HTTP/{HttpVersion.Major}.{HttpVersion.Minor} {StatusCode} {StatusMessage}{Separator}";
			foreach (var header in Headers)
			{
				res += $"{header.Key}:{header.Value}{Separator}";
			}

			res += $"{Separator}{Content}";
			return res;
		}

		public static HttpResponse Parse(string head, string content)
		{
			HttpResponse result = null;
			try
			{
				if (head.StartsWith("HTTP/"))
				{
					var headers = head.Split(new[] { Separator }, StringSplitOptions.RemoveEmptyEntries);
					var responseCode = headers[0].Split(new char[] { ' ' }, 3);
					result = new HttpResponse()
					{
						HttpVersion = Version.Parse(responseCode[0].Remove(0, 5)),
						StatusCode = int.Parse(responseCode[1]),
						Content = content.Trim()
					};

					result.StatusMessage = responseCode[2].Trim();

					for (int i = 1; i < headers.Length; i++)
					{
						var seperatorIndex = headers[i].IndexOf(":");
						result.Headers.TryAdd(headers[i].Substring(0, seperatorIndex), headers[i].Substring(seperatorIndex + 1).Trim());
					}
				}
			}
			catch { }
			return result;
		}

		public static HttpResponse Parse(string response)
		{
			HttpResponse result = null;
			try
			{
				if (response.StartsWith("HTTP/"))
				{
					var packet = response.Split(new[] { Separator + Separator }, StringSplitOptions.RemoveEmptyEntries);
					var headers = packet[0].Split(new[] { Separator }, StringSplitOptions.RemoveEmptyEntries);
					var head = headers[0].Split(' ');
					result = new HttpResponse()
					{
						HttpVersion = Version.Parse(head[0].Remove(0, 5)),
						StatusCode = int.Parse(head[1]),
						Content = packet.Length == 2 ? packet[1].Replace("\0","").Trim() : ""
					};
					for (int i = 2; i < head.Length; i++)
					{
						result.StatusMessage += $"{head[i]} ";
					}

					result.StatusMessage = result.StatusMessage.Trim();
					for (int i = 1; i < headers.Length; i++)
					{
						var seperatorIndex = headers[i].IndexOf(":");
						result.Headers.TryAdd(headers[i].Substring(0, seperatorIndex), headers[i].Substring(seperatorIndex + 1).Trim());
					}
				}
			}
			catch { }
			return result;
		}

		public static HttpResponse Parse(byte[] data)
		{
			if (data != null)
				return Parse(Encoding.UTF8.GetString(data));
			else return null;
		}
	}
}
