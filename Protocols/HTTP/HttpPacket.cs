﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Protocols.HTTP
{
	public abstract class HttpPacket
	{
		public static string Separator = "\r\n";

		public Version HttpVersion { get; set; } = new Version(1, 1);
		public IDictionary<string,string> Headers { get; } = new HttpHeader();
		public string Content { get; set; } = "";

		public byte[] ToBytes()
			=> ToBytes(Encoding.ASCII);

		public byte[] ToBytes(Encoding encoding)
			=> encoding.GetBytes(ToString());
	}
}
