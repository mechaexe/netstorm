﻿namespace Protocols.Extensions
{
	static class StringExtensions
	{
		public static string ForceLF(this string self)
			=> self.Replace("\r\n", "\n").Replace("\r", "\n");
	}
}
