﻿using System.Collections.Generic;
using System.IO;

namespace Protocols.Extensions
{
	public static class StreamExtensions
	{
		public static byte[] ReadToEnd(this Stream stream)
		{
			var data = new List<byte>();
			if (stream.CanRead)
			{
				const int lenght = 256;
				int bytesRead;
				byte[] buffer = new byte[lenght];
				do
				{
					bytesRead = stream.Read(buffer, 0, buffer.Length);
					if(bytesRead > 0)
						data.AddRange(buffer);
					buffer = new byte[lenght];
				} while (bytesRead == buffer.Length);
			}
			return data.ToArray();
		}
	}
}
