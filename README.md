# NetStorm [alpha]
NetStorm is a WIP network discovery tool written in C#. It is still in alpha stage.

Currently, there are six modules available:

* Network discovery
* DHCP discovery
* Device discovery with basic portscanner
* ARP MitM Detector
* ARP Spoofer
* DHCP exhauster

You can download a more or less working release [here](https://bitbucket.org/mechaexe/netstorm/downloads/NetStorm.exe)

## Project layout

### UILib
UILib contains UI elements such as buttons and checkboxes used in the GUI project.

### GUICore
GUI is the WinForms default interface for NetStorm.

### Console
Currently, Console is used for testing. It will contain the server part of NetStorm.

### Common
Common contains all essential classes and interfaces.

### CoreNG
This is the heart of NetStorm. It contains all modules and logic.

### Protocols
This package contains essential parts for some of the used network protocols (such as DHCP, HTTP). The former projects DHCP and UPNP got also merged into this one.

## Why?
I write this tool mainly because I am bored and I want to learn something. You can keep bugs to yourself.

## Modules

### Network discovery
The network scanner uses ARP / ND / ICMP to detect devices on the network. It is capable of resolving

* MAC-Addresses
* DNS-Entries
* OS approximation (naive ICMP-TTL fingerprinting)

For obtaining MAC-Addresses, netstorm uses the following techniques:

1. Sending and receiving ARP packets through pcap
2. Using the native windows [SendArp() function](https://docs.microsoft.com/en-us/windows/win32/api/iphlpapi/nf-iphlpapi-sendarp)
3. Sending ICMP pings using the .NET builtin Ping-Class and evaluating the arp cache 

~~It also contains a dhcp test utility.~~ The dhcp test utility was moved to the dashboard.

### IP-Lookup and Portscanner
This module is able to resolve

* the location of a public ip address by using [IP-Api](http://ip-api.com)
* open ports and performing some bannergrabbing

Portscanning is achived through the following scan-techniques:

* SYN-Scan
* FIN-Scan
* NULL-Scan
* XMAS-Scan
* Connect-Scan

All scan methods except connect() use pcap.

### ARP-Spoofer
This module poisons your network devices' arp cache to interrupt its connection (still a bit buggy).

### ARP-Shield
This module observes ARP packets sent over the network and confirms their origin by utilizing default tcp-stack behaviour. Should an attacker try to poison your arp cache, this module raises alarm.

## Third party software and services

### Services
* [IP-Info](https://ipinfo.is/)
* [IP-Api](http://ip-api.com)

### Libraries
This software uses following 3rd-party libraries and their respected dependencies:

* [SharpPcap](https://github.com/chmorgan/sharppcap)
* [PacketNet](https://github.com/chmorgan/packetnet)
* [IPNetwork2](https://github.com/lduchosal/ipnetwork)
* [Newtonsoft.JSON](https://www.newtonsoft.com/json)
* [DNS](https://github.com/kapetan/dns)

### Recommended Drivers
NetStorm strongly benefits from either npcap, winpcap or libpcap. Some features will not work without it.

You can find those drivers here:

* [npcap](https://nmap.org/npcap/)
* [libpcap](http://www.tcpdump.org/)
* [winpcap](https://www.winpcap.org/) (Deprecated, probably limited support)