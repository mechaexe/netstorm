@echo off
echo We are getting things ready. Please don't close this window until the installation is finished.
cd WinformsUI
dotnet publish -c Release -r win-x64 -p:PublishSingleFile=true --self-contained true
echo.
echo.
if not %errorlevel% == 0 (
echo There was an error while compiling. Make sure you have the dotnet CLI installed 
) else echo Finished compiling... You can find DotNetStorm here: %cd%\bin\Release\netcoreapp3.1\win-x64\publish\NetStorm.exe
echo.
echo.
pause